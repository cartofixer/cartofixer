# Introduction and generalities

- [Introduction and generalities](#introduction-and-generalities)
  - [What is _cartofixer_?](#what-is-cartofixer)
  - [Applications and modularity](#applications-and-modularity)
  - [Open-source development](#open-source-development)
  - [User guide](#user-guide)

## What is _cartofixer_?

<!-- WEB GIS -->

_cartofixer_ is a web application platform offering tools for managing, visualizing and publishing spatial data.

These functionalities are available online, through carefully designed interfaces, with a view to making this tool available to the greatest number of people.

The basic applications of _cartofixer_ allow, in particular, to easily publish maps with legends on the web and to integrate them into third party websites.

_cartofixer_ is a software under free license (AGPL v3), whose code is available at [https://gitlab.com/cartofixer/cartofixer](https://gitlab.com/cartofixer/cartofixer)

## Applications and modularity

_cartofixer_ is structured in two parts: a simple kernel, connected to a spatial database, and a series of applications assuming various functionalities.

In addition to the benefits of use mentioned above, this modular architecture allows great development agility, and promotes the creation of tools that are easy to access in view of the complexity inherent in the processing and visualization of spatial data.

The software kernel is therefore intended to be light, in order to facilitate its maintenance and evolution; while the applications linked to it each follow this logic of compactness, offering users simple and clearly identified tools, and developers a readable and compartmentalized platform.

Some of the applications are generic, responding to common needs (creation of maps, visualization etc...), while other applications are developed to measure, to meet more specific needs.

Applications are activated according to deployment.

## Open-source development

_cartofixer_ is distributed under the free AFFERO GPL 3 license: https://www.gnu.

## User guide

The complete user guide is available here : https://gitlab.com/cartofixer/cartofixer/-/wikis/home
<!-- The complete user guide is available here : https://cartofixer.com/documentation -->
