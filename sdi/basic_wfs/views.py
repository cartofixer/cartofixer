from base64 import b64decode

from django.http import HttpResponse, HttpResponseBadRequest
from django.views import View
from django.contrib.auth import authenticate

from .lingua import LinguaWFS
from .capabilities_2 import get_capabilities
from .geodata import schema_loader, feature_loader
from .xml import typename_ns_to_rid


def attach_user(request):
    auth = request.META['HTTP_AUTHORIZATION']
    auth_type, auth_string = auth.split(' ')
    if auth_type.lower() == 'basic':
        decoded = b64decode(auth_string).decode('utf-8')
        username, password = decoded.split(':')
        user = authenticate(username=username, password=password)
        print('Auth {}:{} -> {}'.format(username, password, user))

        if user is not None:
            request.user = user
        else:
            raise Exception('Could not authenticate user')
    else:
        raise Exception('Expect basic auth, got: {}'.format(auth_type))


class HttpResponseUnauthorized(HttpResponse):
    status_code = 401

    def __init__(self):
        super(HttpResponseUnauthorized, self).__init__(
            """<html><head><title>Basic auth required</title></head>
               <body><h1>Authorization Required</h1></body></html>""", )
        self['WWW-Authenticate'] = 'Basic realm="CS", charset="UTF-8"'


class WFS(View):

    cap_req_parms_m = [
        LinguaWFS.Request.service,
    ]
    cap_req_parms_o = [
        LinguaWFS.Request.version,
        LinguaWFS.Request.format,
    ]

    def get_capabilities(self, request):
        query = request.GET
        params = LinguaWFS.Request.gets(query, WFS.cap_req_parms_m)
        params.update(LinguaWFS.Request.gets(query, WFS.cap_req_parms_o,
                                             False))

        caps = get_capabilities(request)
        return HttpResponse(caps, content_type='application/xml')

    def describe_feature_type(self, request):
        typename = LinguaWFS.Request.get(request.GET,
                                         LinguaWFS.Request.typename)
        ft = schema_loader.get_schema(request, typename_ns_to_rid(typename))
        return HttpResponse(ft, content_type='application/xml')

    def get_feature(self, request):
        typenames = LinguaWFS.Request.get(request.GET,
                                          LinguaWFS.Request.typenames)
        ft = feature_loader.get_feature(request, typename_ns_to_rid(typenames))
        return HttpResponse(ft, content_type='application/xml')

    def get(self, request):
        wfs_req = LinguaWFS.Request.get(request.GET, LinguaWFS.Request.request)
        try:
            attach_user(request)
        except Exception as ex:
            print(str(ex))
            return HttpResponseUnauthorized()

        # GET /basic-wfs/?SERVICE=WFS&REQUEST=GetCapabilities&VERSION=1.0.0
        if LinguaWFS.Token.GetCapabilities == wfs_req:
            return self.get_capabilities(request)

        elif LinguaWFS.Token.DescribeFeatureType == wfs_req:
            return self.describe_feature_type(request)

        elif LinguaWFS.Token.GetFeature == wfs_req:
            return self.get_feature(request)

        return HttpResponseBadRequest(wfs_req)
