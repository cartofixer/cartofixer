from django.test import SimpleTestCase
from uuid import uuid4
import time
from .views import TokenReg

class ActivityTokenTestCase(SimpleTestCase):

    def test_token_is_valid(self):
        """Getting a token that validates"""
        reg = TokenReg(1000)
        tok = reg.get()
        self.assertTrue(reg.is_valid(tok))

    def test_token_is_not_valid(self):
        """Getting a token that does not validate"""
        reg = TokenReg(1000)
        tok = str(uuid4())
        self.assertFalse(reg.is_valid(tok))

    def test_token_is_expired(self):
        """Getting a token that validates and test it does not after ttl"""
        ttl = 2
        reg = TokenReg(ttl)
        tok = reg.get()
        self.assertTrue(reg.is_valid(tok))
        time.sleep(ttl + 1)
        self.assertFalse(reg.is_valid(tok))

