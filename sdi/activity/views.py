import logging
from uuid import uuid4
from datetime import datetime, timedelta
from json import loads, dumps
from cryptography.fernet import Fernet
from base64 import urlsafe_b64encode

from django.http import JsonResponse, HttpResponseBadRequest
from django.views.decorators.http import require_http_methods
from django.views.decorators.csrf import csrf_protect
from django.conf import settings

from .geodata import Geodata

logger = logging.getLogger(__name__)

geodata = Geodata()
TOKEN_TTL = getattr(settings, 'ACTIVITY_TOKEN_TTL', 3600)
DEFAULT_ACTIVITY = 'activity'
FERNET_KEY = urlsafe_b64encode(settings.SECRET_KEY.encode('utf-8')[:32])


def now():
    return datetime.utcnow().timestamp()


def make_expire(ttl):
    return (datetime.utcnow() + ttl).timestamp()


class InvalidTokenData(Exception):
    pass


class ExpiredToken(Exception):
    pass


class Token:
    ttl = timedelta(seconds=TOKEN_TTL)
    crypto = Fernet(FERNET_KEY)

    @classmethod
    def make(cls, time_to_live=None):
        ttl = cls.ttl if time_to_live is None else timedelta(
            seconds=time_to_live)
        tok_data = dict(token=str(uuid4()), expire=make_expire(ttl))
        tok_string = dumps(tok_data).encode('utf-8')
        return cls.crypto.encrypt(tok_string).decode('utf-8')

    @classmethod
    def validate(cls, tok_string):
        try:
            tok_data = loads(
                cls.crypto.decrypt(tok_string.encode('utf-8')).decode('utf-8'))
            expire = tok_data.pop('expire')
            token = tok_data.pop('token')
        except Exception:
            raise InvalidTokenData()

        if expire < now():
            raise ExpiredToken()

        return dict(token=token)


@require_http_methods(["GET"])
@csrf_protect
def get_activity_token(request):
    return JsonResponse(dict(token=Token.make()))


@require_http_methods(["POST"])
@csrf_protect
def post_activity(request):
    data_string = request.body.decode('utf-8')
    data = loads(data_string)
    tok = data.get('token')
    if tok is None:
        return HttpResponseBadRequest('Missing Token')

    try:
        data.update(Token.validate(tok))
    except InvalidTokenData:
        return HttpResponseBadRequest('Invalid Token')
    except ExpiredToken:
        return HttpResponseBadRequest('Expired Token')

    ns = data.get('namespace', DEFAULT_ACTIVITY)
    if ns is not DEFAULT_ACTIVITY:
        geodata.update_activity(ns, data)
    data.update(dict(activity=ns))

    logger.info(dumps(data))
    data.update(dict(id='__none__'))

    return JsonResponse(data)
