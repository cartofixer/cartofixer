from django.urls import path
from .views import post_activity, get_activity_token

urlpatterns = [
    path('token',
        get_activity_token,
        name='activity.get_token'),
    path('post/',
        post_activity,
        name='activity.post'),
]