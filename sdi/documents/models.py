
#########################################################################
#  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#########################################################################

import uuid
from datetime import datetime
from django.db import models
from django.contrib.auth.models import User
import os


def get_file_extension(instance):
    try:
        ext = os.path.splitext(str(instance.document))[1]
    except AttributeError:
        try:
            ext = os.path.splitext(str(instance.image))[1]
        except AttributeError:
            ext = ''
    return ext


def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    if instance.user is not None:
        return 'user_{0}/{1}{2}'.format(
            instance.user.get_username(),
            instance.id,
            get_file_extension(instance)
        )
    return 'anonymous/{0}/{1}{2}'.format(
        instance.session[0],
        instance.id,
        get_file_extension(instance)
    )


class Document(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    document = models.FileField(upload_to=user_directory_path)
    user = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        related_name='documents',
    )


class Image(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    image = models.ImageField(upload_to=user_directory_path)
    user = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        related_name='images',
        null=True,
        blank=None,
    )
    session = models.CharField(max_length=40)
