#
#  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""sdi_api URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.urls import path, include
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.conf import settings

admin.site.site_header = getattr(settings, 'ADMIN_SITE_HEADER', 'cartofixer')
admin.site.site_title = getattr(settings, 'ADMIN_SITE_TITLE', 'cartofixer')
admin.site.index_title = getattr(settings, 'ADMIN_INDEX_TITLE',
                                 'cartofixer administration')

ROOT_WEB_URLS = getattr(settings, 'ROOT_WEB_URLS', 'web.urls')

urlpatterns = [
    path('', include(ROOT_WEB_URLS)),
    path('api/', include('api.urls')),
    path('admin/', admin.site.urls),
    path('client/', include('clients.urls')),
    path('documents/', include('documents.urls')),
    path(
        'login/',
        auth_views.LoginView.as_view(template_name='main/login.html'),
        name='login'),
    path('webservice/', include('webservice.urls')),
    path('remote-manage/', include('remote_manage.urls')),
    # url(r'^render/', include('render.urls')),
    path('basic-wfs/', include('basic_wfs.urls')),
    path('activity/', include('activity.urls')),
]
