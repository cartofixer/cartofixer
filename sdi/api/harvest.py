from urllib.parse import urlparse
from importlib import import_module
from json import loads
from django.apps import apps
from django.views.decorators.http import require_http_methods
from django.shortcuts import get_object_or_404

from .models.metadata import MetaData


class HarvesterNotFound(Exception):
    def __init__(self, app_name):
        super().__init__(
            'Could not find an harvest method for {}'.format(app_name))


harvester_cache = dict()


def find_harvester(app_name):
    if app_name in harvester_cache:
        return harvester_cache[app_name]

    app = apps.app_configs[app_name]
    geodata = getattr(app, 'geodata', None)

    if geodata is None:
        raise HarvesterNotFound(app_name)

    try:
        module = import_module('.harvest', app.name)
        harvester_cache[app_name] = module.harvest
        return module.harvest
    except Exception:
        raise HarvesterNotFound(app_name)


@require_http_methods(['POST'])
def handle_harvest_request(request):
    # print('handle_harvest_request')
    # print(str(request.body))
    post_data = loads(request.body.decode('utf-8'))
    metadata_id = post_data.get('metadata')
    geometry_data = post_data.get('geometry')
    md = get_object_or_404(MetaData, pk=metadata_id)
    parsed_rid = urlparse(md.resource_identifier)
    harvest = find_harvester(parsed_rid.scheme)

    return harvest(request, parsed_rid, geometry_data)