#
#  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import datetime
from json import loads

from django.conf import settings
from django.core.serializers import serialize
from django.core.exceptions import ValidationError
from django.db import connections
from django.http import HttpResponseForbidden, Http404
from django.contrib.gis.db.models.functions import AsGeoJSON, Transform

from api.models.map import LayerInfoExtra
from .models import get_layer

GEOJSON_QUERY = """
SELECT row_to_json(fc)
  FROM (
    SELECT
      'FeatureCollection' AS type,
      array_to_json(array_agg(f)) AS features
    FROM (
        SELECT
          'Feature' AS type,
          ST_AsGeoJSON(ST_Transform(sd.{geometry_column}, 4326), {max_decimal_digits})::json AS geometry,
          {pk_column} as id,
          row_to_json((
            SELECT prop FROM (SELECT {field_names}) AS prop
           )) AS properties
        FROM "{schema}"."{table}" AS sd
        {where_clause}
    ) AS f
  ) AS fc;
"""

WHERE_CLAUSE = """
WHERE sd.{geometry_column} && ST_Transform(ST_MakeEnvelope(%s, %s, %s, %s, 4326), ST_SRID(sd.{geometry_column}))
"""


def get_query(schema, table, query_template, **kwargs):
    model, geometry_field, _geometry_field_type = get_layer(schema, table)
    fields = []
    for field in model._meta.get_fields():
        if field.get_attname() != geometry_field:
            fields.append('"{}"'.format(field.column))
    pk_field = model._meta.pk.column

    # Where clause to restrict SELECT to bbox
    where_clause = ""
    bbox = kwargs.get('bbox')
    if bbox:
        where_clause = WHERE_CLAUSE.format(
            geometry_column=geometry_field, bbox=bbox)

    return query_template.format(
        pk_column=pk_field,
        schema=schema,
        table=table,
        max_decimal_digits=getattr(settings, 'MAX_DECIMAL_DIGITS', 6),
        geometry_column=geometry_field,
        field_names=', '.join(fields),
        where_clause=where_clause,
        **kwargs)


NUMBER = 'number'
STRING = 'string'
BOOLEAN = 'boolean'
DATE = 'date'
DATETIME = 'datetime'

# fields found in django/db/models/fields/__init__.py
MAPPED_TYPES = {
    'AutoField': NUMBER,
    'BigAutoField': NUMBER,
    'BigIntegerField': NUMBER,
    'BinaryField': STRING,
    'BooleanField': BOOLEAN,
    'CharField': STRING,
    # 'CommaSeparatedIntegerField',
    'DateField': DATE,
    'DateTimeField': DATETIME,
    'DecimalField': STRING,
    'DurationField': NUMBER,
    'EmailField': STRING,
    'FilePathField': STRING,
    'FloatField': NUMBER,
    'GenericIPAddressField': STRING,
    'IPAddressField': STRING,
    'IntegerField': NUMBER,
    'NullBooleanField': BOOLEAN,
    'PositiveIntegerField': NUMBER,
    'PositiveSmallIntegerField': NUMBER,
    'SlugField': STRING,
    'SmallIntegerField': NUMBER,
    'TextField': STRING,
    'TimeField': NUMBER,
    'URLField': STRING,
    'UUIDField': STRING,
}


def _with_metadata(schema, table, collection):
    model, geometry_field, _geometry_field_type = get_layer(schema, table)
    fields = []

    for field in model._meta.get_fields():
        field_name = field.get_attname()
        if field_name != geometry_field:
            field_type = field.get_internal_type()
            try:
                fields.append([field_name, MAPPED_TYPES[field_type]])
                print('>> {}: <{}>'.format(field_name,
                                           MAPPED_TYPES[field_type]))
            except KeyError:
                fields.append([field_name, STRING])
                print('Failed to map "{}" to a useful type'.format(field_type))

    collection['fields'] = fields
    return collection


def get_geojson(schema, table, **kwargs):
    query = get_query(schema, table, GEOJSON_QUERY, **kwargs)

    with connections[schema].cursor() as cursor:
        bbox = kwargs.get('bbox')
        if bbox:
            cursor.execute(query, [bbox[0], bbox[1], bbox[2], bbox[3]])
        else:
            cursor.execute(query)

        row = cursor.fetchone()

    feature_collection = row[0]
    if feature_collection['features'] is None:  # can happen with a bbox
        feature_collection['features'] = []

    return _with_metadata(schema, table, feature_collection)


def get_model(schema, table):
    model, _geometry_field, _geometry_field_type = get_layer(schema, table)
    return model


def get_stream_metadata(schema, table):
    model, _geometry_field, _geometry_field_type = get_layer(schema, table)
    return _with_metadata(schema, table, {'count': model.objects.count()})


def validate_sort(sort):
    if sort is None:
        return None
    try:
        type(sort.get('column')) == int
        type(sort.get('columnName')) == str
        d = sort.get('direction').lower()
        has_dir = d == 'asc' or d == 'desc'
        assert has_dir
        return sort
    except Exception as ex:
        raise ValidationError('sort not well formed [{}]'.format(ex))


def get_stream_data(schema, table, offset, limit, sort=None, filters=None):
    sort = validate_sort(sort)
    model, geometry_field, _geometry_field_type = get_layer(schema, table)
    fields = []
    for field in model._meta.get_fields():
        if field.get_attname() != geometry_field:
            fields.append("{}".format(field.column))

    # queryset = model.objects.values_list(*fields)
    queryset = model.objects.get_queryset().annotate(
        geom_4326=Transform(geometry_field, 4326)).annotate(
        geojson=AsGeoJSON('geom_4326'))

    if filters:
        for filter in filters:
            tag = filter['tag']

            if tag == "string":
                queryset = filter_string(queryset, filter)
            if tag == "number":
                queryset = filter_number(queryset, filter)
            if tag == "date":
                queryset = filter_date(queryset, filter)
            if tag == "datetime":
                queryset = filter_datetime(queryset, filter)

    if sort:
        direction = '-' if sort.get('direction').lower() == 'desc' else ''
        queryset = queryset.order_by(direction + sort.get('columnName'))
    else:
        pk_field = model._meta.pk.column
        queryset = queryset.order_by(pk_field)

    total_count = queryset.all().count()
    # data = tuple(queryset[offset:offset + limit])
    slice = queryset[offset:offset + limit]
    features = []
    for obj in slice:
        id = obj.pk
        properties = {f: getattr(obj, f) for f in fields}
        features.append(
            dict(
                type='Feature',
                id=id,
                properties=properties,
                geometry=loads(obj.geojson),
            ))

    data = tuple(features)

    return total_count, data


def get_stream_feature_geojson(schema, table, feature_id):
    model, geometry_field, _geometry_field_type = get_layer(schema, table)
    pk_field = model._meta.pk.column

    return serialize(
        'geojson',
        model.objects.filter(**{pk_field: feature_id}),
        geometry_field=geometry_field,
        srid=4326)


def filter_string(data, filter):
    pattern = filter["pattern"]
    columnName = filter["columnName"]

    return data.filter(**{columnName + '__icontains': pattern})


def filter_number(data, filter):
    value = filter["value"]
    operator = filter["op"]
    columnName = filter["columnName"]

    if (operator == 'eq'):
        return data.filter(**{columnName: value})
    if (operator == 'gt'):
        return data.filter(**{columnName + '__gt': value})
    if (operator == 'lt'):
        return data.filter(**{columnName + '__lt': value})

    return data


def filter_date(data, filter):
    filter["value"] = datetime.datetime.strptime(filter["date"], '%Y-%m-%d')

    return filter_number(data, filter)


def filter_datetime(data, filter):
    filter["value"] = datetime.datetime.strptime(filter["datetime"],
                                                 '%Y-%m-%d %H:%M:%S')

    return filter_number(data, filter)


def get_csv_data(schema, table, layer_info_id, sort=None, filters=None):
    model, geometry_field, _geometry_field_type = get_layer(schema, table)

    # Check authorization for export
    try:
        if not LayerInfoExtra.objects.get(
                layer_info__id=layer_info_id).exportable:
            return HttpResponseForbidden()
    except LayerInfoExtra.DoesNotExist:
        raise Http404

    fields = []
    for field in model._meta.get_fields():
        if field.get_attname() != geometry_field:
            fields.append("{}".format(field.column))

    data = model.objects.values_list(*fields)

    if filters:
        for filter in filters:
            tag = filter['tag']

            if tag == "string":
                data = filter_string(data, filter)
            if tag == "number":
                data = filter_number(data, filter)
            if tag == "date":
                data = filter_date(data, filter)
            if tag == "datetime":
                data = filter_datetime(data, filter)

    if sort:
        direction = '-' if sort.get('direction').lower() == 'desc' else ''
        data = data.order_by(direction + sort.get('column'))
    else:
        pk_field = model._meta.pk.column
        data = data.order_by(pk_field)

    # Build CSV
    csv_data = ""

    for field in fields:
        csv_data += field + ";"
    csv_data += "\n"

    for item in data:
        csv_row = ""
        for attribute in item:
            csv_row += str(attribute) + ";"
        csv_data += csv_row + "\n"

    return csv_data
