#
#  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from django.conf.urls import url
from django.urls import reverse
from django.conf import settings

from .handler import (
    handle_request, 
    handle_csv_request, 
    handle_stream_request, 
    handle_stream_feature_request,
)

POSTGIS_DATA_STREAM = getattr(settings, 'POSTGIS_DATA_STREAM', False)


def _from_identifier_base(route_name, rid, request=None):
    schema = rid.hostname
    table = rid.path[1:]
    rel_url = reverse(route_name, args=[schema, table])
    if request is not None:
        return request.build_absolute_uri(rel_url)
    return rel_url


def from_identifier(rid, request=None):
    return _from_identifier_base('geodata.postgis', rid, request)


def data_stream_url(rid, request=None):
    if POSTGIS_DATA_STREAM:
        return _from_identifier_base('geodata.postgis.stream', rid, request)
    raise Exception('POSTGIS_DATA_STREAM not enabled')


urlpatterns = [
    url(r'^postgis/stream/(?P<schema>.+)/(?P<table>.+)/csv/$',
        handle_csv_request,
        name='geodata.postgis.csv'),
    url(r'^postgis/stream/(?P<schema>.+)/(?P<table>.+)/(?P<feature_id>\d+)/$',
        handle_stream_feature_request,
        name='geodata.postgis.stream.feature'),
    url(r'^postgis/stream/(?P<schema>.+)/(?P<table>.+)/$',
        handle_stream_request,
        name='geodata.postgis.stream'),
    url(r'^postgis/(?P<schema>.+)/(?P<table>.+)/$',
        handle_request,
        name='geodata.postgis'),
]
