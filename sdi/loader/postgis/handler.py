#  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import io
import codecs
import json

from json import loads, dump
from django.core import serializers
from django.core.cache import caches, InvalidCacheBackendError
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse, JsonResponse, FileResponse
from django.views.decorators.http import require_http_methods
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

from .serializer import (
    get_geojson,
    get_stream_metadata,
    get_csv_data,
    get_stream_data,
    get_stream_feature_geojson,
)

POSTGIS_CACHE_LAYERS = getattr(settings, 'POSTGIS_CACHE_LAYERS', True)


def handle_request(request, schema, table):
    user = request.user
    if not user.has_perm('postgis', (schema, table)):
        raise PermissionDenied()

    # Extract bbox if exists
    bbox = None
    try:
        bbox = request.GET.get('bbox').split(",")
    except AttributeError:
        pass

    if not POSTGIS_CACHE_LAYERS or bbox is not None:
        return JsonResponse(get_geojson(schema, table, bbox=bbox))

    try:
        cache = caches['layers']
        ckey = '{}.{}'.format(schema, table)
        try:
            reader = cache.read(ckey)
            reader_type = type(reader)
            # print('ReaderType {} {}'.format(ckey, reader_type))
            if reader_type is io.BufferedReader:
                return FileResponse(reader, content_type='application/json')
            elif reader_type is dict:
                return JsonResponse(reader)

            response = HttpResponse(reader, content_type='application/json')
            return response

        except KeyError:
            # there's been of juggling to force diskcache
            # to return a BufferedReader from cache.read
            stream = io.BytesIO()
            writer = codecs.getwriter("utf-8")(stream)
            data = get_geojson(schema, table)
            dump(data, writer)
            stream.seek(0)
            cache.set(ckey, stream, read=True)

            return JsonResponse(data)

    except InvalidCacheBackendError:
        print('InvalidCacheBackendError')
        return HttpResponse(
            content=get_geojson(schema, table, bbox=bbox),
            content_type='application/json')


@csrf_exempt
def handle_stream_request(request, schema, table):
    user = request.user
    if not user.has_perm('postgis', (schema, table)):
        raise PermissionDenied()

    if request.method == 'GET':
        return JsonResponse(get_stream_metadata(schema, table))

    elif request.method == 'POST':
        params = json.loads(request.body.decode('utf-8'))

        total_count, data = get_stream_data(schema, table,
                                            params.get('offset'),
                                            params.get('limit'),
                                            params.get('sort'),
                                            params.get('filters'))

        return JsonResponse({"data": data, "totalCount": total_count})


@csrf_exempt
def handle_stream_feature_request(request, schema, table, feature_id):
    user = request.user
    if not user.has_perm('postgis', (schema, table)):
        raise PermissionDenied()

    feature_gjs = get_stream_feature_geojson(schema, table, feature_id)
    return HttpResponse(feature_gjs)


@csrf_exempt
def handle_csv_request(request, schema, table):
    user = request.user
    if not user.has_perm('postgis', (schema, table)):
        raise PermissionDenied()

    if request.method == 'GET':
        return JsonResponse(get_stream_metadata(schema, table))

    elif request.method == 'POST':
        params = json.loads(request.body.decode('utf-8'))

        data = get_csv_data(schema, table, params.get('layerInfo'),
                            params.get('sort'), params.get('filters'))

        filename = "{}-{}.csv".format(schema, table)
        response = HttpResponse(data, content_type="text/csv")
        response['Content-Disposition'] = 'attachment; filename="{}"'.format(
            filename)
        return response
