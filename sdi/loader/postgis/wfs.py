from basic_wfs.xml import (
    create,
    create_tree,
    append,
    tree_to_string,
    path_to_typename,
    rid_to_typename_ns,
    strip_ns,
)
from django.shortcuts import get_object_or_404
from django.urls import reverse
from urllib.parse import urlencode

from .models import get_layer
from .serializer import get_geojson


def ensure_list(x):
    if isinstance(x, (
            list,
            tuple,
    )):
        return x
    return [x]


def make_field_type(field):
    """TODO"""
    return 'xsd:string'


def describe_feature_type(request, typename, schema, table):
    typename_name = strip_ns(typename)
    typename_type = '{}Type'.format(typename_name)
    model, geometry_field, geometry_field_type = get_layer(schema, table)
    meta = model._meta
    fields = meta.fields
    attributes = {
        'targetNamespace': "http://www.carto-station.com/postgis",
        'xmlns:postgis': "http://www.carto-station.com/postgis",
        'xmlns:xsd': "http://www.w3.org/2001/XMLSchema",
        'xmlns:gml': "http://www.opengis.net/gml/3.2",
        'elementFormDefault': "qualified",
    }
    schema = create('xsd:schema', attributes)

    # gml
    append(
        schema, 'xsd:import', {
            'namespace': 'http://www.opengis.net/gml/3.2',
            'schemaLocation': 'http://schemas.opengis.net/gml/3.2.1/gml.xsd',
        })

    ct = append(schema, 'xsd:complexType', {
        'name': typename_type,
    })
    cc = append(ct, 'xsd:complexContent')
    aft = append(cc, 'xsd:extension', {'base': 'gml:AbstractFeatureType'})
    seq = append(aft, 'xsd:sequence')
    append(seq, 'xsd:element', {
        'name': geometry_field,
        'type': 'gml:GeometryPropertyType'
    })

    for field in fields:
        append(
            seq, 'xsd:element', {
                'name': field.name,
                'type': make_field_type(field),
                'maxOccurs': '1',
                'minOccurs': '0',
            })

    append(
        schema, 'xsd:element', {
            'name': typename_name,
            'type': typename_type,
            'substitutionGroup': 'gml:_Feature',
        })

    return tree_to_string(create_tree(schema))


def get_feature_type(request, rid_url):
    schema = rid_url.hostname
    table = rid_url.path[1:]
    typename = rid_to_typename_ns(rid_url.geturl())

    return describe_feature_type(request, typename, schema, table)


def encode_point(p):
    return '{},{}'.format(p[0], p[1])


def encode_line(l):
    return ' '.join(list(map(encode_point, l)))


def encode_geometry(feature, geom):
    gt = geom['type']
    coordinates = geom['coordinates']
    if 'MultiPolygon' == gt:
        root = append(feature, 'gml:MultiPolygon', dict(srsName='EPSG:4326'))
        for polygon in coordinates:
            member = append(root, 'gml:polygonMember')
            poly = append(member, 'gml:Polygon')
            outer = append(poly, 'gml:outerBoundaryIs')
            ring = append(outer, 'gml:LinearRing')
            coords = append(ring, 'gml:coordinates')
            coords.text = encode_line(polygon[0])

    elif 'MultiPoint' == gt:
        root = append(feature, 'gml:MultiPoint', dict(srsName='EPSG:4326'))
        for point in coordinates:
            member = append(root, 'gml:pointMember')
            pt = append(member, 'gml:Point')
            pos = append(pt, 'gml:pos')
            pos.text = encode_point(point)

    else:
        raise NotImplementedError(
            'support ony polygons and points for now, not {}'.format(gt))


def ns_tag(ns, tag):
    return '{}:{}'.format(ns, tag)


def encode_feature(coll, feature, ns, typename):
    element = append(coll, 'gml:featureMember')
    root = append(element, typename, {
        ns_tag(ns, 'id'): str(feature['id']),
    })

    props = feature['properties']
    geom = feature['geometry']
    for k, v in props.items():
        append(root, ns_tag(ns, k)).text = str(v)
    encode_geometry(root, geom)


def _results_to_gml(request, rid_url, schema, table):
    ns = rid_url.scheme
    typename = rid_to_typename_ns(rid_url.geturl())
    features = get_geojson(schema, table).get('features', [])

    loc = '{}?{}'.format(
        request.build_absolute_uri(reverse('basic-wfs')),
        urlencode({
            'SERVICE': 'WFS',
            'REQUEST': 'DescribeFeatureType',
            'TYPENAME': typename,
        }),
    )

    attributes = {
        'xmlns:postgis': "http://www.carto-station.com/postgis",
        'xmlns:gml': 'http://www.opengis.net/gml',
        'xmlns:wfs': 'http://www.opengis.net/wfs',
        'xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
        'xsi:schemaLocation':
        'http://www.carto-station.com/postgis {}'.format(loc)
    }
    root = create('wfs:FeatureCollection', attributes)

    for feature in features:
        try:
            geom_data = feature.get('geometry')
            properties = feature.get('properties')
            feature_id = properties.get(
                'id') if properties is not None else None
            if geom_data is not None and feature_id is not None:
                encode_feature(
                    root,
                    dict(
                        id=feature_id,
                        geometry=geom_data,
                        properties=properties,
                    ),
                    ns,
                    typename,
                )

        except Exception as ex:
            print('_results_to_gml#project', str(ex))

    return tree_to_string(create_tree(root))


def get_feature(request, rid_url):
    schema = rid_url.hostname
    table = rid_url.path[1:]
    return _results_to_gml(request, rid_url, schema, table)
