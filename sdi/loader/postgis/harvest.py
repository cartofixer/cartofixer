from json import dumps

from django.db import connections
from django.http import JsonResponse
from django.core.exceptions import PermissionDenied

from .serializer import get_query

HARVEST_QUERY = """
SELECT row_to_json(fc)
  FROM (
    SELECT 
      'FeatureCollection' AS type, 
      array_to_json(array_agg(f)) AS features
    FROM (
        SELECT 
          'Feature' AS type, 
          ST_AsGeoJSON(sd.{geometry_column}, {max_decimal_digits})::json AS geometry,
          {pk_column} as id, 
          row_to_json((
            SELECT prop FROM (SELECT {field_names}) AS prop
           )) AS properties
        FROM "{schema}"."{table}" AS sd 
        WHERE ST_Intersects(ST_Transform(sd.{geometry_column}, 4326), ST_SetSRID(ST_GeomFromGeoJSON('{geometry_request}'), 4326))
    ) AS f 
  ) AS fc;
"""


def harvest(request, rid, geometry_data):
    """
    ...
    """
    schema = rid.hostname
    table = rid.path[1:]

    user = request.user
    if not user.has_perm('postgis', (schema, table)):
        raise PermissionDenied()

    geometry_string = dumps(geometry_data)
    query = get_query(
        schema, table, HARVEST_QUERY, geometry_request=geometry_string)

    with connections[schema].cursor() as cursor:
        cursor.execute(query)
        row = cursor.fetchone()

    fc = row[0]
    if fc['features'] is None:
        fc['features'] = []

    return JsonResponse(fc)
