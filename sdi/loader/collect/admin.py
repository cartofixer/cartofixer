from django.contrib import admin

from .models import (
    Collect,
    Question,
    QuestionLink,
    Choice,
    Observation,
    Answer,
)



class CollectAdmin(admin.ModelAdmin):
    list_display = ('title',)

class QuestionLinkInline(admin.TabularInline):
    model = QuestionLink
    extra = 1

class QuestionAdmin(admin.ModelAdmin):
    list_display = ('title','kind' )
    inlines = [QuestionLinkInline]

class ChoiceAdmin(admin.ModelAdmin):
    list_display = ('title', 'question')


admin.site.register(Collect, CollectAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(Choice, ChoiceAdmin)
admin.site.register(Observation)
admin.site.register(Answer)