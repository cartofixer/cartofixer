from django.contrib.gis.db import models
from django.db.models import UniqueConstraint


class Model(models.Model):
    class Meta:
        abstract = True

    id = models.AutoField(primary_key=True)
    created_at = models.DateTimeField(auto_now_add=True)


class Informed(Model):
    class Meta(Model.Meta):
        abstract = True

    title = models.CharField(max_length=512)
    description = models.TextField()

    def __str__(self):
        return self.title


class Collect(Informed):
    # STATUS = (
    #     ('DR', 'Draft'),
    #     ('PU', 'Published'),
    # )
    extent = models.PolygonField(dim=2, srid=4326)


class Question(Informed):

    OBSERVER = 'R'
    OBSERVED = 'D'
    ON = (
        (OBSERVER, 'Observer'),
        (OBSERVED, 'Observed'),
    )
    kind = models.CharField(max_length=1, choices=ON)
    link = models.ManyToManyField(
        Collect,
        through='collect.QuestionLink',
        through_fields=('question', 'collect'),
    )

    @property
    def collects(self):
        return [{
            'collect': link.collect_id,
            'order': link.order,
        } for link in self.link.through.objects.filter(question=self)]


class QuestionLink(models.Model):

    class Meta:
        constraints = [
            UniqueConstraint(
                fields=['collect', 'question'],
                name='question_link',
            )
        ]
    collect = models.ForeignKey(
        Collect, on_delete=models.CASCADE, related_name='+')
    question = models.ForeignKey(
        Question, on_delete=models.CASCADE, related_name='+')
    order = models.IntegerField(default=1)


class Choice(Informed):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    order = models.IntegerField(default=1)


class Observation(Model):
    user = models.ForeignKey(
        'auth.User', on_delete=models.CASCADE, null=True, blank=None)
    # for max_length, see AbstractBaseSession#session_key
    session = models.CharField(max_length=40)
    collect = models.ForeignKey(Collect, on_delete=models.CASCADE)
    observed_point = models.PointField(srid=4326)
    # observer_point = models.PointField(srid=4326)

    def __str__(self):
        return '{}:{} #{}'.format(self.collect, self.user, self.id)


class Answer(Model):
    observation = models.ForeignKey(Observation, on_delete=models.CASCADE)
    choice = models.ForeignKey(Choice, on_delete=models.CASCADE)

    def __str__(self):
        return '{} #{}'.format(self.observation, self.choice)


class Annotation(Model):
    QUESTION = 'question'
    CHOICE = 'choice'
    OBSERVATION = 'observation'
    ANSWER = 'answer'
    ANNOTATION = 'annotation'
    TARGET_TYPE = (
        (QUESTION, 'Question'),
        (CHOICE, 'Choice'),
        (OBSERVATION, 'Observation'),
        (ANSWER, 'Answer'),
        (ANNOTATION, 'Annotation'),
    )
    target_type = models.CharField(max_length=12, choices=TARGET_TYPE)
    target_id = models.IntegerField()
    user = models.ForeignKey(
        'auth.User', on_delete=models.CASCADE, null=True, blank=None)
    collect = models.ForeignKey(Collect, on_delete=models.CASCADE)

    @staticmethod
    def get_target(target_type, target_id):
        models = {
            Annotation.QUESTION: Question,
            Annotation.CHOICE: Choice,
            Annotation.OBSERVATION: Observation,
            Annotation.ANSWER: Answer,
            Annotation.ANNOTATION: Annotation,
        }
        model = models[target_type]
        return model.objects.get(pk=target_id)


class TextAnnotation(Model):
    annotation = models.OneToOneField(Annotation,
                                      related_name='text',
                                      on_delete=models.CASCADE)
    text = models.TextField()


class ImageAnnotation(Model):
    annotation = models.OneToOneField(Annotation,
                                      related_name='image',
                                      on_delete=models.CASCADE)
    image = models.ForeignKey('documents.Image', on_delete=models.CASCADE)
