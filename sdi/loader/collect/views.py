import itertools as it
from django.shortcuts import get_object_or_404
from django.http import (
    JsonResponse,
    HttpResponseForbidden,
    HttpResponseServerError,
)
from django.shortcuts import get_object_or_404
from django.views.decorators.http import require_http_methods
from django.contrib.auth.decorators import login_required
from django.contrib.gis.geos import GEOSGeometry
from json import loads as parse_json, dumps as stringify
from documents.models import Image

from .models import (
    Collect,
    Question,
    Choice,
    Observation,
    Answer,
    Annotation,
    TextAnnotation,
    ImageAnnotation,
)


def _tojsts(dt):
    return int(dt.timestamp() * 1000)


def get_session_key(request):
    s = request.session
    if s.session_key is None:
        s.save()
    return s.session_key


class SerError(Exception):
    pass


def serialize_collect(collect):
    return {
        'id': collect.id,
        'title': collect.title,
        'description': collect.description,
        'timestamp': _tojsts(collect.created_at),
        'extent': parse_json(collect.extent.geojson),
    }


def serialize_question_kind(question):
    if question.kind == Question.OBSERVED:
        return 'observed'
    return 'observer'


def serialize_question(question):
    kind = serialize_question_kind(question)
    id = question.id
    title = question.title
    description = question.description
    results = []

    for collect in question.collects:
        results.append({
            'kind': kind,
            'id': id,
            'title': title,
            'description': description,
            'collect': collect['collect'],
            'order': collect['order'],
        })

    return results


def serialize_choice(choice):
    return {
        'id': choice.id,
        'title': choice.title,
        'description': choice.description,
        'question': choice.question_id,
        'order': choice.order,
    }


# def serialize_point(p):
#     return {
#         'type': 'Point',
#         'coordinates': p.coords,
#     }


def serialize_observation(observation):
    return {
        'id': observation.id,
        'user': observation.user_id,
        'collect': observation.collect_id,
        'timestamp': _tojsts(observation.created_at),
        'observed': parse_json(observation.observed_point.geojson),
    }


def serialize_answer(answer):
    return {
        'id': answer.id,
        'observation': answer.observation_id,
        'choice': answer.choice_id,
        'timestamp': _tojsts(answer.created_at),
    }


def serialize_annotation(annotation):
    text = getattr(annotation, 'text', None)
    image = getattr(annotation, 'image', None)
    target = dict(kind=annotation.target_type, id=annotation.target_id)
    if text is not None:
        return {
            'id': annotation.id,
            'kind': 'text',
            'collect': annotation.collect_id,
            'target': target,
            'user': annotation.user_id,
            'timestamp': _tojsts(annotation.created_at),
            'text': text.text,
        }

    elif image is not None:
        return {
            'id': annotation.id,
            'kind': 'image',
            'collect': annotation.collect_id,
            'target': target,
            'user': annotation.user_id,
            'timestamp': _tojsts(annotation.created_at),
            'image': str(image.image_id),
        }

    raise SerError('Empty Annotation')


class DeSerError(Exception):
    pass


def deserialize_point(data):
    return GEOSGeometry(stringify(data))


def deserialize_observation(data, request):
    collect = Collect.objects.get(pk=data.pop('collect'))
    user = None if request.user.is_anonymous else request.user
    session = get_session_key(request)
    return Observation(
        collect=collect,
        user=user,
        session=session,
        observed_point=deserialize_point(data.pop('observed')),
    )


def deserialize_answer(data, observation):
    choice = Choice.objects.get(pk=data.pop('choice'))

    if observation.answer_set.filter(choice__question=choice.question_id).count() > 0:
        # raise DeSerError('Question already answered')
        observation.answer_set.filter(
            choice__question=choice.question_id).delete()

    if choice.question.link.filter(pk=observation.collect_id).count() == 0:
        raise DeSerError('Answer is not in the same Collect')

    return Answer(
        observation=observation,
        choice=choice,
    )


def deserialize_annotation(data, request):
    target = data.pop('target')
    target_type = target['kind']
    target_id = target['id']
    # here we just check that the target exists
    Annotation.get_target(target_type, target_id)
    kind = data.pop('kind')
    collect = Collect.objects.get(pk=data.pop('collect'))
    user = None if request.user.is_anonymous else request.user

    if kind == 'text':
        text = data.pop('text')
        annotation = Annotation.objects.create(
            target_type=target_type,
            target_id=target_id,
            user=user,
            collect=collect,
        )
        TextAnnotation.objects.create(
            annotation=annotation,
            text=text,
        )
        return annotation

    elif kind == 'image':
        image_id = data.pop('image')
        image = Image.objects.get(pk=image_id)
        annotation = Annotation.objects.create(
            target_type=target_type,
            target_id=target_id,
            user=user,
            collect=collect,
        )
        ImageAnnotation.objects.create(
            annotation=annotation,
            image=image,
        )
        return annotation

    raise DeSerError('Unkown Annotation Kind')


def base_list(serializer, queryset, flatten=False):
    if flatten:
        return JsonResponse(list(it.chain.from_iterable(map(serializer, queryset))), safe=False)
    return JsonResponse(list(map(serializer, queryset)), safe=False)


@require_http_methods(['GET', 'OPTIONS'])
def get_collect_list(request):
    return base_list(serialize_collect, Collect.objects.all())


@require_http_methods(['GET', 'OPTIONS'])
def get_question_list(request):
    return base_list(serialize_question, Question.objects.all(), flatten=True)


@require_http_methods(['GET', 'OPTIONS'])
def get_choice_list(request):
    return base_list(serialize_choice, Choice.objects.all())


@require_http_methods(['GET', 'OPTIONS'])
def get_observation_list(request):
    return base_list(serialize_observation, Observation.objects.all())


@require_http_methods(['GET', 'OPTIONS'])
def get_answer_list(request):
    return base_list(serialize_answer, Answer.objects.all())


@require_http_methods(['GET', 'OPTIONS'])
def get_annotation_list(request, collect_id):
    collect = get_object_or_404(Collect, pk=collect_id)
    return base_list(serialize_annotation, Annotation.objects.filter(collect=collect))


# def _get_collect_from_answer(answer):
#     return answer.choice.question.collect

# def render_observation(request, id):
#     observation = get_object_or_404(Observation, pk=id)
#     answers = observation.answer_set.all()
#     if answers.count() == 0:
#         return HttpResponseNotFound('Not Enough Answers')

#     collect = _get_collect_from_answer(answers[0])

#     data = {
#         'id': id,
#         'user': observation.user.id,
#         'collect': collect.id,
#         'answers': [serialize_answer(a) for a in answers],
#     }

#     return JsonResponse(data)


@require_http_methods(['POST'])
def post_observation(request, collect_id):
    get_object_or_404(Collect, pk=collect_id)
    try:
        data = parse_json(request.body.decode('utf-8'))
        instance = deserialize_observation(data, request)
        instance.save()
        return JsonResponse(serialize_observation(instance), status=201)
    except Exception as ex:
        return HttpResponseServerError('Error: {}'.format(ex))


def check_obs_identity(request, observation):
    user = getattr(request, 'user')

    # print('req.user>', user)
    # print('req.session>', get_session_key(request))
    # print('user auth>', observation.user == user)

    # print('obs.user>', observation.user)
    # print('obs.session>', observation.session)
    # print('session auth>', observation.session == get_session_key(request))

    if user.is_anonymous:
        return observation.session == get_session_key(request)
    else:
        return observation.user == user


@require_http_methods(['POST'])
def post_answer(request, observation_id):
    observation = get_object_or_404(Observation, pk=observation_id)

    if check_obs_identity(request, observation) is False:
        return HttpResponseForbidden('Answer is not from the observer')

    # # TODO: an edit mode? - pm
    # if observation.answer_set.filter(choice__question=question_id).count() > 0:
    #     return HttpResponseBadRequest('Question already answered')

    data = parse_json(request.body.decode('utf-8'))
    instance = deserialize_answer(data, observation)
    instance.save()
    return JsonResponse(serialize_answer(instance), status=201)


@require_http_methods(['POST'])
def post_annotation(request):
    # try:
    data = parse_json(request.body.decode('utf-8'))
    instance = deserialize_annotation(data, request)
    return JsonResponse(serialize_annotation(instance), status=201)
