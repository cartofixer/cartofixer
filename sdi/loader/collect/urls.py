from django.urls import path

from .views import (
    get_collect_list,
    get_question_list,
    get_choice_list,
    get_observation_list,
    get_answer_list,
    get_annotation_list,
    post_observation,
    post_answer,
    post_annotation,
)

urlpatterns = [
    path('collect/collect/',
         get_collect_list,
         name='collect.get_collect_list'),
    path('collect/question/',
         get_question_list,
         name='collect.get_question_list'),
    path('collect/choice/',
         get_choice_list,
         name='collect.get_choice_list'),
    path('collect/observation/',
         get_observation_list,
         name='collect.get_observation_list'),
    path('collect/answer/',
         get_answer_list,
         name='collect.get_answer_list'),
    path('collect/annotation/<int:collect_id>/',
         get_annotation_list,
         name='collect.get_annotation_list'),

     

    # post
    path('collect/observation/<int:collect_id>',
         post_observation,
         name='collect.post_observation'),
    path('collect/answer/<int:observation_id>/',
         post_answer,
         name='collect.post_answer'),
    path('collect/annotation/',
         post_annotation,
         name='collect.post_annotation'),
]