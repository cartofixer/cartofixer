from django.apps import AppConfig


class CollectConfig(AppConfig):
    name = 'loader.collect'
    label = 'collect'
    verbose_name = 'Collect'
    geodata = True
