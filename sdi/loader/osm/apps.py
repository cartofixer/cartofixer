from django.apps import AppConfig


class OsmConfig(AppConfig):
    name = 'loader.osm'
    label = 'osm'
    verbose_name = 'OpenStreetMap loader'
    geodata = True