from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import overpass

# Create your views here.
def hello_world(request):

    return HttpResponse(
        content='hello world',
        content_type='application/json')

def query(request, query_string):
    """ Make a simple overpass query """
    api = overpass.API()
    response = api.get(query_string, responseformat="geojson")
    print(response)

    return JsonResponse(
        data=response)