from django.db import models


class Server(models.Model):
    hostname = models.CharField(max_length=256, primary_key=True)
    secure = models.BooleanField(default=True)


class Layer(models.Model):
    id = models.AutoField(primary_key=True)
    server = models.ForeignKey(Server, on_delete=models.CASCADE)
    path = models.CharField(max_length=256)


class TransformProj(models.Model):
    id = models.AutoField(primary_key=True)
    layer = models.OneToOneField(
        Layer, on_delete=models.CASCADE, related_name='transform_proj')
    source = models.CharField(max_length=256)


class TransformPython(models.Model):
    id = models.AutoField(primary_key=True)
    layer = models.OneToOneField(
        Layer, on_delete=models.CASCADE, related_name='transform_python')
    code = models.TextField()
