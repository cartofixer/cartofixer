from typing import Any, List, Tuple
from urllib.request import Request, urlopen
from urllib.error import HTTPError
from json import load
from pyproj import CRS, Transformer
from django.shortcuts import get_object_or_404
from django.conf import settings
from django.http import JsonResponse
from .models import (
    Layer,
    Server,
)


PROXY_TIMEOUT = getattr(settings, 'PROXY_TIMEOUT', 6)


def get_raw_data(url: str) -> Any:
    headers = {
        'Content-type': 'application/json'
    }
    request = Request(url, None, headers)
    response = urlopen(request, timeout=PROXY_TIMEOUT)
    return load(response)


def make_url(server: Server, layer: Layer) -> str:
    scheme = 'https' if server.secure else 'http'
    return '{}://{}/{}'.format(scheme, server.hostname, layer.path)


CODE_CACHE = {}


def transform_python(raw: Any, layer: Layer):
    try:
        name = layer.path
        if name not in CODE_CACHE:
            CODE_CACHE[name] = compile(
                layer.transform_python.code, name, 'exec')
        compiled = CODE_CACHE[name]
        context = {'input': raw, 'output': None}
        exec(compiled, {}, context)
        return context['output']
    except Layer.transform_python.RelatedObjectDoesNotExist:
        return raw


PROJ_CACHE = {}
EPSG4326 = CRS.from_epsg(4326)


def transform_point(tr: Transformer, point: Tuple[float, float]) -> Tuple[float, float]:
    x, y = point
    return tr.transform(x, y)


def transform_line(tr: Transformer, line: List[Tuple[float, float]]) -> List[Tuple[float, float]]:
    return [transform_point(tr, p) for p in line]


def transform_polygon(tr: Transformer, poly: List[List[Tuple[float, float]]]) -> List[List[Tuple[float, float]]]:
    return [transform_line(tr, l) for l in poly]


def transform_geometry(tr: Transformer, geometry):
    geometry_type = geometry['type']
    coordinates = geometry['coordinates']

    if geometry_type == 'Point':
        return {'type': geometry_type, 'coordinates': transform_point(tr, coordinates)}

    elif geometry_type == 'LineString':
        return {'type': geometry_type, 'coordinates': transform_line(tr, coordinates)}

    elif geometry_type == 'Polygon':
        return {'type': geometry_type, 'coordinates': transform_polygon(tr, coordinates)}

    elif geometry_type == 'MultiPoint':
        return {'type': geometry_type, 'coordinates': [transform_point(tr, part) for part in coordinates]}

    elif geometry_type == 'MultiLineString':
        return {'type': geometry_type, 'coordinates': [transform_line(tr, part) for part in coordinates]}

    elif geometry_type == 'MultiPolygon':
        return {'type': geometry_type, 'coordinates': [transform_polygon(tr, part) for part in coordinates]}


def transform_proj(raw: Any, layer: Layer):
    try:
        source = layer.transform_proj.source
        if source not in PROJ_CACHE:
            PROJ_CACHE[source] = Transformer.from_crs(
                CRS.from_string(source), EPSG4326, always_xy=True)
        tr = PROJ_CACHE[source]

        for feature in raw['features']:
            feature['geometry'] = transform_geometry(tr, feature['geometry'])
        return raw
    except Layer.transform_proj.RelatedObjectDoesNotExist:
        return raw


def get_layer(request, hostname: str, id: int):
    server = get_object_or_404(Server, pk=hostname)
    layer = get_object_or_404(Layer, pk=id)
    raw = get_raw_data(make_url(server, layer))
    transformed = transform_proj(transform_python(raw, layer), layer)

    return JsonResponse(transformed)
