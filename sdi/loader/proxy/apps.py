from django.apps import AppConfig


class ProxyConfig(AppConfig):
    name = 'loader.proxy'
    label = 'proxy'
    verbose_name = 'Proxy'
    geodata = True
