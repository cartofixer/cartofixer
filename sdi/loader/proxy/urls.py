from django.urls import path
from django.urls import reverse

from .views import (
    get_layer
)


def from_identifier(rid, request=None):
    hostname = rid.hostname
    id = rid.path[1:]
    rel_url = reverse('proxy.get_layer', args=[hostname, id])
    if request is not None:
        return request.build_absolute_uri(rel_url)
    return rel_url


urlpatterns = [
    path('proxy/layer/<hostname>/<int:id>/',
         get_layer,
         name='proxy.get_layer'),
]
