from django.contrib.admin import site, ModelAdmin
from .fields import LinguaField
from .forms import LinguaFormField


class LinguaAdmin(ModelAdmin):
    """Lingua Admin"""
    # formfield_overrides = {LinguaField: {'widget': LinguaWidget}}
    # formfield_overrides = {LinguaField: LinguaFormField}
