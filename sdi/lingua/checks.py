from django.conf import settings
from django.core.checks import Error, Warning, register


@register()
def check_language_code(app_configs, **kwargs):
    print('check_language_code: {}'.format(
        getattr(settings, 'LANGUAGE_CODE', None)))
    errors = []
    if getattr(settings, 'LANGUAGE_CODE', None) is None:
        errors.append(
            Error(
                'No Language Code Configured',
                hint='Set LANGUAGE_CODE in your settings',
                obj=settings,
                id='sdi.lingua.E001',
            ))
    return errors


@register()
def check_languages(app_configs, **kwargs):
    print('check_languages: {}'.format(getattr(settings, 'LANGUAGES', None)))
    errors = []
    languages = getattr(settings, 'LANGUAGES', None)
    if languages is None:
        errors.append(
            Error(
                'No Languages Configured',
                hint='Set LANGUAGES in your settings',
                obj=settings,
                id='sdi.lingua.E002',
            ))
    elif len(languages) == 1 and languages[0][0] == 'en':
        errors.append(
            Warning(
                'Only English Language Configured',
                hint=
                'Looks like a default config, are sure of this? You might want to set LANGUAGES yourself',
                obj=settings,
                id='sdi.lingua.W001',
            ))

    return errors