#  Copyright (C) 2018 Atelier Cartographique <contact@atelier-cartographique.be>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from jsonfield import JSONField

from django.db import models
from django.conf import settings


def code_to_propname(code):
    return code.replace('-', '_')


LANGUAGE_CODE = code_to_propname(getattr(settings, 'LANGUAGE_CODE'))
LANGUAGES = tuple(
    code_to_propname(code) for code, _name in getattr(settings, 'LANGUAGES'))


class MessageManager(models.Manager):
    def __init__(self, *args, **kwargs):
        self.message_type = kwargs.pop('message_type')
        super().__init__(*args, **kwargs)

    def get_queryset(self):
        return super().get_queryset().filter(field_name=self.message_type)

    def create(self, content):
        return super().create(field_name=self.message_type, content=content)

    def create_message(self, **kwargs):
        filterd_args = dict()
        for code in LANGUAGES:
            if code in kwargs:
                filterd_args[code] = kwargs.pop(code)

        return self.create(filterd_args)    


class MessageRecord(models.Model):
    # class Meta:
    #     abstract = True

    id = models.AutoField(primary_key=True)
    field_name = models.CharField(max_length=128)
    content = JSONField()

    def to_object(self):
        return type(self.field_name, (), self.content)

    def __str__(self):
        label = getattr(self.to_object(), LANGUAGE_CODE,
                        'LANGUAGE_CODE should be one of {}.'.format(LANGUAGES))
        end = ''
        if len(label) > 64:
            end = '…'

        return '[{}] {}{}'.format(self.field_name, label[:64], end)

    def tr(self, lang):
        return getattr(self.to_object(), lang, str(self))

    def __getattr__(self, name):
        if name not in LANGUAGES:
            raise AttributeError(name)
        return getattr(self.to_object(), name)

    def update_record(self, **kwargs):
        print('update_record({})'.format(kwargs))
        for k, v in kwargs.items():
            if k in LANGUAGES:
                self.content[k] = v
        self.save()

    def to_dict(self):
        # ret = dict()
        # for code  in LANGUAGES:
        #     ret[code] = getattr(self, code)

        return self.content

    def contains(self, s):
        for v in self.content.values():
            if v == s:
                return True
        return False
