from django.db import models
from django.forms import ModelForm, HiddenInput, CharField
from django.contrib.admin import site, ModelAdmin
import json

from ..apps import LinguaConfig
from .record import MessageRecord, MessageManager, LANGUAGE_CODE, LANGUAGES

lingua_app_name = LinguaConfig.name

models_collection = dict()


def make_form_class():
    class Meta:
        model = MessageRecord
        fields = LANGUAGES

    class F(ModelForm):
        def __init__(self, *args, **kwargs):
            # raise Exception('A')
            if 'instance' in kwargs and kwargs['instance'] is not None:
                kwargs['initial'] = kwargs['instance'].content

            super().__init__(*args, **kwargs)

        def save_m2m(self):
            pass

        def save(self, commit=True):
            instance = self.instance
            cleaned_data = self.cleaned_data
            for c in LANGUAGES:
                if c in cleaned_data and len(cleaned_data[c]):
                    instance.content[c] = cleaned_data[c]
                else:
                    instance.content.pop(c, None)
            # raise Exception('B')
            if commit:
                instance.save()
            return instance

    fields = dict(Meta=Meta)

    for c in LANGUAGES:
        fields[c] = CharField(required=False)

    return type('MessageForm', (F, ), fields)


MessageForm = make_form_class()

# class JSONWidget(jsonfield.widgets.JSONWidget):
#     def render(self, name, value, **kwargs):
#         return super().render(name, value, kwargs.get('attrs'))


class MessageAdmin(ModelAdmin):
    form = MessageForm
    # formfield_overrides = {jsonfield.JSONField: {'widget': JSONWidget}}
    list_display = LANGUAGES
    # search_fields = LANGUAGES


def make_f(c):
    def f(self, i):
        content = i.content
        return content.get(c, '-')

    f.short_description = c
    return f


def register_admin(message_type):
    mod = models_collection[message_type]
    attrs = dict()
    for c in LANGUAGES:
        attrs[c] = make_f(c)

    adm = type(
        'Admin_{}'.format(message_type),
        (MessageAdmin, ),
        attrs,
    )
    site.register(mod, adm)


def make_init(message_type):
    def init(self, *args, **kwargs):
        super(MessageRecord, self).__init__(*args, **kwargs)
        self.field_name = message_type

    return init

def get_model(message_type, app_name):
    if message_type not in models_collection:
        meta = type('Meta', (), dict(app_label=app_name, proxy=True))
        module_name = '{}.models'.format(app_name)
        
        attrs = dict(
            __module__=module_name,
            Meta=meta,
            objects=MessageManager(message_type=message_type),
            __init__=make_init(message_type),
            )

        Model = type(
            message_type,
            (MessageRecord, ),
            attrs,
        )

        models_collection[message_type] = Model
        register_admin(message_type)

    return models_collection[message_type]


def label_field(message_type, app_name=lingua_app_name, **kwargs):
    return models.ForeignKey(
        get_model(message_type, app_name),
        on_delete=models.CASCADE,
        related_name=message_type,
        **kwargs,
    )


def text_field(message_type, app_name=lingua_app_name, **kwargs):
    return models.ForeignKey(
        get_model(message_type, app_name),
        on_delete=models.CASCADE,
        related_name=message_type,
        **kwargs,
    )


def nullable_label_field(message_type, app_name=lingua_app_name, **kwargs):
    if kwargs is not None:
        kwargs.pop('null', None)
        kwargs.pop('blank', None)
    return label_field(message_type, app_name=app_name,null=True, blank=True, **kwargs)


def nullable_text_field(message_type, app_name=lingua_app_name, **kwargs):
    if kwargs is not None:
        kwargs.pop('null', None)
        kwargs.pop('blank', None)
    return text_field(message_type, app_name=app_name, null=True, blank=True, **kwargs)


def message(message_type, **kwargs):
    mod = models_collection[message_type]
    return mod.objects.create_message(**kwargs)


def simple_message(message_type, msg):
    kwargs = dict()
    for code in LANGUAGES:
        kwargs[code] = msg
    return message(message_type, **kwargs)