import json
from django.forms import MultiValueField, MultiWidget, Textarea, CharField
from .language import LANGUAGES


class LinguaWidget(MultiWidget):
    template_name = 'lingua/widgets/lingua.html'

    def __init__(self, attrs=None):
        widgets = [Textarea() for _ in LANGUAGES]
        super().__init__(widgets, attrs)

    def get_context(self, name, value, attrs):
        context = super().get_context(name, value, attrs)
        subwidgets = context['widget']['subwidgets']
        context['widget']['subwidgets'] = zip(LANGUAGES, subwidgets)

        return context

    def decompress(self, value):
        print('decompress', value)
        from .fields import LinguaRecord
        if value is None:
            return []
        getter = getattr if isinstance(
            value, LinguaRecord) else lambda o, k, d: o.get(k, d)

        return [getter(value, k, "-") for k in LANGUAGES]


class LinguaFormField(MultiValueField):
    widget = LinguaWidget

    def __init__(self, *, require_all_fields=True, **kwargs):
        fields = [CharField() for _ in LANGUAGES]
        super().__init__(fields, **kwargs)

    def compress(self, value):
        if value is None:
            return dict()

        return json.dumps(dict(zip(LANGUAGES, value)))
