import { Nullable } from 'sdi/util';
import { IUser } from 'sdi/source';
import { Layout, Route } from '../events/route';
import { ButtonComponent } from 'sdi/components/button';



// State Augmentation
declare module 'sdi/shape' {
    export interface IShape {
        'app/layout': Layout;
        'app/selectd-item': Nullable<string>;
        'data/user': Nullable<IUser>;
        'component/button': ButtonComponent;

        'navigate/next': Nullable<Route>;
        'app/name': string;
    }
}


export const defaultAppShape =
    () => ({
        'app/layout': 'home' as Layout,
        'app/selectd-item': null,
        'data/user': null,
        'component/button': {},
        'navigate/next': null,
        'app/name': '',
    });
