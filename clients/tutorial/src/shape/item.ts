import { Nullable } from 'sdi/util';
import { Item, defaultItem } from '../components/item';


// State Augmentation
declare module 'sdi/shape' {
    export interface IShape {
        'component/items': Item[];
        'component/items/new/name': Nullable<string>;
        'component/items/new/content': Nullable<string>;
    }
}


export const defaultItemsShape =
    () => ({
        'component/items': [defaultItem],
        'component/items/new/name': null,
        'component/items/new/content': null,

    });
