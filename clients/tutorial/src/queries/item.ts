

import { query } from 'sdi/shape';
import { fromNullable } from 'fp-ts/lib/Option';


export const getItems = () => query('component/items');

export const getNewName =
    () => fromNullable(query('component/items/new/name'));

export const getNewContent =
    () => fromNullable(query('component/items/new/content')).getOrElse('');
