import { fromNullable } from 'fp-ts/lib/Option';
import { query } from 'sdi/shape';
import { MessageRecord } from 'sdi/source';
import { Route } from '../events/route';


export const getNext =
    () => fromNullable(query('navigate/next'));


const links: [MessageRecord, Route][] = [
    [{ fr: 'Home', nl: 'Home' }, 'home'],
    [{ fr: 'Map', nl: 'Map' }, 'map'],
    [{ fr: 'Table', nl: 'Table' }, 'table'],
    [{ fr: 'Input', nl: 'Input' }, 'input'],
    [{ fr: 'geothermie-front', nl: 'geothermie-front' }, 'geo-front'],
];


export const getLinks = () => links;
