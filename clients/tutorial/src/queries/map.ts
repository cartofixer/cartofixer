import { IMapInfo, IMapBaseLayer } from 'sdi/source';
import { queryK } from 'sdi/shape';

export const getBaseLayer = (): IMapBaseLayer => ({
  'name': {
    'fr': 'Ortho 2016',
    'nl': 'Ortho 2016'
  },
  'attribution': {},
  'srs': 'EPSG:3857',
  'url': '/webservice/wmsproxy/urbis.irisnet.be',
  'params': {
    'VERSION': '1.1.1',
    'LAYERS': {
      'fr': 'Urbis:Ortho2016',
      'nl': 'Urbis:Ortho2016'
    },
  }
});

export const getView = queryK('port/map/view');

export const getMapInfo = (): IMapInfo => ({
  baseLayer: 'urbis.irisnet.be/urbis_gray',
  id: 'tutorial-map',
  url: `/dev/null/tutorial-map/`,
  lastModified: 1523599299611,
  status: 'published',
  title: { fr: 'TUTORIAL', nl: 'TUTORIAL', en: 'TUTORIAL' },
  description: { fr: 'TUTORIAL', nl: 'TUTORIAL', en: 'TUTORIAL' },
  categories: [],
  attachments: [],
  layers: [],
})