import { DIV, H2, NODISPLAY, SPAN } from 'sdi/components/elements';
import { inputLongText, inputText } from 'sdi/components/input';
import { getItems, getNewName, getNewContent } from 'tutorial/src/queries/item';
import { setNewName, setNewContent, addNewItem } from 'tutorial/src/events/item';
import { navigateItem } from 'tutorial/src/events/route';

import { tr } from 'sdi/locale';
import { Item } from './index';
import { getUsername } from 'tutorial/src/queries/app';



const renderNewItem =
    () => DIV({ className: 'new-item' },
        getNewName().foldL(
            () => inputText(() => '', setNewName, { key: 'new-name', placeholder: 'A name' }),
            newName => inputText(() => newName, setNewName, { key: 'new-name' }),
        ),
        inputLongText(getNewContent, setNewContent, { key: 'new-content' }),
        DIV({
            className: 'submit-new',
            onClick: addNewItem,
        }, tr.tuto('addItem')),
    );


const renderItem =
    (i: Item) =>
        DIV({ className: 'ietm-item', key: i.name },
            H2({
                onClick: () => navigateItem(i.name),
            }, i.name));


export const render =
    () => DIV({ className: 'item-list' },
        DIV({ className: 'greetings' }, getUsername().fold(
            NODISPLAY(),
            username => SPAN({}, `Hello ${username}`),
        )),
        getItems().map(renderItem),
        renderNewItem(),
    );

export default render;
