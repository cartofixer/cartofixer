
export interface Item {
    name: string;
    content: string;
}


export const defaultItem: Item = {
    name: 'Default',
    content: 'I am a default item!',
};


