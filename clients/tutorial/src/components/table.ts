import { baseTable, Config } from 'sdi/components/table';
import { DIV } from 'sdi/components/elements';

import { tableUserQueries } from '../queries/table';
import { tableUserEvents } from '../events/table';

const tableConfig: Config = {
    className: 'table--',
};

const table = baseTable(tableUserQueries, tableUserEvents)(tableConfig);

const render = () => DIV({}, table());

export default render;
