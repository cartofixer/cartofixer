import { create } from 'sdi/map';
import { getBaseLayer, getView, getMapInfo } from '../queries/map';
import { Option, none, some } from 'fp-ts/lib/Option';
import { updateView, setScaleLine } from '../events/map';
import { DIV } from 'sdi/components/elements';


let mapUpdate: Option<() => void> = none;
let mapSetTarget: Option<(e: HTMLElement) => void> = none;

const attachMap = (element: HTMLElement | null) => {
    mapUpdate = mapUpdate.foldL(
        () => {
            const { update, setTarget } = create('tutorial-map', {
                getBaseLayer,
                getView,
                getMapInfo,

                updateView,
                setScaleLine,

                element,
            });
            mapSetTarget = some(setTarget);
            update();
            return some(update);
        },
        update => some(update),
    );

    if (element) {
        mapSetTarget.map(f => f(element));
    }
};

const render =
    () => {
        mapUpdate.map(f => f());
        return (
            DIV({ className: 'map-wrapper' },
                DIV({
                    id: 'tutorial-map',
                    className: 'map',
                    ref: attachMap,
                }))
        );
    };


export default render;
