import { none } from 'fp-ts/lib/Option';
import { A, DIV, H2 } from 'sdi/components/elements';
import { inputLongText, inputText, renderSelect } from 'sdi/components/input';
import { IUser } from 'sdi/source';
import { setoidIdentified } from 'sdi/util';
import { setInputText } from '../events/input';
import { getInputText } from '../queries/input';
import { getUsers } from '../queries/table';

const renderText =
    () => inputText(
        getInputText,
        setInputText,
        { placeholder: 'put your text here' });

const renderTextLong =
    () => inputLongText(
        getInputText,
        setInputText,
        { placeholder: 'put your text here' });


const text =
    () => DIV({},
        H2({}, 'Text Inputs'),
        renderText(),
        renderTextLong(),
    );


const renderUser =
    (u: IUser) => DIV({}, u.name);

const selectUser =
    (u: IUser) => setInputText(`selected user => ${u.name}`);

const renderInputUsers =
    renderSelect(
        setoidIdentified<IUser>(),
        renderUser,
        selectUser,
    );


const render =
    () => DIV({},
        text(),
        A({ href: getInputText() }, getInputText()),
        renderInputUsers(getUsers(), none),
    );

export default render;
