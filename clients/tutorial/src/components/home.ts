import { DIV, SPAN } from 'sdi/components/elements';
import tr, { fromRecord } from 'sdi/locale';
import { MessageRecord } from 'sdi/source';
import { Route, navigate } from '../events/route';
import { getLinks } from '../queries/navigate';
import { openNavigate } from '../events/modal';
import { makeLabel } from '../components/buttons';
import { clearNext } from '../events/navigate';



const navigateButton = makeLabel('navigate', 2, () => tr.core('go'));

const renderLink =
    (name: MessageRecord, onClick: () => void) =>
        DIV({ onClick }, SPAN({}), fromRecord(name));

const goTo =
    (r: Route) => () => navigate(r, []);



export const render =
    () =>
        DIV({ className: 'main-app home' },
            ...(getLinks().map(([n, r]) => renderLink(n, goTo(r)))),
            navigateButton(() => {
                clearNext();
                openNavigate();
            }));

export default render;
