import { MessageStore, fromRecord } from 'sdi/locale';

const messages = {
    addItem: {
        fr: 'ajouter un article',
        en: 'Add an item',
    },

    noItemError: {
        fr: 'Desole, pas d\'article avec ce nom',
        en: 'Sorry',
    },
};

type MDB = typeof messages;
export type TutoMessageKey = keyof MDB;


declare module 'sdi/locale' {
    export interface MessageStore {
        tuto(k: TutoMessageKey): Translated;
    }
}

MessageStore.prototype.tuto = (k: TutoMessageKey) => fromRecord(messages[k]);

