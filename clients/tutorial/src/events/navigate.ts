import { Route } from './route';
import { assign } from 'sdi/shape';



export const setNext = (r: Route) =>
    assign('navigate/next', r);

export const clearNext = () =>
    assign('navigate/next', null);

