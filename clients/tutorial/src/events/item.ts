import { fromNullable } from 'fp-ts/lib/Option';

import { dispatch, assign, query } from 'sdi/shape';
import { scopeOption } from 'sdi/lib';

import { Item } from 'tutorial/src/components/item';


const addItem =
    (i: Item) =>
        dispatch('component/items',
            xs => xs.filter(x => x.name !== i.name).concat([i]));


export const addNewItem =
    () =>
        scopeOption()
            .let('name', fromNullable(query('component/items/new/name')))
            .let('content', fromNullable(query('component/items/new/content')))
            .map(({ name, content }) => addItem({ name, content }))
            .map(() => {
                assign('component/items/new/name', null);
                assign('component/items/new/content', null);
            });


export const setNewName =
    (name: string) => assign('component/items/new/name', name);

export const setNewContent =
    (content: string) => assign('component/items/new/content', content);


export const selectItem =
    (name: string) => {
        assign('app/selectd-item', name);
        assign('app/layout', 'single');
    };
