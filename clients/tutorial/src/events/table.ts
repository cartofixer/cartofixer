import { tableEvents } from 'sdi/components/table';
import { dispatchK, assignK } from 'sdi/shape';
import { fetchAllUsers } from '../remote';
import { getApiUrl } from 'sdi/app';


export const tableUserEvents = tableEvents(dispatchK('component/table'));

export const fetchAllUsersEvents =
    () => fetchAllUsers(getApiUrl('users'))
        .then(assignK('data/users'))
        .catch(err => console.error('cannot load users', err));
