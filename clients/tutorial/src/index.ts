
import { displayException } from 'sdi/app';
import { configure, defaultShape, IShape } from 'sdi/shape';
import { AppConfigIO, getMessage, source } from 'sdi/source';
import { app } from './app';
import './locale';
import { defaultAppShape, defaultInputShape, defaultItemsShape, defaultMapState, defaultTableShape } from './shape';



export const main =
    (SDI: any) =>
        AppConfigIO
            .decode(SDI)
            .fold(
                (errors) => {
                    const textErrors = errors.map(e => getMessage(e.value, e.context));
                    displayException(textErrors.join('\n'));
                },
                (config) => {
                    const initialState: IShape = {
                        ...defaultShape(config),
                        ...defaultAppShape(),
                        ...defaultItemsShape(),
                        ...defaultMapState(),
                        ...defaultTableShape(),
                        ...defaultInputShape(),
                    };

                    const stateActions = source<IShape>([]);
                    const store = stateActions(initialState);
                    configure(store); // too much boilerplate?

                    const start = app(config.args)(store);
                    start();
                });
