import { MessageStore, formatMessage } from 'sdi/locale';

const messages = {

    login: {
        fr: 'Se connecter',
        nl: 'Inloggen', // nldone
        en: 'Sign in',
    },

    logout: {
        fr: 'Se déconnecter',
        nl: 'Uitloggen', // nldone
        en: 'Logout',
    },

    loginError: {
        fr: 'Une erreur s’est produite, vérifiez votre nom d’utilisateur ou votre mot de passe.',
        nl: 'Er is een fout opgetreden, controleer uw gebruikersnaam of wachtwoord.', // nldone
        en: 'An error has occurred, check your username or password.',
    },

    userName: {
        fr: 'Nom d’utilisateur',
        nl: 'Gebruikersnaam', // nldone
        en: 'User name',
    },

    password: {
        fr: 'Mot de passe',
        nl: 'Wachtwoord', // nldone
        en: 'Password',
    },

    connectionSDI: {
        fr: 'Connexion',
        nl: 'Verbinding', // nldone
        en: 'Connection',
    },

};

type MDB = typeof messages;
export type LoginMessageKey = keyof MDB;


declare module 'sdi/locale' {
    export interface MessageStore {
        login(k: LoginMessageKey): Translated;
    }
}


MessageStore.prototype.login =
    function (k: LoginMessageKey) {
        return this.getEdited('angled', k, () => formatMessage(messages[k]));
    };
