import buttonFactory from 'sdi/components/button';
import {
    queryK,
    dispatchK,
} from 'sdi/shape';


export const { makeLabelAndIcon } = buttonFactory(
    queryK('component/button'), dispatchK('component/button'));

