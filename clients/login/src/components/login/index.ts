import { Credentials } from 'sdi/source';
import tr from 'sdi/locale';
import { DIV, H1, INPUT } from 'sdi/components/elements';
import { isENTER } from 'sdi/components/keycodes';

import { getUsername, getPassword, getLoginError } from '../../queries/login';
import { setUsername, setPassword, tryLogin } from '../../events/login';
import { makeLabelAndIcon } from '../button';



export interface LoginForm {
    credentials: Credentials;
}

export const defaultLoginForm =
    (): LoginForm => ({
        credentials: {
            username: '',
            password: '',
        },
    });

const tryOnReturn =
    (e: React.KeyboardEvent<HTMLInputElement>) => {
        if (isENTER(e)) {
            tryLogin();
        }
    };

const loginButton = makeLabelAndIcon('login', 1, 'sign-in', () => tr.login('login'));

const renderUsername =
    () => (
        DIV({ className: 'username' },
            DIV({ className: 'login-label' }, tr.login('userName')),
            INPUT({
                key: 'login-username',
                type: 'text',
                defaultValue: getUsername(),
                onChange: e => setUsername(e.currentTarget.value),
                onKeyUp: tryOnReturn,
            })));


const renderPassword =
    () => (
        DIV({ className: 'password' },
            DIV({ className: 'login-label' }, tr.login('password')),
            INPUT({
                key: 'login-password',
                type: 'password',
                defaultValue: getPassword(),
                onChange: e => setPassword(e.currentTarget.value),
                onKeyUp: tryOnReturn,
            })));

const wrapError =
    (...children: React.ReactNode[]) =>
        getLoginError().foldL(
            () => DIV({ className: 'login-widget' }, ...children),
            _err => DIV({ className: 'login-widget error-wrapper' },
                DIV({ className: 'error-message' }, tr.login('loginError')),
                ...children),
        );

const render =
    () =>
        DIV({ className: 'login-wrapper' },
            H1({}, tr.login('connectionSDI')),
            wrapError(
                renderUsername(),
                renderPassword(),
                loginButton(tryLogin)));

export default render;
