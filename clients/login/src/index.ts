

/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'sdi/polyfill';
import './shape';
import './locale';

import * as debug from 'debug';
import { source, AppConfigIO, getMessage } from 'sdi/source';
import { IShape, configure, defaultShape } from 'sdi/shape';

import App from './app';
import { defaultLoginForm } from './components/login';
import { displayException } from 'sdi/app';

const logger = debug('sdi:index');


export const main =
    (SDI: any) => {
        AppConfigIO
            .decode(SDI)
            .fold(
                (errors) => {
                    const textErrors = errors.map(e => getMessage(e.value, e.context));
                    displayException(textErrors.join('\n'));
                },
                (config) => {
                    const next = config.args.length > 0 ?
                        `${config.root}${config.args.join('/')}` :
                        null;

                    const initialState: IShape = {
                        ...defaultShape(config),
                        'app/lang': 'fr',
                        'app/layout': ['Login'],
                        'app/next': next,

                        'component/button': {},
                        'component/login': defaultLoginForm(),
                        'component/login/error': null,

                        'data/alias': [],
                        'data/user': null,
                    };

                    if ((SDI.user !== null) && (next !== null)) {
                        window.location.assign(next);
                        return;
                    }

                    try {
                        const start = source<IShape>(['app/lang']);
                        const store = start(initialState);
                        configure(store);
                        const app = App(store);
                        logger('start rendering');
                        app();
                    }
                    catch (err) {
                        displayException(`${err}`);
                    }
                });
    };


logger('loaded');
