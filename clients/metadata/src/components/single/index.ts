import { fromPredicate } from 'fp-ts/lib/Either';

import { DIV, H1, INPUT, TEXTAREA, SPAN, NODISPLAY } from 'sdi/components/elements';
import tr, { fromRecord } from 'sdi/locale';
import { Inspire, MessageRecord, getMessageRecord, makeRecord, MessageRecordLang } from 'sdi/source';

import { getLayout } from '../../queries/app';
import { setLayout } from '../../events/app';
import {
    formIsSaving,
    getCurrentDatasetMetadata,
    getMdDescription,
    getMdTitle,
    getTemporalReference,
    getKeywords,
    getPersonOfContact,
    // getTopics,
    // getTopicList,
    // isSelectedTopic,
} from '../../queries/metadata';

import {
    saveMdForm,
    setMdDescription,
    setMdTitle,
    removeKeyword,
    // removeTopic,
    // addTopic,
    mdPublish,
    mdDraft,
} from '../../events/metadata';

import { renderSelectKeyword } from './keywords';

import { makeRemove, makeLabelAndIcon, makeLabel } from '../button';

export interface MdForm {
    title: MessageRecord;
    description: MessageRecord;
    topics: string[];
    keywords: string[];
    published: boolean;
    saving: boolean;
}

const saveButton = makeLabelAndIcon('save', 1, 'floppy-o', () => tr.meta('save'));
const removeButton = makeRemove('remove', 2, () => tr.meta('remove'), () => tr.meta('remove'));
const addKeyword = makeLabelAndIcon('add', 2, 'plus', () => tr.meta('add'));


export const defaultMdFormState =
    (): MdForm => ({
        title: makeRecord(),
        description: makeRecord(),
        topics: [],
        keywords: [],
        published: false,
        saving: false,
    });


type TextGetter = () => string;
type TextSetter = (a: string) => void;

const isNotSaving = fromPredicate<void, React.ReactNode>(() => !formIsSaving(), () => { });


const renderInputText =
    (label: string, get: TextGetter, set: TextSetter) => {
        const defaultValue = get();
        const update = (e: React.ChangeEvent<HTMLInputElement>) => {
            const newVal = e.currentTarget.value;
            set(newVal);
        };
        return (
            INPUT({
                defaultValue,
                placeholder: label,
                type: 'text',
                onChange: update,
            })
        );
    };


const renderTextArea =
    (label: string, get: TextGetter, set: TextSetter) => {
        const defaultValue = get();
        const update = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
            const newVal = e.currentTarget.value;
            set(newVal);
        };
        return (
            TEXTAREA({
                defaultValue,
                placeholder: label,
                onChange: update,
            })
        );
    };


const renderEditLang =
    (l: MessageRecordLang, title: string, abstract: string, notice?: string) =>
        DIV({ className: `app-col-wrapper meta-${l}` },
            DIV({ className: 'app-col-header' }, `${l}${notice !== undefined ? ` (${notice})` : ''}`),
            DIV({ className: 'app-col-main' },
                DIV({ className: 'label' }, title),
                renderInputText(title,
                    getMdTitle(l), setMdTitle(l)),
                DIV({ className: 'label' }, abstract),
                renderTextArea(abstract,
                    getMdDescription(l), setMdDescription(l))));


const renderEdit =
    (m: Inspire) => {
        const leftPane = (
            getLayout() === 'Single' ?
                renderInfo(m) :
                renderCommon(m));
        return (
            DIV({ className: 'meta-edit' },
                leftPane,
                renderEditLang('fr', 'Titre', 'Résumé'),
                renderEditLang('nl', 'Titel', 'Overzicht'),
                renderEditLang('en', 'Title', 'Abstract')));
    };


const renderPoc =
    (m: Inspire) =>
        m.metadataPointOfContact.map(
            id => getPersonOfContact(id).fold(
                NODISPLAY({ key: 'renderPoc-none' }),
                poc =>
                    DIV({ className: 'point-of-contact', key: `renderPoc-${poc.id}` },
                        SPAN({ className: 'contact-name' }, poc.contactName),
                        SPAN({ className: 'contact-email' }, poc.email),
                        SPAN({ className: 'contact-organisation' },
                            fromRecord(getMessageRecord(poc.organisationName))))));




const renderCommon =
    (_m: Inspire) => (
        DIV({ className: 'app-col-wrapper meta-common' },
            DIV({ className: 'app-col-header' }, tr.meta('keywords')),
            DIV({ className: 'app-col-main' },
                // renderSelectTopic(),
                renderSelectKeyword())));


const renderAction =
    (_m: Inspire) => (
        DIV({ className: 'meta-action' },
            isNotSaving(saveButton(saveMdForm)).fold(
                () => DIV({ className: 'saving' },
                    SPAN({ className: 'loader-spinner' }),
                    SPAN({}, tr.meta('saving'))),
                e => e),
        ));

const labeledString =
    (l: string, v: string) => (
        DIV({ className: 'metadata-label' },
            DIV({ className: 'label' }, l),
            DIV({ className: 'value' }, v)));

const labeledNode =
    (l: string, v: React.ReactNode) => (
        DIV({ className: 'metadata-label' },
            DIV({ className: 'label' }, l), v));


// const publishButton = makeLabelAndIcon('toggle-off', 2, 'toggle-off', 'toggle-off');
// const unpublishButton = makeLabelAndIcon('toggle-on', 2, 'toggle-on', 'toggle-on');

const publishButton = makeLabel('publish', 2, () => tr.meta('toggle-on'));
const unpublishButton = makeLabel('publish', 2, () => tr.meta('toggle-off'));


const renderPublishState =
    ({ published }: Inspire) => {
        if (published) {
            return DIV({ className: 'publication' },
                tr.meta('metadata:isPublishedInternalandExternal'),
                DIV({},
                    DIV({ className: 'label' }, tr.meta('metadata:publicationToCatalogue')),
                    unpublishButton(mdDraft)));
        }
        return DIV({ className: 'publication' },
            tr.meta('metadata:isPublishedInternal'),
            DIV({},
                DIV({ className: 'label' }, tr.meta('metadata:publicationToCatalogue')),
                publishButton(mdPublish)));
    };


// const renderPublishState =
//     ({ published }: Inspire) => {
//         if (published) {
//             return DIV({ className: 'toggle' },
//                 DIV({ className: 'no-active' }, tr.meta('internal')),
//                 unpublishButton(mdDraft),
//                 DIV({ className: 'active' }, tr.meta('inspireCompliant')));
//         }
//         return DIV({ className: 'toggle' },
//             DIV({ className: 'active' }, tr.meta('internal')),
//             publishButton(mdPublish),
//             DIV({ className: 'no-active' }, tr.meta('inspireCompliant')));
//     };



const renderKeywords =
    () => DIV({}, ...getKeywords()
        .map(kw => (
            DIV({ className: 'keyword' },
                removeButton(() => removeKeyword(kw.id)),
                SPAN({ className: 'value' }, fromRecord(kw.name))))));

const renderInfo =
    (m: Inspire) => (
        DIV({ className: 'app-col-wrapper metadata-info' },
            DIV({ className: 'app-col-header' }, tr.meta('layerInfo')),
            DIV({ className: 'app-col-main' },
                labeledString(tr.meta('geometryType'), m.geometryType),
                labeledString(tr.meta('layerId'), m.uniqueResourceIdentifier),
                labeledNode(tr.meta('publicationStatus'), renderPublishState(m)),
                labeledString(tr.meta('temporalReference'), getTemporalReference(m.temporalReference)),
                // labeledNode(tr.meta('topics'), renderTopics()),
                labeledNode(tr.meta('keywords'),
                    DIV({},
                        renderKeywords(),
                        addKeyword(() => setLayout('SingleAndKeywords')))),
                labeledNode(tr.meta('pointOfContact'), renderPoc(m)),
                // labeledString(tr.meta('identifier'), m.id),
            )));



const renderEditor =
    (m: Inspire) => (
        DIV({ className: 'metadata-editor' },
            H1({}, tr.meta('metadataEditor')),
            DIV({ className: 'meta-wrapper' },
                renderEdit(m)),
            DIV({ className: 'metadata-actions' },
                renderAction(m))));


const render =
    () => (
        getCurrentDatasetMetadata()
            .fold(
                H1({}, `Loading Current Metadata Failed`),
                renderEditor,
            ));


export default render;
