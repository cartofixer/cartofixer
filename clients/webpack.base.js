
const { resolve, basename } = require('path');
const { readdirSync, lstatSync } = require('fs');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const fs = require('fs')

const MAEP_CLIENT_STATIC = resolve('../sdi/clients/static/clients/apps')
const ASSETS_PUBLIC_PATH = '/static/clients/apps/'

class VersionPlugin {
    constructor(name, id, output_dir) {
        this.name = name
        this.id = id
        this.output_dir = output_dir
    }

    apply(compiler) {
        compiler.hooks.afterEmit.tap('VersionPlugin', () => {
            const path = resolve(this.output_dir, `${this.name}.version`)
            const data = JSON.stringify({
                bundle: `${this.name}-${this.id}.bundle.js`,
                style: `${this.name}-${this.id}.style.css`,
            })
            fs.writeFile(path, data, (err) => {
                if (err) {
                    console.error(`
            [FAILED] VersionPlugin:
            ${err}
            `)
                }
                else {
                    console.log(`
            VersionPlugin: ${path}
            ${data}
            `)
                }
            })
            return true
        });
    }

}

const configure =
    (ROOT, extra_paths = {}) =>
        (env = {}) => {
            if (typeof ROOT !== 'string') {
                throw (new Error('ROOT argument is mandatory'));
            }
            const platform = resolve(env.PLATFORM ? env.PLATFORM : `${__dirname}/platform`)


            const NAME = basename(ROOT);
            const ID = Math.floor(Date.now() / 1000).toString();

            const BUNDLE_ENTRY_PATH = resolve(ROOT, 'src/index.ts');
            const STYLE_ENTRY_PATH = resolve(ROOT, 'style/index.js');
            const OUTPUT_DIR = MAEP_CLIENT_STATIC // resolve(ROOT, 'dist');
            const SDI_ALIAS_ROOT = resolve(ROOT, '../sdi/');
            const SDI_ALIAS = readdirSync(SDI_ALIAS_ROOT).reduce((acc, dir) => {
                const res = resolve(SDI_ALIAS_ROOT, dir);
                if (lstatSync(res).isDirectory()) {
                    acc[`sdi/${dir}`] = res;
                }
                return acc;
            }, {})
            SDI_ALIAS[NAME] = resolve(ROOT);
            SDI_ALIAS['platform'] = resolve(platform);

            Object.keys(extra_paths).forEach(k => SDI_ALIAS[k] = resolve(extra_paths[k]))

            console.log(`NAME                ${NAME}`);
            console.log(`ID                  ${ID}`);
            console.log(`ROOT                ${ROOT}`);
            console.log(`BUNDLE_ENTRY_PATH   ${BUNDLE_ENTRY_PATH}`);
            console.log(`STYLE_ENTRY_PATH    ${STYLE_ENTRY_PATH}`);
            console.log(`OUTPUT_DIR          ${OUTPUT_DIR}`);
            console.log(`SDI_ALIAS           ${JSON.stringify(SDI_ALIAS, null, 20)}`);
            console.log(`PLATFORM            ${platform}`)




            const config = {
                context: ROOT,
                entry: {
                    [`${NAME}-${ID}.bundle`]: BUNDLE_ENTRY_PATH,
                    [`${NAME}-${ID}.style`]: STYLE_ENTRY_PATH,
                },

                output: {
                    path: OUTPUT_DIR,
                    publicPath: '/',
                    filename: '[name].js',
                    library: 'bundle',
                    // libraryExport: 'main',
                    libraryTarget: 'umd',
                },

                resolve: {
                    alias: SDI_ALIAS,
                    // proj4 module declaration is not consistent with its ditribution
                    mainFields: ["browser", "main", /* "module" */],
                    extensions: ['.ts', '.js'],
                },

                module: {
                    rules: [
                        {
                            enforce: 'pre',
                            test: /\.js$/,
                            exclude: resolve(ROOT, '../node_modules/'),
                            loader: 'source-map-loader',
                        },
                        {
                            enforce: 'pre',
                            test: /\.ts$/,
                            use: "source-map-loader"
                        },
                        {
                            test: /\.ts$/,
                            loaders: [
                                {
                                    loader: 'babel-loader',
                                },
                                {
                                    loader: 'ts-loader',
                                }
                            ],
                        },


                        /**
                         * Style
                         */
                        // CSS
                        {
                            test: /\.css$/,
                            use: [
                                { loader: MiniCssExtractPlugin.loader },
                                {
                                    loader: 'css-loader'
                                },
                            ]
                        },

                        // LESS
                        {
                            test: /\.less$/,
                            use: [
                                { loader: MiniCssExtractPlugin.loader },
                                {
                                    loader: 'css-loader'
                                },
                                {

                                    loader: 'less-loader', options: {
                                        // strictMath: true,
                                        // noIeCompat: true,
                                        paths: [resolve(platform, 'style'),]
                                    },
                                },
                            ],
                        },

                        //fonts
                        {
                            test: /\.(eot|ttf|woff|woff2)$/,
                            loader: 'file-loader',
                            options: {
                                publicPath: ASSETS_PUBLIC_PATH
                            }
                        },

                        //images
                        {
                            test: /\.(jpg|png|svg|gif)$/,
                            loader: 'file-loader',
                            options: {
                                publicPath: ASSETS_PUBLIC_PATH
                            }
                        }
                    ]
                },

                plugins: [

                    new VersionPlugin(NAME, ID, OUTPUT_DIR),

                    new MiniCssExtractPlugin({
                        filename: '[name].css',
                        chunkFilename: '[id].css',
                    }),
                ],
                devtool: 'source-map',

                watchOptions: {
                    ignored: /node_modules/
                },
            };

            return config;
        }

module.exports = configure;
