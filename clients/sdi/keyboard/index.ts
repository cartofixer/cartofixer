import * as debug from 'debug';
import * as Mousetrap from 'mousetrap';
import { Collection } from '../util';
// import { dispatch, observe } from '../shape';


const logger = debug('sdi:keyboard');

export type Keys = string[];
export type Bindings = Collection<Keys>;

Mousetrap.prototype.stopCallback = () => false;

// /**
//  * events
//  */

// export const addBinding =
//     (name: string, keys: Keys) =>
//         dispatch('app/keyboard/bindings', bs => ({ ...bs, [name]: keys }));


// observe('app/keyboard/bindings', (bs) => {
//     const names = Object.keys(bs);
//     reset();
//     names.forEach((name) => {
//         const keys = bs[name];
//         bind(keys, () => logger(`hit ${name}`));
//     });
// });

type bubbleEvent = boolean;

interface B {
    [k: string]: (e: ExtendedKeyboardEvent) => bubbleEvent;
}

// tslint:disable-next-line: variable-name
export const Binder =
    <T extends B>(m: T) => {
        Mousetrap.reset();
        // keys.forEach(k => bind(k, (_e, combo: K) => handler(combo)));
        Object.keys(m).forEach(k => Mousetrap.bind(k.split('|'), (e) => m[k](e)));
    };


// Binder(['a', 'b'], (k) => {
//     switch (k) {
//         case 'a': logger('a');
//         case 'b': logger('b');
//     }
//     return true;
// });

logger('loaded');
