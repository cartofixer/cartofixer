import { getApps } from '../app';
import { appName, appDisplayName, appUrl } from '../source';
import tr, { fromRecord } from '../locale';
import { DIV, SPAN, A } from './elements';
// import { nameToString } from './button/names';


export const renderAppSelect = (
    current: string,
) => {
    const apps = getApps();
    const tail = apps
        .filter(a => appName(a) !== current)
        .map(a =>
            appDisplayName(a).map(name =>
                DIV(
                    { className: `app-item ${appName(a)}` },
                    SPAN({ className: 'app-picto' }),
                    A(
                        { className: 'app-name', href: appUrl(a) },
                        fromRecord(name)
                    )
                )
            )
        );

    const head = DIV(
        { className: 'selected head' },
        SPAN(
            { className: 'head-label' },
            tr.core('applications')
        )
    );

    return DIV(
        {
            className: 'select__wrapper app-select',
            onClick: e => e.currentTarget.classList.toggle('active')
        },
        DIV({ className: 'select' }, head, DIV({ className: 'tail' }, ...tail))
    );
};
