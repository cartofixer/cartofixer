import { TableDataRow } from '.';
import { INPUT, SPAN, DIV } from '../elements';
import { uniqId, date8601, parseDate, datetime8601 } from '../../util';
import tr from '../../locale';
import { spanTooltipTop } from '../tooltip';
import { PropertyTypeDescriptor } from '../../source';

export type FilterOp = 'eq' | 'gt' | 'lt';

export interface FilterString {
    readonly tag: 'string';
    column: number;
    pattern: string;
}

export const filterString = (
    column: number,
    pattern: string
): FilterString => ({ tag: 'string', column, pattern });

export interface FilterNumber {
    readonly tag: 'number';
    column: number;
    value: number;
    op: FilterOp;
}

export const filterNumber = (
    column: number,
    value: number,
    op: FilterOp
): FilterNumber => ({
    tag: 'number',
    column,
    value,
    op
});

export interface FilterDate {
    readonly tag: 'date';
    column: number;
    date: string;
    op: FilterOp;
}

export const filterDate = (
    column: number,
    date: string,
    op: FilterOp
): FilterDate => ({
    tag: 'date',
    column,
    date,
    op
});

export interface FilterDateTime {
    readonly tag: 'datetime';
    column: number;
    datetime: string;
    op: FilterOp;
}

export const filterDateTime = (
    column: number,
    datetime: string,
    op: FilterOp
): FilterDateTime => ({
    tag: 'datetime',
    column,
    datetime,
    op
});


export type Filter = FilterDate | FilterNumber | FilterString | FilterDateTime;


export const makeInitialFilter = (
    column: number,
    dataType: PropertyTypeDescriptor,
) => {
    switch (dataType) {
        case 'number':
            return filterNumber(column, 0, 'gt');
        case 'date':
            return filterDate(
                column,
                date8601(new Date(0)),
                'gt'
            );
        case 'datetime':
            return filterDateTime(
                column,
                datetime8601(new Date(0)),
                'gt'
            );
        default:
            return filterString(column, '');
    }
};

const makeStringFilter = (f: FilterString) => {
    const pat = new RegExp(`.*${f.pattern}.*`, 'i');
    const col = f.column;
    return ({ cells }: TableDataRow) => pat.test(cells[col]);
};

const makeNumberFilter = (f: FilterNumber) => {
    const b = f.value;
    const op =
        f.op === 'eq'
            ? (a: number) => a === b
            : f.op === 'gt'
                ? (a: number) => a >= b
                : (a: number) => a <= b;
    const cond = (cell: string) => op(parseFloat(cell));
    const col = f.column;
    return ({ cells }: TableDataRow) => cond(cells[col]);
};

const makeDateFilter = (f: FilterDate) => {
    const b = Date.parse(f.date);
    const op =
        f.op === 'eq'
            ? (a: number) => a === b
            : f.op === 'gt'
                ? (a: number) => a >= b
                : (a: number) => a <= b;
    const cond = (cell: string) => op(Date.parse(cell));
    const col = f.column;

    return ({ cells }: TableDataRow) => cond(cells[col]);
};

const makeDateTimeFilter = (f: FilterDateTime) => {
    const b = Date.parse(f.datetime);
    const op =
        f.op === 'eq'
            ? (a: number) => a === b
            : f.op === 'gt'
                ? (a: number) => a >= b
                : (a: number) => a <= b;
    const cond = (cell: string) => op(Date.parse(cell));
    const col = f.column;

    return ({ cells }: TableDataRow) => cond(cells[col]);
};

const makeFilterFunction = (fs: Filter[]) => {
    const filters = fs.map((f) => {
        switch (f.tag) {
            case 'string':
                return makeStringFilter(f);
            case 'number':
                return makeNumberFilter(f);
            case 'date':
                return makeDateFilter(f);
            case 'datetime':
                return makeDateTimeFilter(f);
        }
    });

    return (row: TableDataRow) =>
        filters.reduce((acc, f) => (acc === false ? acc : f(row)), true);
};


export const filterRows = (filters: Filter[]) => (data: TableDataRow[]) =>
    data.filter(makeFilterFunction(filters));


/// < render

export type SetFilterFn = (filter: Filter, index: number) => void;

export const renderFilter = (
    filter: Filter,
    idx: number,
    keys: string[],
    filterData: SetFilterFn
) => {
    switch (filter.tag) {
        case 'string':
            return renderStringFilter(filter, idx, keys, filterData);
        case 'number':
            return renderNumberFilter(filter, idx, keys, filterData);
        case 'date':
            return renderDateFilter(filter, idx, keys, filterData);
        case 'datetime':
            return renderDateTimeFilter(filter, idx, keys, filterData);
    }
};


const renderStringFilter = (
    filter: FilterString,
    idx: number,
    keys: string[],
    filterData: SetFilterFn
) => {
    const { column, pattern } = filter;
    const colName = keys[column];

    const fieldName = SPAN({ className: 'search-field' }, colName);

    const searchField = INPUT({
        key: `filter-string-${idx}`,
        autoFocus: true,
        type: 'search',
        name: 'search',
        className: 'table-header-search-field',
        defaultValue: pattern,
        onChange: e => filterData(filterString(column, e.currentTarget.value), idx)
    });

    return DIV(
        {
            className: `table-search-item`,
            key: uniqId()
        },
        fieldName,
        searchField
    );
};



const opString = (op: FilterOp) => {
    switch (op) {
        case 'eq': return '=';
        case 'gt': return '≥';
        case 'lt': return '≤';
    }
};

const opFieldName =
    (op: FilterOp, colName: string) =>
        SPAN({ className: 'search-field' }, `${colName} ${opString(op)}`);


const renderOp = (
    op: FilterOp,
    filter: FilterNumber | FilterDate | FilterDateTime,
    idx: number,
    filterData: SetFilterFn
) =>
    op === filter.op
        ? DIV(
            {
                className: `picto filter-op filter-op--${op} selected`
            },
            opString(op)
        )
        : DIV(
            {
                className: `picto filter-op filter-op--${op} interactive`,
                onClick: () => filterData(Object.assign({}, filter, { op }), idx)
            },
            opString(op)
        );

const renderOps = (
    filter: FilterNumber | FilterDate | FilterDateTime,
    idx: number,
    filterData: SetFilterFn
) =>
    DIV(
        { className: 'filter-op__wrapper' },
        SPAN({ className: 'filter-op__label' }, `${tr.core('operator')} : `),
        spanTooltipTop(tr.core('equal'), {}, renderOp('eq', filter, idx, filterData)),
        spanTooltipTop(tr.core('greaterThanOrEqual'), {}, renderOp('gt', filter, idx, filterData)),
        spanTooltipTop(tr.core('lessThanOrEqual'), {}, renderOp('lt', filter, idx, filterData))
    );

const renderNumberFilter = (
    filter: FilterNumber,
    idx: number,
    keys: string[],
    filterData: SetFilterFn
) => {
    const { column, value, op } = filter;
    const colName = keys[column];

    const searchField = INPUT({
        key: `filter-number-${idx}`,
        autoFocus: true,
        type: 'number',
        name: 'search',
        className: 'table-header-search-field',
        defaultValue: value.toString(10),
        onChange: e =>
            filterData(
                filterNumber(column, parseFloat(e.currentTarget.value), op), idx
            )
    });

    return DIV(
        {
            className: `table-search-item`,
            key: uniqId()
        },
        opFieldName(op, colName),
        searchField,
        renderOps(filter, idx, filterData)
    );
};

const renderDateFilter = (
    filter: FilterDate,
    idx: number,
    keys: string[],
    filterData: SetFilterFn
) => {
    const { column, date, op } = filter;
    const colName = keys[column];

    const searchField = INPUT({
        key: `filter-date-${idx}`,
        autoFocus: true,
        type: 'date',
        name: 'search',
        className: 'table-header-search-field',
        defaultValue: date8601(parseDate(date).getOrElse(new Date())),
        onChange: e => filterData(filterDate(column, e.currentTarget.value, op), idx)
    });

    return DIV(
        {
            className: `table-search-item`,
            key: uniqId()
        },
        opFieldName(op, colName),
        searchField,
        renderOps(filter, idx, filterData)
    );
};

const renderDateTimeFilter = (
    filter: FilterDateTime,
    idx: number,
    keys: string[],
    filterData: SetFilterFn
) => {
    const { column, datetime, op } = filter;
    const colName = keys[column];

    const searchField = INPUT({
        key: `filter-time-${idx}`,
        autoFocus: true,
        type: 'datetime-local',
        name: 'search',
        className: 'table-header-search-field',
        defaultValue: datetime8601(parseDate(datetime).getOrElse(new Date())),
        onChange: e => filterData(filterDateTime(column, e.currentTarget.value, op), idx)
    });

    return DIV(
        {
            className: `table-search-item`,
            key: uniqId()
        },
        opFieldName(op, colName),
        searchField,
        renderOps(filter, idx, filterData)
    );
};



/// render >
