import * as debug from 'debug';
import { DIV, SPAN } from '../elements';
import {
    TableDataRow,
    TableState,
    TableDataCell,
    RowSelectHandler,
    TableData,
    SetFilterFn,
} from '.';
import {
    StreamingField,
    streamFieldType,
    streamFieldName,
    PropertyTypeDescriptor,
} from '../../source';
import { fromNullable, Option, some, none } from 'fp-ts/lib/Option';
import { TableSort, SortDirection } from './sort';
import tr from '../../locale';
import { divTooltipBottom } from '../tooltip';
import { makeIcon, makeLabel } from '../button';
import { Filter, renderFilter } from './filter';


const logger = debug('sdi:table2');

type Width = [number, string];


const cellWidths = (
    fields: StreamingField[]
) => fields.map<Width>(([name, typ]) => {
    switch (typ) {
        case 'boolean': return [Math.max(13, name.length * 0.7), 'em'];
        case 'number': return [Math.max(13, name.length * 0.7), 'em'];
        case 'date': return [Math.max(13, name.length * 0.7), 'em'];
        case 'datetime': return [Math.max(13, name.length * 0.7), 'em'];
        case 'string': return [Math.max(13, name.length * 0.7), 'em'];
    }
});


const rowWidth = (
    fields: StreamingField[]
) => cellWidths(fields)
    .reduce<Width>((acc, w) => [acc[0] + w[0], w[1]], [
        0,
        ''
    ]);


const cwString = (
    cw: Width
) => `${cw[0]}${cw[1]}`;


const getType = (
    fields: StreamingField[],
    idx: number
) => fromNullable(fields[idx]).map(streamFieldType);


const renderCell = (
    fields: StreamingField[],
    widths: Width[],
) => (
    data: TableDataCell,
    idx: number
) => DIV(
    {
        key: `cell-${idx}`,
        title: data,
        className: `table-cell data-type-${getType(fields, idx).getOrElse(
            'string'
        )}`,
        style: {
            width: cwString(widths[idx])
        },
    },
    data
);

const renderRowNum = (
    rowNum: number,
    key: string | number
) => DIV(
    {
        key: `row-num-${key}`,
        className: 'table-row-num',
        style: {
            display: 'none'
        }
    },
    (rowNum + 1).toString()
);



const selectRow = (
    rowNum: number,
    row: TableDataRow,
    handler: RowSelectHandler,
) => (
    _event: unknown,
    ) => handler(row, rowNum);



const renderRow = (
    fields: StreamingField[],
    offset: number,
    widths: Width[],
    handler: RowSelectHandler,
    selected: number,
) => (
    data: TableDataRow,
    idx: number,
    ) => {
        const rowNum = offset + idx;
        const selectedClass = selected === rowNum ? 'active' : '';
        const evenOdd = rowNum % 2 > 0 ? 'odd table-row' : 'even table-row';
        return DIV(
            {
                key: `row-${data.from}`,
                className: `${evenOdd} ${selectedClass}`,
                onClick: selectRow(rowNum, data, handler)
            },
            renderRowNum(rowNum, data.from),
            data.cells.map(renderCell(fields, widths))
        );
    };

const renderTableHeaderCell = (
    idx: number,
    width: Width,
    colName: string,
    optSort: Option<TableSort>,
    ty: PropertyTypeDescriptor,
    onSort: (i: number, dir: SortDirection) => void,
    onInitFilter: (i: number, ty: PropertyTypeDescriptor) => void,
) => {
    const [className, newSortDirection] = optSort
        .chain(sort => sort.col === idx ? some(sort) : none)
        .fold<[string, SortDirection]>(
            [`table-cell table-header-cell data-type-${ty}`, 'ASC'],
            sort =>
                sort.direction === 'ASC' ?
                    [`table-cell table-header-cell data-type-${ty}  sorted sorted-asc`, 'DESC'] :
                    [`table-cell table-header-cell data-type-${ty}  sorted sorted-desc`, 'ASC']
        );

    return DIV(
        {
            className,
            style: { width: cwString(width) },
            key: `header-cell-${idx}`,
            onClick: () => onSort(idx, newSortDirection)
        },
        SPAN({}, colName),
        divTooltipBottom(
            tr.core('filter'),
            {},
            makeIcon('filter', 3, 'filter')(() => onInitFilter(idx, ty))
        )
    );
};

export const renderTableHeader = (
    fields: StreamingField[],
    optSort: Option<TableSort>,
    onSort: (i: number, dir: SortDirection) => void,
    onInitFilter: (i: number, ty: PropertyTypeDescriptor) => void,

) => {
    const widths = cellWidths(fields);
    const elems = fields.map((f, idx) =>
        renderTableHeaderCell(
            idx,
            widths[idx],
            streamFieldName(f),
            optSort,
            streamFieldType(f),
            onSort,
            onInitFilter,
        )
    );

    return DIV(
        {
            className: 'table-header',
            key: 'table-header',
            style: {
                width: cwString(rowWidth(fields))
            }
        },
        ...elems
    );
};

export const renderTableBody = (
    setTableSize: (a: Element | null) => void,
    scroll: (e: React.UIEvent<Element>) => void,
    handler: RowSelectHandler,
) => (
    data: TableData,
    fields: StreamingField[],
    { rowHeight, position, window, selected }: TableState,
    ) => {
        const widths = cellWidths(fields);
        const width = cwString(rowWidth(fields));
        logger(`sizer => ${data.total * rowHeight}`);
        return DIV(
            {
                key: 'table-body',
                className: `table-body ${data.total}`,
                onScroll: scroll,
                ref: setTableSize,
                style: { width }
            },
            DIV(
                {
                    className: 'table-body-sizer',
                    style: {
                        minHeight: data.total * rowHeight
                    }
                },
                DIV(
                    {
                        className: 'table-body-fragment',
                        style: {
                            position: 'relative',
                            top: position.y,
                            width
                        },
                        key: `table-body-fragment|${window.offset}`
                    },
                    ...data.rows.map(renderRow(fields, window.offset, widths, handler, selected))
                )
            )
        );
    };


export const renderFilters = (
    filters: Filter[],
    keys: string[],
    setFilter: SetFilterFn,
    clearFilters: () => void,
) => DIV(
    { className: 'table-search' },
    DIV({ className: 'filter--wrapper' },
        ...filters.map((f, i) =>
            renderFilter(f, i, keys, setFilter)
        ),
    ),
    makeLabel('clear', 2, () => tr.core('reset'))(clearFilters),
);

logger('loaded');
