import { StreamingField, PropertyTypeDescriptor, streamFieldName } from '../../source';
import { DIV, NodeOrOptional } from '../elements';
import { renderTableHeader, renderTableBody, renderFilters } from './render';
import { rect } from '../../app';
import { Option, none, some } from 'fp-ts/lib/Option';
import { TableSort, SortDirection } from './sort';
import { Filter, makeInitialFilter } from './filter';


export * from './filter';
export * from './sort';


export interface TableWindow {
    offset: number;
    size: number;
}


export interface TableState {
    position: { x: number; y: number };
    rowHeight: number;
    selected: number;
    viewHeight: number;
    window: TableWindow;
    sort: Option<TableSort>;
    filters: Filter[];
}


export const defaultTableState = (): TableState => ({
    position: { x: 0, y: 0 },
    rowHeight: 19,
    selected: -1,
    viewHeight: -1,
    window: { offset: 0, size: 100 },
    sort: none,
    filters: [],
});

export type TableDataCell = string;

export interface TableDataRow {
    // pointer to origin
    from: number | string;
    cells: TableDataCell[];
}

export interface TableData {
    rows: TableDataRow[];
    total: number;
}


export interface TableSource {
    data: (w: TableWindow) => TableData;
    fields: StreamingField[];
}

export const emptySource = (): TableSource => ({
    data: _w => ({ rows: [], total: 0 }),
    fields: [],
});

export type TableDispatch = (r: (state: TableState) => TableState) => void;
export type TableQuery = () => TableState;
export type RowSelectHandler = (row: TableDataRow, i: number) => void;
export type TableFilterChange = () => void;


export const table = (
    dispatch: TableDispatch,
    query: TableQuery,
    rowHandler: RowSelectHandler,
    filterChange?: TableFilterChange,
) => {
    /// < events
    const setTableWindowOffset = (offset: number) =>
        dispatch(s => ({ ...s, window: { ...s.window, offset } }));

    const setTableWindowSize = (size: number): void =>
        dispatch(s => ({ ...s, window: { ...s.window, size } }));

    const setPosition = (x: number, y: number): void =>
        dispatch(s => ({ ...s, position: { x, y } }));

    const setSort = (col: number, direction: SortDirection) => {
        dispatch(s => ({ ...s, sort: some({ col, direction }) }));
        filterChange?.();
    };

    const rectify = rect(r =>
        setTableWindowSize(Math.ceil(r.height / query().rowHeight)));

    const clearFilters = () =>
        dispatch(s => ({ ...s, filters: [] }));

    const initFilter = (
        column: number,
        dataType: PropertyTypeDescriptor,
    ) => {
        dispatch((state) => {
            const { filters, window } = state;
            return {
                ...state,
                window: { ...window, offset: 0 },
                filters: filters.concat([makeInitialFilter(column, dataType)])
            };
        });

        filterChange?.();
    };

    const setFilter = (
        filter: Filter,
        index: number,
    ) => {
        dispatch((state) => {
            const filters = state.filters.map((f, i) => {
                if (index === i && f.column === filter.column) {
                    return filter;
                }
                return f;
            });

            return { ...state, filters };
        });

        filterChange?.();
    };
    ///  events >

    /// < ref
    let it: number | null = null;
    let scrolls: number[] = [];
    const mount = (_el: Element) => {
        scrolls = [];
        it = window.setInterval(() => {
            const sl = scrolls.length;
            if (sl > 0) {
                const offsetTop = scrolls[sl - 1];
                const { rowHeight, position, window } = query();
                const rowOffset = Math.ceil(offsetTop / rowHeight);
                if (position.y !== offsetTop) {
                    setPosition(0, offsetTop);
                }
                if (window.offset !== rowOffset) {
                    setTableWindowOffset(rowOffset);
                }
            }
            scrolls = [];
        }, 64);
    };
    const unmount = () => {
        if (it !== null) {
            window.clearInterval(it);
        }
    };

    const setTableSize = (el: Element | null) => { if (el) { rectify(el); } };

    const scroll = (e: React.UIEvent<Element>): void => {
        scrolls.push(e.currentTarget.scrollTop);
    };

    const lifeCycle = (el: Element | null) => {
        if (el === null) {
            unmount();
        }
        else {
            mount(el);
        }
    };
    /// ref >

    const renderBody = renderTableBody(setTableSize, scroll, rowHandler);
    const render = (
        source: TableSource,
        toolbar: NodeOrOptional,
    ) => {
        const state = query();
        const data = source.data(state.window);


        const filters = state.filters.length > 0 ?
            some(renderFilters(
                state.filters,
                source.fields.map(streamFieldName),
                setFilter,
                clearFilters,
            )) :
            none;

        const main = DIV(
            { className: 'table-main' },
            renderTableHeader(
                source.fields,
                state.sort,
                setSort,
                initFilter
            ),
            renderBody(data, source.fields, state)
        );

        return DIV({ className: 'infinite-table', ref: lifeCycle }, toolbar, filters, main);
    };

    return render;
};
