/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from "debug";
import { DIV, A, NodeOrOptional } from "./elements";
import langSwitch from "./lang-switch";
import {
  getUserId,
  getRoot,
  getAppManifest,
  navigateRoot,
  getUserName,
} from "../app";
import { tr, fromRecord } from "../locale";
import { getPathElements } from "../util";
import { renderAppSelect } from "./app-select";
import { appDisplayName } from "../source";
import { none } from "fp-ts/lib/Option";

const logger = debug("sdi:header");

const loginURL = () => {
  const path = getPathElements(document.location.pathname);
  const root = getPathElements(getRoot());
  const next = path
    .filter((p, i) => (i < root.length ? p !== root[i] : true))
    .join("/");
  return `${getRoot()}login/${next}`;
};

// const dashboardButton =
//     () => DIV({
//         className: 'navigate dashboard',
//         onClick: () => navigateRoot(),
//     }, SPAN({ className: 'label' }, tr.core('dashboard')));

const rootButton = (current: string) =>
  getUserId().fold(
    DIV(
      { className: "navigate login" },
      A({ href: loginURL() }, tr.core("login"))
    ),
    () => renderAppSelect(current)
  );

// const username = (user: IUser) => {
//     const ut = user.name.trim();
//     if (ut.length === 0) {
//         return `User ${user.id}`;
//     }
//     return ut;
// };

const userAccount = () =>
  getUserName().map((username) =>
    DIV(
      {
        className: "navigate dashboard account",
        onClick: () => navigateRoot(),
      },
      username
    )
  );

const documentationLink = () =>
  DIV(
    { className: "navigate documentation" },
    A(
      {
        href: `https://gitlab.com/cartofixer/cartofixer/-/wikis/home`,
        target: "_blank",
      },
      tr.core("documentation")
    )
  );

const renderTitle = (appCodename: string) =>
  getAppManifest(appCodename)
    .chain(appDisplayName)
    .map<string>((n) => fromRecord(n))
    .getOrElse(appCodename);

const appElementDefault = (): NodeOrOptional => none;

export const header = (appCodename: string, appElement = appElementDefault) =>
  DIV(
    { className: "header" },
    DIV({ className: "logo" }),
    A(
      {
        href: getRoot(),
        target: "_blank",
        title: "cartofixer.be",
        className: "logo-cartofixer",
      },
      "cartofixer"
    ),
    DIV({ className: "app-title" }, renderTitle(appCodename)),
    DIV({ className: "app-listwrapper" }, appElement()),
    DIV(
      { className: "header-toolbar" },
      rootButton(appCodename),
      documentationLink(),
      userAccount(),
      langSwitch()
    )
  );

export default header;

logger("loaded");
