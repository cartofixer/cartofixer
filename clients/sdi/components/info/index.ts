
import { DIV, H2, P } from '../elements';
import tr, { Translated } from '../../locale';
import { renderModal, ModalStatusGetter, ModalStatusSetter, ModalStatus } from '../modal';
import factoryButton, { ButtonComponent } from '../button';
import { fromNullable } from 'fp-ts/lib/Option';
import { queryK, dispatch } from '../../shape';

export interface InfoOptions {
    getModalStatus: ModalStatusGetter;
    setModalStatus: ModalStatusSetter;
}

const getModalStatus = queryK('sdi/component/info/modal/status');


const options = (function () {
    const f = (k: string) => {
        return {
            getModalStatus:
                () =>
                    fromNullable(getModalStatus()[k]).getOrElse('close'),
            setModalStatus:
                (s: ModalStatus) =>
                    dispatch('sdi/component/info/modal/status', S => ({ ...S, [k]: s })),
        };
    };

    return f;
})();


const modal =
    (options: InfoOptions) =>
        renderModal(options.getModalStatus, options.setModalStatus);

const buttons = factoryButton(
    () => ({}),
    (_h: (_a: ButtonComponent) => ButtonComponent) => void 0,
);

const trigger = buttons.makeIcon('info', 3, 'info');
const close = buttons.makeLabel('info', 3, () => tr.core('close'));


const header =
    (title: Translated) =>
        DIV({ className: 'modal__header' },
            H2({}, title),
        );


const body =
    (content: Translated) =>
        DIV({ className: 'modal__content' },
            ...(content.split('\n').map(p => P({}, p))));


const footer =
    (options: InfoOptions) =>
        DIV({ className: 'modal__footer' },
            close(() => options.setModalStatus('close')),
        );


export const renderInfo =
    (title: Translated, content: Translated) =>
        modal(options(title))(
            trigger(() => options(title).setModalStatus('open')),
            DIV({ className: 'modal__inner' },
                header(title),
                body(content),
                footer(options(title))),
        );

export default renderInfo;





