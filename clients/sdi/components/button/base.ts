
/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { DIV, H2 } from '../elements';
import { buttonQueries } from './queries';
import { buttonEvents } from './events';
import { ButtonQuerySet, ButtonEventSet, ButtonName, ButtonGetter, ButtonSetter, ButtonType, ButtonSize } from '.';
import { IconName, nameToString } from './names';
import { tr, Translated } from '../../locale';



const makeClassName =
    (name: ButtonName, bt: ButtonType, runTimeClass: string) => {
        switch (bt.type) {
            case 'Icon': return `btn btn-${name} btn-${bt.size} icon-only ${runTimeClass}`;
            case 'Label': return `btn btn-${name}  btn-${bt.size}  label-only ${runTimeClass}`;
            case 'LabelAndIcon': return `btn btn-${name}  btn-${bt.size} label-and-icon ${runTimeClass}`;
        }
    };


const buttonOld =
    (queries: ButtonQuerySet, events: ButtonEventSet) => {

        const make =
            (name: ButtonName, bt: ButtonType) =>
                (action: () => void, runTimeClass = '') => {
                    const onClick = (e: React.MouseEvent<Element>) => {
                        e.stopPropagation();
                        action();
                    };
                    const className = makeClassName(name, bt, runTimeClass);

                    switch (bt.type) {

                        case 'Icon':
                            return DIV({ className, onClick },
                                DIV({ className: 'icon' },
                                    nameToString(bt.icon)));
                        case 'Label':
                            return DIV({ className, onClick },
                                DIV({ className: 'btn-label' }, bt.label()));
                        case 'LabelAndIcon':
                            return DIV({ className, onClick },
                                DIV({ className: 'btn-label' },
                                    bt.label()),
                                DIV({ className: 'icon' },
                                    nameToString(bt.icon)));
                    }
                };

        const makeIcon =
            (name: ButtonName, size: ButtonSize, icon: IconName) =>
                make(name, { type: 'Icon', size, icon });

        const makeLabel =
            (name: ButtonName, size: ButtonSize, label: () => Translated) =>
                make(name, { type: 'Label', size, label });

        const makeLabelAndIcon =
            (name: ButtonName, size: ButtonSize, icon: IconName, label: () => Translated) =>
                make(name, { type: 'LabelAndIcon', size, label, icon });

        const makeRemove =
            (key: string, size = 3 as ButtonSize, label: () => Translated, confirmMsg: () => Translated) => {
                setTimeout(() => {
                    if (!queries.hasKey(key)) {
                        events.setStep(key, 'initial');
                    }
                }, 1);

                const initialButton = () => {
                    return makeLabel('remove', size, label)(() => {
                        events.setStep(key, 'active');
                    });
                };

                const cancel = makeLabel('cancel', 2, () => tr.core('cancel'));
                const confirm = makeLabel('confirm', 1, () => tr.core('confirm'));

                const activeButton = (action: () => void) => {
                    return DIV({ className: 'remove-confirm' },
                        DIV({ className: 'remove-confirm-box' },
                            DIV({ className: 'remove-confirm-msg' },
                                H2({}, tr.core('confirmDelete')),
                                DIV({}, confirmMsg())),
                            DIV({ className: 'remove-confirm-btns' },
                                cancel(() => { events.setStep(key, 'initial'); }),
                                confirm(() => {
                                    events.setStep(key, 'initial');
                                    action();
                                }),
                            ),
                        ),
                    );
                };

                return ((action: () => void) => {
                    if (queries.isActive(key)) {
                        return activeButton(action);
                    }
                    return initialButton();
                });
            };
        const makeRemoveLabelAndIcon =
            (key: string, size = 3 as ButtonSize, icon: IconName, label: () => Translated, confirmMsg: () => Translated) => {
                setTimeout(() => {
                    if (!queries.hasKey(key)) {
                        events.setStep(key, 'initial');
                    }
                }, 1);

                const initialButton = () => {
                    return makeLabelAndIcon('remove', size, icon, label)(() => {
                        events.setStep(key, 'active');
                    });
                };

                const cancel = makeLabel('cancel', 2, () => tr.core('cancel'));
                const confirm = makeLabel('confirm', 1, () => tr.core('confirm'));

                const activeButton = (action: () => void) => {
                    return DIV({ className: 'remove-confirm' },
                        DIV({ className: 'remove-confirm-box' },
                            DIV({ className: 'remove-confirm-msg' },
                                H2({}, tr.core('confirmDelete')),
                                DIV({}, confirmMsg())),
                            DIV({ className: 'remove-confirm-btns' },
                                cancel(() => { events.setStep(key, 'initial'); }),
                                confirm(() => {
                                    events.setStep(key, 'initial');
                                    action();
                                }),
                            ),
                        ),
                    );
                };

                return ((action: () => void) => {
                    if (queries.isActive(key)) {
                        return activeButton(action);
                    }
                    return initialButton();
                });
            };

        const makeRemoveIcon =
            (key: string, size = 3 as ButtonSize, icon: IconName, confirmMsg: () => Translated) => {
                setTimeout(() => {
                    if (!queries.hasKey(key)) {
                        events.setStep(key, 'initial');
                    }
                }, 1);

                const initialButton = () => {
                    return makeIcon('remove', size, icon)(() => {
                        events.setStep(key, 'active');
                    });
                };

                const cancel = makeLabel('cancel', 2, () => tr.core('cancel'));
                const confirm = makeLabel('confirm', 1, () => tr.core('confirm'));

                const activeButton = (action: () => void) => {
                    return DIV({ className: 'remove-confirm' },
                        DIV({ className: 'remove-confirm-box' },
                            DIV({ className: 'remove-confirm-msg' },
                                H2({}, tr.core('confirmDelete')),
                                DIV({}, confirmMsg())),
                            DIV({ className: 'remove-confirm-btns' },
                                cancel(() => { events.setStep(key, 'initial'); }),
                                confirm(() => {
                                    events.setStep(key, 'initial');
                                    action();
                                }),
                            ),
                        ),
                    );
                };

                return ((action: () => void) => {
                    if (queries.isActive(key)) {
                        return activeButton(action);
                    }
                    return initialButton();
                });
            };

        return {
            make,
            makeIcon,
            makeLabel,
            makeLabelAndIcon,
            makeRemove,
            makeRemoveIcon,
            makeRemoveLabelAndIcon,
        };

    };


export const factory =
    (getter: ButtonGetter, setter: ButtonSetter) => {
        const qs = buttonQueries(getter);
        const es = buttonEvents(setter, qs);
        return buttonOld(qs, es);
    };





export const button =
    () => {

        const make =
            (name: ButtonName, bt: ButtonType) =>
                (action: () => void, runTimeClass = '') => {
                    const onClick = (e: React.MouseEvent<Element>) => {
                        e.stopPropagation();
                        action();
                    };
                    const className = makeClassName(name, bt, runTimeClass);

                    switch (bt.type) {

                        case 'Icon':
                            return DIV({ className, onClick },
                                DIV({ className: 'icon' },
                                    nameToString(bt.icon)));
                        case 'Label':
                            return DIV({ className, onClick },
                                DIV({ className: 'btn-label' }, bt.label()));
                        case 'LabelAndIcon':
                            return DIV({ className, onClick },
                                DIV({ className: 'btn-label' },
                                    bt.label()),
                                DIV({ className: 'icon' },
                                    nameToString(bt.icon)));
                    }
                };

        const makeIcon =
            (name: ButtonName, size: ButtonSize, icon: IconName) =>
                make(name, { type: 'Icon', size, icon });

        const makeLabel =
            (name: ButtonName, size: ButtonSize, label: () => Translated) =>
                make(name, { type: 'Label', size, label });

        const makeLabelAndIcon =
            (name: ButtonName, size: ButtonSize, icon: IconName, label: () => Translated) =>
                make(name, { type: 'LabelAndIcon', size, label, icon });


        return {
            make,
            makeIcon,
            makeLabel,
            makeLabelAndIcon,
        };

    };


