

import * as debug from 'debug';
import { Component, createElement } from 'react';
import { uniqIdGen } from '../../util';
import { INPUT, TEXTAREA } from '../elements';

const logger = debug('sdi:components/input');

export type Getter<T> = () => T;
export type Setter<T> = (a: T) => void;

export type InputAttributes = React.AllHTMLAttributes<HTMLInputElement> & React.RefAttributes<HTMLInputElement>;
export type TextAreaAttributes = React.AllHTMLAttributes<HTMLTextAreaElement> & React.RefAttributes<HTMLTextAreaElement>;
export type AllInputAttributes = InputAttributes & React.Attributes;
export type AllTextAreaAttributes = TextAreaAttributes & React.Attributes;

type AllAttributes = AllInputAttributes | AllTextAreaAttributes;

interface InputProps<T, A extends AllAttributes> {
    get: Getter<T>;
    set: Setter<T>;
    attrs: A;
}

type InputValueT = string | number | null;

interface InputValue<T extends InputValueT, A extends AllAttributes> {
    readonly value: T;
    readonly props: InputProps<T, A>
}

export const CLEAR_INPUT_TEXT = `__${Date.now()}__`;
const INPUT_CHECK_INTERVAL = 500;

const uniqKey = uniqIdGen('input-key-');


const value =
    <T extends InputValueT, A extends AllAttributes>(value: T, props: InputProps<T, A>): InputValue<T, A> => ({ value, props });


class InputStringComponent extends Component<InputProps<string, AllInputAttributes>, InputValue<string, AllInputAttributes>> { }
class TextAreaComponent extends Component<InputProps<string, AllTextAreaAttributes>, InputValue<string, AllTextAreaAttributes>> { }


// FIXME: we dont need to store props in state, it was
//        made when hot fixing inputs not using updated
//        props, but now rushing to deadline -pm


class InputText extends InputStringComponent {
    readonly attrs: () => AllInputAttributes;
    readonly key: string;
    /**
     * At some point we realized that updating global state
     * on `change` event was triggering too many re-renders of
     * the whole application and went for the `blur` event instead.
     * The consequence is, at least, that some components
     * monitoring the content of the state for changes in an input
     * don't get notified soon enough (e.g. button enabling itself
     * when there's enough content for this input). 
     * A solution for this is to watch for the difference between
     * the content of internal state of the input and the value in the 
     * global state on an interval and trigger a global update if 
     * needed. We hope to get the best of both worlds with this. 
     * Crossing fingers that licecycle methods will work as advertised
     * 
     * - pm
     *  
     */
    private watcherId: number | null = null;

    constructor(props: InputProps<string, AllInputAttributes>) {
        super(props);
        this.key = uniqKey();

        const updateInternalState = (n: string) => this.setState(value(n, this.props));
        const updateGlobalState = () => this.props.set(this.state.value);


        this.attrs =
            () => ({
                key: this.key,
                value: this.state.value,
                type: 'text',
                onChange: e => updateInternalState(e.currentTarget.value),
                onBlur: updateGlobalState,
                ...this.props.attrs,
            });
    }

    shouldComponentUpdate() {
        return true;
    }

    componentWillMount() {
        this.setState(value(this.props.get(), this.props));
        this.watcherId = window.setInterval(() => {
            const internalState = this.state.value;
            const globalState = this.props.get();
            if (globalState === CLEAR_INPUT_TEXT) {
                logger(`InputText CLEAR`);
                this.props.set('');
                this.setState(value('', this.props));
            }
            else if (internalState !== globalState) {
                logger(`InputText State Force Update: ${internalState.length}`);
                this.props.set(internalState);
            }
        }, INPUT_CHECK_INTERVAL);
    }

    componentWillUnmount() {
        const wid = this.watcherId;
        if (wid !== null) {
            window.clearInterval(wid);
            this.watcherId = null;
        }
    }

    render() {
        return INPUT(this.attrs());
    }

    componentWillReceiveProps(nextProps: Readonly<InputProps<string, AllInputAttributes>>) {
        logger(`Recycling Input ${this.props.get()} -> ${nextProps.get()}`)
        this.setState(value(nextProps.get(), nextProps));
    }
}

class TextArea extends TextAreaComponent {
    attrs: () => AllTextAreaAttributes;
    readonly key: string;

    // see InputText#watcherId
    private watcherId: number | null = null;

    constructor(props: InputProps<string, AllTextAreaAttributes>) {
        super(props);
        this.key = uniqKey();

        const updateInternalState = (n: string) => this.setState(value(n, this.props));
        const updateGlobalState = () => this.props.set(this.state.value);

        this.attrs =
            () => ({
                key: this.key,
                value: this.state.value,
                onChange: e => updateInternalState(e.currentTarget.value),
                onBlur: updateGlobalState,
                ...props.attrs,
            });
    }

    componentWillMount() {
        this.setState(value(this.props.get(), this.props));
        this.watcherId = window.setInterval(() => {
            const internalState = this.state.value;
            const globalState = this.props.get();
            if (globalState === CLEAR_INPUT_TEXT) {
                this.props.set('');
                this.setState(value('', this.props));
            }
            else if (internalState !== globalState) {
                logger(`TextArea State Force Update: ${internalState.length}`);
                this.props.set(internalState);
            }
        }, INPUT_CHECK_INTERVAL);
    }


    componentWillUnmount() {
        const wid = this.watcherId;
        if (wid !== null) {
            window.clearInterval(wid);
            this.watcherId = null;
        }
    }

    render() {
        return TEXTAREA(this.attrs());
    }

    componentWillReceiveProps(nextProps: Readonly<InputProps<string, AllTextAreaAttributes>>) {
        this.setState(value(nextProps.get(), nextProps));
    }
}

class InputNumber extends Component<InputProps<number, AllInputAttributes>, InputValue<number, AllInputAttributes>> {
    attrs: () => AllInputAttributes;
    readonly key: string;

    constructor(props: InputProps<number, AllInputAttributes>) {
        super(props);
        this.key = uniqKey();

        const update =
            (n: number) => {
                if (!isNaN(n) && props.get() !== n) {
                    // this.setState(value(n));
                    this.props.set(n);
                    this.setState(value(n, this.props));
                }
            };

        this.attrs =
            () => ({
                key: this.key,
                value: this.state.value,
                type: 'number',
                onChange: (e) => {
                    // due to https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/669685/
                    // we can'use valueAsNumber
                    // update(e.currentTarget.valueAsNumber);
                    update(parseFloat(e.currentTarget.value));
                },
                ...props.attrs,
            });
    }

    componentWillMount() {
        this.setState(value(this.props.get(), this.props));
    }

    render() {
        return INPUT(this.attrs());
    }

    componentWillReceiveProps(nextProps: Readonly<InputProps<number, AllInputAttributes>>) {
        this.setState(value(nextProps.get(), nextProps));
    }
}

// TODO FIXME fusionner InputNumber2 et InputNumber ?
class InputNumber2 extends InputStringComponent {
    attrs: () => AllInputAttributes;
    readonly key: string;

    constructor(props: InputProps<string, AllInputAttributes>) {
        super(props);
        this.key = uniqKey();

        const update =
            (n: string) => {
                this.setState(value(n, this.props));
                this.props.set(n);
            };


        this.attrs =
            () => ({
                key: this.key,
                value: this.state.value,
                type: 'number',
                onChange: e => update(e.currentTarget.value),
                ...props.attrs,
            });
    }

    componentWillMount() {
        this.setState(value(this.props.get(), this.props));
    }

    render() {
        return INPUT(this.attrs());
    }

    componentWillReceiveProps(nextProps: Readonly<InputProps<string, AllInputAttributes>>) {
        this.setState(value(nextProps.get(), nextProps));
    }
}

class InputNullableNumber extends Component<InputProps<number | null, AllInputAttributes>, InputValue<number | null, AllInputAttributes>> {
    attrs: () => AllInputAttributes;
    readonly key: string;

    constructor(props: InputProps<number, AllInputAttributes>) {
        super(props);
        this.key = uniqKey();

        const update =
            (n: number) => {
                this.setState(value(n, this.props));
                this.props.set(n);
            };

        this.attrs =
            () => ({
                key: this.key,
                value: this.state.value !== null ? this.state.value : undefined,
                type: 'number',
                onChange: e => update(e.currentTarget.valueAsNumber),
                ...props.attrs,
            });
    }

    componentWillMount() {
        this.setState(value(this.props.get(), this.props));
    }

    render() {
        return INPUT(this.attrs());
    }

    componentWillReceiveProps(nextProps: Readonly<InputProps<number, AllInputAttributes>>) {
        this.setState(value(nextProps.get(), nextProps));
    }
}

const extraAttributesFn =
    <A extends AllAttributes>(attrs = {} as A): A => attrs
// 'key' in attrs ? attrs : { ...attrs, key: uniqKey() };

export const inputText =
    (get: Getter<string>, set: Setter<string>, attrs?: AllInputAttributes) =>
        createElement(InputText, { set, get, attrs: extraAttributesFn(attrs) });

export const inputLongText =
    (get: Getter<string>, set: Setter<string>, attrs?: AllTextAreaAttributes) =>
        createElement(TextArea, { set, get, attrs: extraAttributesFn(attrs) });

// TODO FIXME fusionner inputNumber2 et inputNumber ?
export const inputNumber2 =
    (get: Getter<string>, set: Setter<string>, attrs?: AllInputAttributes) =>
        createElement(InputNumber2, { set, get, attrs: extraAttributesFn(attrs) });

// TODO FIXME fusionner inputNumber2 et inputNumber ?
export const inputYear =
    (get: Getter<string>, set: Setter<string>, attrs?: AllInputAttributes) =>
        createElement(InputNumber2, { set, get, attrs: extraAttributesFn({ pattern: '[0-9]{4}', ...attrs }) });


export const inputNumber =
    (get: Getter<number>, set: Setter<number>, attrs?: AllInputAttributes) =>
        createElement(InputNumber, { set, get, attrs: extraAttributesFn(attrs) });

export const inputNullableNumber =
    (get: Getter<number | null>, set: Setter<number | null>, attrs?: AllInputAttributes) =>
        createElement(InputNullableNumber, { set, get, attrs: extraAttributesFn(attrs) });


logger('loaded');
