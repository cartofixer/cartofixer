import { DIV } from '../elements';
import { Setoid } from 'fp-ts/lib/Setoid';
import { ReactNode } from 'react';
import { Option } from 'fp-ts/lib/Option';


const renderRadioItem =
    <T>(
        renderItem: (a: T) => ReactNode,
        select: (a: T) => void,
    ) =>
        (v: T) =>
            DIV({ className: 'radio__item', onClick: () => select(v) },
                DIV({ className: 'radio__bullet' }),
                renderItem(v));

const renderRadioSelected =
    <T>(
        renderItem: (a: T) => ReactNode,
    ) =>
        (v: T) =>
            DIV({ className: 'radio__item selected' },
                DIV({ className: 'radio__bullet' }),
                renderItem(v));




export const renderRadio =
    <T>(
        S: Setoid<T>,
        renderItem: (a: T) => ReactNode,
        select: (a: T) => void,
    ) =>
        (
            list: T[],
            selected: Option<T>,
        ) => {
            const mkItem = renderRadioItem<T>(renderItem, select);
            const mkSelected = renderRadioSelected<T>(renderItem);
            const items = list.map((i) => {
                if (selected.fold(false, s => S.equals(i, s))) {
                    return mkSelected(i);
                }
                return mkItem(i);
            });

            return (
                DIV({ className: 'radio' }, ...items));
        };
