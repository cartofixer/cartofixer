import { DIV } from '../elements';
import { ReactNode } from 'react';
import { nameToString } from '../button/names';


const renderFalse =
    (
        renderItem: (a: boolean) => ReactNode,
        select: (a: boolean) => void,
        runtimeClassName: string,
    ) =>
        DIV({ className: `boolean ${runtimeClassName}` },
            renderItem(false),
            DIV({
                className: 'boolean__false boolean--unselected',
                onClick: () => select(true),
            },
                DIV({ className: 'switch-icon switch-icon--false' }, nameToString('toggle-off')),
                renderItem(true),
            ),
        );


const renderTrue =
    (
        renderItem: (a: boolean) => ReactNode,
        select: (a: boolean) => void,
        runtimeClassName: string,
    ) =>
        DIV({ className: `boolean ${runtimeClassName}` },
            DIV({
                className: 'boolean__false boolean--selected',
                onClick: () => select(false),
            },
                renderItem(false),
                DIV({ className: 'switch-icon switch-icon--true' }, nameToString('toggle-on')),
            ),
            renderItem(true),
        );



export const renderSwitch =
    (
        renderItem: (a: boolean) => ReactNode,
        select: (a: boolean) => void,
    ) =>
        (selected: boolean, runtimeClassName = '') => {
            if (selected) {
                return renderTrue(renderItem, select, runtimeClassName);
            }
            return renderFalse(renderItem, select, runtimeClassName);
        };
