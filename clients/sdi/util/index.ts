import bbox from '@turf/bbox';
import {
    IMapBaseLayer,
    Feature,
    Properties,
    FeatureCollection,
    DirectGeometryObject,
    Point
} from '../source';
import { MessageKey, tr, fromRecord, formatNumber } from '../locale';
import { Setoid, setoidString, setoidNumber } from 'fp-ts/lib/Setoid';
import { some, none, fromNullable, Option } from 'fp-ts/lib/Option';
import { P } from '../components/elements';
import { Coordinate } from 'ol/coordinate';
import { Coord2D } from '../map';


export interface Foldable {
    folded: boolean;
}

export type NullOrUndefined = null | undefined;
export type Nullable<T> = T | NullOrUndefined;

export interface IdentifiedS {
    id: string;
}
export interface IdentifiedN {
    id: number;
}
export type Identified = IdentifiedS | IdentifiedN;

export interface Collection<T, _K extends string = string> {
    [k: string]: T;
}

// an equivalent of `concat`
export const updateCollection = <T>(
    coll: Collection<T>,
    key: string,
    value: T
) => {
    coll[key] = value;
    return coll;
};


export const mapCollection = <T, R>(f: (a: string, b: T) => R, col: Collection<T>): R[] =>
    Object.keys(col).map(key => f(key, col[key]));

export const mapCollectionC = <T, R>(f: (a: string, b: T) => R) => (col: Collection<T>): R[] =>
    mapCollection(f, col);

export const getCollectionItem = <T>(
    key: string,
    col: Collection<T>,
): Option<T> => fromNullable(col[key]);

export const getCollectionItemC = <T>(
    key: string,
) => (
    col: Collection<T>,
    ): Option<T> => fromNullable(col[key]);


/**
 * A more granular Partial type.
 * Use case is POSTing a new object without an id
 */
export type PrecisePartial<T, PART extends keyof T> = {
    [K in PART]?: T[K];
} &
    Pick<T, Exclude<keyof T, PART>>;

export const asArray = <T>(a: T | T[]) => (Array.isArray(a) ? a : [a]);

export const asMutable = <T>(a: Readonly<T> | T) =>
    JSON.parse(JSON.stringify(a)) as T;

export const getNumber = (s?: string) => {
    if (s) {
        const n = parseFloat(s);
        if (!Number.isNaN(n)) {
            return some(n);
        }
    }
    return none;
};

export const defaultCollection = <T>(): Collection<T> => ({});

export interface IMapBaseLayerTranslated {
    name: string;
    srs: string;
    params: {
        LAYERS: string;
        VERSION: string;
    };
    url: string;
}

export const hashMapBaseLayer = (l: IMapBaseLayer) => {
    return `${fromRecord(l.name)}|${l.url}|${fromRecord(l.params.LAYERS)}`;
};

export const setoidIdentified = <T extends Identified>(): Setoid<T> => ({
    equals: (a: Identified, b: Identified) => a.id === b.id
});

export const uniq = <T>(o: Setoid<T>) => (as: T[]): T[] =>
    as.reduce(
        (acc, a) => acc.filter(i => !o.equals(a, i)).concat([a]),
        [] as T[]
    );

export const uniqString = uniq(setoidString);
export const uniqNumber = uniq(setoidNumber);
export const uniqIdentified = <T extends Identified>(as: T[]) =>
    uniq(setoidIdentified<T>())(as);

export const once = (f: Function) => {
    let done = false;

    return () => {
        if (!done) {
            done = true;
            f();
        }
    };
};

export const translateMapBaseLayer = (
    l: IMapBaseLayer
): IMapBaseLayerTranslated => ({
    name: fromRecord(l.name),
    srs: l.srs,
    params: {
        LAYERS: fromRecord(l.params.LAYERS),
        VERSION: l.params.VERSION
    },
    url: l.url
});

export const uniqIdGen = (prefix = '') => {
    let counter = 0;
    return () => {
        counter += 1;
        return `${prefix}${counter}`;
    };
};

export const uniqId = uniqIdGen('sdi-');

const APP_ID_KEY = '__app_id__';

export const addAppIdToFeature = (f: Feature) => {
    f.properties = {
        [APP_ID_KEY]: uniqId(),
        ...f.properties
    };
};

const FEATURE_PROPS_BLACKLIST = new Set([APP_ID_KEY]);


export const point = (
    coordinates: Coord2D
): Point => ({
    type: 'Point',
    coordinates
})

export const getFeatureProperties = (f: Feature): Properties => {
    const props = f.properties;
    if (!props) {
        return null;
    }
    return Object.keys(props).reduce<Properties>((acc, k) => {
        if (FEATURE_PROPS_BLACKLIST.has(k)) {
            return acc;
        }
        return {
            [k]: props[k],
            ...acc
        };
    }, {});
};



export const getFeaturePropOption = <T>(f: Feature, key: string): Option<T> => {
    const props = f.properties;
    if (!props) {
        return none;
    }
    return fromNullable(props[key]);
};


export const getLayerPropertiesKeys = (fc: FeatureCollection): string[] => {
    if (fc.features.length === 0) {
        return [];
    }
    const f = fc.features[0]; // OUCH!
    const props = f.properties;
    if (!props) {
        return [];
    }
    return Object.keys(props).reduce<string[]>((acc, k) => {
        if (FEATURE_PROPS_BLACKLIST.has(k)) {
            return acc;
        }
        acc.push(k);
        return acc;
    }, []);
};

export const getLayerPropertiesKeysFiltered = (
    fc: FeatureCollection,
    pred: (a: any) => boolean
): string[] => {
    if (fc.features.length === 0) {
        return [];
    }
    const f = fc.features[0];
    const props = f.properties;
    if (!props) {
        return [];
    }
    return Object.keys(props).reduce<string[]>((acc, k) => {
        if (FEATURE_PROPS_BLACKLIST.has(k) || !pred(props[k])) {
            return acc;
        }
        acc.push(k);
        return acc;
    }, []);
};

export const splitString = (s: string) =>
    s.split('\n').map((ss, index) => P({ key: `split-${index}` }, ss));

export const stringToParagraphs = splitString;

/*
 * implements binary search (recursive)
 *
 * https://en.wikipedia.org/wiki/Binary_search_algorithm
 * Where it's different from general implementation lies in the fact
 * that's the predicate which evaluates rather then numeric comparision.
 * Thus the predicate must know the key.
 *
 * @param min Number minimum value
 * @param max Number maximun value
 * @predicate Function(pivot) a function that evaluates the current mid value a la compareFunction
 * @context Object context to which the predicate is applied
 *
 */

export type BinaryPredicate = (a: number) => number;

export const binarySearch: (
    a: number,
    b: number,
    c: BinaryPredicate
) => number = (min, max, predicate) => {
    const interval = max - min;
    const pivot = min + Math.floor(interval / 2);

    if (max === min) {
        return pivot;
    } else if (max < min) {
        // throw (new Error('MaxLowerThanMin'));
        return pivot;
    }

    if (predicate(pivot) > 0) {
        return binarySearch(min, pivot, predicate);
    } else if (predicate(pivot) < 0) {
        return binarySearch(pivot + 1, max, predicate);
    }
    return pivot;
};

/**
 *
 * At this point, I'm tired of of checking
 * both at all times when prod brakes.
 * Let's verbose!
 *
 */
export const isNotNullNorUndefined = <T>(x: Nullable<T>): x is T =>
    x !== undefined && x !== null;

export const filterNotNull = <T>(xs: Nullable<T>[]): T[] => {
    const r: T[] = [];
    for (let i = 0; i < xs.length; i += 1) {
        const v = xs[i];
        if (isNotNullNorUndefined(v)) {
            r.push(v);
        }
    }
    return r;
};

const withUnit = (k: MessageKey) => (value: number, tf = 0) =>
    `${formatNumber(parseFloat(value.toFixed(tf)))} ${tr.core(k, { value })}`;

export const withEuro = withUnit('unitEuro');
export const withEuroExclVAT = withUnit('unitEuroExclVAT');
export const withEuroInclVAT = withUnit('unitEuroInclVAT');
export const withEuroY = withUnit('unitEuroY');
export const withEuroY10 = withUnit('unitEuroY10');
export const withEuroY25 = withUnit('unitEuroY25');
export const withKWc = withUnit('unitKWc');
export const withTonsCO2 = withUnit('unitTonsCO2');
export const withTCO2 = withUnit('unitTCO2');
export const withTCO2Y = withUnit('unitTCO2Y');
export const withTCO2Y10 = withUnit('unitTCO2Y10');
export const withTCO2Y25 = withUnit('unitTCO2Y25');
export const withYear = withUnit('unitYear');
export const withM2 = withUnit('unitM2');
export const withPercent = withUnit('unitPercent');
export const withKWh = withUnit('unitKWh');
export const withKWhY = withUnit('unitKWhY');
export const withLiter = withUnit('unitLiter');
export const withLiterDay = withUnit('unitLiterDay');

export const getPathElements = (p: string) =>
    p.split('/').filter(e => e.length > 0);

export const ensureArray = <T>(a: T | T[]): T[] => {
    if (Array.isArray(a)) {
        return a;
    }
    return [a];
};


export const parseDate = (s: string) => {
    const d = Date.parse(s);
    return Number.isNaN(d) ? none : some(new Date(d));
};
export const datetime8601 = (d: Date) => d.toISOString().slice(0, 16);
export const date8601 = (d: Date) => datetime8601(d).slice(0, 10);



export const encodeUrlQuery =
    <D extends (string | number | boolean)>(o: Collection<D>) =>
        mapCollection((k, v) => `${k}=${encodeURIComponent(v)}`, o).join('&');



// we put it there as @turf/center doesn't come with types
export const center = (geojson: DirectGeometryObject): Coordinate => {
    const ext = bbox(geojson);
    const x = (ext[0] + ext[2]) / 2;
    const y = (ext[1] + ext[3]) / 2;
    return [x, y];
};


export const noop = (..._args: any[]) => void 0;


export const tryNumber = (n: unknown) => {
    if (typeof n === 'number') {
        return some(n);
    }
    else if (typeof n === 'string') {
        const pn = parseFloat(n);
        if (Number.isNaN(pn)) {
            return none;
        }
        return some(pn);
    }
    return none;
};

export const tryBoolean = (n: unknown) => {
    if (typeof n === 'boolean') {
        return some(n);
    }
    return none;
};

export const tryString = (n: unknown) => {
    if (typeof n === 'string') {
        return some(n);
    }
    return none;
};

export const tryTuple1 = <T>(a: T[]): Option<[T]> => {
    if (a.length === 1) {
        return some([a[0]]);
    }
    else {
        return none;
    }
}
export const tryTuple2 = <T>(a: T[]): Option<[T, T]> => {
    if (a.length === 2) {
        return some([a[0], a[1]]);
    }
    else {
        return none;
    }
}
export const tryTuple3 = <T>(a: T[]): Option<[T, T, T]> => {
    if (a.length === 3) {
        return some([a[0], a[1], a[2]]);
    }
    else {
        return none;
    }
}
export const tryTuple4 = <T>(a: T[]): Option<[T, T, T, T]> => {
    if (a.length === 4) {
        return some([a[0], a[1], a[2], a[3]]);
    }
    else {
        return none;
    }
}

export const partitionArray = <T>(
    xs: T[],
    f: (x: T) => boolean,
) => {
    const left: T[] = [];
    const right: T[] = [];
    xs.forEach(x => f(x) ? left.push(x) : right.push(x));
    return [left, right];
};


export const toDataURL = (f: File) => {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.onload = () => {
            const result = reader.result;
            if (result) {
                resolve(result);
            }
            else {
                reject();
            }
        };
        reader.onerror = reject;
        reader.onabort = reject;
        reader.readAsDataURL(f);
    });
};


export const notEmpty = <T>(ns: T[]) => ns.length > 0 ? some(ns) : none;
