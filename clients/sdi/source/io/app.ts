/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


import { MessageRecordIO, TypeOf } from './io';
import * as io from 'io-ts';
import { fromNullable } from 'fp-ts/lib/Option';


// tslint:disable-next-line: variable-name
export const AppManifestIO = io.tuple([
    (io.string), // codename
    io.union([MessageRecordIO, io.null]), // display name
    io.string, // path
], 'AppManifestIO');
export type AppManifest = TypeOf<typeof AppManifestIO>;


export const appName = (a: AppManifest) => a[0];
export const appDisplayName = (a: AppManifest) => fromNullable(a[1]);
export const appUrl = (a: AppManifest) => a[2];



// tslint:disable-next-line: variable-name
export const AppConfigIO = io.interface({
    user: io.union([io.string, io.null]),
    args: io.array(io.string),
    api: io.string,
    csrf: io.string,
    root: io.string,
    apps: io.array(AppManifestIO),
}, 'AppConfigIO');
export type AppConfig = TypeOf<typeof AppConfigIO>;
