/**
 * @license
 *
 * Copyright (c) 2011-2014, Christopher Jeffrey. (MIT Licensed)
 * https://github.com/chjj/marked
 *
 * Copyright (c) 2018, Костя Третяк. (MIT Licensed)
 * https://github.com/KostyaTretyak/marked-ts
 */

import { InlineLexer } from './inline-lexer';
import { Links, MarkedOptions, SimpleRenderer, Token, TokenType } from './interfaces';
import { Marked } from './marked';
import { Renderer } from './renderer';
import { NodeOrOptional } from '../../components/elements';
import { none } from 'fp-ts/lib/Option';
import { filterNotNull } from '../../util';
import { condL } from '../../lib';


type ParseResult = [TokenType, NodeOrOptional, Token[]];

const fst = (r: ParseResult) => r[0];
const snd = (r: ParseResult) => r[1];
const tail = (r: ParseResult) => r[2];

/**
 * Parsing & Compiling.
 */


const space =
    (_parser: Parser, _token: Token, rest: Token[]): ParseResult =>
        [TokenType.space, none, rest];

const paragraph =
    (parser: Parser, token: Token, rest: Token[]): ParseResult => {
        const text = parser.inlineLexer.output(token.text ?? '');
        return [
            TokenType.paragraph,
            parser.renderer.paragraph(text),
            rest,
        ];
    };


const text =
    (parser: Parser, token: Token, rest: Token[]): ParseResult => {
        const text = parser.inlineLexer.output(token.text ?? '');
        return [
            TokenType.text,
            parser.renderer.paragraph(text),
            rest,
        ];
    };

const heading =
    (parser: Parser, token: Token, rest: Token[]): ParseResult =>
        [
            TokenType.heading,
            parser.renderer.heading(
                parser.inlineLexer.output(token.text ?? ''),
                token.depth ?? 0,
                token.text ?? ''),
            rest,
        ];

const listStart =
    (parser: Parser, token: Token, rest: Token[]): ParseResult => {
        // this.clearAccumulator()
        const ordered = token.ordered;
        // while (this.next().type != TokenType.listEnd) {
        //   body += this.tok();
        // }
        const body: NodeOrOptional[] = [];
        let depth = 0;
        for (let i = 0; i < rest.length; i += 1) {
            const p = parser.tok(rest[i], rest.slice(i + 1));
            const t = fst(p);
            // console.log('TokenType.listStart', i, TokenType[t], snd(p));
            if (t === TokenType.listItemStart && depth === 0) {
                body.push(snd(p));
            }
            else if (t === TokenType.listStart) {
                depth += 1;
            }
            else if (t === TokenType.listEnd && depth > 0) {
                depth -= 1;
            }
            else if (t === TokenType.listEnd && depth === 0) {
                return [TokenType.listStart, parser.renderer.list(body, ordered), rest.slice(i + 1)];
            }
        }
        return [TokenType.listStart, parser.renderer.list(body, ordered), []];
    };

const listItemStart =
    (parser: Parser, _token: Token, rest: Token[]): ParseResult => {
        const body: NodeOrOptional[] = [];
        let depth = 0;
        for (let i = 0; i < rest.length; i += 1) {
            const p = parser.tok(rest[i], rest.slice(i + 1));
            const t = fst(p);
            // console.log('TokenType.listItemStart', i, TokenType[t], snd(p));
            if (t !== TokenType.listItemEnd && depth === 0) {
                body.push(snd(p));
            }
            else if (t === TokenType.listItemStart) {
                depth += 1;
            }
            else if (t === TokenType.listItemEnd && depth > 0) {
                depth -= 1;
            }
            else if (t === TokenType.listItemEnd && depth === 0) {
                return [TokenType.listItemStart, parser.renderer.listitem(filterNotNull(body)), rest.slice(i + 1)];
            }
        }
        return [TokenType.listItemStart, parser.renderer.listitem(filterNotNull(body)), []];
    };

const looseItemStart =
    (parser: Parser, _token: Token, rest: Token[]): ParseResult => {
        const body: NodeOrOptional[] = [];
        let depth = 0;
        for (let i = 0; i < rest.length; i += 1) {
            const p = parser.tok(rest[i], rest.slice(i + 1));
            const t = fst(p);
            if (t === TokenType.listItemStart && depth === 0) {
                body.push(snd(p));
            }
            else if (t === TokenType.listItemStart) {
                depth += 1;
            }
            else if ((t === TokenType.listItemEnd || t === TokenType.looseItemEnd) && depth > 0) {
                depth -= 1;
            }
            else if ((t === TokenType.listItemEnd || t === TokenType.looseItemEnd) && depth === 0) {
                return [TokenType.listItemStart, parser.renderer.listitem(filterNotNull(body)), rest.slice(i + 1)];
            }
        }

        return [TokenType.listItemStart, parser.renderer.listitem(filterNotNull(body)), []];
    };


const code =
    (parser: Parser, token: Token, rest: Token[]): ParseResult =>
        [
            TokenType.code,
            parser.renderer.code(token.text ?? '', token.lang, token.escaped),
            rest,
        ];


const table =
    (parser: Parser, token: Token, rest: Token[]): ParseResult => {
        let header: NodeOrOptional = none;
        let body: NodeOrOptional = none;
        let cell: NodeOrOptional[] = [];

        // header
        if (token.header) {
            for (let i = 0; i < token.header?.length; i += 1) {
                const flags = { header: true, align: token.align![i] }; // FIXME !
                const out = parser.inlineLexer.output(token.header[i]);

                cell.push(parser.renderer.tablecell(out, flags));
            }

            header = parser.renderer.tablerow(cell.slice(0));
        }

        if (token.cells) {
            for (const row of token.cells) {
                cell = [];

                for (let j = 0; j < row.length; j += 1) {
                    cell.push(parser.renderer.tablecell(parser.inlineLexer.output(row[j]), {
                        header: false,
                        align: token.align![j]
                    }));
                }

                body = parser.renderer.tablerow(cell);
            }
        }

        return [TokenType.table, parser.renderer.table(header, body), rest];
    };

const blockquoteStart =
    (parser: Parser, _token: Token, rest: Token[]): ParseResult => {
        const body: NodeOrOptional[] = [];

        // while (this.next().type != TokenType.blockquoteEnd) {
        //   body += this.tok();
        // }
        for (let i = 0; i < rest.length; i += 1) {
            const p = parser.tok(rest[i], rest.slice(i + 1));
            const t = fst(p);
            // console.log(`TokenType.blockquoteStart ${i}`, rest[i]);
            if (t === TokenType.blockquoteEnd) {
                return [TokenType.blockquoteStart, parser.renderer.blockquote(filterNotNull(body)), rest.slice(i + 1)];
            }
            body.push(snd(p));
        }

        return [TokenType.blockquoteStart, parser.renderer.blockquote(filterNotNull(body)), []];
    };

const hr =
    (parser: Parser, _token: Token, rest: Token[]): ParseResult =>
        [TokenType.hr, parser.renderer.hr(), rest];


const html =
    (parser: Parser, token: Token, rest: Token[]): ParseResult => {
        const html =
            !token.pre && !parser.options.pedantic ?
                parser.inlineLexer.output(token.text!) :
                token.text!;
        return [TokenType.html, parser.renderer.html(html), rest];
    };


export class Parser {
    simpleRenderers: SimpleRenderer[] = [];

    protected line: number = 0;
    readonly options: MarkedOptions;
    readonly renderer: Renderer;
    inlineLexer: InlineLexer;

    constructor(options?: MarkedOptions) {
        this.options = options || Marked.options;
        this.renderer = new Renderer(this.options);
    }


    static parse(tokens: Token[], links: Links, options?: MarkedOptions): NodeOrOptional[] {
        const parser = new this(options);
        return parser.parse(links, tokens);
    }

    parse(links: Links, tokens: Token[]) {
        this.inlineLexer = new InlineLexer(InlineLexer, links, this.options, this.renderer);
        let toks = tokens;
        const nodes: NodeOrOptional[] = [];
        while (toks.length > 0) {
            const result = this.tok(toks[0], toks.slice(1));
            nodes.push(snd(result));
            toks = tail(result);
        }
        return nodes;
    }


    tok(token: Token, rest: Token[]): ParseResult {
        const isType = (t: TokenType) => () => t === token.type;
        const ap = (f: (p: Parser, t: Token, r: Token[]) => ParseResult) => () => f(this, token, rest);
        return condL(
            [isType(TokenType.space), ap(space)],
            [isType(TokenType.paragraph), ap(paragraph)],
            [isType(TokenType.text), ap(text)],
            [isType(TokenType.heading), ap(heading)],
            [isType(TokenType.listStart), ap(listStart)],
            [isType(TokenType.listItemStart), ap(listItemStart)],
            [isType(TokenType.looseItemStart), ap(looseItemStart)],
            [isType(TokenType.code), ap(code)],
            [isType(TokenType.table), ap(table)],
            [isType(TokenType.blockquoteStart), ap(blockquoteStart)],
            [isType(TokenType.hr), ap(hr)],
            [isType(TokenType.html), ap(html)],
        ).getOrElse([token.type, none, []]);
    }
}
