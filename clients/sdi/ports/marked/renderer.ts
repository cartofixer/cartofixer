/**
 * @license
 *
 * Copyright (c) 2011-2014, Christopher Jeffrey. (MIT Licensed)
 * https://github.com/chjj/marked
 *
 * Copyright (c) 2018, Костя Третяк. (MIT Licensed)
 * https://github.com/KostyaTretyak/marked-ts
 */

import { Align, MarkedOptions } from './interfaces';
import { Marked } from './marked';
import { NodeOrOptional, PRE, CODE, BLOCKQUOTE, H1, H2, H3, H4, H5, HR, UL, OL, LI, P, TABLE, THEAD, TBODY, TR, TD, TH, STRONG, EM, BR, DEL, A, IMG, DIV } from '../../components/elements';
import { none } from 'fp-ts/lib/Option';
import { cond, } from '../../lib';
import { uniqId } from '../../util';

export class Renderer {
  protected options: MarkedOptions;

  constructor(options?: MarkedOptions) {
    this.options = options || Marked.options;
  }

  code(code: NodeOrOptional, lang?: string, _escaped?: boolean): NodeOrOptional {
    // if (this.options.highlight) {
    //   const out = this.options.highlight(code, lang);

    //   if (out != null && out !== code) {
    //     _escaped = true;
    //     code = out;
    //   }
    // }

    if (!lang) {
      return PRE({ key: uniqId(), }, CODE({}, code));
      // return '\n<pre><code>' + (escaped ? code : this.options.escape!(code, true)) + '\n</code></pre>\n';

    }

    return PRE({ key: uniqId(), }, CODE({
      className: `${this.options.langPrefix}${this.options.escape!(lang, true)}`
    }, code));
    // return (
    //   '\n<pre><code class="' +
    //   this.options.langPrefix +
    //   this.options.escape!(lang, true) +
    //   '">' +
    //   (escaped ? code : this.options.escape!(code, true)) +
    //   '\n</code></pre>\n'
    // );
  }

  blockquote(quote: NodeOrOptional): NodeOrOptional {
    return BLOCKQUOTE({ key: uniqId(), }, quote);
    // return '<blockquote>\n' + quote + '</blockquote>\n';
  }

  html(_html: NodeOrOptional): NodeOrOptional {
    return none;
    // return html;
  }

  heading(text: NodeOrOptional, level: number, raw: string): NodeOrOptional {
    const id: string = this.options.headerPrefix + raw.toLowerCase().replace(/[^\w]+/g, '-');
    const lev = (n: number) => () => n === level;
    const factory = cond(
      [lev(1), H1],
      [lev(2), H2],
      [lev(3), H3],
      [lev(4), H4],
      [lev(5), H5],
    ).getOrElse(DIV);

    return factory({ key: uniqId(), id }, text);
    // return `<h${level} id="${id}">${text}</h${level}>\n`;
  }

  hr(): NodeOrOptional {
    return HR({ key: uniqId(), });
    // return this.options.xhtml ? '<hr/>\n' : '<hr>\n';
  }

  list(body: NodeOrOptional[], ordered?: boolean): NodeOrOptional {
    const factory = ordered ? OL : UL;

    return factory({ key: uniqId(), }, body);
    // return `\n<${type}>\n${body}</${type}>\n`;
  }

  listitem(text: NodeOrOptional): NodeOrOptional {
    return LI({ key: uniqId(), }, text);
    // return '<li>' + text + '</li>\n';
  }

  paragraph(text: NodeOrOptional): NodeOrOptional {
    return P({ key: uniqId() }, text);
    // return '<p>' + text + '</p>\n';
  }

  table(header: NodeOrOptional, body: NodeOrOptional): NodeOrOptional {
    return TABLE({ key: uniqId(), }, THEAD({}, header), TBODY({}, body));
    //     return `
    // <table>
    // <thead>
    // ${header}</thead>
    // <tbody>
    // ${body}</tbody>
    // </table>
    // `;
  }

  tablerow(content: NodeOrOptional): NodeOrOptional {
    return TR({ key: uniqId() }, content);
    // return '<tr>\n' + content + '</tr>\n';
  }

  tablecell(content: NodeOrOptional, flags: { header?: boolean; align?: Align }): NodeOrOptional {
    const factory = flags.header ? TH : TD;
    return factory({ key: uniqId() }, content);
    // const tag = flags.align ? '<' + type + ' style="text-align:' + flags.align + '">' : '<' + type + '>';
    // return tag + content + '</' + type + '>\n';
  }

  // *** Inline level renderer methods. ***

  strong(text: NodeOrOptional): NodeOrOptional {
    return STRONG({ key: uniqId(), }, text);
  }

  em(text: NodeOrOptional): NodeOrOptional {
    return EM({ key: uniqId(), }, text);
  }

  codespan(text: NodeOrOptional): NodeOrOptional {
    return CODE({ key: uniqId(), }, text);
  }

  br(): NodeOrOptional {
    return BR({ key: uniqId(), });
  }

  del(text: NodeOrOptional): NodeOrOptional {
    return DEL({ key: uniqId(), }, text);
  }

  link(href: string, title: string, text: NodeOrOptional): NodeOrOptional {
    if (this.options.sanitize) {
      let prot: string;

      try {
        prot = decodeURIComponent(this.options.unescape!(href))
          .replace(/[^\w:]/g, '')
          .toLowerCase();
      }
      catch (e) {
        return text;
      }

      if (prot.indexOf('javascript:') === 0 || prot.indexOf('vbscript:') === 0 || prot.indexOf('data:') === 0) {
        return text;
      }
    }

    return A({ key: uniqId(), href, title, target: '_blank' }, text);

    // let out = '<a href="' + href + '"';

    // if (title) {
    //   out += ' title="' + title + '"';
    // }

    // out += '>' + text + '</a>';

    // return out;
  }

  image(src: string, title: string, alt: string): NodeOrOptional {
    return IMG({ key: uniqId(), src, title, alt });
    // let out = '<img src="' + href + '" alt="' + text + '"';

    // if (title) {
    //   out += ' title="' + title + '"';
    // }

    // out += this.options.xhtml ? '/>' : '>';

    // return out;
  }

  text(text: NodeOrOptional): NodeOrOptional {
    return text;
  }
}
