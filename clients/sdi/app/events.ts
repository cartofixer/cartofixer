import { assignK, dispatch, observe } from '../shape';
import { getRoot, getAppManifest, getUserId, getApiUrl } from './queries';
import { MessageRecordLang, fetchIO, appDisplayName, fetchAlias, fetchUserDetail } from '../source';
import { ActivityTokenIO } from '../source/io/activity';
import { activityURLs } from '../activity';
import { fromRecord } from '../locale';

// here a bit of a hack, because...
let currentAppname: null | string = null

export const setApplicationTitle = (
    codename: string,
) => getAppManifest(codename)
    .chain(appDisplayName)
    .map(fromRecord)
    .map((name) => {
        currentAppname = codename;
        const head = document.head;
        const title = document.createElement('title');
        title.appendChild(document.createTextNode(name));
        head.querySelectorAll('title').forEach(t => head.removeChild(t));
        head.appendChild(title);
    });



export const setLang = (l: MessageRecordLang) => {
    document.body.setAttribute('lang', l);
    dispatch('app/lang', () => l);
    if (currentAppname !== null) {
        setApplicationTitle(currentAppname);
    }
};

export const observeLang =
    (f: (l: MessageRecordLang) => void) => observe('app/lang', f);

export const navigateRoot =
    () => window.location.assign(getRoot());


export const setActivityToken =
    () =>
        fetchIO(ActivityTokenIO, activityURLs.token)
            .then(tok => dispatch('app/activityToken',
                () => tok.token));

export const loadAlias = (url: string) => {
    fetchAlias(url)
        .then((alias) => {
            dispatch('data/alias', () => alias);
        });
};



export const loadUserDetail =
    () => getUserId().map(id =>
        fetchUserDetail(getApiUrl(`users/${id}`))
            .then(assignK('app/user-detail'))
            .catch((_err: unknown) => {
                // TODO
            }))