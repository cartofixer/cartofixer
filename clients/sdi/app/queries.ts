import { query } from '../shape';
import { fromNullable, none, some } from 'fp-ts/lib/Option';
import { IAliasCollection, appName } from '../source';
import { fromRecord } from '../locale';
import { identity } from 'fp-ts/lib/function';

export const getUserId = () => fromNullable(query('app/user'));

export const getUserIdAstNumber =
    () => getUserId().chain(id => Number.isNaN(parseInt(id, 10)) ? none : some(parseInt(id, 10)));

export const getUserName = () =>
    fromNullable(query('app/user-detail'))
        .map(user => user.name);

export const isUserLogged = () => getUserId().isSome();

export const getApiUrl = (path: string) => `${query('app/api-root')}${path}`;

export const getLang = () => query('app/lang');

export const getCSRF = () => fromNullable(query('app/csrf'));

export const getRoot = () => query('app/root');

export const getRootUrl = (path: string) => getRoot() + path;

export const getActivityToken = () => fromNullable(query('app/activityToken'));

const blackList = ['dashboard', 'login', 'solar'];

export const getApps =
    () =>
        query('app/apps')
            .filter(a => blackList.indexOf(appName(a)) === -1);

export const getAppManifest =
    (codename: string) => fromNullable(query('app/apps').find(a => appName(a) === codename));


const getAliasInDictOption =
    (dict: Readonly<IAliasCollection>, k: string) =>
        fromNullable(dict.find(alias => alias.select === k))
            .map(alias => fromRecord(alias.replace));

// const getAliasInDict =
//     (dict: Readonly<IAliasCollection>, k: string) =>
//         getAliasInDictOption(dict, k).fold(k, identity);


export const getAliasOption =
    (k: string) =>
        fromNullable(query('data/alias'))
            .chain(dict => getAliasInDictOption(dict, k));


export const getAlias =
    (k: string) => getAliasOption(k).fold(k, identity);
