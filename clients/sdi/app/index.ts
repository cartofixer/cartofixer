/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
import { render } from 'react-dom';
import { Component, createElement } from 'react';

import { IShape } from '../shape';
import { IStoreInteractions, MessageRecord, ILayerInfo, Inspire } from '../source';
import { getLang } from './queries';
import { none, some } from 'fp-ts/lib/Option';
import { loadUserDetail, setApplicationTitle } from './events';

export * from './queries';
export * from './events';
export * from './rect';

const logger = debug('sdi:sdi/app');


export interface SyntheticLayerInfo {
    name: MessageRecord;
    info: ILayerInfo;
    metadata: Inspire;
}


export type RenderMain = () => React.ReactElement<any>;


export const displayException = (err: string) => {
    const title = document.createElement('h1');
    const errorBlock = document.createElement('div');
    const link = document.createElement('a');
    const body = document.body;
    while (body.firstChild) {
        body.removeChild(body.firstChild);
    }
    title.appendChild(document.createTextNode('Sorry, Application Crashed'));
    err.split('\n').forEach((line) => {
        const e = document.createElement('pre');
        e.appendChild(document.createTextNode(line));
        errorBlock.appendChild(e);
    });
    link.setAttribute('href', document.location.href);
    link.appendChild(document.createTextNode('Reload the application'));

    body.appendChild(title);
    body.appendChild(link);
    body.appendChild(errorBlock);

};


const { push, pop } = (() => {

    let stack: number[] = [];

    const push = (n: number) => stack.push(n);

    const pop = () => {
        if (stack.length > 0) {
            const n = stack[stack.length - 1];
            stack = [];
            return some(n);
        }
        return none;
    };
    return { push, pop };
})();

const OPTIMAL_FRAME_RATE = 16;
let FRAME_RATE = OPTIMAL_FRAME_RATE;


export const loop =
    (name: string, renderMain: RenderMain, effects?: () => void) =>
        (store: IStoreInteractions<IShape>) => {

            class Main extends Component<{}, { version: number, ts: number }> {
                private interval: number | null = null;
                constructor(props: {}) {
                    super(props);
                    this.state = { version: 0, ts: performance.now() };

                    store.observeVersion(push);
                }

                componentDidMount() {
                    const frame = (animTimestamp: number) => {
                        pop()
                            .map((newVersion) => {
                                const { version, ts } = this.state;
                                if (version < newVersion && (animTimestamp - ts) > FRAME_RATE) {
                                    this.setState({ version: newVersion, ts: performance.now() });
                                }
                                else {
                                    push(newVersion);
                                }
                            });
                        this.interval = window.requestAnimationFrame(frame);
                    };

                    this.interval = window.requestAnimationFrame(frame);
                }


                render() {
                    store.lock();
                    const rendered = renderMain();
                    store.release();
                    return rendered;
                }


                /**
                 * Here we monitor the time spent updating 
                 * in order to adjust the framerate.
                 */
                componentDidUpdate() {
                    const ts = performance.now();
                    const elapsed = Math.round(ts - this.state.ts);
                    if (elapsed > FRAME_RATE) {
                        FRAME_RATE += 10;
                    }
                    else if (FRAME_RATE > OPTIMAL_FRAME_RATE) {
                        FRAME_RATE = Math.max(OPTIMAL_FRAME_RATE, FRAME_RATE - 10);
                    }
                    logger(`Update(${this.state.version}) -> ${elapsed} / ${FRAME_RATE}`);
                }

                componentWillUnmount() {
                    const itv = this.interval;
                    if (itv !== null) {
                        window.cancelAnimationFrame(itv);
                        this.interval = null;
                    }
                }
            }

            const root = document.createElement('div');
            root.setAttribute('class', `root ${name}`);
            document.body.appendChild(root);

            const start = () => {
                loadUserDetail();
                document.body.setAttribute('lang', getLang());
                setApplicationTitle(name);
                render(createElement(Main, {}), root);

                if (effects) {
                    effects();
                }
            };

            return start;
        };

logger('loaded');
