/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
import Map from 'ol/Map';
import Collection from 'ol/Collection';
import Feature from 'ol/Feature';
import RenderFeature from 'ol/render/Feature';
import Circle from 'ol/style/Circle';
import Fill from 'ol/style/Fill';
import Stroke from 'ol/style/Stroke';
import Style from 'ol/style/Style';
import Select from 'ol/interaction/Select';
import { click } from 'ol/events/condition';
import { Geometry } from 'ol/geom';

// import { fontSizeExtractRegexp, fontSizeReplaceRegexp } from '../style';
import {
    withInteraction,
    SelectOptions,
    InteractionSelect,
    FeaturePath,
    SingleSelectOptions,
    MultiSelectOptions,
} from '..';
import { fromNullable } from 'fp-ts/lib/Option';
import { VectorLayer } from '../../map/map';



const logger = debug('sdi:map/select');


// const fontSizeIncrement = (s: string) => {
//     const result = fontSizeExtractRegexp.exec(s);
//     if (!result) {
//         return s;
//     }
//     if (result.length !== 2) {
//         return s;
//     }
//     const ret = parseFloat(result[1]) * 1.3;
//     if (isNaN(ret)) {
//         return s;
//     }
//     return s.replace(fontSizeReplaceRegexp,
//         (_m: string, p1: string, p2: string) => (
//             `${p1} ${ret.toFixed(1)}px ${p2}`
//         ));
// };

// const getSelectionStyleForPoint = (style: Style) => {
//     const text = style.getText();
//     if (text && text.getText()) {
//         return (new Style({
//             text: new Text({
//                 font: fontSizeIncrement(text.getFont()),
//                 text: text.getText(),
//                 textAlign: text.getTextAlign(),
//                 textBaseline: text.getTextBaseline(),
//                 offsetX: text.getOffsetX(),
//                 offsetY: text.getOffsetY(),
//                 fill: new Fill({
//                     color: '#3FB2FF',
//                 }),
//                 stroke: new Stroke({
//                     width: 2,
//                     color: 'white',
//                 }),
//             }),
//         }));
//     }
//     return (new Style());
// };


// const ensureArray = <T>(a: T | T[]): T[] => {
//     if (Array.isArray(a)) {
//         return a;
//     }
//     return [a];
// };

// const getStylesForFeature = (layers: Collection<layer.Vector>, f: Feature, res: number) => {
//     const fn = f.getStyleFunction();
//     if (fn) {
//         return ensureArray<Style>(fn.call(f, res));
//     }
//     const fs = f.getStyle();
//     if (fs) {
//         if (typeof fs === 'function') {
//             return ensureArray<Style>(fs.call(f, res));
//         }
//         return ensureArray(fs);
//     }


//     const layerRef = layers
//         .getArray()
//         .reduce<layer.Vector | null>((result, layer) => {
//             if (layer.getSource().getFeatureById(f.getId())) {
//                 return layer;
//             }
//             return result;
//         }, null);

//     if (layerRef) {
//         const fn = layerRef.getStyleFunction();
//         if (fn) {
//             return ensureArray(fn(f, res));
//         }
//         const fs = layerRef.getStyle();
//         if (fs) {
//             if (typeof fs === 'function') {
//                 return ensureArray(fs(f, res));
//             }
//             return ensureArray(fs);
//         }
//     }
//     return null;
// };

// const selectionStyle =
//     (layers: Collection<layer.Vector>) =>
//         (f: Feature, res: number) => {
//             const geometryType = f.getGeometry().getType();
//             if (geometryType === 'Point') {
//                 const styles = getStylesForFeature(layers, f, res);
//                 if (styles) {
//                     return styles.map(getSelectionStyleForPoint);
//                 }
//             }
//             else if (geometryType === 'LineString' || geometryType === 'MultiLineString') {
//                 return [
//                     new Style({
//                         stroke: new Stroke({
//                             width: 4,
//                             color: 'white',
//                         }),
//                     }),
//                     new Style({
//                         stroke: new Stroke({
//                             width: 2,
//                             color: '#3FB2FF',
//                         }),
//                     }),
//                 ];
//             }

//             return [new Style({
//                 fill: new Fill({
//                     color: '#3FB2FF',
//                 }),
//                 stroke: new Stroke({
//                     width: 2,
//                     color: 'white',
//                 }),
//             })];
//         };

const selectionStyle =
    (f: Feature<Geometry> | RenderFeature, _res: number) => {
        const geom = f.getGeometry();
        if (geom !== undefined) {
            const geometryType = geom.getType();
            if (geometryType === 'Point' || geometryType === 'MultiPoint') {
                return [
                    new Style({
                        image: new Circle({
                            radius: 12,
                            fill: new Fill({
                                color: '#3FB2FF',
                            }),
                            stroke: new Stroke({
                                width: 2,
                                color: 'white',
                            }),
                        }),
                    }),
                ];
            }
            else if (geometryType === 'LineString' || geometryType === 'MultiLineString') {
                return [
                    new Style({
                        stroke: new Stroke({
                            width: 4,
                            color: 'white',
                        }),
                    }),
                    new Style({
                        stroke: new Stroke({
                            width: 2,
                            color: '#3FB2FF',
                        }),
                    }),
                ];
            }

            return [new Style({
                fill: new Fill({
                    color: '#3FB2FF',
                }),
                stroke: new Stroke({
                    width: 2,
                    color: 'white',
                }),
            })];
        }

        return [];
    };

const initSingle = (
    options: SingleSelectOptions,
    selectedFeature: Collection<Feature<Geometry>>,
) => {



    const selecteHandler = () => {
        logger('selectInteraction.on select');
        if (selectedFeature.getLength() > 0) {
            const f = selectedFeature.item(0);
            const lid = f.get('lid') as string;
            // const j: GeoJSONFeature = formatGeoJSON.writeFeatureObject(f) as any;
            fromNullable(f.getId()).map(fid => options.selectFeature(lid, fid));
            selectedFeature.clear();
            selectedFeature.push(f);
        }
        else {
            options.clearSelection();
        }
    };
    const selectInteraction = new Select({
        style: selectionStyle,
        features: selectedFeature,
        condition: click,
        multi: false,
        hitTolerance: 6,
        filter: options.filter,
    });
    selectInteraction.on('select', selecteHandler);


    const syncSelection =
        () => {
            const s = options.getSelected();
            if (s.featureId === null && selectedFeature.getLength() > 0) {
                selectedFeature.clear();
            }
        };

    return { selectInteraction, syncSelection };
};


const initMulti = (
    options: MultiSelectOptions,
    selectedFeature: Collection<Feature<Geometry>>,
) => {

    const selecteHandler = () => {
        const optSelect = options.selectFeatures;
        const ps: FeaturePath[] = selectedFeature.getArray()
            .map(f => ({
                layerId: fromNullable(f.get('lid')).getOrElse('__None__'),
                featureId: fromNullable(f.getId()).getOrElse('__None__'),
            }));
        optSelect(ps);
    };
    const selectInteraction = new Select({
        style: selectionStyle,
        features: selectedFeature,
        condition: click,
        multi: true,
        hitTolerance: 6,
        filter: options.filter,
    });
    selectInteraction.on('select', selecteHandler);


    const syncSelection =
        () => {
            const ps = options.getSelected();
            if (ps.length < selectedFeature.getLength()) {
                selectedFeature.clear();
            }
        };

    return { selectInteraction, syncSelection };
};

export const select =
    (options: SelectOptions, _layers: Collection<VectorLayer>) => {
        const selectedFeature = new Collection<Feature<Geometry>>();

        const { selectInteraction, syncSelection } = (() => {
            switch (options.tag) {
                case 'single': return initSingle(options, selectedFeature);
                case 'multi': return initMulti(options, selectedFeature);
            }
        })();

        const init =
            (map: Map) => {
                map.addInteraction(selectInteraction);
            };

        const update = withInteraction<InteractionSelect>('select',
            () => {
                syncSelection();
                selectInteraction.setActive(true);
            },
            () => {
                syncSelection();
                selectInteraction.setActive(false);
            });



        return { init, update };
    };




logger('loaded');
