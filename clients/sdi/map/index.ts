/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// import * as proj4 from 'proj4';
// import * as proj from 'ol/proj';
import { GeoJSON } from 'ol/format';
import { Coordinate } from 'ol/coordinate';
import { Extent } from 'ol/extent';
import { FilterFunction } from 'ol/interaction/Select';
import { fromLonLat as olFromLonLat } from 'ol/proj';
import { Option, some, none } from 'fp-ts/lib/Option';
import { Either } from 'fp-ts/lib/Either';

import {
    IMapBaseLayer,
    IMapInfo,
    FeatureCollection,
    GeometryType,
    Feature,
    DirectGeometryObject,
    MessageRecord,
} from '../source';
import { Getter, Setter } from '../shape';


export const formatGeoJSON = new GeoJSON({
    dataProjection: 'EPSG:4326',
    featureProjection: 'EPSG:3857'
});


export type Coord2D = [number, number];
export const tryCoord2D = (c: Coordinate | undefined): Option<Coord2D> => {
    if (c !== undefined && c.length > 1) {
        return some([c[0], c[1]]);
    }
    return none;
};

// proj.setProj4(proj4);
// proj4.defs('EPSG:31370',
//     '+proj=lcc +lat_1=51.16666723333333 +lat_2=49.8333339 +lat_0=90 +lon_0=4.367486666666666 +x_0=150000.013 +y_0=5400088.438 +ellps=intl +towgs84=-106.8686,52.2978,-103.7239,-0.3366,0.457,-1.8422,-1.2747 +units=m +no_defs');

// const EPSG31370 = new proj.Projection({
//     code: 'EPSG:31370',
//     extent: [14697.30, 22635.80, 291071.84, 246456.18],
// });

// proj.addProjection(EPSG:31370);

// export type FeatureCollectionOrNull = FeatureCollection | null;
export type FetchData = () => Either<string, Option<FeatureCollection>>;
export type SetScaleLine = (count: number, unit: string, width: number) => void;

export const fromLonLat = (
    geom: DirectGeometryObject,
) => {
    switch (geom.type) {
        case 'Point': return {
            ...geom,
            coordinates: olFromLonLat(geom.coordinates)
        };
        case 'LineString': return {
            ...geom,
            coordinates: geom.coordinates.map(c => olFromLonLat(c))
        };
        case 'Polygon': return {
            ...geom,
            coordinates: geom.coordinates.map(ring => ring.map(c => olFromLonLat(c))),
        };
        case 'MultiPoint': return {
            ...geom,
            coordinates: geom.coordinates.map(c => olFromLonLat(c))
        };
        case 'MultiLineString': return {
            ...geom,
            coordinates: geom.coordinates.map(line => line.map(c => olFromLonLat(c))),
        };
        case 'MultiPolygon': return {
            ...geom,
            coordinates: geom.coordinates.map(poly => poly.map(ring => ring.map(c => olFromLonLat(c)))),
        };
    }
};



export interface IMapScale {
    count: number;
    unit: string;
    width: number;
}

export type ViewDirt = 'none' | 'geo' | 'geo/feature' | 'geo/extent' | 'style';

export interface IMapViewData {
    dirty: ViewDirt;
    srs: string;
    center: Coordinate;
    rotation: number;
    zoom: number;
    feature: Feature | null;
    extent: Extent | null;
}

export interface IGeoSelect {
    selected: string | null;
}

export interface TrackerCoordinate {
    coord: Coordinate;
    accuracy: number;
}

export interface IGeoTracker {
    track: TrackerCoordinate[];
}

export interface TrackerOptions {
    updateTrack(t: TrackerCoordinate): void;
    resetTrack(): void;
    setCenter(c: Coordinate): void;
}

export type IGeoMeasureType = 'Polygon' | 'LineString';

export interface IGeoMeasure {
    geometryType: IGeoMeasureType;
    coordinates: Coordinate[];
}

export interface MeasureOptions {
    updateMeasureCoordinates(c: Coordinate[]): void;
    stopMeasuring(): void;
}

export interface ExtractFeature {
    layerId: string;
    featureId: string | number;
}

export interface ExtractOptions {
    setCollection(c: ExtractFeature[]): void;
}

export interface PositionOptions {
    setPosition(p: Coordinate): void;
    stopPosition(p: Option<Coordinate>): void;
}

export interface SingleClickOptions {
    setPosition(p: Coordinate, isActive?: boolean): void;
}

export interface IMark {
    started: boolean;
    endTime: number;
    coordinates: Coordinate;
}

export interface MarkOptions {
    endMark(): void;
    startMark(): void;
}

export const defaultMark =
    (): IMark => ({
        started: false,
        endTime: 0,
        coordinates: [0, 0],
    });

export type MapEditableMode = 'none' | 'select' | 'create' | 'modify';
export type MapEditableSelected = string | number | null;

export interface IGeoCreate {
    geometryType: GeometryType;
}

export interface IGeoModify {
    selected: MapEditableSelected;
    geometryType: GeometryType;
}

// TODO: make the whole thing nullable rather than pieces of it
export interface FeaturePath {
    layerId: string | null;
    featureId: number | string | null;
}

export type FeaturePathGetter = Getter<FeaturePath>;


export const getFeaturePath = (
    { layerId, featureId }: FeaturePath,
) => {
    if (layerId !== null && featureId !== null) {
        return some({ layerId, featureId });
    }
    return none;
};


export interface IPosition {
    coordinates: Coordinate;
    after: (c: Coordinate) => void;
}

export interface InteractionBase<L extends string, T> {
    label: L;
    state: T;
}

// export type InteractionSelect = InteractionBase<'select', IGeoSelect>;
// export type InteractionCreate = InteractionBase<'create', IGeoCreate>;
// export type InteractionModify = InteractionBase<'modify', IGeoModify>;
// export type InteractionTrack = InteractionBase<'track', IGeoTracker>;
// export type InteractionMeasure = InteractionBase<'measure', IGeoMeasure>;
// export type InteractionExtract = InteractionBase<'extract', ExtractFeature[]>;
// export type InteractionMark = InteractionBase<'mark', IMark>;
// export type InteractionPrint = InteractionBase<'print', null>;
// export type InteractionPosition = InteractionBase<'position', IPosition>;
// export type InteractionSingleClick = InteractionBase<'singleclick', null>;

export interface InteractionSelect { label: 'select'; state: IGeoSelect; }
export interface InteractionCreate { label: 'create'; state: IGeoCreate; }
export interface InteractionModify { label: 'modify'; state: IGeoModify; }
export interface InteractionTrack { label: 'track'; state: IGeoTracker; }
export interface InteractionMeasure { label: 'measure'; state: IGeoMeasure; }
export interface InteractionExtract { label: 'extract'; state: ExtractFeature[]; }
export interface InteractionMark { label: 'mark'; state: IMark; }
export interface InteractionPrint { label: 'print'; state: null; }
export interface InteractionPosition { label: 'position'; state: IPosition; }
export interface InteractionSingleClick { label: 'singleclick'; state: null; }


interface InteractionMap {
    'select': InteractionSelect;
    'create': InteractionCreate;
    'modify': InteractionModify;
    'track': InteractionTrack;
    'measure': InteractionMeasure;
    'extract': InteractionExtract;
    'mark': InteractionMark;
    'print': InteractionPrint;
    'position': InteractionPosition;
    'singleclick': InteractionSingleClick;
}

export type InteractionLabel = keyof InteractionMap;
export type Interaction = InteractionMap[InteractionLabel];


export const defaultInteraction =
    (): Interaction => ({
        label: 'select',
        state: { selected: null },
    });


export const singleClickInteraction =
    (): Interaction => ({
        label: 'singleclick',
        state: null,
    });



export type InteractionGetter = Getter<Interaction>;
export type InteractionSetter = Setter<Interaction>;


const noop = () => { };

/**
 *
 * withInteraction('select', (i) => do(i), () => doNothing());
 *
 */
export const withInteraction =
    <T extends Interaction>(
        label: InteractionLabel,
        w: (i: T) => void,
        wo = noop) =>
        (i: Interaction | Readonly<Interaction>) => {
            switch (i.label) {
                case 'select': label === 'select' ? w(i as T) : wo(); break;
                case 'create': label === 'create' ? w(i as T) : wo(); break;
                case 'modify': label === 'modify' ? w(i as T) : wo(); break;
                case 'track': label === 'track' ? w(i as T) : wo(); break;
                case 'measure': label === 'measure' ? w(i as T) : wo(); break;
                case 'extract': label === 'extract' ? w(i as T) : wo(); break;
                case 'mark': label === 'mark' ? w(i as T) : wo(); break;
                case 'print': label === 'print' ? w(i as T) : wo(); break;
                case 'position': label === 'position' ? w(i as T) : wo(); break;
                case 'singleclick': label === 'singleclick' ? w(i as T) : wo(); break;
            }
        };

export const withInteractionOpt =
    <T extends Interaction, R = unknown>(
        label: InteractionLabel, w: (i: T | Readonly<T>) => R) =>
        (i: Interaction | Readonly<Interaction>) => {
            switch (i.label) {
                case 'select': return label === 'select' ? some(w(i as T)) : none;
                case 'create': return label === 'create' ? some(w(i as T)) : none;
                case 'modify': return label === 'modify' ? some(w(i as T)) : none;
                case 'track': return label === 'track' ? some(w(i as T)) : none;
                case 'measure': return label === 'measure' ? some(w(i as T)) : none;
                case 'extract': return label === 'extract' ? some(w(i as T)) : none;
                case 'mark': return label === 'mark' ? some(w(i as T)) : none;
                case 'print': return label === 'print' ? some(w(i as T)) : none;
                case 'position': return label === 'position' ? some(w(i as T)) : none;
                case 'singleclick': return label === 'singleclick' ? some(w(i as T)) : none;
            }
        };

export interface EditOptions {
    getCurrentLayerId(): string;
    getGeometryType(lid: string): GeometryType;

    // editFeature(fid: string | number): void;
    addFeature(f: Feature): void; // create
    setGeometry(geom: DirectGeometryObject): void; // modify
}

export interface SingleSelectOptions {
    readonly tag: 'single';
    selectFeature(lid: string, id: string | number): void;
    clearSelection(): void;
    getSelected: FeaturePathGetter;
    filter?: FilterFunction;
}

export const singleSelectOptions = (
    options: Pick<SingleSelectOptions, 'getSelected' | 'selectFeature' | 'clearSelection' | 'filter'>,
): SingleSelectOptions => ({
    ...options,
    tag: 'single',
});

export interface MultiSelectOptions {
    readonly tag: 'multi';
    getSelected: () => FeaturePath[];
    selectFeatures: (ps: FeaturePath[]) => void;
    filter?: FilterFunction;
}

export const multiSelectOptions = (
    options: Pick<MultiSelectOptions, 'getSelected' | 'selectFeatures' | 'filter'>,
): MultiSelectOptions => ({
    ...options,
    tag: 'multi',
});


export type SelectOptions = SingleSelectOptions | MultiSelectOptions;

export interface IMapOptions {
    element: HTMLElement | null;
    getBaseLayer(): IMapBaseLayer | null;
    getView(): IMapViewData;
    getMapInfo(): IMapInfo | null;

    updateView(v: IViewEvent): void;
    setScaleLine: SetScaleLine;
    setLoading?: (ms: MessageRecord[]) => void;
}


export interface IViewEvent {
    dirty?: ViewDirt;
    center?: Coordinate;
    rotation?: number;
    zoom?: number;
    feature?: Feature;
    extent?: Extent;
}


export interface PrintRequest<T> {
    id: string | null;
    width: number;
    height: number;
    resolution: number;
    props: T | null;
}
export const defaultPrintRequest =
    (): PrintRequest<null> => ({
        id: null,
        width: 0,
        height: 0,
        resolution: 0,
        props: null,
    });

export type PrintResponseStatus = 'none' | 'start' | 'end' | 'error' | 'done';

export interface PrintResponse<T> {
    id: string | null;
    status: PrintResponseStatus;
    data: string;
    extent: [number, number, number, number];
    props: T | null;
}
export const defaultPrintResponse =
    (): PrintResponse<null> => ({
        id: null,
        data: '',
        extent: [0, 0, 0, 0],
        status: 'none',
        props: null,
    });

export interface PrintOptions<T> {
    getRequest(): PrintRequest<T>;
    setResponse(r: PrintResponse<T>): void;
}


export * from './map';
export * from './events';
export * from './queries';

