

/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import Feature from 'ol/Feature';
import RenderFeature from 'ol/render/Feature';
import Style from 'ol/style/Style';
import { toContext } from 'ol/render';
import CanvasImmediateRenderer from 'ol/render/canvas/Immediate';
import { Geometry } from 'ol/geom';

export { default as pointStyle } from './point';
export { default as lineStyle } from './line';
export { default as polygonStyle } from './polygon';

export type StyleFn = (a: Feature<Geometry> | RenderFeature, b?: number) => Style[];

export interface IOLContext {
    canvas: HTMLCanvasElement;
    canvasContext: CanvasRenderingContext2D;
    olContext: CanvasImmediateRenderer;
}

export const getContext = (width: number, height: number): IOLContext | null => {
    const canvas = document.createElement('canvas');
    const canvasContext = canvas.getContext('2d');
    if (!canvasContext) {
        return null;
    }
    const olContext = toContext(canvasContext, {
        size: [height, width],
    });
    return { canvas, canvasContext, olContext };
};


export const markerFont = (sz: number) => `${sz}px forkawesome`;
export const labelFont = (sz: number) => `bold ${sz}px open_sans`;
export const fontSizeExtractRegexp = new RegExp('.*?(\\d+)px.*');
export const fontSizeReplaceRegexp = new RegExp('(.*?)\\d+px(.*)');
