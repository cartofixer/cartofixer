

export const credit =
    () => {
        const elem = document.createElement('div');
        const anchor = document.createElement('a');

        elem.setAttribute('class', 'credit');
        anchor.appendChild(document.createTextNode('©'));
        anchor.setAttribute('href', 'https://cartofixer.be');
        anchor.setAttribute('title', 'cartofixer © cartofixer');
        anchor.setAttribute('target', '_blank');

        elem.appendChild(anchor);

        return elem;
    };
