import * as debug from 'debug';
import { DirectGeometryObject, Position } from '../source';
import bbox from '@turf/bbox';
import { Option, none, some } from 'fp-ts/lib/Option';
import { index } from 'fp-ts/lib/Array';

const logger = debug('sdi:map/mini');

type BBox2d = [number, number, number, number];
type Size = [number, number];

export type MiniStep =
    | { step: 'loading'; }
    | { step: 'failed'; }
    | { step: 'loaded'; data: string; };


export const miniMapLoading = (): MiniStep => ({ step: 'loading' });
export const miniMapFailed = (): MiniStep => ({ step: 'failed' });
export const miniMapLoaded = (data: string): MiniStep => ({ step: 'loaded', data });


// tslint:disable-next-line:variable-name
const WMSBaseRequest = {
    SERVICE: 'WMS',
    VERSION: '1.1.1',
    REQUEST: 'GetMap',
    FORMAT: 'image/png',
    TRANSPARENT: 'true',
    LAYERS: 'urbisFRGray',
    WIDTH: 256,
    HEIGHT: 256,
    SRS: 'EPSG:3857',
    STYLES: '',
    BBOX: '',
};

export type WMSRequest = typeof WMSBaseRequest;
export type WMSRequestKey = keyof WMSRequest;

const encodeRequest =
    (r: WMSRequest) => Object.entries(r)
        .map(([k, v]) => `${k}=${encodeURIComponent(v.toString())}`)
        .join('&');

export const mapCoordinates =
    <T>(geom: DirectGeometryObject, f: (p: Position) => T) => {
        switch (geom.type) {
            case 'Point': return f(geom.coordinates);
            case 'MultiPoint': return geom.coordinates.map(f);
            case 'LineString': return geom.coordinates.map(f);
            case 'MultiLineString': return geom.coordinates.map(l => l.map(f));
            case 'Polygon': return geom.coordinates.map(l => l.map(f));
            case 'MultiPolygon': return geom.coordinates.map(p => p.map(l => l.map(f)));
        }
    };

const buffer =
    (width: number, geom: DirectGeometryObject) => {
        switch (geom.type) {
            // case 'Point': return 100; // TODO generalize the "100" value for other CRS than 31370.
            case 'Point': return (geom.coordinates[0] > 180.0 ? 100 : 0.1);
            // case 'MultiPoint': return Math.max(100, width * 2);
            case 'MultiPoint': return (geom.coordinates[0][0] > 180.0 ? 100 : 0.1);//  width * 2);
            case 'LineString':
            case 'MultiLineString':
            case 'Polygon':
            // case 'MultiPolygon': return Math.max(100, width * 1.3);
            case 'MultiPolygon': return width * 1.3;
        }
    };


const drawPoint =
    (ctx: CanvasRenderingContext2D) =>
        (p: Position) => {
            const [x, y] = p;
            logger(`drawPoint (${x}, ${y});`);
            ctx.fillRect(x - 5, y - 5, 10, 10);
        };

const drawLine =
    (ctx: CanvasRenderingContext2D) =>
        (p: Position[]) => {
            index(0, p).map(([x, y]) => ctx.moveTo(x, y));
            p.slice(1).map(([x, y]) => ctx.lineTo(x, y));
            ctx.stroke();
        };

const drawPolygon =
    (ctx: CanvasRenderingContext2D) =>
        (p: Position[][]) => {
            p.map((inner) => {
                index(0, inner).map(([x, y]) => ctx.moveTo(x, y));
                inner.slice(1).map(([x, y]) => ctx.lineTo(x, y));
                ctx.stroke();
            });
        };


const draw =
    (ctx: CanvasRenderingContext2D, geom: DirectGeometryObject) => {
        switch (geom.type) {
            case 'Point': return drawPoint(ctx)(geom.coordinates);
            case 'MultiPoint': return geom.coordinates.map(drawPoint(ctx));
            case 'LineString': return drawLine(ctx)(geom.coordinates);
            case 'MultiLineString': return geom.coordinates.map(drawLine(ctx));
            case 'Polygon': return drawPolygon(ctx)(geom.coordinates);
            case 'MultiPolygon': return geom.coordinates.map(drawPolygon(ctx));
        }
    };


const drawGeomOnImage =
    (
        image: CanvasImageSource,
        geom: Readonly<DirectGeometryObject>,
        box: BBox2d,
        sz: Size,
    ) => {
        const [minx, miny, maxx, maxy] = box;
        logger(`drawGeomOnImage (${minx}. ${miny})`);
        const canvas = document.createElement('canvas');
        const hscale = sz[0] / (maxx - minx);
        const vscale = sz[1] / (maxy - miny);
        canvas.width = sz[0];
        canvas.height = sz[1];
        logger(`drawGeomOnImage (${hscale}. ${vscale})`);
        const ctx = canvas.getContext('2d');
        if (ctx) {
            ctx.drawImage(image, 0, 0);
            const coordinates = mapCoordinates(geom, p => [
                (p[0] - minx) * hscale,
                sz[1] - ((p[1] - miny) * vscale),
            ]);
            const transformed = { ...geom, coordinates } as DirectGeometryObject;

            ctx.beginPath();
            draw(ctx, transformed);
            return some(canvas.toDataURL('image/png', false));
        }
        return none;
    };



/**
 * Build a static image based on a
 * geometry and a  WMS URL
 */
export const miniMap =
    (url: string, options: Partial<WMSRequest>) =>
        (geometry: Readonly<DirectGeometryObject>): Promise<Option<string>> => {
            const [minxOrig, minyOrig, maxx, maxy] = bbox(geometry);
            const widthOrig = maxx - minxOrig;
            const heightOrig = maxy - minyOrig;
            const width = buffer(widthOrig, geometry);
            const height = buffer(heightOrig, geometry);
            const sideMax = Math.max(width, height);
            const sideMin = Math.min(width, height);
            const minx = minxOrig - ((width - widthOrig) / 2);
            const miny = minyOrig - ((height - heightOrig) / 2);
            const abbox: BBox2d = [
                minx - ((sideMax - sideMin) / 2),
                miny - ((sideMax - sideMin) / 2),
                minx + sideMax,
                miny + sideMax,
            ];
            const bboxString = abbox.map(c => c.toFixed(2)).join(',');
            const parameters = { ...WMSBaseRequest, ...options, BBOX: bboxString };
            const querystring = encodeRequest(parameters);
            const requestUrl = `${url}?${querystring}`;
            logger(`URL: ${requestUrl}`);
            return fetch(requestUrl)
                .then((response) => {
                    if (response.ok) { return response.blob(); }
                    throw (new Error(response.statusText));
                })
                .then(blob => createImageBitmap(blob))
                .then(image => drawGeomOnImage(image, geometry, abbox, [parameters.WIDTH, parameters.HEIGHT]))
                .catch((err) => {
                    logger(`fetch(${requestUrl}) error ${err}`);
                    return none;
                });
        };

export default miniMap;

logger('loaded');
