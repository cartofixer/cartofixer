/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
import { fromNullable, Option, some, none } from 'fp-ts/lib/Option';
import Map from 'ol/Map';
import View from 'ol/View';
import Feature from 'ol/Feature';
import Collection from 'ol/Collection';
import RenderFeature from 'ol/render/Feature';
import { Extent } from 'ol/extent';
import VectorSource from 'ol/source/Vector';
import TileWMS from 'ol/source/TileWMS';
import { Geometry } from 'ol/geom';
import SourceTile from 'ol/source/Tile';
import LayerTile from 'ol/layer/Tile';
import LayerVector from 'ol/layer/Vector';
import LayerGroup from 'ol/layer/Group';


import SourceVector from 'ol/source/Vector';
import { ProjectionLike, get, toLonLat, fromLonLat } from 'ol/proj';

import { SyntheticLayerInfo } from '../app';
import { translateMapBaseLayer, hashMapBaseLayer } from '../util';
import {
    DirectGeometryObject,
    Feature as GeoFeature,
    FeatureCollection,
    getMessageRecord,
    IMapBaseLayer,
    MessageRecord,
    Position as GeoPosition,
    ILayerInfo,
} from '../source';
// import { fromRecord } from '../locale';

import {
    ExtractOptions,
    FeaturePathGetter,
    FetchData,
    formatGeoJSON,
    IMapOptions,
    InteractionGetter,
    MarkOptions,
    MeasureOptions,
    PositionOptions,
    PrintOptions,
    SelectOptions,
    TrackerOptions,
    SingleClickOptions,
    EditOptions,
    IViewEvent,
    Coord2D,
    tryCoord2D,
} from '.';
import { StyleFn, lineStyle, pointStyle, polygonStyle } from './style';
import { scaleLine, zoomControl, rotateControl, fullscreenControl, loadingMon } from './controls';
import { select, highlight } from './actions';
import { measure, track, extract, mark, print, position, singleclick, edit } from './tools';

import { credit } from './credit';
import { scopeOption } from '../lib';


// import { setTimeout } from 'timers';


const logger = debug('sdi:map');

export type TileLayer = LayerTile<SourceTile>;
export type VectorLayer = LayerVector<SourceVector<Geometry>>;

type CSMap = ReturnType<typeof create>;

const { addMap, getMap } = (function () {
    const maps: { [k: string]: CSMap } = {};

    const addMap = (name: string, m: CSMap) => {
        maps[name] = m;
        return m;
    };
    const getMap = (name: string) => fromNullable(maps[name]);

    return { addMap, getMap };
})();

const withMap =
    <ARGS extends unknown[], R>(f: (m: CSMap, ...args: ARGS) => R) =>
        (mapName: string, ...args: ARGS) =>
            getMap(mapName).map(m => f(m, ...args));


export const mapExists = (mapName: string) => getMap(mapName).map(() => true).getOrElse(false);

const removeLayerWithMap =
    (m: CSMap, lid: string) => {
        logger(`===== removeLayer ${lid} ====`);
        const layersArray = m.mainLayerGroup.getLayers().getArray();
        const toRemove = layersArray.find(l => l.get('id') === lid);
        if (toRemove !== undefined) {
            m.mainLayerGroup.getLayers().remove(toRemove);
        }
        else {
            logger(`layer(${lid}) not in collection`);
        }
    };

export const removeLayer = withMap(removeLayerWithMap);

const removeLayerAllWithMap =
    (m: CSMap) => {
        logger(`===== remove All Layers ====`);
        const lyrs = m.mainLayerGroup.getLayers();
        lyrs.getArray()
            // .slice(1)
            .forEach(l => lyrs.remove(l));
    };

export const removeLayerAll = withMap(removeLayerAllWithMap);

const getStyleFn =
    (layerInfo: () => Option<SyntheticLayerInfo>) =>
        (a: Feature<Geometry> | RenderFeature, b?: number) =>
            layerInfo().fold([], ({ info }) => {
                switch (info.style.kind) {
                    case 'polygon-continuous':
                    case 'polygon-discrete':
                    case 'polygon-simple':
                        return polygonStyle(info.style)(a, b);
                    case 'point-discrete':
                    case 'point-simple':
                    case 'point-continuous':
                        return pointStyle(info.style)(a, b);
                    case 'line-simple':
                    case 'line-discrete':
                    case 'line-continuous':
                        return lineStyle(info.style)(a, b);
                }
            }
            );

const addLayerWithMap =
    (
        m: CSMap,
        layerInfo: () => Option<SyntheticLayerInfo>,
        fetchData: FetchData,
        retryCount = 0
    ) => {
        const infoOption = layerInfo();
        if (infoOption.isSome()) {
            infoOption.map(({ info, metadata }) => {

                logger(`===== addLayer ${info.id} ====`);
                const layers = m.mainLayerGroup.getLayers();
                const alayers = layers.getArray();
                if (alayers.find(l => l.get('id') === info.id)) {
                    logger(`addLayer.abort`);
                    return;
                }
                const styleFn: StyleFn = getStyleFn(layerInfo);


                const vs = new VectorSource();
                const vl = new LayerVector({
                    // renderMode: 'image', // IMPORTANT - but has disappeared from OL6
                    source: vs,
                    style: styleFn,
                    maxResolution: getResolutionForZoomLMerc(info.minZoom, 0),
                    minResolution: getResolutionForZoomLMerc(info.maxZoom, 30),
                });
                vs.set('id', info.id);
                vl.set('id', info.id);
                vl.setVisible(info.visible);
                layers.push(vl);
                layers.forEach(l => logger(`>> ${l.get('id')}`));
                if (metadata) {
                    const title = getMessageRecord(metadata.resourceTitle);
                    m.loadingMonitor.add(title);
                    m.getLayerData(fetchData, vs, vl, some(title));
                }
                else {
                    m.getLayerData(fetchData, vs, vl, none);
                }
            });

        }
        else if (retryCount < 120) {
            setTimeout(() => {
                addLayerWithMap(m, layerInfo, fetchData, retryCount + 1);
            }, retryCount * retryCount * 250);
        }
        // return null;
    };

export const addLayer = withMap(addLayerWithMap);

const addFeaturesToLayerWithMap =
    (m: CSMap, info: ILayerInfo, features: GeoFeature[]) => {
        logger(`addFeaturesToLayer`, features.length);
        const layers = m.mainLayerGroup.getLayers();
        const alayers = layers.getArray();
        const layer = alayers.find(l => l.get('id') === info.id);
        if (layer) {
            const source = layer.get('source');
            m.addFeatures(source, features);
        }
    };

// const addFeaturesToLayerWithMap =
//     (m: CSMap, layerInfo: () => Option<SyntheticLayerInfo>, fetchData: FetchData) => {
//         layerInfo().fold(null, ({ info, metadata }) => {
//             logger(`===== addFeaturesToLayer ${info.id} ====`);

//             const layers = m.mainLayerGroup.getLayers();
//             const alayers = layers.getArray();
//             const layer = alayers.find(l => l.get('id') === info.id);
//             if (layer) {
//                 const source = layer.get('source');
//                 const vectorLayer = <layer.Vector>layer;
//                 vectorLayer.setVisible(info.visible);

//                 if (metadata) {
//                     const title = getMessageRecord(metadata.resourceTitle);

//                     m.loadingMonitor.add(title);

//                     m.getLayerData(fetchData, source, vectorLayer, some(title));
//                 }
//                 else {
//                     m.getLayerData(fetchData, source, vectorLayer, none);
//                 }
//             }
//             else {
//                 logger(`addFeaturesToLayer.abort`);
//                 return;
//             }
//         });
//     };

export const addFeaturesToLayer = withMap(addFeaturesToLayerWithMap);

const getResolutionForZoom =
    (projectionLike: ProjectionLike) => {
        const view = new View({
            projection: get(projectionLike),
            center: [0, 0],
            rotation: 0,
            zoom: 0,
        });

        return (z: number | undefined, defaultVal: number) =>
            view.getResolutionForZoom(z === undefined ? defaultVal : z);

        // // from https://github.com/openlayers/openlayers/blob/v4.6.4/src/ol/view.js#L845
        // return (z: number | undefined, defaultVal: number) => (
        //     view.constrainResolution(
        //         view.getMaxResolution(), z === undefined ?
        //         defaultVal : z, 0)
        // );
    };



const getResolutionForZoomLMerc = getResolutionForZoom('EPSG:3857');

const extentToLatLon = ([minx, miny, maxx, maxy]: Extent): Extent => {
    const [minxll, minyll] = toLonLat([minx, miny]);
    const [maxxll, maxyll] = toLonLat([maxx, maxy]);
    return [minxll, minyll, maxxll, maxyll];
};

const extentFromLatLon = ([minx, miny, maxx, maxy]: Extent): Extent => {
    const [minxp, minyp] = fromLonLat([minx, miny]);
    const [maxxp, maxyp] = fromLonLat([maxx, maxy]);
    return [minxp, minyp, maxxp, maxyp];
};

type FeatureWindow = [number, number];

export const create =
    (mapName: string, options: IMapOptions) => {

        const baseLayerCollection = new Collection<TileLayer>();
        const baseLayerGroup = new LayerGroup({
            layers: baseLayerCollection,
        });

        const mainLayerCollection = new Collection<VectorLayer>();
        const mainLayerGroup = new LayerGroup({
            layers: mainLayerCollection,
        });

        mainLayerCollection.on('remove',
            (e: any) => {
                logger(`mainLayerCollection::remove`, e);
            });

        const toolsLayerCollection = new Collection<VectorLayer>();
        const toolsLayerGroup = new LayerGroup({
            layers: toolsLayerCollection,
        });


        const isTracking = false;
        const isMeasuring = false;

        const isWorking =
            () => {
                return (isTracking || isMeasuring);
            };

        const loadingMonitor = loadingMon();

        const featureBatchInterval = 50;

        // const loadLayerData =
        //     (vs: source.Vector, fc: FeatureCollection, featureBatchSize = 1000) => {
        //         logger(`loadLayerData ${featureBatchSize} ${fc.features.length}`);
        //         const ts = performance.now();
        //         const lid = vs.get('id');
        //         const featuresRef = fc.features;
        //         const featuresSlice = featuresRef.slice(0, featureBatchSize);
        //         const data: FeatureCollection = Object.assign(
        //             {}, fc, { features: featuresSlice });
        //         const features = formatGeoJSON.readFeatures(data);
        //         vs.addFeatures(features);
        //         vs.forEachFeature((f) => {
        //             f.set('lid', lid, true);
        //             // if (!f.getId()) {
        //             //     f.setId(f.getProperties()['__app_id__']);
        //             // }
        //         });
        //         const timed = performance.now() - ts;
        //         const newBatchSize = timed > 16 ? featureBatchSize - 100 : featureBatchSize + 100;
        //         if (featuresRef.length >= newBatchSize) {
        //             const featuresNext = featuresRef.slice(newBatchSize);
        //             const nextData: FeatureCollection = Object.assign(
        //                 {}, fc, { features: featuresNext });
        //             setTimeout(() => loadLayerData(vs, nextData, newBatchSize), featureBatchInterval);
        //         }
        //     };

        const readFeatures =
            (fc: FeatureCollection, start: number, end: number, lid: unknown) => {
                const result: Feature<Geometry>[] = [];
                for (let i = start; i < end; i += 1) {
                    const f = formatGeoJSON.readFeature(fc.features[i]);
                    f.set('lid', lid);
                    result.push(f);
                }
                return result;
            };


        const loadLayerData =
            (vs: SourceVector<Geometry>, fc: FeatureCollection, [offset, limit]: FeatureWindow) => {
                logger(`loadLayerData ${offset} ${limit} ${fc.features.length}`);
                const lid = vs.get('id');
                const end = Math.min(fc.features.length, offset + limit);
                const ts = performance.now();
                vs.addFeatures(readFeatures(fc, offset, end, lid));
                const timed = performance.now() - ts;

                if (end < fc.features.length) {
                    const newLimit = timed > 16 ? Math.max(limit - 10, 10) : limit + 32;
                    setTimeout(() => loadLayerData(vs, fc, [end, newLimit]), featureBatchInterval);
                }
            };

        const addFeatures =
            (vs: SourceVector<Geometry>, features: GeoFeature[]) => {
                const lid = vs.get('id');
                vs.addFeatures(
                    features
                        .map((f) => {
                            const fo = formatGeoJSON.readFeature(f);
                            fo.set('lid', lid);
                            return fo;
                        })
                        .filter(f => fromNullable(f.getId())
                            .map(fid => vs.getFeatureById(fid) === null)
                            .getOrElse(false)));
            };

        const getLayerData =
            (fetchData: FetchData, vs: SourceVector<Geometry>, vl: VectorLayer, optTitle: Option<MessageRecord>) => {
                const fetcher =
                    (count: number) => {
                        // logger(`getLayerData ${fromRecord(title)} ${count}`);

                        const cleanup =
                            () => {
                                // logger(`getLayerData GiveUp on ${fromRecord(title)}`);
                                optTitle.map(title => loadingMonitor.remove(title));

                            };

                        fetchData().fold(
                            () => cleanup(),
                            opt => opt.foldL(
                                () => {
                                    if (count < 100) {
                                        setTimeout(() => fetcher(count + 1), 1000);
                                    }
                                    else {
                                        cleanup();
                                    }
                                },
                                (data) => {
                                    const complete = () => loadLayerData(vs, data, [0, 300]);
                                    if (vl.getVisible()) {
                                        complete();
                                    }
                                    else {
                                        vl.once('change:visible', complete);
                                    }
                                    optTitle.map(title => loadingMonitor.remove(title));
                                }));
                    };

                fetcher(0);
            };


        const fromBaseLayer =
            (baseLayer: IMapBaseLayer) => {
                const baseLayerTranslated = translateMapBaseLayer(baseLayer);
                const l = new LayerTile({
                    source: new TileWMS({
                        projection: get(baseLayerTranslated.srs),
                        params: {
                            ...baseLayerTranslated.params,
                            // TILED: true, // Breaks some WMS (eg : IGN be)
                        },
                        url: baseLayerTranslated.url,
                    }),
                });
                l.set('id', hashMapBaseLayer(baseLayer));
                return l;
            };

        type UpdateFn = () => void;
        interface Updatable {
            name: string;
            fn: UpdateFn;
        }

        const updateBaseLayer =
            (getBaseLayer: IMapOptions['getBaseLayer']) =>
                () => fromNullable(getBaseLayer())
                    .map((queriedBaseLayer) => {
                        const currentBaseLayer = baseLayerCollection.item(0);
                        const id = hashMapBaseLayer(queriedBaseLayer);
                        if ((!currentBaseLayer)
                            || (currentBaseLayer && (currentBaseLayer.get('id') !== id))) {
                            baseLayerCollection.clear();
                            baseLayerCollection.push(fromBaseLayer(queriedBaseLayer));
                        }
                    });

        const updateLayers =
            (getMapInfo: IMapOptions['getMapInfo']) =>
                () => fromNullable(getMapInfo())
                    .map((mapInfo) => {
                        const ids = mapInfo.layers.map(info => info.id);
                        logger(`updateLayers ${ids}`);
                        mainLayerGroup
                            .getLayers()
                            .forEach((l) => {
                                if (l) {
                                    const lid = <string>(l.get('id'));
                                    if (ids.indexOf(lid) < 0) {
                                        mainLayerGroup.getLayers().remove(l);
                                    }
                                }
                            });
                        mapInfo.layers.forEach((info, z) => {
                            const { id, visible } = info;
                            mainLayerGroup.getLayers()
                                .forEach((l) => {
                                    if (id === <string>(l.get('id'))) {
                                        // logger(`Layer ${id} ${visible} ${z}`);
                                        l.setVisible(visible);
                                        l.setZIndex(z);
                                        l.setMaxResolution(
                                            getResolutionForZoomLMerc(info.minZoom, 0));
                                        l.setMinResolution(
                                            getResolutionForZoomLMerc(info.maxZoom, 30));
                                    }
                                });
                        });
                    });


        const forceRedraw =
            () => {
                mainLayerCollection.forEach((layer) => {
                    layer.changed();
                });
            };

        const viewEquals =
            (z: number, r: number, oc: Option<Coord2D>) =>
                (rz: number, rr: number, orc: Option<Coord2D>) =>
                    scopeOption()
                        .let('c', oc)
                        .let('rc', orc)
                        .map(({ c, rc }) => z === rz && r === rr && c[0] === rc[0] && c[1] === rc[1])
                        .getOrElse(false);

        // concat :: ([a],[a]) -> [a]
        const concat =
            <A>(xs: A[], ys: A[]) =>
                xs.concat(ys);

        const flatten =
            <T>(xs: T[][]) =>
                xs.reduce(concat, []);

        const flattenCoords =
            (g: DirectGeometryObject): GeoPosition[] => {
                switch (g.type) {
                    case 'Point': return [g.coordinates];
                    case 'MultiPoint': return g.coordinates;
                    case 'LineString': return g.coordinates;
                    case 'MultiLineString': return flatten(g.coordinates);
                    case 'Polygon': return flatten(g.coordinates);
                    case 'MultiPolygon': return flatten(flatten(g.coordinates));
                }
            };

        const getExtent =
            (feature: GeoFeature, buf = 0.0002): Extent => {
                const initialExtent: Extent = [
                    Number.MAX_VALUE,
                    Number.MAX_VALUE,
                    Number.MIN_VALUE,
                    Number.MIN_VALUE,
                ];

                if (feature.geometry.type === 'Point') {
                    const [x, y] = feature.geometry.coordinates;
                    return [x - buf, y - buf, x + buf, y + buf];
                }

                return flattenCoords(feature.geometry).reduce<Extent>((acc, c) => {
                    return [
                        Math.min(acc[0], c[0]),
                        Math.min(acc[1], c[1]),
                        Math.max(acc[2], c[0]),
                        Math.max(acc[3], c[1]),
                    ];
                }, initialExtent);
            };

        const updateView =
            (map: Map,
                getView: IMapOptions['getView'],
                setView: IMapOptions['updateView'],
            ) =>
                () => {
                    const { dirty, zoom, rotation, center, feature, extent } = getView();
                    const centerP = fromLonLat(center);
                    const view = map.getView();
                    const eq = viewEquals(zoom, rotation, tryCoord2D(centerP));
                    const size = map.getSize();
                    const mapExtent = () => extentToLatLon(view.calculateExtent(size));

                    switch (dirty) {
                        case 'geo/feature': {
                            fromNullable(feature)
                                .map((feature) => {
                                    const extent = extentFromLatLon(getExtent(feature));
                                    view.fit(extent, {
                                        size,
                                        callback: () => setView({ dirty: 'none', extent }),
                                    });
                                });
                            break;
                        }
                        case 'geo/extent': {
                            fromNullable(extent)
                                .map((extent) => {
                                    const transformed = extentFromLatLon(extent);
                                    view.fit(transformed, {
                                        size: map.getSize(),
                                        callback: () => setView({ dirty: 'none', extent: transformed }),
                                    });
                                });
                            break;
                        }
                        case 'geo': {
                            const z = view.getZoom();
                            if (z !== undefined && !eq(z, view.getRotation(), tryCoord2D(view.getCenter()))) {
                                view.animate({ zoom, rotation, center: centerP },
                                    () => setView({ dirty: 'none', extent: mapExtent() }));
                            }
                            break;
                        }
                        case 'style': {
                            window.setTimeout(() => setView({ dirty: 'none', extent: mapExtent() }), 0);
                            forceRedraw();
                            break;
                        }
                        case 'none': break;
                    }
                };

        const updateSize =
            (map: Map) => {
                let containerWidth = 0;
                let containerHeight = 0;

                const inner =
                    () => {
                        const container = map.getViewport();
                        const rect = container.getBoundingClientRect();
                        if ((rect.width !== containerWidth)
                            || (rect.height !== containerHeight)) {
                            containerHeight = rect.height;
                            containerWidth = rect.width;
                            map.updateSize();
                            onViewChange();
                        }
                    };

                return () => setTimeout(inner, 1);
            };


        const makeControlBox =
            () => {
                const element = document.createElement('div');
                element.setAttribute('class', 'control-box');
                element.appendChild(credit());
                return element;
            };

        const view = new View({
            projection: 'EPSG:3857',
            // center: [4.3623067, 50.8383887],
            center: [485609, 6592756],
            rotation: 0,
            zoom: 30,
        });

        const controlBox = makeControlBox();

        const map = new Map({
            view,
            // renderer: 'canvas',
            layers: [
                baseLayerGroup,
                mainLayerGroup,
                toolsLayerGroup,
            ],
            controls: [
                rotateControl(controlBox),
                zoomControl(controlBox),
                fullscreenControl(controlBox),
                scaleLine({
                    setScaleLine: options.setScaleLine,
                    minWidth: 100,
                }),
            ],
        });



        const updatables: Updatable[] = [
            { name: 'BaseLayer', fn: updateBaseLayer(options.getBaseLayer) },
            { name: 'Layers', fn: updateLayers(options.getMapInfo) },
            { name: 'View', fn: updateView(map, options.getView, options.updateView) },
            { name: 'Size', fn: updateSize(map) },
        ];

        const followers: [string, (v: IViewEvent) => void][] = [];


        fromNullable(options.element)
            .map(e => map.setTarget(e));

        fromNullable(options.setLoading)
            .map(s => loadingMonitor.onUpdate(s));

        const lastKnownViewUpdate = [0, 0, 0];

        const onViewChange = () => {
            if (!isWorking()) {
                const zoom = view.getZoom() ?? 0;
                const center = toLonLat(tryCoord2D(view.getCenter()).getOrElse([0, 0]));
                // quick check
                if (lastKnownViewUpdate[0] !== zoom
                    || lastKnownViewUpdate[1] !== center[0]
                    || lastKnownViewUpdate[2] !== center[1]) {
                    const rotation = view.getRotation();
                    const extent = extentToLatLon(view.calculateExtent(map.getSize()));
                    options.updateView({ dirty: 'none', center, rotation, zoom, extent });
                    followers.forEach(([_name, u]) => u({ dirty: 'geo', center, rotation, zoom, extent, }));
                    lastKnownViewUpdate[0] = zoom;
                    lastKnownViewUpdate[1] = center[0];
                    lastKnownViewUpdate[2] = center[1];
                }
            }
        };

        view.on('change', onViewChange);


        const follow = (name: string, u: (v: IViewEvent) => void) => {
            if (followers.findIndex(f => f[0] === name) < 0) {
                followers.push([name, u]);
            }
        };



        const update =
            () => {
                /*const us = */updatables.map((u) => {
                u.fn();
                return u.name;
            });
                // logger(`updated ${us.join(', ')} @ z${view.getZoom()}`);
            };


        const setTarget =
            (t: HTMLElement | null) => {
                if (t) {
                    map.setTarget(t);
                    t.appendChild(controlBox);
                }
            };

        const andThen = (f: (m: Map) => void) => f(map);


        const selectable =
            (o: SelectOptions, g: InteractionGetter) => {
                const { init, update } = select(o, mainLayerCollection);
                init(map);
                updatables.push({ name: 'Select', fn: () => update(g()) });
            };


        const trackable =
            (o: TrackerOptions, g: InteractionGetter) => {
                const { init, update } = track(o);
                init(map, toolsLayerCollection);
                updatables.push({ name: 'Tracker', fn: () => update(g()) });
            };


        const measurable =
            (o: MeasureOptions, g: InteractionGetter) => {
                const { init, update } = measure(o);
                init(map, toolsLayerCollection);
                updatables.push({ name: 'Measure', fn: () => update(g()) });
            };


        const extractable =
            (o: ExtractOptions, g: InteractionGetter) => {
                const { init, update } = extract(o);
                init(map, mainLayerCollection);
                updatables.push({ name: 'Extract', fn: () => update(g()) });
            };


        const markable =
            (o: MarkOptions, g: InteractionGetter) => {
                const { init, update } = mark(o);
                init(map);
                updatables.push({ name: 'Mark', fn: () => update(g()) });
            };

        const highlightable =
            (fpg: FeaturePathGetter) => {
                const { init, update } = highlight(fpg);
                init(mainLayerCollection, toolsLayerCollection);
                updatables.push({ name: 'Highlight', fn: () => update() });
            };

        const printable =
            <T>(o: PrintOptions<T>, g: InteractionGetter) => {
                const { init, update } = print(o);
                init(map, baseLayerCollection);
                updatables.push({ name: 'Print', fn: () => update(g()) });
            };

        const positionable =
            (o: PositionOptions, g: InteractionGetter) => {
                const { init, update } = position(o);
                init(map);
                updatables.push({ name: 'Position', fn: () => update(g()) });
            };

        const clickable =
            (o: SingleClickOptions, g: InteractionGetter) => {
                const { init, update } = singleclick(o);
                init(map);
                updatables.push({ name: 'SingleClick', fn: () => update(g()) });
            };

        const editable =
            (o: EditOptions, g: InteractionGetter) => {
                const { init, update } = edit(o);
                init(map, toolsLayerCollection, mainLayerCollection);
                updatables.push({ name: 'Edit', fn: () => update(g()) });
            };




        const m = {
            setTarget,
            update,
            follow,
            selectable,
            trackable,
            measurable,
            extractable,
            markable,
            highlightable,
            printable,
            positionable,
            clickable,
            editable,
            // the following attributes are private-ish
            // and mostly a temp hack to get things going
            getLayerData,
            addFeatures,
            loadingMonitor,
            mainLayerGroup,
            andThen,
        };
        addMap(mapName, m);
        return m;
    };

logger('loaded');
