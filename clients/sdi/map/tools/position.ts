import { some } from 'fp-ts/lib/Option';
import Map from 'ol/Map';
import MapBrowserEvent from 'ol/MapBrowserEvent';
import { toLonLat } from 'ol/proj';

import {
    PositionOptions,
    withInteraction,
    InteractionPosition,
} from '..';


export const position =
    ({ setPosition, stopPosition }: PositionOptions) => {
        let isActive = false;

        const update = withInteraction<InteractionPosition>('position',
            () => {
                isActive = true;
            },
            () => {
                isActive = false;
            });

        const init =
            (map: Map) => {
                map.on('pointermove', (event: MapBrowserEvent<UIEvent>) => {
                    if (isActive) {
                        setPosition(toLonLat(event.coordinate));
                    }
                });

                map.on('singleclick', (event: MapBrowserEvent<UIEvent>) => {
                    if (isActive) {
                        stopPosition(some(toLonLat(event.coordinate)));
                    }
                });
            };

        return { init, update };
    };
