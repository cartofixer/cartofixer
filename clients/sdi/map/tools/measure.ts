import Map from 'ol/Map';
import Feature from 'ol/Feature';
import Collection from 'ol/Collection';
import Draw from 'ol/interaction/Draw';
import LayerVector from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import Geometry from 'ol/geom/Geometry';
import { toLonLat } from 'ol/proj';

import {
    MeasureOptions,
    withInteraction,
    InteractionMeasure,
    VectorLayer,
} from '..';
import GeometryType from 'ol/geom/GeometryType';
import LineString from 'ol/geom/LineString';
import Polygon from 'ol/geom/Polygon';
import SourceVector from 'ol/source/Vector';
import { DrawEvent } from 'ol/interaction/Draw';


// measure
const measureHandlers =
    ({ updateMeasureCoordinates, stopMeasuring, source }: MeasureOptions & { source: SourceVector<Geometry> }) => {
        const startMeasureLength = (e: DrawEvent) => {
            const feature: Feature<Geometry> = e.feature;
            const line = <LineString>feature.getGeometry();
            line.on('change', () => {
                updateMeasureCoordinates(line.getCoordinates().map(c => toLonLat(c)));
            });
        };

        const stopMeasureLength = (_e: any) => () => {
            source.clear();
            stopMeasuring();
        };


        const startMeasureArea = (e: DrawEvent) => {
            const feature: Feature<Geometry> = e.feature;
            const polygon = <Polygon>feature.getGeometry();
            polygon.on('change', () => {
                updateMeasureCoordinates(
                    polygon.getLinearRing(0).getCoordinates().map(c => toLonLat(c)));
            });
        };

        const stopMeasureArea = (_e: any) => () => {
            source.clear();
            stopMeasuring();
        };

        return {
            startMeasureLength,
            stopMeasureLength,
            startMeasureArea,
            stopMeasureArea,
        };
    };

export const measure =
    (options: MeasureOptions) => {
        const measureSource = new VectorSource();
        const measureLayer = new LayerVector({
            source: measureSource,
        });
        const measureLength = new Draw({
            type: GeometryType.LINE_STRING,
            source: measureSource,
        });
        const measureArea = new Draw({
            type: GeometryType.POLYGON,
            source: measureSource,
        });

        const {
            startMeasureLength,
            stopMeasureLength,
            startMeasureArea,
            stopMeasureArea,
        } = measureHandlers({ ...options, source: measureSource });

        measureLength.on('drawstart', startMeasureLength);
        measureLength.on('drawend', stopMeasureLength);
        measureArea.on('drawstart', startMeasureArea);
        measureArea.on('drawend', stopMeasureArea);

        const isMeasuring =
            () => measureLength.getActive() || measureArea.getActive();



        const update = withInteraction<InteractionMeasure>('measure',
            ({ state }) => {
                if (!isMeasuring()) {
                    measureSource.clear();
                }
                switch (state.geometryType) {
                    case 'LineString':
                        measureLength.setActive(true);
                        break;
                    case 'Polygon':
                        measureArea.setActive(true);
                        break;
                }

            },
            () => {
                measureSource.clear();
                measureLength.setActive(false);
                measureArea.setActive(false);
            });

        const init =
            (map: Map, layers: Collection<VectorLayer>) => {
                layers.push(measureLayer);
                map.addInteraction(measureLength);
                map.addInteraction(measureArea);
            };

        return { init, update };
    };
