import { makeRecord } from 'sdi/source';
import { fromRecord } from 'sdi/locale';

export const attribution = () => 'cartofixer';

export const credits = () =>
    fromRecord(
        makeRecord(
            'Fond de plan: © OpenStreetMap contributors',
            'Achtergrond: © OpenStreetMap contributors'
        )
    );
