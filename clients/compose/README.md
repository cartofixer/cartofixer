![illustration](../assets/image/studio.png)

This app, called _studio_ in _cartofixer_ interface, provides functionalities to create and publish maps from the available data.

_compose_ brings tools to create simple, categorized and continuous legends, as well as to define more advanced settings such as editing the available information by data layer element.

Published maps are accessible in the _atlas_ application.

## User guide

The complete user guide is available here : https://gitlab.com/cartofixer/cartofixer/-/wikis/home
