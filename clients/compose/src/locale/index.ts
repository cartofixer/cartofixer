import { MessageStore, formatMessage } from 'sdi/locale';


const messages = {
    myMaps: {
        fr: 'Mes cartes',
        nl: 'Mijn kaarten', // nldone
        en: 'My maps',
    },

    infoStudio: {
        fr: `Créer et publier de nouvelles cartes à partir des données disponibles.  
Le studio permet notamment de créer des légendes simples, catégorisées et continues, ainsi que de définir des réglages plus avancés tels que l'édition des informations disponibles par élément de la couche de données. 
Les cartes publiées sont accessibles dans l'application Atlas.`,
        nl: `Creëer en publiceer nieuwe kaarten op basis van beschikbare gegevens.  
In de studio kunt u eenvoudige, gecategoriseerde en doorlopende bijschriften maken, maar ook meer geavanceerde instellingen zoals het bewerken van beschikbare informatie met behulp van datalaagelementen. 
De gepubliceerde kaarten zijn beschikbaar in de Atlas-applicatie.`, // nltocheck
        en: `Create and publish new maps from available data.  
The studio allows you to create simple, categorized and continuous captions, as well as more advanced settings such as editing available information by data layer element. 
The published maps are available in the Atlas application.`,
    },

    createMap: {
        fr: 'Créer une carte',
        nl: 'Maak een kaart', // nltocheck
        en: 'Create a map',
    },

    layerId: {
        fr: 'Identifiant de la ressource',
        nl: 'Identificatie van de bron', // nldone
        en: 'Resource ID',
    },

    publicationStatus: {
        fr: 'État de publication',
        nl: 'Status van publicatie', // nldone
        en: 'Publication state',
    },

    geometryType: {
        fr: 'Geométrie',
        nl: 'Geometrie', // nldone
        en: 'Geometry',
    },

    title: {
        fr: 'Titre',
        nl: 'Titel', // nldone
        en: 'Title',
    },

    temporalReference: {
        fr: 'Référence temporelle',
        nl: 'Tijdelijke referentie', // nldone
        en: 'Temporal reference',
    },

    draft: {
        fr: 'Brouillon',
        nl: 'Ontwerp', // nldone
        en: 'Draft',
    },

    published: {
        fr: 'Publié',
        nl: 'Gepubliceerd', // nldone
        en: 'Published',
    },

    extentBegin: {
        fr: 'Début',
        nl: 'Begin', // nldone
        en: 'Begining',
    },

    extentEnd: {
        fr: 'Fin',
        nl: 'Einde', // nldone
        en: 'End',
    },

    lastModified: {
        fr: 'Dernière mise à jour le ',
        nl: 'Laatste wijziging', // nldone
        en: 'Last update on ',
    },

    responsibleAndContact: {
        fr: 'Organisation responsable',
        nl: 'Verantwoordelijke organisatie', // nldone
        en: 'Responsible organisation',
    },

    selectLayer: {
        fr: 'Sélection de couche',
        nl: 'Selecteer een laag', // nldone
        en: 'Layer selection',
    },

    newMap: {
        fr: 'Nouvelle carte',
        nl: 'Nieuwe kaart', // nldone
        en: 'New map',
    },

    displayLabel: {
        fr: 'Afficher le titre de la colonne',
        nl: 'Geef de naam van de kolom weer', // nldone
        en: 'Display column title',
    },

    displayTableView: {
        fr: 'Table attributaire',
        nl: 'Attributentabel', // nldone
        en: 'Attributes table',
    },

    displayMapView: {
        fr: 'Carte',
        nl: 'Kaart', // nldone
        en: 'Map',
    },

    displayFeatureEdit: {
        fr: 'Mise en forme des données',
        nl: 'gegevensformattering', // nltocheck
        en: 'Data formating',
    },


    featureText: {
        fr: 'Texte',
        nl: 'Tekst', // nldone
        en: 'Text',
    },

    alias: {
        fr: 'Alias',
        nl: 'Alias', // nldone
        en: 'Alias',
    },

    columnPicker: {
        fr: 'Sélecteur de colonne',
        nl: 'Kolomkiezer', // nldone
        en: 'Column selector',
    },

    columnName: {
        fr: 'Nom de colonne',
        nl: 'Kolomnaam', // nldone
        en: 'Column Name',
    },

    widgets: {
        fr: 'Widgets',
        nl: 'Widgets', // nldone
        en: 'Widgets',
    },

    columns: {
        fr: 'Colonnes',
        nl: 'Kolommen', // nldone
        en: 'Column',
    },

    featureTemplateEditor: {
        fr: 'Éditeur de fiche individuelle',
        nl: 'Individuele fiche bewerken', // nldone
        en: 'Feature view editor',
    },

    legendTypeSimple: {
        fr: 'Simple',
        nl: 'Enkel', // nldone
        en: 'Simple',
    },

    legendTypeDiscrete: {
        fr: 'Catégorisée',
        nl: 'Gecategoriseerd', // nldone
        en: 'Categorised',
    },

    legendTypeContinuous: {
        fr: 'Continue',
        nl: 'Doorlopend', // nldone
        en: 'Continuous',
    },

    styleGroupDefaultName: {
        fr: 'Catégorie sans titre',
        nl: 'Categorie zonder titel', // nldone
        en: 'Category without title',
    },

    addTerm: {
        fr: 'Ajouter un terme',
        nl: 'Een term toevoegen', // nldone
        en: 'Add term',
    },

    addThisTermToGroup: {
        fr: 'Ajouter ce terme à la catégorie',
        nl: 'Voeg deze term toe aan de categorie', // nltocheck
        en: 'Add this term to the group',
    },

    lineWidth: {
        fr: 'Épaisseur du filet (px)',
        nl: 'Lijnbreedte (px)', // nldone
        en: 'Stroke width (px)',
    },

    lineColor: {
        fr: 'Couleur du filet',
        nl: 'Lijnkleur', // nldone
        en: 'Stroke color',
    },

    fillColor: {
        fr: 'Couleur de remplissage',
        nl: 'Opvulkleur', // nldone
        en: 'Fill color',
    },

    usePattern: {
        fr: 'Hachures',
        nl: 'Arcering', // nldone
        en: 'Hatches',
    },

    fontColor: {
        fr: 'Couleur du texte',
        nl: 'Letterkleur van de tekst', // nldone
        en: 'Text color',
    },

    fontSize: {
        fr: 'Corps du texte (px)',
        nl: 'Lettergrootte van de tekst (px)', // nldone
        en: 'Text size (px)',
    },

    offsetX: {
        fr: 'Offset X',
        nl: 'Offset X', // nldone
        en: 'Offset X',
    },

    offsetY: {
        fr: 'Offset Y',
        nl: 'Offset Y', // nldone
        en: 'Offset Y',
    },

    labelResolution: {
        fr: 'Resolution (zoom)',
        nl: 'Resolution (zoom)', // nltocheck
        en: 'Resolution (zoom)',
    },

    propNameForLabel: {
        fr: 'Colonne à utiliser pour le label.',
        nl: 'Kolom als label te gebruiken.', // nltocheck
        en: 'Column to be used as a label.',
    },

    helptextForLabel: {
        fr: 'Si aucune colonne n\'est définie, aucun label ne sera affiché. ',
        nl: 'Als er geen kolom is gedefinieerd, wordt er geen label weergegeven.', // nltocheck
        en: 'If no column is defined, no label will be displayed.',
    },

    size: {
        fr: 'Taille (px)',
        nl: 'Afmetingen (px)', // nldone
        en: 'Size (px)',
    },

    pointColor: {
        fr: 'Couleur du pictogramme',
        nl: 'Kleur van het pictogram', // nldone
        en: 'Pictogram color',
    },

    labelPostion: {
        fr: 'Position du label',
        nl: 'Plaats van het label', // nldone
        en: 'Label position',
    },

    legendBuilder: {
        fr: 'Éditeur de légende',
        nl: 'Legende editor', // nldone
        en: 'Legend editor',
    },

    layerLegendDefaultLabel: {
        fr: 'Étiquette de légende',
        nl: 'Legendelabel', // nldone
        en: 'Legend label',
    },

    minZoom: {
        fr: 'Zoom minimal',
        nl: 'Minimale zoom', // nldone
        en: 'Zoom min',
    },

    maxZoom: {
        fr: 'Zoom maximal',
        nl: 'Maximale zoom', // nldone
        en: 'Zoom max',
    },

    minZoomShort: {
        fr: 'z. min.',
        nl: 'min. z.', // nltocheck
        en: 'z. min.',
    },

    maxZoomShort: {
        fr: 'z. max.',
        nl: 'max. z.', // nltocheck
        en: 'z. max.',
    },

    legendLabelHeader: {
        fr: 'Réglages des labels',
        nl: 'Instellingen van de labels', // nldone
        en: 'Label settings',
    },

    legendTypeSelect: {
        fr: 'Type de légende',
        nl: 'Type legende', // nldone
        en: 'Legend type',
    },

    style: {
        fr: 'Réglage des styles',
        nl: 'Instellingen van stijlen', // nldone
        en: 'Styles settings',
    },

    legendItems: {
        fr: 'Classification',
        nl: 'Classificatie', // nldone
        en: 'Classification',
    },

    highValue: {
        fr: 'Valeur haute (exclue)',
        nl: 'Bovengrens (uitgesloten)', // nldone
        en: 'High value (excluded)',
    },

    lowValue: {
        fr: 'Valeur basse (incluse)',
        nl: 'Ondergrens (ingesloten)', // nldone
        en: 'Low value (included)',
    },

    autoClass: {
        fr: 'Création automatique de classes',
        nl: 'Automatische creatie van klassen', // nldone
        en: 'Autoamtic classes generation',
    },

    columnPickerMessage: {
        fr: 'Sélectionnez une colonne pour construire la légende.',
        nl: 'Selecteer een kolom om de legende te maken.', // nldone
        en: 'Select a column to build a legend',
    },

    all: {
        fr: 'Tout',
        nl: 'Alles', // nldone
        en: 'All',
    },

    skipFirstLine: {
        fr: 'Ne pas prendre en compte la première ligne',
        nl: 'Houdt geen rekening met de eerste lijn', // nldone
        en: 'Do not take the first line',
    },

    separatedBy: {
        fr: 'Séparateur',
        nl: 'Scheidingsteken', // nldone
        en: 'Separator',
    },

    textDelimiter: {
        fr: 'Délimiteur de texte',
        nl: 'Begrenzer van tekst',
        en: 'Text delimiter',
    },

    comma: {
        fr: 'Virgule',
        nl: 'Komma', // nldone
        en: 'Comma',
    },

    semicolon: {
        fr: 'Point virgule',
        nl: 'Puntkomma', // nldone
        en: 'Semicolon',
    },

    tab: {
        fr: 'Tabulation',
        nl: 'Tab', // nldone
        en: 'Tab',
    },

    space: {
        fr: 'Espace',
        nl: 'Spatie', // nldone
        en: 'Space',
    },

    quotationMark: {
        fr: 'Guillemet',
        nl: 'Aanhalingsteken', // nldone
        en: 'Quotation mark',
    },

    apostrophe: {
        fr: 'Apostrophe',
        nl: 'Apostrof', // nldone
        en: 'Apostrophe',
    },


    setLatitude: {
        fr: 'Latitude (Y)',
        nl: 'Breedtegraad (Y)', // nldone
        en: 'Latitude (Y)',
    },

    setLongitude: {
        fr: 'Longitude (X)',
        nl: 'Lengtegraad (X)', // nldone
        en: 'Longitude (X)',
    },

    uploadShpInfos: {
        fr: 'Veuillez séléctionner les quatre fichiers requis composant le shapefile.',
        nl: 'Selecteer de vier nodige bestanddelen van de shapefile.', // nldone
        en: 'Please select the four requiered files composing the shapefile.',
    },

    upload: {
        fr: 'Upload',
        nl: 'Uploaden', // nldone
        en: 'Uppload',
    },

    uploadDatas: {
        fr: 'Upload de données',
        nl: 'Uploaden van de gegevens', // nldone
        en: 'Datas upload',
    },

    relatedMapsLabel: {
        fr: 'Cartes associées',
        nl: 'Gerelateerde kaarten',  // nltocheck
        en: 'Related maps',
    },

    'compose:addLinkedMap': {
        fr: 'Associer une carte',
        nl: 'Een kaart koppelen', // nltocheck
        en: 'Link a map',
    },

    'compose:publishingTools': {
        fr: 'Outils de publication',
        nl: 'Publiceertools', // nldone
        en: 'Publishing tools',
    },

    'compose:linkedMaps': {
        fr: 'Cartes associées',
        nl: 'Gekoppelde kaarten', // nltocheck
        en: 'Related maps',
    },

    'compose:unlinkedMaps': {
        fr: 'Cartes disponibles',
        nl: 'Beschikbare kaarten', // nldone
        en: 'Maps available ',
    },

    selectLinkedMap: {
        fr: 'Sélectionner des cartes à lier',
        nl: 'Selecteer kaarten om naar te linken', // nldone
        en: 'Select maps to link to',
    },


    changeBackgroundMap: {
        fr: 'Changer de fond de carte',
        nl: 'Wijzig de achtergrond', // nldone
        en: 'Change background map',
    },

    editLegend: {
        fr: 'Éditer',
        nl: 'Bewerken', // nldone
        en: 'Edit',
    },

    mapLegend: {
        fr: 'Légende',
        nl: 'Legende', // nldone
        en: 'Legend',
    },

    attachmentName: {
        fr: 'Nom du lien',
        nl: 'Naam van de link', // nldone
        en: 'Link name',
    },

    attachmentUrl: {
        fr: 'URL du lien',
        nl: 'URL van de link', // nldone
        en: 'Link URL',
    },

    links: {
        fr: 'Liens externes',
        nl: 'Links',
        en: 'Links',
    },

    'compose:externalLinkInfo': {
        fr: 'Créer des liens vers des pages web et documents externes à la plateforme.',
        nl: 'Koppelingen naar webpagina\'s en documenten buiten het platform maken.', // nltocheck
        en: 'Create links to web pages and documents external to the platform.',
    },

    'compose:addLayerInfo': {
        fr: 'Ajouter des couches de données et constituer la légende de la carte.',
        nl: 'Voeg gegevenslagen toe en maak de kaartlegende aan.', // nltocheck
        en: 'Add data layers and create the map legend.',
    },

    'compose:helptext:mapPublished': {
        fr: 'La carte est actuellement publiée.',
        nl: 'De kaart wordt momenteel gepubliceerd.', // nldone
        en: 'The map is currently being published.',
    },

    'compose:helptext:mapUnpublished': {
        fr: 'La carte n\'est actuellement visible que par vous.',
        nl: 'De kaart is momenteel alleen voor u zichtbaar.', // nldone
        en: 'The map is currently only visible to you.',
    },

    preview: {
        fr: 'Aperçu',
        nl: 'Voorvertoning', // nldone
        en: 'Preview',
    },

    emptyMapTitle: {
        fr: 'Adapter le titre de la carte',
        nl: 'Pas de kaarttitel aan', // nldone
        en: 'Adjust map title',
    },

    emptyMapDescription: {
        fr: 'Description de la carte',
        nl: 'Kaartbeschrijving', // nldone
        en: 'Map description',
    },


    imageGeneratingPreview: {
        fr: 'Création de la prévisualisation',
        nl: 'Voorvertoning genereren', // nldone
        en: 'Generating preview',
    },

    imagePreview: {
        fr: 'Aperçu',
        nl: 'Voorvertoning', // nldone
        en: 'Preview',
    },

    imageUploading: {
        fr: 'Uploading',
        nl: 'Afbeelding wordt geupload', // nldone
        en: 'Uploading',
    },

    mapInfoAddIllustration: {
        fr: 'Sélectionnez une image',
        nl: 'Selecteer een afbeelding', // nldone
        en: 'Select an image',
    },

    mapInfoChangeIllustration: {
        fr: 'Sélectionnez une autre image',
        nl: 'Selecteer een andere afbeelding', // nldone
        en: 'Select another image',
    },

    studio: {
        fr: 'Studio',
        nl: 'Studio', // nldone
        en: 'Studio',
    },

    loadingData: {
        fr: 'Chargement des données',
        nl: 'Data worden geladen', // nldone
        en: 'Loading datas',
    },

    attachmentAdd: {
        fr: 'Ajouter un lien externe',
        nl: 'Een externe link toevoegen', // nltocheck
        en: 'Add an external link',
    },

    remove: {
        fr: 'Supprimer',
        nl: 'Verwijderen', // nldone
        en: 'Remove',
    },

    removeGroup: {
        fr: 'Supprimer la catégorie',
        nl: 'De categorie verwijderen', // nldone
        en: 'Remove group',
    },

    moveLayerUp: {
        fr: 'Remonter',
        nl: 'Naar boven brengen', // nldone
        en: 'Move up',
    },

    moveLayerDown: {
        fr: 'Decendre',
        nl: 'Naar beneden brengen', // nldone
        en: 'Move down',
    },

    'compose:removeMap': {
        fr: 'Supprimer',
        nl: 'Verwijder', // nldone
        en: 'Delete',
    },

    styleGroupAdd: {
        fr: 'Ajouter une catégorie',
        nl: 'Categorie toevoegen', // nldone
        en: 'Add a category',
    },

    addInterval: {
        fr: 'Ajouter un intervale',
        nl: 'Interval toevoegen', // nldone
        en: 'Add an interval',
    },

    addLayer: {
        fr: 'Ajouter une couche de données',
        nl: 'Een datalaag toevoegen', // nltocheck
        en: 'Add a data layer',
    },

    switchLabel: {
        fr: 'Label',
        nl: 'Label', // nldone
        en: 'Label',
    },

    switchMarker: {
        fr: 'Icône',
        nl: 'Icoon', // nldone
        en: 'Icon',
    },

    add: {
        fr: 'Ajouter',
        nl: 'Toevoegen', // nldone
        en: 'Add',
    },

    close: {
        fr: 'Fermer',
        nl: 'Sluiten', // nldone
        en: 'Close',
    },

    'compose:publishMap': {
        fr: 'Publier la carte',
        nl: 'De kaart publiceren', // nldone
        en: 'Publish map',
    },

    'compose:unpublishMap': {
        fr: 'Dépublier la carte',
        nl: 'De kaart niet langer publiceren', // nldone
        en: 'Unpublish map',
    },

    cancel: {
        fr: 'Annuler',
        nl: 'Annuleren', // nldone
        en: 'Cancel',
    },

    validate: {
        fr: 'Valider',
        nl: 'Bevestigen', // nldone
        en: 'Validate',
    },

    addToLegend: {
        fr: 'Ajouter à la légende',
        nl: 'Toevoegen aan de legende', // nldone
        en: 'Add to legend',
    },

    textFormat: {
        fr: 'Format du text',
        nl: 'Tekst opmaak', // nldone
        en: 'Text format',
    },

    textStyle: {
        fr: 'Style du texte',
        nl: 'Tekst stijl', // nldone
        en: 'Text style',
    },

    piechartScale: {
        fr: 'Échelle',
        nl: 'Schaal', // nldone
        en: 'Scale',
    },

    piechartRadius: {
        fr: 'Rayon',
        nl: 'Straal', // nldone
        en: 'Radius',
    },

    timeserieTemplateURL: {
        fr: 'Un gabarit d’URL pointant vers une ressource produisant une série temporelle de la forme "http://exemple.com/ts/\\{columnName\\}.json".',
        nl: 'Een template voor de URL die verwijst naar de gegevensbron voor het maken van een tijdreeks. Gebruik de volgende vorm: "http://voorbeeld.com/ts/\\{kolomNaam\\}.json".',
        en: 'URL template pointing to a timeserie ressource "http://exemple.com/ts/\\{columnName\\}.json".',
    },

    timeserieReference: {
        fr: 'Valeur de référence',
        nl: 'Referentiewaarde', // nldone
        en: 'Reference value',
    },

    infoChoice: {
        fr: 'Choix des informations',
        nl: 'Keuze van informatie', // nldone
        en: 'Chose informations',
    },
    infoReorder: {
        fr: 'Organisation des informations',
        nl: 'Ordening van informatie', // nldone
        en: 'Ordering informations',
    },

    dataType: {
        fr: 'Type de données',
        nl: 'Gegevenstype', // nldone
        en: 'Datas type',
    },

    editMap: {
        fr: 'Éditer',
        nl: 'Bewerken', // nldone
        en: 'Edit',
    },

    resetLegend: {
        fr: 'Réinitialiser la légende',
        nl: 'Reset legende', // nldone
        en: 'Reset legend',
    },

    makeVisible: {
        fr: 'Rendre visible par défaut',
        nl: '?? Standaard zichtbaarheid',
        en: 'Make visible',
    },

    makeUnvisible: {
        fr: 'Rendre invisible par défaut',
        nl: '?? Standaard zichtbaarheid',
        en: 'Make unvisible',
    },

    rmvMsgGeneric: {
        fr: 'Souhaitez-vous supprimer l\'élément ?',
        nl: 'Wilt u het element verwijderen?', // nldone
        en: 'Would you like to delete the element?',
    },

    rmvMsgResetLegend: {
        fr: 'Souhaitez-vous ré-initialiser la légende ? Cette action supprimera tous les styles créés pour cette couche.',
        nl: 'Wilt u de legende resetten? Deze actie verwijdert alle stijlen die voor deze laag zijn gemaakt.', // nltocheck
        en: 'Would you like to reset the legend?  This action will delete all styles created for this layer.',
    },

    rmvMsgDeletLegendItem: {
        fr: 'Souhaitez-vous supprimer cette couche de la légende ?',
        nl: 'Wilt u deze laag uit de legende verwijderen ?', // nldone
        en: 'Would you like to remove this layer from the legend ?',
    },

    rmvMsgRemoveImage: {
        fr: 'Souhaitez-vous supprimer l\'image ?',
        nl: 'Wilt u de afbeelding verwijderen?', // nldone
        en: 'Would you like to delete the image?',
    },

    rmvMsgRemoveMap: {
        fr: 'Souhaitez-vous supprimer la carte ?',
        nl: 'Wilt u de kaart verwijderen ?', // nldone
        en: 'Would you like to delete the map ?',
    },

    rmvMsgRemoveAttachment: {
        fr: 'Souhaitez-vous supprimer le lien ?',
        nl: 'Wilt u de link verwijderen ?', // nldone
        en: 'Would you like to delete the link ?',
    },

    'compose:infoRelatedMap': {
        fr: 'Lier des cartes entre elles afin de constituer un atlas thématique.',
        nl: 'Koppel kaarten aan elkaar om een thematische atlas te vormen.', // nltocheck
        en: 'Link maps to create a thematic atlas.',
    },


    backToMap: {
        fr: 'Retour à la carte',
        nl: 'Terug naar de kaart', // nltocheck
        en: 'Back to map',
    },


    exportable: {
        fr: `Afficher l'option d'export`,
        nl: 'De exportoptie weergeven',
        en: 'Display export option',
    },

    mapZoomLevel: {
        fr: 'Niveau de zoom',
        nl: 'Zoomniveau',
        en: 'Zoom level',
    },

    'helptext:displayExport': {
        fr: `Lorsque l'option est cochée, un bouton sera disponible dans le tableau de données de la couche afin de faciliter le téléchargement des informations sélectionnées.`,
        nl: `Wanneer de optie is aangevinkt, zal er een knop beschikbaar zijn in de laagdatatabel om het downloaden van de geselecteerde informatie te vergemakkelijken.`, // nltocheck
        en: `When the option is checked, a button will be available in the layer data table to make it easier to download the selected information.`,
    },

    'helptext:featureView': {
        fr: `L'ensemble des information est affiché par défaut, avec une mise en forme simple. 
        Pour ajuster les informations à afficher ainsi que leur mise en forme, veuillez les sélectionner dans la colonne de gauche.`,
        nl: `Alle informatie wordt standaard weergegeven, met een eenvoudige opmaak. 
        Om de weer te geven informatie en de opmaak aan te passen, selecteert u deze in de linkerkolom.`, // nltocheck
        en: `All the information is displayed by default, with a simple formatting. 
        To adjust the information to be displayed and its formatting, please select it in the left column.`,
    },

    'helptext:widgetFeature': {
        fr: `Les widgets sont des outils spécialisés pour manipuler les données.`,
        nl: `Widgets zijn gespecialiseerde hulpmiddelen voor het manipuleren van gegevens..`, // nltocheck
        en: `Widgets are specialized tools for manipulating data.`,
    },

    'tooltip:zoomMin': {
        fr: `Valeur de zoom (0 à 30) en dessous de laquelle la couche sera cachée.`,
        nl: `Zoomwaarde (0 tot 30) waaronder de laag wordt verborgen.`, // nltocheck
        en: `Zoom value (0 to 30) below which the layer will be hidden.`,
    },

    'tooltip:zoomMax': {
        fr: `Valeur de zoom (0 à 30) au dessus de laquelle la couche sera cachée.`,
        nl: `Zoomwaarde (0 tot 30) waarboven de laag wordt verborgen.`, // nltocheck
        en: `Zoom value (0 to 30) above which the layer will be hidden.`,
    },

    setExtent: {
        fr: `emprise`,
        nl: `---`, // nltocheck
        en: `set extent`,
    },

};

type MDB = typeof messages;
export type ComposeMessageKey = keyof MDB;


declare module 'sdi/locale' {
    export interface MessageStore {
        compose(k: ComposeMessageKey): Translated;
    }
}


MessageStore.prototype.compose = function (k: ComposeMessageKey) {
    return this.getEdited('compose', k, () => formatMessage(messages[k]));
};
