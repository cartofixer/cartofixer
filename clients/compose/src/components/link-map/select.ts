
import { fromPredicate } from 'fp-ts/lib/Either';
import { compose } from 'fp-ts/lib/function';

import { DIV, H1, IMG, P, H3, H2 } from 'sdi/components/elements';
import tr, { fromRecord } from 'sdi/locale';
import { IMapInfo } from 'sdi/source';

import { setLayout } from 'compose/src/events/app';
import { AppLayout } from 'compose/src/shape/types';
import { getLinkedMaps, getUnLinkedMaps } from 'compose/src/queries/map-info';
import { makeLabelAndIcon } from '../button';
import { addLinkMap, removeLinkMap } from 'compose/src/events/map-info';
import { fromNullable } from 'fp-ts/lib/Option';

const unlinkButton = makeLabelAndIcon('remove', 2, 'chain-broken', () => tr.compose('remove'));
const linkButton = makeLabelAndIcon('add', 2, 'link', () => tr.compose('add'));
const backButton = makeLabelAndIcon('navigate', 1, 'times-circle-o', () => tr.compose('close'));


const mapTitle =
    (map: IMapInfo) => H3({ className: 'map-title' }, fromRecord(map.title));


const isGreaterThan100 = fromPredicate<string, string>(rec => rec.length >= 100, rec => rec);

const trimDescription =
    (record: string) =>
        isGreaterThan100(record).fold(
            rec => rec,
            rec => `${rec.substr(0, 100)}...`,
        );

const mapDescription =
    compose((s: string) => P({}, s), trimDescription, fromRecord);



const img =
    (map: IMapInfo) =>
        fromNullable(map.imageUrl).map(
            src =>  IMG({ src }));


const unlinkAction =
    (minfo: IMapInfo) =>
        DIV({ className: 'link-action' },
            unlinkButton(() => removeLinkMap(minfo.id)));

const linkAction =
    (minfo: IMapInfo) =>
        DIV({ className: 'link-action' },
            linkButton(() => addLinkMap(minfo.id)));


const mapSummary =
    (minfo: IMapInfo) =>
        DIV({ className: 'map-summary' },
            DIV({ className: 'map-tile-img' },
                img(minfo)),
            DIV({ className: 'map-infos' },
                mapTitle(minfo),
                mapDescription(minfo)));


const renderLinkedMap =
    (minfo: IMapInfo) =>
        DIV({ className: 'linked-map' },
            mapSummary(minfo),
            unlinkAction(minfo));

const renderUnlinkedMap =
    (minfo: IMapInfo) =>
        DIV({ className: 'un-linked-map' },
            mapSummary(minfo),
            linkAction(minfo));


const renderLinked =
    () =>
        DIV({ className: 'linked-map-list' },
            H2({}, tr.compose('compose:linkedMaps')),
            DIV({ className: 'map-list-wrapper' },
                ...(getLinkedMaps('forward').map(renderLinkedMap))),
        );

const renderUnlinked =
    () =>
        DIV({ className: 'un-linked-map-list' },
            H2({}, tr.compose('compose:unlinkedMaps')),
            DIV({ className: 'map-list-wrapper' },
                ...(getUnLinkedMaps().map(renderUnlinkedMap))),
        );

export const render =
    () => (
        DIV({ className: 'select-related-maps-wrapper' },
            H1({}, tr.compose('selectLinkedMap')),
            backButton(() => setLayout(AppLayout.MapAndInfo)),
            DIV({ className: 'map-selector-wrapper' },
                renderUnlinked(),
                renderLinked(),
            ))
    );
