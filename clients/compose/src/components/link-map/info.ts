import { DIV, H2 } from 'sdi/components/elements';
import tr, { fromRecord } from 'sdi/locale';

import { setLayout } from 'compose/src/events/app';
import { AppLayout } from 'compose/src/shape/types';
import { getLinkedMaps } from 'compose/src/queries/map-info';
import { makeIcon } from '../button';
import { divTooltipTopLeft } from 'sdi/components/tooltip';

const addButton = makeIcon('add', 1, 'map');


const renderLinks =
    () =>
        getLinkedMaps('forward')
            .map(link =>
                DIV({ className: 'link' }, fromRecord(link.title)));



const renderAddButton = () => (
    divTooltipTopLeft(
        tr.compose('compose:addLinkedMap'),
        {},
        addButton(() => setLayout(AppLayout.MapLink)),
    )
);

export const render =
    () => (
        DIV({ className: 'map-related-maps' },
            H2({}, tr.compose('relatedMapsLabel')),
            DIV({ className: 'description-wrapper' },
                DIV({ className: 'helptext' }, tr.compose('compose:infoRelatedMap')),
                renderAddButton(),
            ),
            DIV({ className: 'related-maps' }, ...renderLinks()),
        )
    );


