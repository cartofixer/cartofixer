/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';

import { A, DIV, H2, SPAN } from 'sdi/components/elements';
import tr, { fromRecord } from 'sdi/locale';
import { Attachment, IMapInfo } from 'sdi/source';
import { addAttachment, setAttachmentName, removeAttachment, setAttachmentUrl } from '../../events/attachments';


import editable from '../editable';
import { makeRemove, makeIcon } from '../button';
import { getAttachment, getAttachmentForm } from '../../queries/attachments';
import { fromPredicate } from 'fp-ts/lib/Option';
import { divTooltipTopLeft } from 'sdi/components/tooltip';


const logger = debug('sdi:map-info/attachments');
const nonEmptyString = fromPredicate<string>(s => s.length > 0);

const addButton = makeIcon('add', 1, 'link');

const renderAttachmentEditableName =
    (props: React.AllHTMLAttributes<HTMLElement> & React.Attributes, name: string, a: Attachment) => {
        return DIV({ className: 'map-file-label' },
            SPAN({ className: 'file-label' },
                A({
                    href: fromRecord(a.url),
                    ...props,
                }, nonEmptyString(name).fold(
                    tr.compose('attachmentName'),
                    n => n,
                ))));
    };

const renderAttachmentEditableUrl =
    (props: React.AllHTMLAttributes<HTMLElement> & React.Attributes, url: string, _a: Attachment) => {
        return DIV({ className: 'map-file-url' },
            SPAN({ ...props, className: 'file-url' },
                nonEmptyString(url).fold(
                    tr.compose('attachmentUrl'),
                    u => u,
                )));
    };



const renderAttachmentName =
    (a: Attachment) =>
        (props: React.ClassAttributes<Element>) =>
            getAttachmentForm(a.id)
                .map(f => renderAttachmentEditableName(props, f.name, a))
                .getOrElse(DIV({}));

const renderAttachmentUrl =
    (a: Attachment) =>
        (props: React.ClassAttributes<Element>) =>
            getAttachmentForm(a.id)
                .map(f => renderAttachmentEditableUrl(props, f.url, a))
                .getOrElse(DIV({}));


const renderAddButton = () => (
    divTooltipTopLeft(
        tr.compose('attachmentAdd'),
        {},
        addButton(() => addAttachment()),
    )
);


const attachments =
    (mapInfo: IMapInfo) =>
        mapInfo.attachments.map(
            k => getAttachment(k)
                .map(
                    a => DIV({ className: 'map-file' },
                        editable(`ata_name_${k}`,
                            () => a.name,
                            n => setAttachmentName(a.id, n),
                            renderAttachmentName(a))(),
                        editable(`ata_url_${k}`,
                            () => a.url,
                            n => setAttachmentUrl(a.id, n),
                            renderAttachmentUrl(a))(),
                        makeRemove(`renderAttachmentEditable-${a.id}`, 3, () => tr.compose('remove'), () => tr.compose('rmvMsgRemoveAttachment'))
                            (() => removeAttachment(a.id)),
                    )));

const render =
    (mapInfo: IMapInfo) => (
        DIV({ className: 'map-attached-files' },
            H2({}, tr.compose('links')),
            DIV({ className: 'description-wrapper' },
                DIV({ className: 'helptext' }, tr.compose('compose:externalLinkInfo')),
                renderAddButton(),
            ),
            ...attachments(mapInfo),
        ));


export default render;

logger('loaded');
