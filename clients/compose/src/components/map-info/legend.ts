/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
import { fromNullable } from 'fp-ts/lib/Option';

import { ILayerInfo, getMessageRecord, Inspire, IMapInfo, makeRecord, MessageRecord } from 'sdi/source';
import tr, { fromRecord } from 'sdi/locale';
import { DIV, H2, SPAN } from 'sdi/components/elements';
import { divTooltipTop, divTooltipTopRight, divTooltipTopLeft } from 'sdi/components/tooltip';


import legendPoint from './legend-point';
import legendLinestring from './legend-linestring';
import legendPolygon from './legend-polygon';
import { getLayerData, getSynteticLayerInfoOption, getCurrentLayerId } from '../../queries/app';
import { resetLayerTable } from '../../events/table';
import legendEvents from '../../events/legend-editor';
import { AppLayout } from '../../shape/types';
import { makeIcon, makeRemoveIcon } from '../button';
import { getDatasetMetadata } from '../../queries/metadata';
import { resetFeatureConfigEditor } from 'compose/src/events/feature-config';
import editable from '../editable';
import { inputNumber } from 'sdi/components/input';
import { IconName } from 'sdi/components/button/names';
import mapQueries from '../../queries/map';
import {
    setLayerVisibility,
    setCurrentLayerId,
    setLayout,
    resetLegendEditor
} from '../../events/app';

const addButton = makeIcon('add', 1, 'table');
const upButton = makeIcon('move-up', 3, 'arrow-up');
const downButton = makeIcon('move-down', 3, 'arrow-down');
const featureEditButton = makeIcon('edit', 3, 'cog');
// const editButton = makeIcon('edit', 3, 'pencil');
// const resetLegendButton = makeRemoveIcon('reset', 3, 'refresh', () => tr.compose('rmvMsgResetLegend'));

const logger = debug('sdi:info/legend');


const renderLayerTitle =
    (info: ILayerInfo) => {
        const md = getDatasetMetadata(info.metadataId).fold(
            null,
            md => md);
        const label = md === null ? '' : fromRecord(getMessageRecord(md.resourceTitle));

        const wrap = wrapItem(md, (): boolean => {
            const zoom = mapQueries.getView().zoom;
            const minZoom = fromNullable(info.minZoom).getOrElse(0);
            const maxZoom = fromNullable(info.maxZoom).getOrElse(30);

            return info.visible && zoom >= minZoom && zoom <= maxZoom;
        });

        return (
            DIV({
                className: 'table-name',
            },
                SPAN({}, ...wrap(label))
            )
        );
    };


const wrapItem =
    (md: Inspire | null, visible: () => boolean) =>
        (...nodes: React.ReactNode[]) =>
            fromNullable(md).fold(nodes,
                md => getLayerData(md.uniqueResourceIdentifier).fold(
                    err => [SPAN({
                        className: 'load-error',
                        title: err,
                    }, ...nodes)],
                    o => o.fold(nodes.concat(SPAN({
                        className: visible() ? 'loader-spinner' : '',
                    })), () => nodes)));


const renderLegendItem =
    (layerInfo: ILayerInfo): React.ReactNode[] => {
        switch (layerInfo.style.kind) {
            case 'polygon-continuous':
            case 'polygon-discrete':
            case 'polygon-simple':
                return legendPolygon(layerInfo.style, layerInfo);
            case 'point-discrete':
            case 'point-simple':
            case 'point-continuous':
                return legendPoint(layerInfo.style, layerInfo);
            case 'line-simple':
            case 'line-discrete':
            case 'line-continuous':
                return legendLinestring(layerInfo.style, layerInfo);
            default:
                throw (new Error('UnknownStyleKind'));
        }
    };


const renderVisible =
    (info: ILayerInfo) => {
        const isVisible = info.visible;
        const icon: IconName = isVisible ? 'eye' : 'eye-slash';
        const toolTipMessage = isVisible ? tr.compose('makeUnvisible') : tr.compose('makeVisible');
        const buttonFn = makeIcon('view', 3, icon);
        const button = buttonFn(() => setLayerVisibility(info.id, !isVisible));

        return divTooltipTopRight(toolTipMessage, {}, button);
    };



const handleFeatureEditButton =
    (info: ILayerInfo) => () => {
        setCurrentLayerId(info.id);
        resetFeatureConfigEditor();
        setLayout(AppLayout.FeatureConfig);
    };

const renderFeatureEditButton =
    (info: ILayerInfo) =>
        divTooltipTop(
            tr.compose('displayFeatureEdit'),
            {},
            featureEditButton(handleFeatureEditButton(info)),
        );


const handleResetButton =
    (info: ILayerInfo) => () => {
        setCurrentLayerId(info.id);
        resetLegendEditor();
        legendEvents.resetLegend();
    };


const renderResetLegendButton =
    (info: ILayerInfo) => {
        const button = makeRemoveIcon(
            `reset-layer-${info.id}`, 3, 'refresh',
            () => tr.compose('rmvMsgResetLegend'));
        return divTooltipTop(
            tr.compose('resetLegend'),
            {},
            button(handleResetButton(info)),
        );
    };


const inputLabel =
    (info: ILayerInfo) => {
        const getLabel =
            () =>
                info.legend !== null ? info.legend : makeRecord();
        const setLabel =
            (r: MessageRecord) =>
                legendEvents.setLegendLabel(info.id, r);

        const input = editable(
            `layer_legend_label_${info.id}`, getLabel, setLabel,
            (props, label: string) => {
                const isEmpty = label.trim().length === 0;
                const text = isEmpty ? tr.compose('layerLegendDefaultLabel') : label;
                return DIV(props, text);
            });

        return input();
    };


const renderZoomRange =
    (lid: string) => {
        const g = () => getSynteticLayerInfoOption(lid).map(s => s.info);

        const getMin =
            () => g().fold(0, i => i.minZoom || 0);

        const getMax =
            () => g().fold(0, i => i.maxZoom || 30);

        const setMin =
            (n: number) => {
                legendEvents.setZoomRange(lid, n, getMax());
            };

        const setMax =
            (n: number) => {
                legendEvents.setZoomRange(lid, getMin(), n);
            };

        const minInput = inputNumber(
            getMin, setMin, { key: `min-zoom-${lid}` });

        const maxInput = inputNumber(
            getMax, setMax, { key: `max-zoom-${lid}` });

        return (
            DIV({ className: 'zoom-range' },
                divTooltipTop(
                    tr.compose('tooltip:zoomMin'),
                    {className: 'zoom-range-input-box'},
                    DIV({ className: 'label' },tr.compose('minZoomShort')),
                    minInput,
                ),
                divTooltipTop(
                    tr.compose('tooltip:zoomMax'),
                    {className: 'zoom-range-input-box'},
                    DIV({ className: 'label' },tr.compose('maxZoomShort')),
                    maxInput,
                ),
        ));


    };

// {
//     const { info } = getSynteticLayerInfoOption(lid);
//     if (info) {
//         const getMin =
//             () => getInfo(lid).fold(0, i => i.minZoom || 0);
//         const getMax =
//             () => getInfo(lid).fold(0, i => i.maxZoom || 30);
//         const setMin =
//             (n: number) => {
//                 legendEvents.setZoomRange(lid, n, getMax());
//             };
//         const setMax =
//             (n: number) => {
//                 legendEvents.setZoomRange(lid, getMin(), n);
//             };
//         const minInput = inputNumber(
//             getMin, setMin, { key: `min-zoom-${lid}` });
//         const maxInput = inputNumber(
//             getMax, setMax, { key: `max-zoom-${lid}` });

//         return (
//             DIV({ className: 'zoom-range' },
//                 DIV({ className: 'zoom-range-input-box' },
//                     DIV({ className: 'label' }, tr.compose('minZoomShort')),
//                     minInput),
//                 DIV({ className: 'zoom-range-input-box' },
//                     DIV({ className: 'label' }, tr.compose('maxZoomShort')),
//                     maxInput))
//         );
//     }
//     return DIV();
// };



const renderDeleteButton =
    (info: ILayerInfo) => {
        const removeButton = makeRemoveIcon(`legend::renderDeleteButton-${info.id}`, 3, 'trash', () => tr.compose('rmvMsgDeletLegendItem'));
        return (
            divTooltipTopLeft(
                tr.compose('remove'),
                {},
                removeButton(() => {
                    legendEvents.removeLayer(info);
                    setLayout(AppLayout.MapAndInfo);
                }),
            )
        );
    };

const renderUpButton =
    (info: ILayerInfo) =>
        divTooltipTop(
            tr.compose('moveLayerUp'),
            {},
            upButton(() => {
                legendEvents.moveLayerUp(info.id);
            }),
        );


const renderDownButton =
    (info: ILayerInfo) => {
        return (
            divTooltipTop(
                tr.compose('moveLayerDown'),
                {},
                downButton(() => {
                    legendEvents.moveLayerDown(info.id);
                }),
            )
        );
    };


const renderTools =
    (info: ILayerInfo, idx: number, len: number) => {
        const tools: React.ReactNode[] = [];
        if (len === 1) {
            tools.push(
                renderFeatureEditButton(info),
                renderVisible(info),
                renderResetLegendButton(info),
                renderDeleteButton(info),
            );
        }
        else if (idx === 0) {
            tools.push(
                renderFeatureEditButton(info),
                renderVisible(info),
                renderDownButton(info),
                renderResetLegendButton(info),
                renderDeleteButton(info),
            );
        }
        else if (idx === (len - 1)) {
            tools.push(
                renderFeatureEditButton(info),
                renderVisible(info),
                renderUpButton(info),
                renderResetLegendButton(info),
                renderDeleteButton(info),
            );
        }
        else {
            tools.push(
                renderFeatureEditButton(info),
                renderVisible(info),
                renderUpButton(info),
                renderDownButton(info),
                renderResetLegendButton(info),
                renderDeleteButton(info),
            );
        }
        return DIV({ className: 'legend-block-tools' },
            renderZoomRange(info.id),
            DIV({ className: 'tool-box' }, ...tools),
        );
    };




const renderLayer =
    (info: ILayerInfo, idx: number, layers: ILayerInfo[]) => {
        const items = renderLegendItem(info);
        const active = getCurrentLayerId() === info.id ? 'active' : '';
        return (
            DIV({
                className: `legend-block ${active}`,
                // probably hacky : couldn't intergate handleEditLayer
                onClick: () => {
                    setCurrentLayerId(info.id);
                    resetLegendEditor();
                    setLayout(AppLayout.LegendEditor);
                },
            },
                renderLayerTitle(info),
                renderTools(info, idx, layers.length),
                inputLabel(info),
                ...items)
        );
    };

const renderAddButton =
    () => {
        return (
            divTooltipTopLeft(
                tr.compose('addLayer'),
                {},
                addButton(() => {
                    resetLayerTable();
                    setLayout(AppLayout.LayerSelect);
                }),
            )
        );
    };


const reverse =
    (a: ILayerInfo[]): ILayerInfo[] =>
        a.reduceRight<ILayerInfo[]>((acc, v) => acc.concat([v]), []);

const render =
    (mapInfo: IMapInfo) => {
        const blocks = reverse(mapInfo.layers).map(renderLayer);
        return (
            DIV({},
                H2({}, tr.compose('mapLegend')),
                DIV({ className: 'description-wrapper' },
                    DIV({ className: 'helptext' }, tr.compose('compose:addLayerInfo')),
                    renderAddButton(),
                ),
                ...blocks,
            )
        );
    };

export default render;


logger('loaded');
