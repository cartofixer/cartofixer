/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';

import { DIV } from 'sdi/components/elements';
import { fromNullable } from 'fp-ts/lib/Option';
import { IMapInfo } from 'sdi/source';

import { getMapInfo } from '../../queries/app';
import info from './info';
import mapPublishingTools from './publishing-tools';
import attachments from './attachments';
import legend from './legend';
import { renderLinkInfo } from '../link-map';

const logger = debug('sdi:map-info');



const sideBarHeader =
    (mi: IMapInfo) => DIV({ className: 'sidebar-header' },
        mapPublishingTools(mi));


const sideBarMain =
    (mi: IMapInfo) => DIV({ className: 'sidebar-main' },
        info(mi),
        renderLinkInfo(),
        attachments(mi),
        legend(mi));


const render =
    () => fromNullable(getMapInfo())
        .fold(
            DIV({ className: 'sidebar-right map-legend' }),
            mi => (
                DIV({ className: 'sidebar-right map-legend' },
                    sideBarHeader(mi),
                    sideBarMain(mi),
                )));

export default render;

logger('loaded');
