import buttonFactory from 'sdi/components/button';
import {
    queryK,
    dispatchK,
} from 'sdi/shape';


export const { makeLabel, makeIcon, makeLabelAndIcon, makeRemove, makeRemoveIcon } = buttonFactory(
    queryK('component/button'), dispatchK('component/button'));

