
/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { DIV, INPUT } from 'sdi/components/elements';
import tr from 'sdi/locale';

const render =
    () => {
        return (
            DIV({ className: 'upload-widget csv' },
                DIV({ className: 'upload-title' }, 'CSV'),
                DIV({ className: 'input-wrapper' },
                    INPUT({ type: 'file', name: '', value: '', accept: '.csv, text/csv' })),
                DIV({ className: 'input-wrapper options' },
                    DIV({ className: 'checkbox' }, tr.compose('skipFirstLine'))),
                DIV({ className: 'input-wrapper separator' },
                    DIV({ className: 'upload-label' }, tr.compose('separatedBy')),
                    DIV({ className: 'radio' }, tr.compose('comma')),
                    DIV({ className: 'radio' }, tr.compose('semicolon')),
                    DIV({ className: 'radio' }, tr.compose('tab')),
                    DIV({ className: 'radio' }, tr.compose('space'))),
                DIV({ className: 'input-wrapper text-delimiter' },
                    DIV({ className: 'upload-label' }, tr.compose('textDelimiter')),
                    DIV({ className: 'radio' }, tr.compose('quotationMark')),
                    DIV({ className: 'radio' }, tr.compose('apostrophe'))),
                DIV({ className: 'input-wrapper lat-lon' },
                    DIV({ className: 'input-wrapper' },
                        DIV({ className: 'input-label' }, tr.compose('setLatitude')),
                        INPUT({ type: 'number', name: '', value: '', placeholder: 'columnNumber' })),
                    DIV({ className: 'input-wrapper' },
                        DIV({ className: 'input-label' }, tr.compose('setLongitude')),
                        INPUT({ type: 'number', name: '', value: '', placeholder: 'columnNumber' }))),
                DIV({ className: 'btn-upload' }, tr.compose('upload')))
        );
    }

export default render;