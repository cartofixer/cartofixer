
/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
import { ReactNode } from 'react';

import { DIV } from 'sdi/components/elements';
import { renderScaleline } from 'sdi/map/controls';

import { IMapOptions, create } from 'sdi/map';
import { getCurrentBaseLayer, getMapInfo } from '../queries/app';
import { signalReadyMap } from '../events/app';
import mapQueries, { getZoom } from '../queries/map';
import * as mapEvents from '../events/map';
import tr from 'sdi/locale';

const logger = debug('sdi:comp:map');
export const mapName = 'main-view';

const options: IMapOptions = {
    element: null,
    getBaseLayer: getCurrentBaseLayer,
    getView: mapQueries.getView,
    getMapInfo,

    updateView: mapEvents.updateMapView,
    setScaleLine: mapEvents.setScaleLine,
};

let mapSetTarget: (t: HTMLElement | null) => void;
let mapUpdate: () => void;

const attachMap = (element: HTMLElement | null) => {
    if (!mapUpdate) {
        const { update, setTarget } = create(mapName, {
            ...options,
            element,
        });
        mapSetTarget = setTarget;
        mapUpdate = update;
        signalReadyMap();
    }
    if (element) {
        logger('mapSetTarget');
        mapSetTarget(element);
    }
};

const renderZoomLevel =
    () => DIV({ className: 'map__zoom-level' },
        `${tr.compose('mapZoomLevel')} : ${Math.round(getZoom())}`
    );

const render = () => {
    if (mapUpdate) {
        mapUpdate();
    }

    const overlays: ReactNode[] = [];

    overlays.push(DIV({ className: 'scale-and-zoom' },
        renderScaleline(mapQueries.getScaleLine()),
        renderZoomLevel()),
    );

    return (
        DIV({ className: 'map-wrapper', key: 'the-map-wrapper' },
            DIV({
                id: mapName,
                className: 'map',
                ref: attachMap,
            }),
            ...overlays)
    );
};


export default render;

logger('loaded');
