
import { fromNullable } from 'fp-ts/lib/Option';

import { ILayerInfo } from 'sdi/source';
import { DIV } from 'sdi/components/elements';
import featureView from 'sdi/components/feature-view';

import { getCurrentFeature, getCurrentLayerInfo } from '../queries/app';


const noView = () => DIV({ className: 'feature-view' });


const withInfo =
    (info: ILayerInfo) =>
        fromNullable(getCurrentFeature())
            .fold(noView(),
                feature => featureView(
                    info.featureViewOptions, feature));



const render =
    () =>
        DIV({ className: 'sidebar-right' },
            getCurrentLayerInfo()
                .fold(noView(), ({ info }) => withInfo(info)));


export default render;
