

/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
import { none } from 'fp-ts/lib/Option';

import { DIV } from 'sdi/components/elements';
import tr from 'sdi/locale';

import { getCurrentLayerInfo, getLayerData } from '../../queries/app';
import { setLayout, setCurrentFeatureData } from '../../events/app';
// import legendEvents from '../../events/legend-editor';
import { getSelectedFeature, getFeatureRow } from '../../queries/table';
import { selectFeatureRow } from '../../events/table';
import { AppLayout } from '../../shape/types';
import { makeLabelAndIcon } from '../button';
import config from './config';

const logger = debug('sdi:feature-config');


// TODO move to events
const ensureTableSelection =
    () => {
        if (getSelectedFeature() < 0) {
            const row = getFeatureRow(0);
            if (row) {
                getCurrentLayerInfo()
                    .map(s => {
                        getLayerData(s.metadata.uniqueResourceIdentifier)
                            .getOrElse(none)
                            .map((layer) => {
                                selectFeatureRow(0);
                                const idx = row.from as number;
                                const feature = layer.features[idx];
                                setCurrentFeatureData(feature);
                            });
                    });

                // const { metadata } = getCurrentLayerInfo();
                // if (metadata) {
                //     getLayerData(metadata.uniqueResourceIdentifier)
                //         .getOrElse(none)
                //         .map((layer) => {
                //             selectFeatureRow(0);
                //             const idx = row.from as number;
                //             const feature = layer.features[idx];
                //             setCurrentFeatureData(feature);
                //         });
                // }

            }
        }
    };

export interface FeatureConfig {
    currentRow: number;
    editedValue: string | null;
}

export const initialFeatureConfigState =
    (): FeatureConfig => ({
        currentRow: -1,
        editedValue: null,
    });

const closeButton = makeLabelAndIcon('close', 2, 'check', () => tr.compose('backToMap'));


const renderHeader =
    () => (
        DIV({ className: 'app-split-header' },
            DIV({ className: 'app-split-title' },
                DIV({ className: 'editor-title' }, tr.compose('displayFeatureEdit'))),


            DIV({ className: 'app-header-close' },
                closeButton(() => { setLayout(AppLayout.LegendEditor); })))
    );

const render =
    () => {
        ensureTableSelection();
        return DIV({ className: 'app-split-wrapper feature-config' },
            renderHeader(),
            config());
    };

export default render;

logger('loaded');
