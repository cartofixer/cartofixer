

/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';

import { DIV, SPAN } from 'sdi/components/elements';
import tr, { fromRecord } from 'sdi/locale';
import { SubType } from 'sdi/source';

import queries from '../../queries/legend-editor';
import { getLayout, getSynteticLayerInfoOption, getCurrentLayerInfo } from '../../queries/app';
import { setLayout } from '../../events/app';
import * as featureConfigEvents from '../../events/feature-config';
import selectMain from './select-main';
import selectType from './select-type';
import { renderStyleSettings, renderLabelSettings } from './tools-main';
import continuous from './select-item-continuous';
import discrete from './select-item-discrete';
import { AppLayout } from '../../shape/types';
import { makeIcon } from '../button';
import { divTooltipBottomLeft } from 'sdi/components/tooltip';

const logger = debug('sdi:legend-editor');

export type LegendPage = 'legend' | 'tools';

export type PointConfigSelected = 'label' | 'marker';

export interface ILegend {
    currentPage: LegendPage;
}


export interface ILegendEditor {
    mainSelected: number;
    itemSelected: number;
    styleGroupSelected: number;
    styleGroupEditedValue: string | null;
    pointConfig: PointConfigSelected;
    autoClassValue: number;
}


export const initialLegendEditorState = (): ILegendEditor => ({
    mainSelected: -1,
    itemSelected: -1,
    styleGroupSelected: -1,
    pointConfig: 'marker',
    styleGroupEditedValue: null,
    autoClassValue: 2,
});


const closeButton = makeIcon('close', 3, 'times');
const switchTableButton = makeIcon('layerInfoSwitchTable', 3, 'table');
const switchMapButton = makeIcon('layerInfoSwitchMap', 3, 'map');


const renderCloseButton =
    () => (divTooltipBottomLeft(
        tr.core('close'),
        {},
        closeButton(() => setLayout(AppLayout.MapAndInfo)),
    ));


const renderSwitchTableButton =
    () => (divTooltipBottomLeft(
        tr.compose('displayTableView'),
        {},
        switchTableButton(() => {
            setLayout(AppLayout.LegendEditorAndTable);
        }, 'table'),
    ));


const renderSwitchMapButton =
    () => (divTooltipBottomLeft(
        tr.compose('displayMapView'),
        {},
        switchMapButton(() => {
            setLayout(AppLayout.LegendEditor);
        }, 'map'),
    ));




const renderMapTableSwitch = () => {
    const l = getLayout();
    if (AppLayout.LegendEditor === l) {
        return (
            renderSwitchTableButton()
        );
    }
    return (
        renderSwitchMapButton()
    );
};

const layerTableSwitch =
    (lid: string) =>
        getSynteticLayerInfoOption(lid)
            .map(() => renderMapTableSwitch());


// {
//     const { name, metadata } = getSynteticLayerInfoOption(lid);
//     if (name && metadata) {
//         return (
//             renderMapTableSwitch());
//     }
//     return NODISPLAY();
// };

const layerName =
    (lid: string) =>
        getSynteticLayerInfoOption(lid)
            // .map(({ name }) => fromRecord(name))
            .map(({ name }) => (DIV({ className: 'layer-name' }, fromRecord(name))));

// {
//     const { name, metadata } = getSynteticLayerInfoOption(lid);
//     if (name && metadata) {
//         return (DIV({ className: 'layer-name' }, fromRecord(name)));
//     }
//     return NODISPLAY();
// };


const renderHeader =
    (legendType: SubType, _lid: string, _propName: string) => {

        const titleComps: React.ReactNode[] = [
            DIV({ className: 'app-title' }, tr.compose('legendBuilder')),
            renderLegendType(legendType),
        ];


        return (
            DIV({ className: 'app-split-header' },
                DIV({ className: 'app-split-title' },
                    ...titleComps),
                DIV({ className: 'app-split-tools' },
                    layerName(_lid),
                    layerTableSwitch(_lid),
                    renderCloseButton(),
                ))
        );
    };



const renderLegendType = (legendType: SubType) => {
    if (legendType === 'simple') {
        return (
            DIV({ className: 'legend-type' },
                SPAN({ className: 'legend-type-value' }, tr.compose('legendTypeSimple')),
            )
        );
    }
    else {
        const label = (legendType === 'continuous') ? tr.compose('legendTypeContinuous') : tr.compose('legendTypeDiscrete');

        return (
            DIV({ className: 'legend-type' },
                SPAN({ className: 'legend-type-value' }, label))
        );
    }
};






const column =
    (className: string, title: string, ...children: React.ReactNode[]): React.ReactNode => (
        DIV({ className: `column ${className}` },
            DIV({ className: 'column__title' },
                DIV({ className: 'title' }, title)),
            ...children)
    );


const renderBody = (lid: string) => {
    const style = queries.getStyle();
    const legendType = queries.getLegendType();

    if (style !== null) {
        if (legendType === 'simple') {
            return DIV({ className: 'app-split-main' },
                column('picker', tr.compose('legendTypeSelect'), selectType()),
                column('settings', tr.compose('style'), renderStyleSettings()),
                column('label', tr.compose('legendLabelHeader'), renderLabelSettings()),
            );
        }
        else {
            const propName = queries.getSelectedMainName();
            if (propName === '') {
                return DIV({ className: 'app-split-main' },
                    column('picker', tr.compose('legendTypeSelect'), selectType()),
                    column('settings', tr.compose('columnPicker'), selectMain(lid)),
                    column('label', tr.compose('legendLabelHeader'), renderLabelSettings()),
                );
            }
            else {

                if (legendType === 'discrete') {
                    return DIV({ className: 'app-split-main' },
                        column('picker', `${tr.compose('legendItems')} - ${propName}`, discrete()),
                        column('settings', tr.compose('style'), renderStyleSettings()),
                        column('label', tr.compose('legendLabelHeader'), renderLabelSettings()),
                    );
                }
                else {
                    return DIV({ className: 'app-split-main' },
                        column('picker', `${tr.compose('legendItems')} - ${propName}`, continuous()),
                        column('settings', tr.compose('style'), renderStyleSettings()),
                        column('label', tr.compose('legendLabelHeader'), renderLabelSettings()),
                    );
                }
            }
        }
    }

    return DIV({});
};

const render = () =>
    getCurrentLayerInfo()
        .fold(
            DIV({ className: 'app-split-wrapper legend-builder' }),
            ({ info }) => {
                featureConfigEvents.ensureSelectedFeature();
                return (DIV({ className: 'app-split-wrapper legend-builder' },
                    renderHeader(queries.getLegendType(),
                        info.id,
                        queries.getSelectedMainName()),
                    renderBody(info.id)));
            },
        );

// {
//                 const { info } = getCurrentLayerInfo();
//                 if(!info) {
//                     return DIV({ className: 'app-split-wrapper legend-builder' });
//                 }
//     featureConfigEvents.ensureSelectedFeature();
//                 return(
//                     DIV({ className: 'app-split-wrapper legend-builder' },
//                         renderHeader(queries.getLegendType(),
//                             info.id,
//                             queries.getSelectedMainName()),
//                         renderBody(info.id))
//         );
// };

export default render;

logger('loaded');
