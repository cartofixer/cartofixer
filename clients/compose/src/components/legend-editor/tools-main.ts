

/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import { ReactNode } from 'react';
import { fromNullable } from 'fp-ts/lib/Option';

import tr from 'sdi/locale';
import { DIV, SPAN } from 'sdi/components/elements';

import legendEvents from '../../events/legend-editor';
import queries from '../../queries/legend-editor';
import { renderLine, renderLineForGroup } from './tool-line';
import { renderPolygon, renderPolygonForGroup } from './tool-polygon';
import { renderMarker, renderMarkerForGroup } from './tool-point';
import { propName, fontColor, fontSize, offsetX, offsetY, labelResolution } from './tool-input';

export const renderStyleSettings =
    () => {
        const lt = queries.getLegendType();
        const idx = queries.getSelectedStyleGroup();

        const tool =
            (simple: () => React.ReactNode, group: (a: number) => React.ReactNode) => {
                if (lt === 'simple') {
                    return simple();
                }
                else if (idx >= 0) {
                    return group(idx);
                }
                return DIV({});
            };


        switch (queries.getGeometryType()) {
            case 'Point':
            case 'MultiPoint': return tool(renderMarker, renderMarkerForGroup);
            case 'LineString':
            case 'MultiLineString': return tool(renderLine, renderLineForGroup);
            case 'Polygon':
            case 'MultiPolygon': return tool(renderPolygon, renderPolygonForGroup);
            case null: return DIV({}, 'ERROR - No Geometry Type');
        }
    };


const renderPointLabelPosition =
    () => {
        const pos = queries.getPositionForLabel();
        let cp = queries.getMarkerCodepoint(0);
        if (0 === cp) {
            cp = queries.getMarkerCodepointForGroup(0, 0);
            if (0 === cp) {
                cp = 0xf111;
            }
        }
        const setPos =
            (p: typeof pos) => () => {
                legendEvents.setPositionForLabel(p);
            };
        const elem = (p: typeof pos) => {
            const className = (p === pos) ? `label active ${p}` : `label ${p}`;
            return DIV({ className, onClick: setPos(p) });
        };

        return (
            DIV({ className: 'style-tool label-position' },
                SPAN({ className: 'style-tool-label' }, tr.compose('labelPostion')),
                DIV({ className: 'label-position-widget' },
                    DIV({ className: 'row' },
                        elem('above')),
                    DIV({ className: 'row' },
                        elem('right'),
                        DIV({ className: 'picto' }, String.fromCodePoint(cp)),
                        elem('left')),
                    DIV({ className: 'row' },
                        elem('under'))))
        );
    };


const wrapper =
    (...children: ReactNode[]) =>
        DIV({ className: 'column__body label ' },
            ...children);


const labelHelptext =
    () => DIV({ className: 'helptext' }, tr.compose('helptextForLabel'));


export const renderLabelSettings =
    () =>
        fromNullable(queries.getGeometryType())
            .map(
                (gt) => {
                    switch (gt) {
                        case 'Point':
                        case 'MultiPoint': return wrapper(
                            labelHelptext(),
                            propName(),
                            fontColor(),
                            fontSize(),
                            renderPointLabelPosition(),
                            offsetX(),
                            offsetY(),
                            labelResolution(),
                        );

                        case 'LineString':
                        case 'MultiLineString': return wrapper(
                            labelHelptext(),
                            propName(),
                            fontColor(),
                            fontSize(),
                            offsetY(),
                            labelResolution(),
                        );

                        case 'Polygon':
                        case 'MultiPolygon': return wrapper(
                            labelHelptext(),
                            propName(),
                            fontColor(),
                            fontSize(),
                            labelResolution(),
                        );
                    }
                },
            );
