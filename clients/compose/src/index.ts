/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'sdi/polyfill';
import './shape';
import './locale';

import * as debug from 'debug';
import { source, AppConfigIO, getMessage } from 'sdi/source';
import { initialTableState } from 'sdi/components/table';
import { IShape, configure, defaultShape } from 'sdi/shape';
import { defaultInteraction } from 'sdi/map';

import App from './app';
import { AppLayout, MapInfoIllustrationState } from './shape/types';
import { initialLegendEditorState } from './components/legend-editor';
import { initialFeatureConfigState } from './components/feature-config';
import { initialLayerEditorState } from './components/layer';
import { displayException } from 'sdi/app';

const logger = debug('sdi:index');


export const main =
    (SDI: any) => {
        AppConfigIO
            .decode(SDI)
            .fold(
                (errors) => {
                    const textErrors = errors.map(e => getMessage(e.value, e.context));
                    displayException(textErrors.join('\n'));
                },
                (config) => {

                    const initialState: IShape = {
                        ...defaultShape(config),
                        'app/lang': 'fr',
                        'app/layout': AppLayout.Splash,
                        'app/map-ready': false,
                        'app/current-map': null,
                        'app/current-layer': null,
                        'app/current-feature': null,
                        'app/map-info/illustration': MapInfoIllustrationState.showImage,
                        'app/current-metadata': null,

                        'component/splash': 0,
                        'component/table': initialTableState(),
                        'component/legend-editor': initialLegendEditorState(),
                        'component/editable': {},
                        'component/button': {},
                        'component/feature-config': initialFeatureConfigState(),
                        'component/layer-editor': initialLayerEditorState(),
                        'component/attachments': [],

                        'data/user': null,
                        'data/layers': {},
                        'data/maps': [],
                        'data/alias': [],
                        'data/datasetMetadata': [],
                        'data/categories': [],
                        'data/attachments': [],
                        'data/baselayers': {},
                        'data/md/org': [],
                        'data/md/poc': [],
                        'data/links': {},

                        'port/map/scale': {
                            count: 0,
                            unit: '',
                            width: 0,
                        },

                        'port/map/view': {
                            dirty: 'geo',
                            srs: 'EPSG:3857',
                            center: [485609, 6592756],
                            rotation: 0,
                            zoom: 6,
                            feature: null,
                            extent: null,
                        },

                        'port/map/interaction': defaultInteraction(),

                        'remote/errors': {},

                    };


                    try {
                        const start = source<IShape>(['app/lang']);
                        const store = start(initialState);
                        configure(store);
                        const app = App(store);
                        logger('start rendering');
                        app();
                    }
                    catch (err) {
                        displayException(`${err}`);
                    }
                });
    };


logger('loaded');
