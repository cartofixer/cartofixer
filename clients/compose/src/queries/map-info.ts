/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { query } from 'sdi/shape';
import { IMapInfo } from 'sdi/source';
import { fromNullable } from 'fp-ts/lib/Option';
import { mapOption } from 'fp-ts/lib/Array';

const queries = {
    getState() {
        return query('app/map-info/illustration');
    },
};


export type LinkDirection = 'forward' | 'backward';

const formatLink =
    (maps: Readonly<IMapInfo[]>) => (mid: string) =>
        fromNullable(maps.find(m => m.id === mid));

export const getLinks =
    (ld: LinkDirection) => {
        const mid = query('app/current-map');
        const allLinks = query('data/links');
        if (mid === null || !(mid in allLinks)) {
            return [];
        }
        const links = allLinks[mid];
        return links.filter(link => mid === (ld === 'forward' ? link.source : link.target));
    };

export const getLinkedMaps =
    (ld: LinkDirection) => {
        const fm = formatLink(query('data/maps'));
        const mids = getLinks(ld).map(link => (ld === 'forward' ? link.target : link.source));
        return mapOption(mids, fm);
    };


export const getMapInfo =
    (mid: string) => fromNullable(query('data/maps').find(m => m.id === mid));

export const getUnLinkedMaps =
    () => {
        const mid = query('app/current-map');
        const allLinks = query('data/links');
        const maps = query('data/maps');
        if (mid === null) {
            return [];
        }
        if (!(mid in allLinks)) {
            return maps;
        }
        const links = allLinks[mid];
        const isUnlinked = (m: IMapInfo) => undefined === links.find(l => l.target === m.id);

        return maps.filter(isUnlinked).filter(m => m.id !== mid);
    };


export default queries;
