
/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Option, none, some, fromNullable } from 'fp-ts/lib/Option';
import { Either, right, left } from 'fp-ts/lib/Either';

import { query } from 'sdi/shape';
import { getMessageRecord, FeatureCollection, IMapInfo } from 'sdi/source';
import { SyntheticLayerInfo } from 'sdi/app';

import { getDatasetMetadata } from './metadata';
import { scopeOption } from 'sdi/lib';


type FCContext = (fc: FeatureCollection) => FeatureCollection;



export const getUserData = () => {
    return query('data/user');
};


export const mapReady = () => {
    return query('app/map-ready');
};


export const getLayout = () => {
    return query('app/layout');
};

export const getMaps = () => {
    return query('data/maps');
};

export const getUserMaps = () => {
    const dataMaps = query('data/maps');
    const findMap = (id: string) => fromNullable(dataMaps.find(m => m.id === id));
    return fromNullable(
        query('data/user'))
        .fold(
            [],
            ({ maps }) => maps.reduce<IMapInfo[]>(
                (acc, id) => findMap(id).fold(acc, m => acc.concat([m])), []));
};


export const getLayerData = (id: string): Either<string, Option<FeatureCollection>> => {
    const layers = query('data/layers');
    const errors = query('remote/errors');
    if (id in layers) {
        return right(some<FeatureCollection>(layers[id]));
    }
    else if (id in errors) {
        return left(errors[id]);
    }
    return right(none);
};


export const getLayerDataWithContext = (id: string, context: FCContext) => {
    const layers = query('data/layers');
    if (id && id in layers) {
        return context(layers[id]);
    }
    return null;
};


export const getMap = (mid: string) => {
    const maps = query('data/maps');
    return maps.find(m => m.id === mid);
};


export const getMapInfo = () => {
    const mid = query('app/current-map');
    const info = query('data/maps').find(m => m.id === mid);
    return (info !== undefined) ? info : null;
};


export const getSynteticLayerInfoOption = (layerId: string): Option<SyntheticLayerInfo> => {

    return scopeOption()
        .let('mid', fromNullable(query('app/current-map')))
        .let('info', ({ mid }) => fromNullable(query('data/maps').find(m => m.id === mid)))
        .let('layerInfo', ({ info }) => fromNullable(info.layers.find(l => l.id === layerId)))
        .let('metadata', ({ layerInfo }) => getDatasetMetadata(layerInfo.metadataId))
        .map(({ layerInfo, metadata }) => ({
            name: getMessageRecord(metadata.resourceTitle),
            info: layerInfo,
            metadata
        }))
};


export const getCurrentMap = () => {
    return query('app/current-map');
};


export const getCurrentLayerId = () => {
    return query('app/current-layer');
};


export const getCurrentLayerInfo = () => {
    const lid = query('app/current-layer');
    if (lid) {
        return getSynteticLayerInfoOption(lid);
    }
    return none;
};


export const getCurrentFeature = () => {
    return query('app/current-feature');
};


export const getCurrentBaseLayerName = () => {
    const mid = query('app/current-map');
    const map = query('data/maps').find(m => m.id === mid);
    if (map) {
        return map.baseLayer;
    }
    return null;
};

export const getCurrentBaseLayer = () => {
    const name = getCurrentBaseLayerName();
    const bls = query('data/baselayers');
    if (name && name in bls) {
        return bls[name];
    }
    return null;
};


export const getCategories = () => {
    return query('data/categories');
};


export const getSplash = () => {
    return query('component/splash');
};
