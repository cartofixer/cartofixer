/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Option, none } from 'fp-ts/lib/Option';

import { queryK } from 'sdi/shape';
import {
    TableDataRow,
    TableDataType,
    tableQueries,
    emptySource,
    TableSourceGetter,
    TableSource,
} from 'sdi/components/table';
import {
    FeatureCollection,
    Feature,
    Properties,
    getFields,
    PropertyTypeDescriptor,
} from 'sdi/source';
import { getLayerPropertiesKeys } from 'sdi/util';

import { getCurrentLayerInfo, getLayerData as appGetLayerData } from './app';
import { SyntheticLayerInfo } from 'sdi/app';


// Layer / FeatureCollection

const extractDataOption =
    ({ metadata }: SyntheticLayerInfo): Option<FeatureCollection> =>
        appGetLayerData(metadata.uniqueResourceIdentifier).getOrElse(none);

export const getLayer =
    (): Option<FeatureCollection> =>
        getCurrentLayerInfo()
            .fold(none, extractDataOption);



// {
//     const { metadata } = getCurrentLayerInfo();
//     if (metadata !== null) {
//         return getLayerData(metadata.uniqueResourceIdentifier).getOrElse(none);

//     }
//     return none;
// }

export const getLayerOption =
    () => getLayer();

export const getFeatureData =
    (numRow: number): Feature | null =>
        getLayer().fold(null,
            (layer) => {
                if (layer && numRow >= 0 && numRow < layer.features.length) {
                    return layer.features[numRow];
                }
                return null;
            });

const getLayerData =
    (layer: FeatureCollection): TableDataRow[] => {
        const keys = getLayerKeys(layer);
        const features = layer.features;

        return (
            features.map<TableDataRow>((f) => {
                if ('properties' in f) {
                    const props: Properties = f.properties;
                    const row = keys.map((k) => {
                        if (props && props[k] && props[k] != null) {
                            return props[k].toString();
                        }

                        return '';
                    });
                    return { from: f.id, cells: row };
                }

                return { from: 'empy-row', cells: [] };
            }).filter(r => r.cells.length > 0)
        );
    };

const getLayerKeys =
    (layer: FeatureCollection) => getLayerPropertiesKeys(layer);


const getTyper = (layer: FeatureCollection) => {
    const fd = getFields(layer).map(fds => {
        const result: { [k: string]: PropertyTypeDescriptor } = {};
        fds.forEach(([f, t]) => result[f] = t);
        return result;
    }).getOrElseL(() => {
        const keys = getLayerKeys(layer);
        const firstRow = layer.features[0].properties!; // OUCH!
        const result: { [k: string]: PropertyTypeDescriptor } = {};
        keys.forEach(k => {
            const val = firstRow[k];

            switch (typeof val) {
                case 'string':
                    result[k] = 'string';
                    break;
                case 'number':
                    result[k] = 'number';
                    break;
                case 'boolean':
                    result[k] = 'boolean';
                    break;
                default:
                    result[k] = 'string';
            }
        });
        return result;
    });

    type FD = typeof fd;

    return <K extends keyof FD>(key: K) => fd[key];
};

const getLayerTypes =
    (layer: FeatureCollection): TableDataType[] => {
        const typer = getTyper(layer);
        return getLayerKeys(layer).map(typer);
    };

export const getSource: TableSourceGetter =
    () => getLayerOption()
        .fold<TableSource>(
            emptySource(),
            layer => ({
                kind: 'local',
                data: getLayerData(layer),
                keys: getLayerKeys(layer),
                types: getLayerTypes(layer),
            }));



export const layerTableQueries = tableQueries(queryK('component/table'), getSource);

export const getSelectedFeature =
    () => layerTableQueries.getSelected();

export const getFeatureRow =
    (idx: number) => layerTableQueries.getRow(idx);

export const getSelectedFeatureRow =
    () => getFeatureRow(getSelectedFeature());


