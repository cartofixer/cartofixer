import { MessageStore, formatMessage } from 'sdi/locale';

const messages = {
    aliasDictonary: {
        fr: 'Dictionnaire des alias',
        nl: 'Woordenboek van aliassen', // nldone
        en: 'Alias dictionary'
    },

    createAlias: {
        fr: 'Créer un nouvel alias',
        nl: 'Maak een nieuwe alias aan', // nldone
        en: 'Create a new alias'
    },

    term: {
        fr: 'Terme',
        nl: 'Term', // nldone
        en: 'Term'
    },

    replaceFR: {
        fr: 'Alias FR',
        nl: 'Alias FR', // nldone
        en: 'Alias FR'
    },

    replaceNL: {
        fr: 'Alias NL',
        nl: 'Alias NL', // nldone
        en: 'Alias NL'
    },

    save: {
        fr: 'Sauvegarder',
        nl: 'Opslaan', // nldone
        en: 'Save'
    },

    remove: {
        fr: 'Supprimer',
        nl: 'Verwijderen', // nldone
        en: 'Remove'
    },

    rmvMsgKeyword: {
        fr: "Souhaitez-vous supprimer l'alias ?",
        nl: 'Wilt u de alias verwijderen?', // nldone
        en: 'Would you like to delete the alias?'
    },

    insertTerm: {
        fr: 'Insérez le terme original',
        nl: 'De oorspronkelijke term invoegen', // nltocheck
        en: 'Insert original term'
    },
    insertAliasFr: {
        fr: `Insérez l'alias en français`,
        nl: 'Voer de alias in het Frans in.', // nltocheck
        en: 'Insert french alias'
    },
    insertAliasNl: {
        fr: `Insérez l'alias en néerlandais`,
        nl: 'Voer de alias in het nederlands in.', // nltocheck
        en: 'Insert dutch alias'
    }
};

type MDB = typeof messages;
export type AliasMessageKey = keyof MDB;

declare module 'sdi/locale' {
    export interface MessageStore {
        alias(k: AliasMessageKey): Translated;
    }
}

MessageStore.prototype.alias = (k: AliasMessageKey) =>
    formatMessage(messages[k]);
