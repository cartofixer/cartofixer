/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'sdi/polyfill';
import './shape';
import './locale';

import * as debug from 'debug';
import { source, AppConfigIO, getMessage } from 'sdi/source';
import { initialTableState } from 'sdi/components/table';
import { IShape, configure, defaultShape } from 'sdi/shape';

import App from './app';
import { defaultFormAlias } from './components/alias';
import { displayException } from 'sdi/app';

const logger = debug('sdi:index');



export const main =
    (SDI: any) => {


        AppConfigIO
            .decode(SDI)
            .fold(
                (errors) => {
                    const textErrors = errors.map(e => getMessage(e.value, e.context));
                    displayException(textErrors.join('\n'));
                },
                (config) => {

                    try {
                        const start = source<IShape>(['app/lang']);
                        const store = start({
                            ...defaultShape(config),
                            'app/lang': 'fr',
                            'app/layout': ['Alias'],

                            'component/button': {},
                            'component/table/alias': initialTableState(),
                            'component/form': defaultFormAlias(),

                            'data/user': null,
                        });
                        configure(store);
                        const app = App(store);
                        logger('start rendering');
                        app();
                    }
                    catch (err) {
                        displayException(`${err}`);
                    }
                },
            );
    };


logger('loaded');
