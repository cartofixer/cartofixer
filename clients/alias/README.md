The _alias_ application allows you to create aliases for values from the underlying data structures that are configured to be alias-supported.

For example, column names for database tables may be abbreviations or technical terms that are of little interest when this information is to be communicated.

Especially since in some contexts this information must be available in different languages.

The _alias_ application therefore makes it possible to build up a dictionary of aliases and to translate these abbreviations into information that is more comprehensible to a targeted audience.

The complete user guide is available here : https://gitlab.com/cartofixer/cartofixer/-/wikis/home
