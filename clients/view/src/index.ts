/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'sdi/polyfill';
import './locale';

import * as debug from 'debug';
import { source, AppConfigIO, getMessage, } from 'sdi/source';
import { IShape, configure, defaultShape } from 'sdi/shape';

import App from './app';
import { displayException } from 'sdi/app';
import { initialAppState, initialComponentState, initialPortState, initialDataState } from './shape';

const logger = debug('sdi:index');



export const main =
    (SDI: any) => {

        AppConfigIO
            .decode(SDI)
            .fold(
                (errors) => {
                    const textErrors = errors.map(e => getMessage(e.value, e.context));
                    displayException(textErrors.join('\n'));
                },
                (config) => {


                    const initialState: IShape = {
                        ...defaultShape(config),
                        ...initialAppState(),
                        ...initialComponentState(),
                        ...initialPortState(),
                        ...initialDataState(),
                    };

                    try {
                        const start = source<IShape>(['app/lang']);
                        const store = start(initialState);
                        configure(store);
                        const app = App(store);
                        logger('start rendering');
                        app();
                    }
                    catch (err) {
                        displayException(`${err}`);
                    }
                });
    };



logger('loaded');
