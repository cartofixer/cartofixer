/*
*  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, version 3 of the License.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import {
    MessageRecord,
} from 'sdi/source';
import { IMapViewData, IMapScale, Interaction, PrintRequest, PrintResponse, defaultInteraction, defaultPrintRequest, defaultPrintResponse, ViewDirt } from 'sdi/map';

import { PrintProps, } from '../components/print';





declare module 'sdi/shape' {
    export interface IShape {
        'port/map/view': IMapViewData;
        'port/map/scale': IMapScale;
        'port/map/interaction': Interaction;
        'port/map/loading': MessageRecord[];
        'port/map/printRequest': PrintRequest<PrintProps | null>;
        'port/map/printResponse': PrintResponse<PrintProps | null>;
    }
}

export const initialPortState = () => ({
    'port/map/scale': {
        count: 0,
        unit: '',
        width: 0,
    },

    'port/map/view': {
        dirty: 'geo' as ViewDirt,
        srs: 'EPSG:3857',
        center: [4.36, 50.83] as [number, number],
        rotation: 0,
        zoom: 6,
        feature: null,
        extent: null,
    },

    'port/map/interaction': defaultInteraction(),
    'port/map/printRequest': defaultPrintRequest(),
    'port/map/printResponse': defaultPrintResponse(),
});
