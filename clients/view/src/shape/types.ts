import { Coordinate } from 'ol/coordinate';

import { IMapBaseLayer } from 'sdi/source';

import { Foldable } from 'sdi/util';


export enum AppLayout {
    MapFS,
    MapAndInfo,
    MapAndFeature,
    MapAndExtract,
    TableFs,
    MapAndTable,
    MapNavigatorFS,
    MapAndTableAndFeature,
    MapAndTracker,
    MapAndMeasure,
    Print,
    Query,
    QueryPrint,
}

export enum SortDirection {
    ascending,
    descending,
}

export type TableDataKey = string;



export interface IMapNavigator {
    query: string;
}

export interface IMenuData extends Foldable { }

export interface IToolWebServices extends Foldable {
    url: string;
    layers: IMapBaseLayer[];
}


export type LegendPage =
    | 'base-map'
    | 'data'
    | 'harvest'
    | 'info'
    | 'locate'
    | 'measure'
    | 'print'
    | 'share'
    ;

export interface ILegend {
    currentPage: LegendPage;
}


export interface TrackerCoordinate {
    coord: Coordinate;
    accuracy: number;
}

export interface IGeoTracker {
    track: TrackerCoordinate[];
    active: boolean;
}

export interface IPositioner {
    point: {
        latitude: number;
        longitude: number;
    };
}

export interface IGeoMeasure {
    active: boolean;
    geometryType: 'Polygon' | 'LineString';
    coordinates: Coordinate[];
}




export interface IShare {
    withView: boolean;
}

