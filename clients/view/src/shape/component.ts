/*
*  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, version 3 of the License.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import {
    GeometryObject,
    GeometryType,
} from 'sdi/source';
import { TableState, defaultTableState } from 'sdi/components/table2';
import { IDataTable, initialTableState } from 'sdi/components/table';
import { ButtonComponent } from 'sdi/components/button';
import { Collection, Nullable } from 'sdi/util';

import {
    ILegend,
    IMenuData,
    IMapNavigator,
    IToolWebServices,
    IPositioner,
    IShare,
    LegendPage,
} from './types';
import { PrintState, defaultPrintState } from '../components/print';
import { HarvestedLayer } from 'view/src/components/query';
import { MiniStep } from 'sdi/map/mini';
import { IToolGeocoder, defaultToolGeocoder } from 'sdi/ports/geocoder';





declare module 'sdi/shape' {
    export interface IShape {
        'component/legend': ILegend;
        'component/menu': IMenuData;
        'component/table': TableState;
        'component/table/extract': IDataTable;
        'component/mapnavigator': IMapNavigator;
        'component/legend/show-wms-legend': boolean;
        'component/legend/webservices': IToolWebServices;
        'component/legend/geocoder': IToolGeocoder;
        'component/legend/positioner': IPositioner;
        'component/legend/share': IShare;
        'component/button': ButtonComponent;
        'component/print': PrintState;
        'component/bookmark/current-index': Nullable<number>;

        'component/harvest/geometry': Nullable<GeometryObject>;
        'component/harvest/geometry-type': Nullable<GeometryType>;
        'component/harvest/results': HarvestedLayer[];
        'component/harvest/minimap': Collection<MiniStep>;
    }
}

export const initialComponentState = () => ({

    'component/legend': {
        currentPage: 'info' as LegendPage,
    },

    'component/menu': {
        folded: true,
    },

    'component/mapnavigator': {
        query: '',
    },

    'component/table': defaultTableState(),
    'component/table/extract': initialTableState(),

    'component/timeserie': {},

    'component/legend/show-wms-legend': false,

    'component/legend/webservices': {
        folded: true,
        url: '',
        layers: [],
    },

    'component/legend/geocoder': defaultToolGeocoder(),

    'component/legend/positioner': {
        point: {
            latitude: 0,
            longitude: 0,
        },
    },

    'component/legend/share': {
        withView: false,
    },

    'component/harvest/geometry': null,
    'component/harvest/geometry-type': null,
    'component/harvest/results': [],
    'component/harvest/minimap': {},

    'component/button': {},
    'component/bookmark/current-index': null,


    'component/print': defaultPrintState(),
});
