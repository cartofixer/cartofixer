/*
*  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, version 3 of the License.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import {
    Attachment,
    Category,
    FeatureCollection,
    IMapBaseLayer,
    IMapInfo,
    Inspire,
    MapLink,
    StreamingState,
    IUser,
    initialStreamingState,
    RemoteResource,
    MdPointOfContact,
    ResponsibleOrganisation,
} from 'sdi/source';
import { Collection, } from 'sdi/util';



declare module 'sdi/shape' {
    export interface IShape {
        'remote/errors': Collection<string>;
        'data/layers': Collection<FeatureCollection>;
        'data/maps': IMapInfo[];
        'data/categories': Category[];
        'data/datasetMetadata': Collection<Inspire>;
        'data/attachments': Attachment[];
        'data/baselayers': Collection<IMapBaseLayer>;
        'data/links': Collection<MapLink[]>;
        'data/user': IUser | null;
        'data/features/stream': StreamingState;
        'data/md/poc': Collection<RemoteResource<MdPointOfContact>>;
        'data/md/org': Collection<RemoteResource<ResponsibleOrganisation>>;
    }
}


export const initialDataState = () => ({
    'remote/errors': {},
    'port/map/loading': [],
    'data/layers': {},
    'data/maps': [],
    'data/alias': [],
    'data/categories': [],
    'data/datasetMetadata': {},
    'data/attachments': [],
    'data/baselayers': {},
    'data/links': {},

    'data/features/stream': initialStreamingState(),
    'data/user': null,
    'data/md/poc': {},
    'data/md/org': {},
});
