import { fromNullable } from 'fp-ts/lib/Option';
import { query, queryK } from 'sdi/shape';

export const getGeometry =
    () => fromNullable(query('component/harvest/geometry'));

export const getGeometryType =
    () => fromNullable(query('component/harvest/geometry-type'));

export const getHarvests = queryK('component/harvest/results')

export const findHarvest =
    (mid: string) =>
        fromNullable(getHarvests()
            .find(h => h.metadataId === mid))


export const getMiniMap =
    (k: string) => fromNullable(query('component/harvest/minimap')[k])