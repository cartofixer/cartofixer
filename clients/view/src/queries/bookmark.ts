/*
 *  Copyright (C) 2019 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


import * as debug from 'debug';
import { query } from 'sdi/shape';
import { bookmarkLayerID, defaultBookmarks } from '../components/bookmark';
import { ILayerInfo } from 'sdi/source';
import { fromNullable } from 'fp-ts/lib/Option';
import { scopeOption } from 'sdi/lib';

const logger = debug('sdi:query/bookmark');

export const getBookmarks =
    () => {
        const layers = query('data/layers');
        if (bookmarkLayerID in layers) {
            return layers[bookmarkLayerID];
        }
        return defaultBookmarks();
    };

export const withoutBookmarks =
    (ls: ILayerInfo[]) =>
        ls.filter(l => l.id !== bookmarkLayerID);



export const getBookmarkIndex =
    () => fromNullable(query('component/bookmark/current-index'));


export const getBookmarkName =
    () =>
        scopeOption()
            .let('bix', getBookmarkIndex())
            .let('bml', fromNullable(query('data/layers')[bookmarkLayerID]))
            .let('name', ({ bix, bml }) => fromNullable(bml.features?.[bix]?.properties?.name as string))
            .pick('name');

logger('loaded');
