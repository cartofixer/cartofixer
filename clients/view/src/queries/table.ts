/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
import { Option, none, fromNullable, fromPredicate, some } from 'fp-ts/lib/Option';

import {
    FeatureCollection,
    Feature,
    FeatureViewOptions,
    FeatureViewConfig,
    foldRemote,
    getFields,
    PropertyTypeDescriptor,
    StreamingField,
    getFeatureProp,
    streamFieldName,
    Inspire,
    getSparse,
    StreamingRequestSort,
    StreamingRequestFilter,
    hashRequestFilter,
} from 'sdi/source';
import { queryK, subscribe, query } from 'sdi/shape';
import { getLayerPropertiesKeys, isNotNullNorUndefined } from 'sdi/util';
import {
    TableDataRow,
    emptySource,
    TableSource,
    TableDataCell,
    TableWindow,
    TableData,
} from 'sdi/components/table2';
import { formatNumber } from 'sdi/locale';
import { getAlias, getLang } from 'sdi/app';

import { getCurrentLayerInfo, getLayerData } from './app';
import { withExtract } from './map';
import { makeStreamingRequestSort, makeStreamingRequestFilters } from '../events/table';

const logger = debug('sdi:queries/table');

type ObjOrNull = { [k: string]: any } | null;

// Layer / FeatureCollection

export const getLayer = (): Option<FeatureCollection> =>
    getCurrentLayerInfo().chain(({ metadata }) =>
        getLayerData(metadata.uniqueResourceIdentifier).getOrElse(none)
    );

export const getLayerOption = () => getLayer();

export const getFeatureData = (numRow: number): Feature | null => {
    return getLayer().fold(null, (layer) => {
        if (layer && numRow >= 0 && numRow < layer.features.length) {
            return layer.features[numRow];
        }
        return null;
    });
};

const getLayerDataFromFeatures = (layer: FeatureCollection, w: TableWindow): TableData => {
    const keys = getLayerKeys(layer);
    const features = withExtract().fold(layer.features, ({ state }) =>
        layer.features.filter(
            f => state.findIndex(fe => fe.featureId === f.id) >= 0
        )
    );

    const rows = features
        .map<TableDataRow>((f) => {
            if ('properties' in f) {
                const props: ObjOrNull = f.properties;
                const row = keys.map((k) => {
                    if (props && props[k] && props[k] != null) {
                        if (typeof props[k] === 'number') {
                            return formatNumber(props[k]);
                        }
                        return props[k].toString();
                    }

                    return '';
                });
                return { from: f.id, cells: row };
            }

            return { from: -1, cells: [] };
        })
        .filter(r => r.from >= 0)
        .slice(w.offset, w.offset + w.size);

    return { rows, total: features.length };
};

// FeatureViewOptions

const fromConfig = fromPredicate<FeatureViewOptions>(
    options => options.type === 'config'
);


const getTyper = (layer: FeatureCollection) => {
    const fd = getFields(layer).map((fds) => {
        const result: { [k: string]: PropertyTypeDescriptor } = {};
        fds.forEach(([f, t]) => result[f] = t);
        return result;
    }).getOrElseL(() => {
        const keys = getLayerKeys(layer);
        const firstRow = layer.features[0].properties!;
        const result: { [k: string]: PropertyTypeDescriptor } = {};
        keys.forEach((k) => {
            const val = firstRow[k];

            switch (typeof val) {
                case 'string':
                    result[k] = 'string';
                    break;
                case 'number':
                    result[k] = 'number';
                    break;
                case 'boolean':
                    result[k] = 'boolean';
                    break;
                default:
                    result[k] = 'string';
            }
        });
        return result;
    });

    type FD = typeof fd;

    return <K extends keyof FD>(key: K): PropertyTypeDescriptor => fd[key];
};


const getConfig = () =>
    getCurrentLayerInfo()
        .chain(({ info }) => fromNullable(info.featureViewOptions))
        .chain(fromConfig)
        .getOrElse({ type: 'config', rows: [] }) as FeatureViewConfig;


const getRows = () => {
    const lc = getLang();
    const allRows = getConfig().rows;
    return allRows.filter(r => r.lang === lc);
};


const getLayerKeys = (layer: FeatureCollection) => {
    const configKeys = getRows();

    if (configKeys.length > 0) {
        return configKeys.map(row => row.propName);
    }

    return getLayerPropertiesKeys(layer).map(getAlias);
};


const getLayerFields = (layer: FeatureCollection) => {
    const typer = getTyper(layer);
    return getLayerKeys(layer).map<StreamingField>(k => [k, typer(k)]);
};


const makeLayerSource = () =>
    getLayerOption().fold(
        emptySource(),
        layer =>
            ({
                data: w => getLayerDataFromFeatures(layer, w),
                fields: getLayerFields(layer),
            })
    );

const makeRow = (cells: TableDataCell[], from: number): TableDataRow => ({
    cells,
    from,
});

const makeEmptyRow = (size: number) => (id: number) =>
    // tslint:disable-next-line: prefer-array-literal
    makeRow(new Array(size).fill('-'), id);

const makeEmptyTable = (width: number, length: number) =>
    // tslint:disable-next-line: prefer-array-literal
    new Array(length).fill(0).map((_, i) => makeEmptyRow(width)(i));


export const hashWindow = (
    w: TableWindow,
    s: Option<StreamingRequestSort>,
    f: StreamingRequestFilter[]
) => {
    let hash = `${w.offset}-${w.size}`;

    s.map((s) => { hash += `-${s.column}/${s.direction}`; });
    f.forEach(filter => hash += `-${hashRequestFilter(filter)}`);

    return hash;
};


export const getStreamingUrl =
    (metadata: Inspire) =>
        isNotNullNorUndefined(metadata.dataStreamUrl) ?
            some(metadata.dataStreamUrl) :
            none;


export const getStream =
    () => query('data/features/stream');



export const getStreamInfo =
    () => {
        const stream = getStream();
        const table = query('component/table');
        const optSort = makeStreamingRequestSort(stream, table.sort);
        const filters = makeStreamingRequestFilters(
            stream,
            table.filters
        );
        const hash = hashWindow(table.window, optSort, filters);

        return {
            optSort,
            filters,
            hash,
            window: table.window,
            totalCount: stream.totalCount,
            storage: stream.storage,
            remotes: stream.remotes,
        };
    };

const makeStreamData = (
    url: string,
    fields: StreamingField[],
    initialCount: number,
) => {
    logger(`makeStreamData ${url} ${initialCount}`);
    let lastKnownCount = initialCount;

    return (
        window: TableWindow,
    ): TableData => {
        const { storage, totalCount } = getStreamInfo();
        const { offset, size } = window;
        const total = totalCount >= 0 ? totalCount : lastKnownCount;
        const adjustedSize = Math.min(size, total);
        const emptyRows = makeEmptyTable(fields.length, adjustedSize);
        lastKnownCount = total;

        const dataOpt = getSparse(storage, offset, adjustedSize);
        logger(`Query(${offset}, ${adjustedSize}) => ${dataOpt.isSome()}`);

        const rows: TableDataRow[] = dataOpt
            .map(data => data.map(feature => ({
                from: feature.id,
                cells: fields.map(field => getFeatureProp(feature, streamFieldName(field), '-'))
            })))
            .getOrElse(emptyRows);

        return { rows, total };
    };
};

// const STREAM_CACHE: { [k: string]: TableSource } = {};


const makeStreamingSource = (url: string) => {
    // if (url in STREAM_CACHE) { return STREAM_CACHE[url]; }
    return foldRemote(
        emptySource,
        emptySource,
        emptySource,
        ({ fields, count }) => {
            const source = ({ data: makeStreamData(url, fields, count), fields });
            // STREAM_CACHE[url] = source;
            return source;
        }
    )(getStream().meta);
};


const makeLocalSource = subscribe(
    'app/current-layer',
    makeLayerSource,
    'app/current-map',
    'data/layers',
    'data/maps',
    'port/map/interaction',
    'app/lang'
);



export const layerSource = (): TableSource =>
    getCurrentLayerInfo()
        .fold(emptySource(),
            ({ metadata }) => {
                if (metadata.dataStreamUrl) {
                    return makeStreamingSource(metadata.dataStreamUrl);
                }
                return makeLocalSource();
            });


export const tableQuery = queryK('component/table');


export type StreamLoadingStatus = 'no-stream' | 'loading' | 'loaded' | 'error';

export const streamLoadingStatus = (): StreamLoadingStatus =>
    getCurrentLayerInfo()
        .map<StreamLoadingStatus>(({ metadata }) => {
            const { remotes, hash } = getStreamInfo();
            if (
                metadata.dataStreamUrl
                && hash in remotes
            ) {
                return foldRemote<number, string, StreamLoadingStatus>(
                    () => 'no-stream',
                    () => 'loading',
                    () => 'error',
                    () => 'loaded',
                )(remotes[hash]);

            }
            return 'no-stream';
        }).getOrElse('no-stream');


logger('loaded');
