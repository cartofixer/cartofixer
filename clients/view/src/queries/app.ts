
/*
*  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { fromNullable, none, some, Option } from 'fp-ts/lib/Option';
import { left, right, Either } from 'fp-ts/lib/Either';

import { query } from 'sdi/shape';
import { getMessageRecord, IMapInfo, FeatureCollection, ILayerInfo, Feature } from 'sdi/source';
import { SyntheticLayerInfo } from 'sdi/app';
import { scopeOption } from 'sdi/lib';
import { fromRecord } from 'sdi/locale';


export const mapReady =
    () => {
        return query('app/map-ready');
    };

export const getLayout =
    () => {
        const ll = query('app/layout');
        if (ll.length === 0) {
            throw (new Error('PoppingEmptyLayoutList'));
        }
        return ll[ll.length - 1];
    };

export const getLayerData =
    (ressourceId: string): Either<string, Option<FeatureCollection>> => {
        const layers = query('data/layers');
        const errors = query('remote/errors');
        if (ressourceId in layers) {
            return right(some<FeatureCollection>(layers[ressourceId]));
        }
        else if (ressourceId in errors) {
            return left(errors[ressourceId]);
        }
        return right(none);
    };

export const getLayerDataFromInfo =
    (layerId: string): Option<FeatureCollection> =>
        scopeOption()
            .let('mid', fromNullable(query('app/current-map')))
            .let('mapInfo', ({ mid }) => fromNullable(query('data/maps').find(m => m.id === mid)))
            .let('layerInfo', ({ mapInfo }) => fromNullable(mapInfo.layers.find(l => l.id === layerId)))
            .let('metadata', ({ layerInfo }) => getDatasetMetadataOption(layerInfo.metadataId))
            .let('data', ({ metadata }) => fromNullable(query('data/layers')[metadata.uniqueResourceIdentifier]))
            .pick('data');

export const getMap =
    (mid: string) => {
        const maps = query('data/maps');
        return maps.find(m => m.id === mid);
    };

export const getDatasetMetadata =
    (id: string) => {
        const collection = query('data/datasetMetadata');
        if (id in collection) {
            return collection[id];
        }
        return null;
    };

export const getSyntheticLayerInfoOption =
    (layerId: string): Option<SyntheticLayerInfo> =>
        scopeOption()
            .let('mid', fromNullable(query('app/current-map')))
            .let('info', ({ mid }) => fromNullable(query('data/maps').find(m => m.id === mid)))
            .let('layerInfo', ({ info }) => fromNullable(info.layers.find(l => l.id === layerId)))
            .let('metadata', ({ layerInfo }) => getDatasetMetadataOption(layerInfo.metadataId))
            .map(({ layerInfo, metadata }) => ({
                name: getMessageRecord(metadata.resourceTitle),
                info: layerInfo,
                metadata
            }));

export const getSyntheticLayerInfo = getSyntheticLayerInfoOption;




export const getCurrentMap =
    () => query('app/current-map');

export const getCurrentLayer =
    () => query('app/current-layer');

export const getCurrentLayerOpt = () => fromNullable(getCurrentLayer());

export const getCurrentLayerInfoOption =
    (): Option<SyntheticLayerInfo> =>
        fromNullable(query('app/current-layer'))
            .chain<SyntheticLayerInfo>(getSyntheticLayerInfoOption);


export const getCurrentLayerInfo = getCurrentLayerInfoOption;

export const getCurrentMetadata =
    () => getCurrentLayerInfo().map(i => i.metadata);
export const getCurrentInfo =
    () => getCurrentLayerInfo().map<ILayerInfo>(i => i.info);
export const getCurrentName =
    () => getCurrentLayerInfo().map<string>(i => fromRecord(i.name));

export const getCurrentFeature =
    (): Option<Feature> => fromNullable(query('app/current-feature'));


export const getCurrentBaseLayerName =
    () => {
        const mid = query('app/current-map');
        const map = query('data/maps').find(m => m.id === mid);
        if (map) {
            return map.baseLayer;
        }
        return null;
    };

export const getBaseLayer =
    (id: string | null) => {
        const bls = query('data/baselayers');
        if (id && id in bls) {
            return bls[id];
        }
        return null;
    };

export const getCurrentBaseLayer =
    () => {
        const name = getCurrentBaseLayerName();
        return getBaseLayer(name);
    };

export const getBaseLayerServices =
    () => {
        const names = Object.keys(query('data/baselayers')).map(id => id.split('/')[0]);
        return names.reduce<string[]>((acc, name) => {
            if (acc.indexOf(name) >= 0) {
                return acc;
            }
            return acc.concat([name]);
        }, []);
    };

export const getBaseLayersForService =
    (name: string) => {
        const collection = query('data/baselayers');
        const layers =
            Object.keys(collection)
                .filter(id => id.split('/')[0] === name);
        return layers;
    };



export const getMapInfo =
    () => {
        const mid = query('app/current-map');
        const info = query('data/maps').find(m => m.id === mid);
        return (info !== undefined) ? info : null;
    };

export const getMapInfoOption = () => fromNullable(getMapInfo());

export const getDatasetMetadataOption = (id: string) => fromNullable(getDatasetMetadata(id));

export const hasPrintTitle =
    () => null === query('component/print').customTitle;

export const getPrintTitle =
    (info: IMapInfo) =>
        fromNullable(query('component/print').customTitle)
            .fold(
                info.title,
                s => s);

export const getPointOfContact = (
    id: number,
) => fromNullable(query('data/md/poc')[id.toString()]);

export const getResponsibleOrg = (
    id: number,
) => fromNullable(query('data/md/org')[id.toString()]);
