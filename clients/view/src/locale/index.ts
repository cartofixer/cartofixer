import { MessageStore, formatMessage } from 'sdi/locale';

const messages = {
    mapList: {
        fr: 'Liste des cartes',
        nl: 'Lijst van kaarten', // nldone
        en: 'Maps list',
    },

    'tooltip:info': {
        fr: 'Informations sur la carte',
        nl: 'Informatie op de kaart', // nldone
        en: 'Map informations',
    },

    'tooltip:legend': {
        fr: 'Légende',
        nl: 'Legende', // nldone
        en: 'Legend',
    },

    'tooltip:dataAccess': {
        fr: 'Accès aux données',
        nl: 'Toegang tot de gegevens', // nldone
        en: 'Access to data',
    },

    'tooltip:dataAndSearch': {
        fr: 'Données et recherche',
        nl: 'Data en zoeken', // nldone
        en: 'Data and search',
    },

    'tooltip:base-map': {
        fr: 'Fond de carte',
        nl: 'Achtergrond van de kaart', // nldone
        en: 'Background map',
    },

    'tooltip:print': {
        fr: 'Exporter et imprimer',
        nl: 'Exporteren en afdrukken', // nldone
        en: 'Export and print',
    },

    'tooltip:ishare': {
        fr: 'Partager la carte',
        nl: 'Deel de kaart', // nldone
        en: 'Share the map',
    },

    'tooltip:measure': {
        fr: 'Faire des mesures',
        nl: 'Metingen uitvoeren', // nldone
        en: 'Take measurements',
    },

    'tooltip:locate': {
        fr: 'Outils de géolocalisation',
        nl: 'Geolokalisatie tools', // nldone
        en: 'Geolocation tools',
    },

    'helptext:measureTool': {
        fr: 'Effectuez des mesures sur la carte à l’aide des outils ci-dessous. ',
        nl: 'Voer metingen uit op de kaart met behulp van de onderstaande tools.', // nldone
        en: 'Make measurements on the map using the tools below.',
    },

    'helptext:bookmark': {
        fr: 'Cliquez sur la carte pour créer un marque-page et conserver une localisation sur l\'ensemble de la plateforme.',
        nl: 'Klik op de kaart om een bladwijzer aan te maken en een localisatie op het hele platform te behouden.', // nldone
        en: 'Click on the map to create a bookmark and keep a location on the entire platform.',
    },

    'helptext:measureHelp': {
        fr: 'Double-cliquez pour terminer une mesure et en ajouter une nouvelle.',
        nl: 'Dubbelklik om een meting te voltooien en een nieuwe meting toe te voegen.', // nldone
        en: 'Double-click to complete a measurement and add a new one.',
    },

    identifier: {
        fr: 'Identifiant',
        nl: 'Identificatie', // nldone
        en: 'Identifier',
    },

    gpsTracker: {
        fr: 'Tracker GPS',
        nl: 'GPS tracker', // nldone
        en: 'GPS tracker',
    },

    'helptext:gpsTracker': {
        fr: 'Cette fonctionnalité requière d’acceder à la localisation de votre appareil. Ces informations ne sont ni stockées, ni partagées.',
        nl: 'Deze functie vereist dat u toegang heeft tot de localisatie van uw toestel. Deze informatie wordt niet opgeslagen of gedeeld.', // nldone
        en: 'This feature requires the app to access the location of your device. This information is not stored nor shared.',
    },


    extractFeatures: {
        fr: 'Forage',
        nl: 'Doorzoeken', // nldone
        en: 'Extract',
    },

    links: {
        fr: 'Liens',
        nl: 'Links', // nldone
        en: 'Links',
    },

    extractOn: {
        fr: 'Intersection avec la zone visible',
        nl: 'Intersectie met het weergegeven gebied', // nldone
        en: 'Intersection with visible zone',
    },

    extractOff: {
        fr: 'Tous les éléments',
        nl: 'Alle elementen', // nldone
        en: 'All items',
    },

    emptyMapDescription: {
        fr: 'Description de la carte',
        nl: 'Kaartbeschrijving', // nldone
        en: 'Map description',
    },

    emptyMapTitle: {
        fr: 'Adapter le titre de la carte',
        nl: 'De kaarttitel aanpassen', // nldone
        en: 'Adjust map title',
    },

    printDownloadingBaseMap: {
        fr: 'Téléchargement du fond de carte',
        nl: 'De achtergrond van de kaart downloaden', // nldone
        en: 'Downloading background map',
    },

    printNotStarted: {
        fr: 'Rendu des couches vectorielles',
        nl: 'Weergave van vectorlagen', // nldone
        en: 'Rendering datas',
    },

    printPreparingPDF: {
        fr: 'Génération du PDF',
        nl: 'PDF voorbereiden', // nldone
        en: 'PDF generation',
    },

    relatedMapsLabel: {
        fr: 'Cartes Liées',
        nl: 'Gerelateerde kaarten', // nldone
        en: 'Related maps',
    },

    wmsSwitch: {
        fr: 'Fond de carte',
        nl: 'Achtergrond van de kaart', // nldone
        en: 'Background map',
    },


    'helptext:wmsSwitchTool': {
        fr: 'Sélectionnez un des webservices référencés ci-dessous pour changer le fond de carte.',
        nl: 'Selecteer één van de onderstaande webservices om de basiskaart te wijzigen.', // nldone
        en: 'Select one of the webservices listed below to change the base map.',
    },

    measureTool: {
        fr: 'Outils de mesure',
        nl: 'Meetinstrumenten', // nldone
        en: 'Measure tools',
    },


    geocode: {
        fr: 'Chercher une adresse',
        nl: 'Een adres zoeken', // nldone
        en: 'Search an adress',
    },


    copy: {
        fr: 'Copier',
        nl: 'Kopiëren', // nldone
        en: 'Copy',
    },

    copied: {
        fr: 'Copié !',
        nl: 'Gekopieerd !', // nldone
        en: 'Copied !',
    },

    sharingTools: {
        fr: 'Partage de la carte',
        nl: 'Deel de kaart', // nldone
        en: 'Sharing the map',
    },

    shareWithView: {
        fr: 'Partager avec le niveau de zoom et le centrage actuel.',
        nl: 'Delen met het huidige zoomniveau en middelpunt.', // nldone
        en: 'Share with zoom level and actual center',
    },

    'helptext:share': {
        fr: 'Utilisez les liens ci-dessous pour partager la carte, ou une vue détaillée de celle-ci.',
        nl: 'Gebruik de onderstaande links om de kaart te delen, of een gedetailleerde weergave ervan.', // nldone
        en: 'Use the links below to share the map, or a detailed view of it.',
    },

    'helptext:embed': {
        fr: 'Utilisez le code HTML ci-dessous pour intégrer la carte, ou une vue détaillée de celle-ci dans votre site-web.',
        nl: 'Gebruik de onderstaande HTML-code om de kaart, of een gedetailleerde weergave ervan in uw website te integreren.', // nldone
        en: 'Use the HTML code below to embed the map, or a detailed view of it in your website.',
    },

    'helptext:locationTool': {
        fr: 'Obtenez ou utilisez des données de géolocalisation à l’aide des outils ci-dessous.',
        nl: 'Verkrijgen of gebruiken van de geolokalisatiedata met behulp van onderstaande tools.', // nldone
        en: 'Obtain or use geolocation data using the tools below.',
    },

    'helptext:pointLocationTool': {
        fr: 'Les coordonnées doivent être encodées dans le système GPS (EPSG:4326)',
        nl: 'Coördinaten moeten worden geëncodeerd in het GPS systeem  (EPSG: 4326)', // nldone
        en: 'Coordinates must be encoded in GPS system (EPSG:4326)',
    },


    measureLength: {
        fr: 'Mesurer une longueur',
        nl: 'Een lengte meten', // nldone
        en: 'Measure a length',
    },

    measureArea: {
        fr: 'Mesurer une superficie',
        nl: 'Een oppervlakte meten', // nldone
        en: 'Measure an area',
    },

    mapEmbed: {
        fr: 'Inclure la carte',
        nl: 'De kaart invoegen', // nldone
        en: 'Embed map',
    },

    mapEmbedWithView: {
        fr: 'Inclure la vue actuelle de la carte',
        nl: 'De huidige weergave van de kaart invoegen', // nldone
        en: 'Embed current view',
    },

    location: {
        fr: 'Outils de géolocalisation',
        nl: 'Tools voor geolokalisatie', // nldone
        en: 'Geolocation tools',
    },

    cursorLocalisation: {
        fr: 'Géolocaliser le curseur',
        nl: 'Geolokaliseer de cursor', // nldone
        en: 'Geo-locate the cursor',
    },

    'helptext:cursorLocationOn': {
        fr: `Visualiser des coordonnées au survol du curseur. 
        Cliquez sur la carte pour enregistrer les coordonnées.
         Coordonnées GPS - EPSG:4326.`,
        nl: `Coördinaten bekijken wanneer u boven de cursor zweeft. 
        Klik op de kaart om de coördinaten op te slaan.
         EPSG:4326 - contactgegevens`, // nltocheck
        en: `View coordinates when hovering over the cursor. 
        Click on the map to save the coordinates.
        EPSG:4326 contact details`,
    },

    'helptext:cursorLocationOff': {
        fr: 'Obtenir des coordonnées en cliquant sur la carte.',
        nl: 'Klik op de kaart voor contactgegevens.', // nltocheck
        en: 'Get contact information by clicking on the map.',
    },

    longitude: {
        fr: 'Longitude (X)',
        nl: 'Lengtegraad (X)', // nldone
        en: 'Longitude (X)',
    },

    latitude: {
        fr: 'Latitude (Y)',
        nl: 'Breedtegraad (Y)', // nldone
        en: 'Latitude (Y)',
    },

    pointLocation: {
        fr: 'Localisation d’un point',
        nl: 'Localisatie van een punt', // nldone
        en: 'Point location',
    },

    bookmark: {
        fr: 'Repère',
        nl: 'Markeerstift', // nldone
        en: 'Marker',
    },

    bookmarks: {
        fr: 'Repères',
        nl: 'Markeerstiften', // nldone
        en: 'Markers',
    },

    editBookmark: {
        fr: 'Editer le nom du repère',
        nl: 'Bewerk de naam van de marker', // nldone
        en: 'Edit marker name',
    },


    addBookmark: {
        fr: 'Ajouter un repère sur la carte',
        nl: 'Voeg een markering toe op de kaart', // nldone
        en: 'Add a marker on the map',
    },

    printSmallFormat: {
        fr: 'Petit format (A4)',
        nl: 'Klein formaat (A4)', // nldone
        en: 'Small format (A4)',
    },

    printBigFormat: {
        fr: 'Grand format (A0)',
        nl: 'Groot formaat (A0)', // nldone
        en: 'Large format (A0)',
    },

    printMap: {
        fr: 'Export et impression',
        nl: 'Exporteren en printen', // nldone
        en: 'Export and print',
    },

    'helptext:printMapTool': {
        fr: 'Exportez la vue actuelle de la carte en un fichier .PDF prêt à imprimer en sélectionnant le format et l’orientation souhaités.',
        nl: 'Exporteer de huidige weergave van de kaart als een drukklaar PDF-bestand door het gewenste formaat en de oriëntatie te selecteren.', // nldone
        en: 'Export the current map view as a print-ready PDF file by selecting the desired format and orientation.',
    },


    visible: {
        fr: 'Visibilité',
        nl: 'Zichtbaarheid', // nldone
        en: 'Visibility',
    },

    attributesTable: {
        fr: 'Table attributaire',
        nl: 'Attributentabel', // nldone
        en: 'Attribute table',
    },

    wmsLegendDisplay: {
        fr: 'Montrer la légende du fond de carte',
        nl: 'De legende van de kaart op de achtergrond tonen', // nldone
        en: 'Show background legend',
    },

    wmsLegendHide: {
        fr: 'Masquer la légende du fond de carte',
        nl: 'De legende van de kaart op de achtergrond verbergen', // nldone
        en: 'Hide background legend',
    },

    lastModified: {
        fr: 'Dernière mise à jour le ',
        nl: 'Laatste update op',  // nldone
        en: 'Last update on ',
    },

    mapLegend: {
        fr: 'Légende',
        nl: 'Legende', // nldone
        en: 'Legend',
    },

    mapData: {
        fr: 'Données',
        nl: 'Data', // nldone
        en: 'Data',
    },

    'helptext:mapdataTool': {
        fr: 'Affichez ou masquez certaines couches, et explorez les données brutes en utilisant la table attributaire.',
        nl: 'Toon of verberg bepaalde lagen en verken de ruwe gegevens met behulp van de attributententabel.', // nldone
        en: 'Show or hide certain layers, and explore the raw data using the attribute table',
    },

    zoomOnFeature: {
        fr: 'Zoomer sur l’entité',
        nl: 'Zoom in op de entiteit', // nldone
        en: 'Zoom to item',
    },

    remove: {
        fr: 'Supprimer',
        nl: 'Verwijderen', // nldone
        en: 'Remove',
    },
    total: {
        fr: 'total',
        nl: 'totaal', // nldone
        en: 'total',
    },

    loadingData: {
        fr: 'Chargement des données',
        nl: 'Data worden geladen', // nldone
        en: 'Loading datas',
    },

    layerId: {
        fr: 'Identifiant de la ressource',
        nl: 'Identificatie van de bron', // nldone
        en: 'Resource ID',
    },


    mapLink: {
        fr: 'Lien vers la carte',
        nl: 'Link naar de kaart', // nldone
        en: 'Link to the map',
    },

    mapLinkWithView: {
        fr: 'Lien vers la vue actuelle de la carte',
        nl: 'Link naar de huidige kaartweergave', // nldone
        en: 'Link to the current view of the map',
    },

    embed: {
        fr: 'Inclure dans votre site web',
        nl: 'Integreer in uw website', // nldone
        en: 'Embed in your web site',
    },


    stop: {
        fr: 'Arrêter',
        nl: 'Stoppen', // nldone
        en: 'Stop',
    },


    startGPS: {
        fr: 'Démarrer la localisation GPS',
        nl: 'Start de GPS-localisatie', // nldone
        en: 'Start GPS location',
    },

    originalTitle: {
        fr: 'Titre original',
        nl: 'Originele titel', // nldone
        en: 'Original title',
    },

    searchAtlas: {
        fr: 'Chercher dans l’atlas',
        nl: 'In de atlas zoeken', // nldone
        en: 'Search in atlas',
    },

    embedCommentSnippet1: {
        fr: 'Le code suivant propose une intégration responsive, avec un rapport de taille fixé par défaut.',
        nl: 'Dit codefragment moet een schone en responsieve inbedding bieden voor desktop en mobiel, met een standaardgrootteverhouding.', // nltodo
        en: 'This code snippet should provides a clean and responsive embed for desktop and mobile, with a default size ratio.',
    },

    embedCommentSnippet2: {
        fr: 'Si vous souhaitez gérer vous-même la taille de la boîte contenant l\'iframe, supprimez le style ci-dessous et la DIV #iFrameAtlas.',
        nl: 'Als je zelf de grootte van de doos met het iframe wilt beheren, verwijder dan de onderstaande stijl en de #iFrameAtlas DIV', // nltodo
        en: 'If you want to manage yourself the size of the box containing the iframe, remove the style below and the #iFrameAtlas DIV.',
    },

    queryTitle: {
        fr: 'Recherche par forage',
        nl: '',
        en: '',
    },


    queryZone: {
        fr: 'Zone de recherche',
        nl: '',
        en: '',
    },

    queryiedData: {
        fr: 'Données explorées',
        nl: '',
        en: '',
    },

    currentMap: {
        fr: 'Carte courante',
        nl: '',
        en: '',
    },


    // queryHelpText1: {
    //     fr: `Veuillez definir la zone à explorer. 
    //     Pour fermer la forme, vous pouvez double-cliquer sur le dernier point.`,
    //     nl: '', // nltodo
    //     en: '',
    // },

    queryHelpText2: {
        fr: `Veuillez sélectionner les couches desquelles vous souhaitez extraire des informations`,
        nl: '', // nltodo
        en: '',
    },

    querySelectAll: {
        fr: 'Tout sélectionner',
        nl: 'Alles selecteren',
        en: 'Select all',
    },

    queryDeselectAll: {
        fr: 'Tout désélectionner',
        nl: 'Alles deselecteren',
        en: 'Deselect all',
    },

    harvestTitle: {
        fr: 'Sélection par forage',
        nl: '',
        en: '',
    },

    harvestResults: {
        fr: 'Résultats de la sélection',
        nl: 'Resultaten van de selectie',
        en: 'Results of the selection',
    },

    harvestInitiatePoint: {
        fr: 'avec un point',
        nl: 'met een punt',
        en: 'with a point',
    },

    harvestInitiateLine: {
        fr: 'avec une ligne',
        nl: 'met één regel',
        en: 'with a line',
    },

    harvestInitiatePolygon: {
        fr: 'avec un polygone',
        nl: 'met een veelhoek',
        en: 'with a polygon',
    },

    harvestInfoPoint: {
        fr: `Cliquez sur la carte pour créer un point de forage.`,
        nl: `Klik op de kaart om een boorpunt te maken.`,
        en: `Click on the map to create a drilling point.`,
    },

    harvestInfoLine: {
        fr: `Cliquez sur la carte pour tracer des segments.
        Pour valider, cliquez une deuxième fois sur le dernier point.
        Pour tracer une ligne à main levée, appuyez sur Shift (Majuscule) tout en maintenant le clic de la souris enfoncé.`,
        nl: `Klik op de kaart om segmenten te tekenen.
        Om te valideren, klik een tweede keer op het laatste punt.
        Om een lijn uit de vrije hand te tekenen, drukt u op Shift terwijl u de muisklik ingedrukt houdt.`,
        en: `Click on the map to draw segments.
        To validate, click a second time on the last point.
        To draw a freehand line, press Shift while holding down the mouse click.`,
    },

    harvestInfoPolygon: {
        fr: `Cliquez sur la carte pour tracer les sommets du polygone. 
        Pour valider, fermez le polygone en cliquant sur le premier sommet créé.
        Pour tracer une surface à main levée, appuyez sur Shift (Majuscule) tout en maintenant le clic de la souris enfoncé. `,
        nl: `Klik op de kaart om de hoekpunten van de veelhoek te tekenen. 
        Om te valideren, sluit u de veelhoek door op het eerste aangemaakte hoekpunt te klikken.
        Om een oppervlak uit de vrije hand te tekenen, drukt u op Shift terwijl u de muisklik ingedrukt houdt.`,
        en: `Click on the map to draw the vertices of the polygon. 
        To validate, close the polygon by clicking on the first vertex created.
        To draw a freehand surface, press Shift while holding down the mouse click.`,
    },

    metadataLabel: {
        fr: 'Informations sur les données',
        nl: 'Gegevens Informatie',
        en: 'Data Information',
    },

    abstractLabel: {
        fr: 'Résumé',
        nl: 'Abstract',
        en: 'Abstract',
    },

    pocLabel: {
        fr: 'Contact',
        nl: 'Contact',
        en: 'Contact',
    },

};

type MDB = typeof messages;
export type ViewMessageKey = keyof MDB;


declare module 'sdi/locale' {
    export interface MessageStore {
        view(k: ViewMessageKey): Translated;
    }
}


MessageStore.prototype.view = function (k: ViewMessageKey) {
    return this.getEdited('view', k, () => formatMessage(messages[k]));
};
