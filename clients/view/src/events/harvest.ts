
import { getApiUrl } from 'sdi/app';
import { assign, dispatchK, dispatch } from 'sdi/shape';
import { GeometryObject, GeometryType, FeatureCollection } from 'sdi/source';
import { defaultInteraction } from 'sdi/map';

import { setLayout } from 'view/src/events/app';
import { setPage } from 'view/src/events/legend';
import { postHarvestRequest } from 'view/src/remote';
import { getGeometry, findHarvest } from '../queries/harvest';
import { HarvestedLayer } from 'view/src/components/query';
import { getLinks } from '../queries/map';
import { getDatasetMetadataOption, getMapInfoOption } from '../queries/app';
import { loadMetadata } from './app';
import { MiniStep } from 'sdi/map/mini';
import { AppLayout } from '../shape/types';
import { withoutBookmarks } from '../queries/bookmark';


const harvest = dispatchK('component/harvest/results');

export const request =
    (metadata: string) =>
        getGeometry()
            .map((geometry) => {
                const h = findHarvest(metadata).getOrElse({ tag: 'initial', metadataId: metadata });

                if ('initial' === h.tag) {
                    const start = harvestLoading(metadata);
                    postHarvestRequest(getApiUrl('harvest/'), { metadata, geometry })
                        .then(results => harvestLoaded(metadata, results, start))
                        .catch(err => harvestError(metadata, `${err}`, start));
                }
            });



export const setGeometry =
    (g: GeometryObject) => assign('component/harvest/geometry', g);


export const clearGeometry =
    () => assign('component/harvest/geometry', null);


export const setGeometryType =
    (t: GeometryType) => assign('component/harvest/geometry-type', t);


export const clearGeometryType =
    () => assign('component/harvest/geometry-type', null);


export const startDrawing =
    (geometryType: GeometryType) => {
        clearGeometry();
        assign('component/harvest/geometry-type', geometryType);
        assign('port/map/interaction', {
            label: 'create',
            state: { geometryType },
        });
    };

export const endDrawing =
    () => {
        assign('port/map/interaction', defaultInteraction());
        getLinks('forward')
            .map(links => links.map((info) => {
                withoutBookmarks(info.layers).map((layer) => {
                    const md = getDatasetMetadataOption(layer.metadataId);
                    if (md.isNone()) {
                        loadMetadata(layer.metadataId);
                    }
                });
            }));

        getMapInfoOption()
            .map(info =>
                withoutBookmarks(info.layers).map((l) => {
                    if (l.visible) request(l.metadataId);
                }));
    };

export const removeHarvest =
    (mid: string) =>
        dispatch('component/harvest/results', hs => hs.filter(h => h.metadataId !== mid));

const filterHarvest =
    (mid: string) => (h: HarvestedLayer) => h.metadataId !== mid;

export const harvestInitial =
    (metadataId: string) => {
        const f = filterHarvest(metadataId);
        harvest(hs => hs.filter(f).concat({ tag: 'initial', metadataId }));
    };

export const harvestLoading =
    (metadataId: string) => {
        const f = filterHarvest(metadataId);
        const start = Date.now();
        harvest(hs => hs.filter(f).concat({ tag: 'loading', metadataId, start }));
        return start;
    };

export const harvestError =
    (metadataId: string, error: string, start: number) => {
        const f = filterHarvest(metadataId);
        const end = Date.now();
        harvest(hs => hs.filter(f).concat({ tag: 'error', metadataId, start, end, error }));
    };

export const harvestLoaded =
    (metadataId: string, data: FeatureCollection, start: number) => {
        const f = filterHarvest(metadataId);
        const end = Date.now();
        harvest(hs => hs.filter(f).concat({ tag: 'loaded', metadataId, start, end, data }));
    };


export const setMiniMap =
    (k: string, step: MiniStep) =>
        dispatch('component/harvest/minimap', (mm) => {
            mm[k] = step;
            return mm;
        });

export const clearHarvested =
    () => assign('component/harvest/results', []);

export const endHarvest =
    () => {
        clearHarvested();
        clearGeometry();
        clearGeometryType();
        setLayout(AppLayout.MapAndInfo);
        setPage('data');
    };

export const printHarvest =
    () => {
        setLayout(AppLayout.QueryPrint);
        window.setTimeout(() => window.print(), 300);
        window.setTimeout(endHarvest, 1000);
    };
