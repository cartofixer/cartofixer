/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
import { fromNullable, fromPredicate } from 'fp-ts/lib/Option';
import { Extent } from 'ol/extent';

import { dispatch, observe, dispatchK, query, assign } from 'sdi/shape';
import { Feature, IMapInfo, MessageRecord, ILayerInfo, remoteLoading, remoteSuccess, remoteError } from 'sdi/source';
import { removeLayerAll, addLayer, addFeaturesToLayer } from 'sdi/map';
import { getApiUrl, getRootUrl, getUserId } from 'sdi/app';
import { scopeOption } from 'sdi/lib';
import { activity } from 'sdi/activity';
import { addStream, clearStreams, mapStream, pushStreamExtent } from 'sdi/geodata-stream';

import { AppLayout } from '../shape/types';
import { fetchLayer, fetchAlias, fetchAllMaps, fetchCategories, fetchDatasetMetadata, fetchMap, fetchAttachment, fetchBaseLayer, fetchBaseLayerAll, fetchLinks, fetchUser, fetchOrganisation, fetchPointOfContact } from '../remote';
// import { addAppIdToFeature } from '../util/app';
import {
    getSyntheticLayerInfo,
    getLayerData,
    mapReady,
    getMapInfo,
    getCurrentMap,
    getMap,
    getLayerDataFromInfo,
    getPointOfContact,
    getResponsibleOrg,
} from '../queries/app';
import { addBookmarksToMap } from './bookmark';
import { notABookmarkLayer } from '../components/bookmark';
import { mapName } from '../components/map';
import { getView } from '../queries/map';
import { isNotNullNorUndefined, updateCollection } from 'sdi/util';
import { viewEvents } from './map';

const logger = debug('sdi:events/app');
export const activityLogger = activity('view');


observe('data/maps',
    () => fromNullable(getMapInfo())
        .map(
            info => info.attachments.forEach(
                aid => fetchAttachment(getApiUrl(`attachments/${aid}`))
                    .then(a => attachments(s => s.concat([a]))))));


observe('port/map/view',
    view =>
        fromNullable(view.extent)
            .map(e =>
                mapStream(e, ({ uri, lid }, extent) =>
                    loadLayerDataExtent(lid, uri, extent))));



const layerInRange =
    (info: ILayerInfo) => {
        const low = fromNullable(info.minZoom).getOrElse(0);
        const high = fromNullable(info.maxZoom).getOrElse(30);
        const zoom = getView().zoom;
        return info.visible && ((zoom > low) && (zoom < high));
    };

const whenInRange = fromPredicate(layerInRange);


export const loadUser = () =>
    getUserId()
        .map(userId =>
            fetchUser(getApiUrl(`users/${userId}`))
                .then(user => assign('data/user', user)));


const loadLayer =
    (l: ILayerInfo) => {
        loadMetadata(l.metadataId)
            .then((md) => {
                addLayer(mapName,
                    () => getSyntheticLayerInfo(l.id),
                    () => getLayerData(md.uniqueResourceIdentifier));

                if (isNotNullNorUndefined(md.dataStreamUrl)) {
                    addStream({ uri: md.uniqueResourceIdentifier, lid: l.id });
                    dispatch('data/layers', (state) => {
                        state[md.uniqueResourceIdentifier] = { type: 'FeatureCollection', features: [] };
                        return state;
                    });
                    if (l.visible && layerInRange(l)) {
                        fromNullable(getView().extent)
                            .map((e) => {
                                loadLayerDataExtent(l.id, md.uniqueResourceIdentifier, e);
                            });
                    }
                }
                else {
                    loadLayerData(md.uniqueResourceIdentifier);
                }
            })
            .catch(err => logger(`Failed to load MD ${l.metadataId}: ${err}`));
    };



const loadLayerDataExtent = (
    layerId: string,
    url: string,
    bbox: Extent,
) =>
    getSyntheticLayerInfo(layerId)
        .map(({ info }) =>
            whenInRange(info)
                .map((info) => {
                    pushStreamExtent(bbox, { lid: layerId, uri: url });
                    fetchLayer(`${url}?bbox=${bbox[0]},${bbox[1]},${bbox[2]},${bbox[3]}`)
                        .then((layer) => {
                            if (layer.features !== null) {
                                addFeaturesToLayer(mapName, info, layer.features);
                                dispatch('data/layers', (state) => {
                                    if (url in state) {
                                        state[url].features = state[url].features.concat(layer.features);
                                    }
                                    else {
                                        state[url] = layer;
                                    }
                                    return state;
                                });
                            }
                        })
                        .catch((err) => {
                            logger(`Failed to load features at ${url} due to ${err}`);
                            dispatch('remote/errors', state => ({ ...state, [url]: `${err}` }));
                        });
                })
        );


const loadLayerData = (url: string) => {
    logger(`loadLayerData(${url})`);
    fetchLayer(url)
        .then((layer) => {
            dispatch('data/layers', (state) => {
                logger(`Put layer ${url} on state`);
                state[url] = layer;
                return state;
            });
        })
        .catch((err) => {
            logger(`Failed to load layer at ${url} due to ${err}`);
            dispatch('remote/errors', state => ({ ...state, [url]: `${err}` }));
        });
};



const loadMapFromInfo =
    (info: IMapInfo, delay?: number) => {
        const mapIsReady = mapReady();
        logger(`loadMap ${info.id} ${mapIsReady} ${delay}`);
        if (mapIsReady) {
            removeLayerAll(mapName);
            info.layers.forEach(l => notABookmarkLayer(l).map(loadLayer));
            addBookmarksToMap();
            fromNullable(info.extent)
                .map(extent => viewEvents.updateMapView({
                    dirty: 'geo/extent',
                    extent,
                }));
        }
        else {
            const d = (delay !== undefined) ? (2 * delay) : 2;
            setTimeout(() => loadMapFromInfo(info, d), d);
        }
    };



const findMap = (mid: string) => fromNullable(getMap(mid));


const attachments = dispatchK('data/attachments');


const loadLinks =
    (mid: string) =>
        fetchLinks(getApiUrl(`map/links?mid=${mid}`))
            .then((links) => {
                dispatch('data/links', data => ({ ...data, [mid]: links }));
            });


export const setLayout = (l: AppLayout) => {
    logger(`setLayout ${AppLayout[l]}`);
    dispatch('app/layout', state => state.concat([l]));
};


export const signalReadyMap =
    () => dispatch('app/map-ready', () => true);



export const loadMap =
    () =>
        fromNullable(getCurrentMap())
            .map(mid =>
                findMap(mid)
                    .foldL(
                        () => {
                            fetchMap(getApiUrl(`maps/${mid}`))
                                .then((info) => {
                                    dispatch('data/maps', maps => maps.concat([info]));
                                    loadMapFromInfo(info);
                                })
                                .then(() => loadLinks(mid))
                                .catch((response) => {
                                    const user = query('app/user');

                                    if (response.status === 403 && !user) {
                                        window.location.assign(getRootUrl(`login/view/${mid}`));
                                        return;
                                    }
                                });

                        },
                        (info) => {
                            loadLinks(mid);
                            loadMapFromInfo(info);
                        },
                    )
            );



export const loadBaseLayer = (id: string, url: string) => {
    fetchBaseLayer(url)
        .then((bl) => {
            dispatch('data/baselayers', state => ({ ...state, [id]: bl }));
        });
};


export const loadAllBaseLayers = (url: string) => {
    fetchBaseLayerAll(url)
        .then((blc) => {
            dispatch('data/baselayers', () => blc);
        });
};


export const loadAllMaps =
    () => fetchAllMaps(getApiUrl(`maps`))
        .then(maps => assign('data/maps', maps));


export const loadAlias = (url: string) => {
    fetchAlias(url)
        .then((alias) => {
            dispatch('data/alias', () => alias);
        });
};


export const loadCategories = (url: string) => {
    fetchCategories(url)
        .then((categories) => {
            dispatch('data/categories', () => categories);
        });
};


export const setLayerVisibility = (id: string, visible: boolean) => {
    const mid = getCurrentMap();
    dispatch('data/maps', (maps) => {
        const mapInfo = maps.find(m => m.id === mid);
        if (mapInfo) {
            mapInfo.layers.forEach((l) => {
                if (l.id === id) {
                    l.visible = visible;
                }
            });
        }
        return maps;
    });
};


export const setMapBaseLayer = (id: string) => {
    const mid = getCurrentMap();
    dispatch('data/maps', (maps) => {
        const idx = maps.findIndex(m => m.id === mid);

        if (idx !== -1) {
            const m = maps[idx];
            m.baseLayer = id;
        }

        return maps;
    });
};


export const setCurrentLayer = (id: string) => {
    dispatch('app/current-layer', () => id);
    dispatch('app/current-feature', () => null);
    dispatch('component/table', state => ({
        ...state,
        selected: -1,
        loaded: 'none',
    }));
};


export const setCurrentFeature =
    (data: Feature | null) => dispatch('app/current-feature', () => data);


export const unsetCurrentFeature =
    () => dispatch('app/current-feature', () => null);


export const setCurrentFeatureById =
    (layerInfoId: string, id: string | number) =>
        scopeOption()
            .let('data', getLayerDataFromInfo(layerInfoId))
            .let('feature',
                s => fromNullable(
                    s.data.features.find(f => f.id === id)))
            .map(({ feature }) => {
                dispatch('app/current-layer', () => layerInfoId);
                dispatch('app/current-feature', () => feature);
            });


export const clearMap = () => {
    dispatch('app/current-map', () => null);
    dispatch('app/current-layer', () => null);
    dispatch('app/current-feature', () => null);
    dispatch('component/table', (state) => {
        state.selected = -1;
        return state;
    });
    clearStreams();
};


export const setPrintTitle =
    (customTitle: MessageRecord) =>
        dispatch('component/print',
            s => ({ ...s, customTitle }));


export const resetPrintTitle =
    () =>
        dispatch('component/print',
            s => ({ ...s, customTitle: null }));


export const loadMetadata =
    (mid: string) => fetchDatasetMetadata(getApiUrl(`metadatas/${mid}`))
        .then((md) => {
            dispatch('data/datasetMetadata', (state) => {
                state[md.id] = md;
                return state;
            });

            md.metadataPointOfContact.forEach(loadPersonOfContact);
            md.responsibleOrganisation.forEach(loadResponsibleOrganisation);
            return md;
        });
// .catch(err => logger(`Failed to load MD ${mid}: ${err}`));



const loadPersonOfContact =
    (id: number) => getPointOfContact(id)
        .foldL(
            () => {
                const key = id.toString();
                dispatch('data/md/poc', s => updateCollection(s, key, remoteLoading));
                fetchPointOfContact(id)
                    .then(poc =>
                        dispatch('data/md/poc',
                            s => updateCollection(s, key, remoteSuccess(poc))))
                    .catch(err =>
                        dispatch('data/md/poc',
                            s => updateCollection(s, key, remoteError(err.toString()))));
            },
            () => { }
        );


const loadResponsibleOrganisation =
    (id: number) => getResponsibleOrg(id)
        .foldL(
            () => {
                const key = id.toString();
                dispatch('data/md/org', s => updateCollection(s, key, remoteLoading));
                fetchOrganisation(id)
                    .then(org =>
                        dispatch('data/md/org',
                            s => updateCollection(s, key, remoteSuccess(org))))
                    .catch(err =>
                        dispatch('data/md/org',
                            s => updateCollection(s, key, remoteError(err.toString()))));
            },
            () => { }
        );


logger('loaded');
