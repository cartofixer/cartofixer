/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import { Option, some, none, fromNullable } from 'fp-ts/lib/Option';
import * as debug from 'debug';

import { observe, dispatchK, query, dispatch } from 'sdi/shape';
import {
    TableDataRow,
    TableSort,
    Filter
} from 'sdi/components/table2';
import {
    remoteLoading,
    StreamingMetaIO,
    fetchIO,
    remoteSuccess,
    remoteError,
    postUnrelatedIO,
    StreamingRequest,
    StreamingDataIO,
    StreamingRequestSort,
    StreamingState,
    StreamingRequestFilter,
    streamFieldName,
    Inspire,
    Feature,
    pushSparse,
    newSparseArray,
    foldRemote,
    StreamingMeta,
    getRanges,
    CSVRequest,
    downloadBlobIO,
    CSVDataIO,
} from 'sdi/source';
import { isNotNullNorUndefined, uniqId, updateCollection } from 'sdi/util';
import {
    getSyntheticLayerInfoOption,
    getCurrentLayerInfo,
    getLayerData,
    getCurrentLayerInfoOption,
} from '../queries/app';
import { getStreamInfo, getStreamingUrl, hashWindow, layerSource } from '../queries/table';
import { scopeOption } from 'sdi/lib';
import { setCurrentFeature, setLayout } from './app';
import { AppLayout } from '../shape/types';


const logger = debug('sdi:events/table');

export const tableDispatch = dispatchK('component/table');
const dispatchStreaming = dispatchK('data/features/stream');

/**
 * Stream observer
 */
observe('app/current-layer', (layerId) => {
    if (layerId === null) {
        return;
    }

    console.log(`Stream observer ${layerId}`);

    const inner = () => {
        getSyntheticLayerInfoOption(layerId)
            .foldL(
                () => { window.setTimeout(inner, 60); },
                ({ metadata }) => {
                    if (metadata.dataStreamUrl) {
                        initStreamingResource(metadata.dataStreamUrl);
                    }
                }
            );
    };

    inner();
});



// TODO - break down this function into manageable pieces - pm
observe('component/table', ({ window }) =>
    getCurrentLayerInfoOption()
        .chain(({ metadata }) => getStreamingUrl(metadata).map(url => ({ url, metadata })))
        .map(({ url, metadata }) => {
            const observeId = uniqId();
            const { storage, optSort, filters, remotes } = getStreamInfo();
            const missingRanges = getRanges(storage, window.offset, window.size);
            const hash = hashWindow(window, optSort, filters);
            logger(`Window [${window.offset}, ${window.size}] ${hash}`);

            if (
                isNotNullNorUndefined(url)
                && window.size > 0
                && missingRanges.length > 0
                && !(hash in remotes)
            ) {
                logger(`In(${observeId}) ${missingRanges.map(r => ` [${r[0]}, ${r[1]}]`)}`);

                dispatchStreaming(ss => ({
                    ...ss,
                    remotes: updateCollection(remotes, hash, remoteLoading)
                }));

                const requests = missingRanges.map(([offset, size]) => {
                    logger(`Map(${observeId})`, offset, size);
                    const req: StreamingRequest = {
                        offset,
                        limit: size,
                    };

                    optSort.map(sort => req.sort = sort);
                    req.filters = filters;
                    return postUnrelatedIO(StreamingDataIO, url, req);
                });

                Promise.all(requests)
                    .then((responses) => {
                        const streamInfo = getStreamInfo();

                        if (hash === streamInfo.hash) {

                            const oldStorage = streamInfo.storage;
                            let newStorage = oldStorage;
                            const debugOld = oldStorage.windows.map(w => `[${w[0]}, ${w[1]}] `);
                            logger(`Old << ${debugOld}`);
                            let totalCount = -1;
                            for (let i = 0; i < missingRanges.length; i += 1) {
                                const [offset, size] = missingRanges[i];
                                const response = responses[i];
                                newStorage = pushSparse(newStorage, offset, response.data);
                                totalCount = response.totalCount;
                                logger(`Store(${observeId}) ${offset} ${size} >>  ${newStorage.windows.map(w => `[${w[0]}, ${w[1]}] `)}`);
                            }

                            logger(`\nHash: ${hash}\n${newStorage.windows}`)

                            dispatchStreaming(ss => ({
                                ...ss,
                                storage: newStorage,
                                totalCount,
                                remotes: updateCollection(
                                    streamInfo.remotes,
                                    hash,
                                    remoteSuccess(responses.reduce((acc, v) => v.data.length + acc, 0)))
                            }));

                            responses.map(({ data }) => updateLayer(metadata.uniqueResourceIdentifier, data));
                        }
                    })
                    .catch((err) => {
                        logger(`ERR ${err}`);
                        dispatchStreaming(ss => ({
                            ...ss,
                            remotes: updateCollection(
                                getStreamInfo().remotes,
                                hash,
                                remoteError(`${err}`))
                        }));
                    });
            }
        }));


const computeTotalCount = (
    ss: StreamingState,
) => {
    return foldRemote<StreamingMeta, string, number>(
        () => ss.totalCount,
        () => ss.totalCount,
        () => ss.totalCount,
        ({ count }) => count,
    )(ss.meta);
};


export const clearStreamData =
    () => dispatchStreaming(ss => ({
        ...ss,
        storage: newSparseArray(),
        totalCount: computeTotalCount(ss),
        remotes: {},
    }));


export const clearCurrentStreamData = () =>
    getCurrentLayerInfoOption()
        .chain(({ metadata }) => getStreamingUrl(metadata))
        .map(clearStreamData);


const updateLayer = (
    uri: string,
    features: Feature[]
) => dispatch('data/layers', (layers) => {
    const layer = layers[uri];
    if (layer !== undefined) {
        features.forEach((feature) => {
            if (layer.features.findIndex(f => f.id === feature.id) < 0) {
                layer.features.push(feature);
            }
        });
    }
    return layers;
});



export const makeStreamingRequestSort = (
    stream: StreamingState,
    optSort: Option<TableSort>
) =>
    optSort.chain<StreamingRequestSort>(({ col, direction }) => {
        if (stream.meta.tag === 'success') {
            return some({
                column: col,
                columnName: streamFieldName(stream.meta.data.fields[col]),
                direction,
            });
        }
        return none;
    });


export const makeStreamingRequestFilters = (
    stream: StreamingState,
    tableFilters: Filter[]
) => {
    const streamingRequestFilters: StreamingRequestFilter[] = [];

    tableFilters.forEach((filter) => {
        if (stream.meta.tag === 'success') {
            const cond = (() => {
                switch (filter.tag) {
                    case 'string': return filter.pattern.trim().length > 0;
                    default: return true;
                }
            })();
            if (cond) {
                streamingRequestFilters.push({
                    ...filter,
                    columnName: streamFieldName(stream.meta.data.fields[filter.column])
                });
            }
        }
    });

    return streamingRequestFilters;
};





export const initStreamingResource = (url: string) => {
    const streams = query('data/features/stream');
    if (url in streams) { return; }

    dispatchStreaming(() => ({
        url,
        totalCount: -1,
        meta: remoteLoading,
        storage: newSparseArray(),
        remotes: {},
    }));

    fetchIO(StreamingMetaIO, url)
        .then(data =>
            dispatchStreaming(() => ({
                url,
                totalCount: data.count,
                meta: remoteSuccess(data),
                storage: newSparseArray(),
                remotes: {},
            }))
        )
        .catch(err =>
            dispatchStreaming(() => ({
                url,
                totalCount: -1,
                meta: remoteError(err.toString()),
                storage: newSparseArray(),
                remotes: {},
            }))
        );
};

export const exportCSV = (
    metadata: Inspire,
    layerInfoId: string,
    tableName: string,
) => {
    if (metadata.dataStreamUrl) {
        downloadCSV(layerInfoId);
    }
    else {
        extractAsCSV(tableName);
    }
};

const downloadCSV = (
    layerInfoId: string,
) => {
    const stream = query('data/features/stream');
    const table = query('component/table');
    const url = stream.url + 'csv/';

    const req: CSVRequest = {
        layerInfo: layerInfoId,
        filters: makeStreamingRequestFilters(stream, table.filters),
    };

    makeStreamingRequestSort(
        stream,
        table.sort
    ).map(sort => req.sort = sort);


    downloadBlobIO(CSVDataIO, url, req);
};

const extractAsCSV =
    (tableName: string) => {
        const separator = ';';
        const filename = tableName + '.csv';
        let csvContent = '';
        const source = layerSource();

        // Append keys as CSV header
        csvContent += source.fields.map(streamFieldName).join(separator) + '\r\n';

        // Append values
        source.data({ offset: 0, size: Number.MAX_SAFE_INTEGER })
            .rows
            .forEach((rowValue) => {
                const row = rowValue.cells.join(separator);
                csvContent += row + '\r\n';
            });

        // Create the blob
        const blob = new Blob([csvContent], { type: 'text/csv' });
        const url = window.URL.createObjectURL(blob);

        // Download
        if ((navigator as any).msSaveBlob) {
            (navigator as any).msSaveBlob(blob, filename);
        }
        else {
            const a = document.createElement('a');
            a.href = url;
            a.download = filename;
            document.body.appendChild(a);
            a.click();
            document.body.removeChild(a);
        }
        window.URL.revokeObjectURL(url);
    };



export const selectFeatureFromRow = (
    row: TableDataRow
) => scopeOption()
    .let('info',
        getCurrentLayerInfo())
    .let('layer',
        ({ info }) => getLayerData(
            info.metadata.uniqueResourceIdentifier).getOrElse(none))
    .let('feature',
        ({ layer }) => fromNullable(
            layer.features.find(f => f.id === row.from)))
    .map(({ feature }) => {
        setCurrentFeature(feature);
        setLayout(AppLayout.MapAndTableAndFeature);
    });



logger('loaded');
