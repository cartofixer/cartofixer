import { DIV, H2 } from 'sdi/components/elements';
import { stringToParagraphs } from 'sdi/util';
import tr, { Translated } from 'sdi/locale';

import { setLayout } from 'view/src/events/app';
import { setPage } from 'view/src/events/legend';
import { resetInteraction } from 'view/src/events/map';
import { AppLayout } from 'view/src/shape/types';
import { makeLabelAndIcon } from '../button';
import { getGeometryType } from 'view/src/queries/harvest';
import { clearHarvested, clearGeometry, clearGeometryType } from 'view/src/events/harvest';


const buttonCancel = makeLabelAndIcon('cancel', 2, 'times', () => tr.core('cancel'));

const renderTitle = () => H2({}, tr.view('harvestTitle'));

const wrapInfo =
    (t: Translated) => DIV({ className: 'helptext' }, stringToParagraphs(t));

const renderInfo =
    () => {
        const gt = getGeometryType().getOrElse('Polygon');
        switch (gt) {
            case 'Point':
            case 'MultiPoint': return wrapInfo(tr.view('harvestInfoPoint'));
            case 'LineString':
            case 'MultiLineString': return wrapInfo(tr.view('harvestInfoLine'));
            case 'Polygon':
            case 'MultiPolygon': return wrapInfo(tr.view('harvestInfoPolygon'));
        }
    };


export const renderHarvest =
    () =>
        DIV({ className: 'xxx' },
            renderTitle(),
            renderInfo(),
            buttonCancel(() => {
                clearHarvested();
                clearGeometry();
                clearGeometryType();
                setLayout(AppLayout.MapAndInfo);
                setPage('data');
                resetInteraction();
            }),
        );


export default renderHarvest;
