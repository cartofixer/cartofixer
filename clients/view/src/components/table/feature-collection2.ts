/*
 *  Copyright (C) 2020 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';

import { DIV, SPAN, NODISPLAY } from 'sdi/components/elements';
import { table, } from 'sdi/components/table2';

import { tableQuery, layerSource, streamLoadingStatus } from '../../queries/table';
import { tableDispatch, selectFeatureFromRow, clearCurrentStreamData, exportCSV } from '../../events/table';
import { getCurrentName, getCurrentLayerInfo } from '../../queries/app';
import { unsetCurrentFeature, setLayout, } from '../../events/app';
import { AppLayout } from '../../shape/types';
import { makeIcon } from '../button';
import { none, some, fromNullable } from 'fp-ts/lib/Option';
import tr, { fromRecord } from 'sdi/locale';

const logger = debug('sdi:table/feature-collection');

const closeButton = makeIcon('close', 3, 'times');


const renderExportCSV =
    () => getCurrentLayerInfo()
        .map(s => fromNullable(s.info)
            .fold(
                NODISPLAY(),
                i => fromNullable(i.layerInfoExtra)
                    .fold(
                        NODISPLAY(),
                        extra => extra.exportable ? DIV({ className: 'table-download' }, SPAN({
                            className: 'dl-item',
                            onClick: () => exportCSV(
                                s.metadata,
                                i.id,
                                fromRecord(s.name)
                            ),
                        }, 'csv')) : NODISPLAY()),
            ),
        ).getOrElse(NODISPLAY());


const renderStreamingIndicator =
    () => {
        const s = streamLoadingStatus();
        switch (s) {
            case 'error': return some(DIV({}, '~error'));
            case 'loading': return some(DIV({},
                SPAN({ className: 'loader-spinner' }), tr.core('loadingData')));
            case 'no-stream': return none;
            case 'loaded': return none;
        }
    };


const toolbar =
    () =>
        DIV({ className: 'table-toolbar', key: 'table-toolbar' },
            DIV({ className: 'table-title' }, getCurrentName().getOrElse('...')),
            DIV({ className: 'table-download' }, renderExportCSV()),
            renderStreamingIndicator(),
            closeButton(() => {
                unsetCurrentFeature();
                setLayout(AppLayout.MapFS);
            }));


const renderTable = table(
    tableDispatch,
    tableQuery,
    selectFeatureFromRow,
    clearCurrentStreamData,
);

export default () => renderTable(layerSource(), toolbar());

logger('loaded');

