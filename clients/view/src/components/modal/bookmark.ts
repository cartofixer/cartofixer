import { DIV, H2 } from 'sdi/components/elements';
import { makeLabel } from 'sdi/components/button';
import tr from 'sdi/locale';
import { inputText } from 'sdi/components/input';
import { getBookmarkName } from 'view/src/queries/bookmark';
import { setBookmarkName } from 'view/src/events/bookmark';

const closeModalButton = makeLabel('close', 2, () => tr.core('close'));

const header = () => H2({}, tr.view('editBookmark'));

const footer = (close: () => void) => DIV({}, closeModalButton(close));

const body = () =>
    inputText(() => getBookmarkName().getOrElse(''), setBookmarkName);

export const render = { header, footer, body };
