
/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';

import { DIV, SPAN, H2 } from 'sdi/components/elements';
import { renderScaleline } from 'sdi/map/controls';
import { create, IMapOptions, FeaturePath, singleSelectOptions } from 'sdi/map';
import tr, { fromRecord } from 'sdi/locale';
import { MessageRecord, Feature, IMapBaseLayer } from 'sdi/source';
import { markdown } from 'sdi/ports/marked';

import {
    getCurrentBaseLayer,
    getMapInfo,
    getCurrentFeature,
    getCurrentLayer,
} from '../queries/app';
import {
    setCurrentFeatureById,
    setLayout,
    unsetCurrentFeature,
    signalReadyMap,
} from '../events/app';
import { setPage } from '../events/legend';
import {
    getInteraction,
    getInteractionMode,
    getLoading,
    getPrintRequest,
    getScaleLine,
    getView,
} from '../queries/map';
import {
    endMark,
    measureEvents,
    scalelineEvents,
    setExtractCollection,
    setPointerPosition,
    setPrintResponse,
    startMark,
    stopPointerPosition,
    trackerEvents,
    updateLoading,
    viewEvents,
} from '../events/map';
import { AppLayout } from '../shape/types';

import geocoder from './legend-tools/geocoder';
import { setGeometry, endDrawing } from '../events/harvest';
import { getGeometryType } from '../queries/harvest';
import { fromNullable } from 'fp-ts/lib/Option';
// import baseSwitch from './base-layer-switch';

const logger = debug('sdi:comp/map');
export const mapName = 'main-view';


const options: IMapOptions = {
    element: null,
    getBaseLayer: getCurrentBaseLayer,
    getMapInfo,
    getView,

    updateView: viewEvents.updateMapView,
    setScaleLine: scalelineEvents.setScaleLine,
    setLoading: updateLoading,
};

let mapSetTarget: (t: Element | null) => void;
let mapUpdate: () => void;


const selectFeature =
    (lid: string, fid: string | number) => {
        setCurrentFeatureById(lid, fid);
        setLayout(AppLayout.MapAndFeature);
    };

const clearSelection =
    () => {
        unsetCurrentFeature();
        setLayout(AppLayout.MapAndInfo);
        setPage('info');
    };


const getSelected =
    () =>
        getCurrentFeature()
            .fold<FeaturePath>(
                { featureId: null, layerId: null },
                f => ({ featureId: f.id, layerId: getCurrentLayer() })
            );


const prepareGeometryForHarvesting =
    (f: Feature) => {
        setGeometry(f.geometry);
        endDrawing();
        setLayout(AppLayout.Query);
    };



const attachMap =
    () =>
        (element: HTMLElement | null) => {
            // logger(`attachMap ${typeof element}`);
            if (!mapUpdate) {
                const {
                    update,
                    setTarget,
                    selectable,
                    measurable,
                    trackable,
                    extractable,
                    markable,
                    highlightable,
                    printable,
                    positionable,
                    editable,
                } = create(mapName, { ...options, element });
                mapSetTarget = setTarget;
                mapUpdate = update;
                signalReadyMap();

                selectable(singleSelectOptions({
                    selectFeature,
                    clearSelection,
                    getSelected,
                }), getInteraction);

                measurable({
                    updateMeasureCoordinates: measureEvents.updateMeasureCoordinates,
                    stopMeasuring: measureEvents.stopMeasure,
                }, getInteraction);

                trackable({
                    resetTrack: trackerEvents.resetTrack,
                    setCenter: center => (viewEvents.updateMapView(
                        { dirty: 'geo', center })),
                    updateTrack: trackerEvents.updateTrack,
                }, getInteraction);

                extractable({
                    setCollection: setExtractCollection,
                }, getInteraction);

                markable({ startMark, endMark }, getInteraction);

                highlightable(getSelected);

                printable({
                    getRequest: getPrintRequest,
                    setResponse: setPrintResponse,
                }, getInteraction);

                positionable({
                    setPosition: setPointerPosition,
                    stopPosition: stopPointerPosition,
                }, getInteraction);

                editable({
                    addFeature: prepareGeometryForHarvesting,
                    setGeometry: () => void 0,
                    getCurrentLayerId: () => 'query-geometry',
                    getGeometryType: () => getGeometryType().getOrElse('Polygon'),
                }, getInteraction);

            }
            if (element) {
                mapSetTarget(element);
            }
            else {
                mapSetTarget(null);
            }
        };

const renderLoading =
    (ms: Readonly<MessageRecord[]>) => DIV({
        className: `loading-layer-wrapper ${ms.length === 0 ? 'hidden' : ''}`,
    },
        H2({}, tr.view('loadingData')),
        ms.map(
            r => DIV({
                className: 'loading-layer',
                key: fromRecord(r),
            },
                SPAN({ className: 'loader-spinner' }),
                fromRecord(r))));

const renderAttribution = (
    bl: IMapBaseLayer
) => DIV({ className: 'attribution' }, tr.core('baseMap'), markdown(fromRecord(bl.attribution)));


const render =
    () => {

        if (mapUpdate) {
            mapUpdate();
        }



        return DIV({
            className: `map-wrapper map-interaction-${getInteractionMode()}`
        },
            DIV({
                key: '__this_is_a_unique_map__',
                className: 'map',
                ref: attachMap(),
            }),
            renderLoading(getLoading()),
            geocoder(),
            // baseSwitch(),
            renderScaleline(getScaleLine()),
            fromNullable(getCurrentBaseLayer()).map(renderAttribution),
        );
    };


export default render;

logger('loaded');
