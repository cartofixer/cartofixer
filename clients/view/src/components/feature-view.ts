

import { ILayerInfo, Feature, getMessageRecord } from 'sdi/source';
import { DIV, H1 } from 'sdi/components/elements';
import tr, { fromRecord } from 'sdi/locale';
import { renderConfig, renderDefault as defaultView } from 'sdi/components/feature-view';


import {
    getCurrentFeature,
    getDatasetMetadataOption,
    getMapInfoOption,
    getCurrentInfo,
} from '../queries/app';
// import appEvents from '../events/app';
// import legendEvents from '../events/legend';
// import { AppLayout } from '../shape/types';
import { viewEvents } from '../events/map';
import { makeLabelAndIcon } from './button';


const zoomBtn = makeLabelAndIcon('zoomOnFeature', 2, 'dot-circle-o', () => tr.view('zoomOnFeature'));

const renderZoom =
    (feature: Feature) =>
        zoomBtn(() => viewEvents.updateMapView({
            dirty: 'geo/feature',
            feature,
        }));


const zoomToFeature =
    () =>
        getCurrentFeature()
            .map(renderZoom);


// export const switcher =
//     () =>
//         DIV({ className: 'switcher infos' },
//             DIV({
//                 className: `switch-legend`,
//                 title: tr.view('mapLegend'),
//                 onClick: () => {
//                     appEvents.setLayout(AppLayout.MapFS);
//                     appEvents.unsetCurrentFeature();
//                     legendEvents.setPage('legend');
//                 },
//             }),
//             zoomToFeature());



const noView = () => DIV({ className: 'sidebar-main feature-view no' });



export const renderDefault =
    () =>
        getCurrentFeature()
            .fold(noView(), defaultView);

const featureHeader =
    (info: ILayerInfo) =>
        getDatasetMetadataOption(info.metadataId)
            .map(
                md => DIV({ className: 'sidebar-header feature-header' },
                    H1({}, getMapInfoOption()
                        .fold('', mapInfo => fromRecord(mapInfo.title) as string)),
                    DIV({ className: 'layer-name' }, fromRecord(getMessageRecord(md.resourceTitle))),
                ),
            );

const withInfo =
    (info: ILayerInfo) =>
        getCurrentFeature()
            .fold(noView(),
                feature => DIV({ className: 'sidebar-right feature-info-page' },
                    featureHeader(info),
                    zoomToFeature(),
                    renderConfig(info.featureViewOptions, feature)));



const render =
    () =>
        getCurrentInfo()
            .fold(noView(), withInfo);


export default render;
