import * as debug from 'debug';

import { DIV, H1, H2, NODISPLAY, H3 } from 'sdi/components/elements';
import tr, { fromRecord, formatDate } from 'sdi/locale';
import { getMessageRecord, FeatureCollection, IMapInfo, ILayerInfo } from 'sdi/source';

import { request, endHarvest, printHarvest, removeHarvest } from 'view/src/events/harvest';
import { getMapInfoOption, getDatasetMetadataOption } from 'view/src/queries/app';
import { makeLabelAndIcon, makeLabel } from '../button';
import { findHarvest, getGeometry, getHarvests } from 'view/src/queries/harvest';
import { renderResults } from './results';
import { withoutBookmarks } from 'view/src/queries/bookmark';
import { getLinks } from 'view/src/queries/map';
import minimap from './geometry';
import { scopeOption } from 'sdi/lib';

const logger = debug('sdi:query');


export interface HarvestedLayerInitial {
    tag: 'initial';
    metadataId: string;
}

export interface HarvestedLayerLoading {
    tag: 'loading';
    start: number;
    metadataId: string;
}

export interface HarvestedLayerLoaded {
    tag: 'loaded';
    start: number;
    end: number;
    metadataId: string;
    data: FeatureCollection;
}

export interface HarvestedLayerError {
    tag: 'error';
    start: number;
    end: number;
    metadataId: string;
    error: string;
}

export type HarvestedLayer =
    | HarvestedLayerInitial
    | HarvestedLayerLoading
    | HarvestedLayerError
    | HarvestedLayerLoaded
    ;

const btnCancel = makeLabelAndIcon('close', 2, 'times', () => tr.core('cancel'));
const btnPrint = makeLabelAndIcon('confirm', 1, 'print', () => tr.core('print'));
const btnSelectAll = makeLabel('select', 2, () => tr.view('querySelectAll'));
const btnDeselectAll = makeLabel('select', 2, () => tr.view('queryDeselectAll'));


const queryToolTitle = () => H1({}, tr.view('queryTitle'));

const actions =
    () => DIV({ className: 'query-actions' },
        btnCancel(endHarvest),
        btnPrint(printHarvest),
    );


const helptxtSelect =
    () =>
        DIV({ className: 'helptext' }, tr.view('queryHelpText2'));


const renderTitle =
    (t: string) =>
        DIV({ className: 'map-card__title' }, t);


const renderUpdated =
    (u: string) =>
        DIV({ className: 'map-card__updated' }, tr.view('lastModified') + u);


const foldSelected =
    <T>(no: () => T, yes: () => T) =>
        (mid: string) =>
            findHarvest(mid).foldL(no, yes);


const classSelected = foldSelected(
    () => 'map-card__layer unselected',
    () => 'map-card__layer selected');


const renderLayer =
    (l: ILayerInfo) => {
        const key = `query-select-layer-${l.id}`;
        const className = classSelected(l.metadataId);
        const onClick = foldSelected(
            () => () => { logger(`request ${l.metadataId}`); request(l.metadataId); },
            () => () => { logger(`removeHarvest ${l.metadataId}`); removeHarvest(l.metadataId); },
        )(l.metadataId);
        const name =
            getDatasetMetadataOption(l.metadataId)
                .fold(
                    l.id,
                    m => fromRecord(getMessageRecord(m.resourceTitle)));

        return DIV({ key, onClick, className }, name);
    };


const renderMapInfo =
    (info: IMapInfo) => {
        const key = `select-map-info-${info.id}`;
        const title = renderTitle(fromRecord(info.title));
        const updated = renderUpdated(formatDate(new Date(info.lastModified)));
        const selectAll = btnSelectAll(() => withoutBookmarks(info.layers).map(l => request(l.metadataId)));
        const deselectAll = btnDeselectAll(() => withoutBookmarks(info.layers).map(l => removeHarvest(l.metadataId)));
        const layers = withoutBookmarks(info.layers).map(renderLayer);
        return (
            DIV({ className: 'map-card map-card--select', key },
                title,
                updated,
                selectAll,
                deselectAll,
                layers)
        );
    };


const renderCurrentMap =
    () => getMapInfoOption().map(renderMapInfo);


const renderLinkedMaps =
    () =>
        scopeOption()
            .let('forward', getLinks('forward'))
            .let('backward', getLinks('backward'))
            .map(
                ({ forward, backward }) =>
                    DIV({}, forward.concat(backward).map(renderMapInfo)));


const layerSelector =
    () =>
        DIV({ className: 'layer-selector' },
            H2({}, tr.view('queryiedData')),
            helptxtSelect(),
            H3({}, tr.view('currentMap')),
            renderCurrentMap(),
            H3({}, tr.view('relatedMapsLabel')),
            renderLinkedMaps(),
        );


const renderLayerFromHarvest =
    (h: HarvestedLayer) => {
        const key = `renderLayerFromHarvest-${h.metadataId}`;
        const name =
            getDatasetMetadataOption(h.metadataId)
                .fold(
                    h.metadataId,
                    m => fromRecord(getMessageRecord(m.resourceTitle)));
        switch (h.tag) {
            case 'loaded': return DIV({ key, className: 'selected-layer--print' }, name);
            default: return NODISPLAY({ key });
        }
    };

const layerSelectorPrint =
    () =>
        DIV({ className: 'layer-selector' },
            H2({}, tr.view('queryiedData')),
            getHarvests().map(renderLayerFromHarvest),
        );


const resultMapIllu =
    () =>
        DIV({ className: 'result__map-illu-wrapper' },
            H2({}, tr.view('queryZone')),
            DIV({ className: 'result__map-illu' },
                getGeometry()
                    .map(
                        geom => minimap(geom),
                    )),
        );


export const render =
    (print = false) => {
        if (print) {
            return (
                DIV({},
                    queryToolTitle(),
                    DIV({ className: 'selector-wrapper' },
                        resultMapIllu(),
                        layerSelectorPrint(),
                    ),
                    H2({}, tr.view('harvestResults')),
                    renderResults(),
                )
            );
        }

        return (
            DIV({},
                queryToolTitle(),
                actions(),
                DIV({ className: 'selector-wrapper' },
                    resultMapIllu(),
                    layerSelector(),
                ),
                H2({}, tr.view('harvestResults')),
                renderResults(),
            )
        );
    };

export default render;

logger('loaded');
