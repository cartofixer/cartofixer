/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
import { none } from 'fp-ts/lib/Option';
import { ReactNode } from 'react';

import { DIV, SPAN, H1, H2, NODISPLAY, IMG, NodeOrOptional, DETAILS, SUMMARY } from 'sdi/components/elements';
import {
    getMessageRecord,
    LayerGroup,
    ILayerInfo,
    IMapInfo,
    GeometryType,
    foldRemote,
    MdPointOfContact,
    ResponsibleOrganisation,
} from 'sdi/source';
import tr, { fromRecord, formatDate } from 'sdi/locale';
import { translateMapBaseLayer } from 'sdi/util';
import { divTooltipLeft, divTooltipTopRight } from 'sdi/components/tooltip';

import queries from '../../queries/legend';
import { setPage, setWMSLegendVisible } from '../../events/legend';
import {
    setLayerVisibility,
    setCurrentLayer,
    setLayout,
    unsetCurrentFeature,
} from '../../events/app';
import {
    getMapInfoOption,
    getDatasetMetadataOption,
    getLayerData,
    getCurrentBaseLayer,
    getPointOfContact,
    getResponsibleOrg,
} from '../../queries/app';
import legendItem from './legend-item';
import info from './../map-info';
import { AppLayout, LegendPage } from '../../shape/types';
import { ViewMessageKey } from '../../locale';
import webservices from '../legend-tools/webservices';
import print from '../legend-tools/print';
import share from '../legend-tools/share';
import location from '../legend-tools/location';
import measure from '../legend-tools/measure';
import harvest from '../legend-tools/harvest';
import { helpText } from 'sdi/components/helptext';
import { bookmarkLayerID, notABookmarkLayer } from '../bookmark/index';
import { makeLabel } from '../button';
import { startDrawing } from 'view/src/events/harvest';

const logger = debug('sdi:legend');

const harvestPointButton = makeLabel('info', 2, () =>
    tr.view('harvestInitiatePoint')
);
const harvestLineButton = makeLabel('info', 2, () =>
    tr.view('harvestInitiateLine')
);
const harvestPolygonButton = makeLabel('info', 2, () =>
    tr.view('harvestInitiatePolygon')
);

interface Group {
    g: LayerGroup | null;
    layers: ILayerInfo[];
}

const groupItems = (layers: ILayerInfo[]) =>
    layers
        .slice()
        .reverse()
        .reduce<Group[]>((acc, info) => {
            return notABookmarkLayer(info).fold(acc, info => {
                const ln = acc.length;
                if (ln === 0) {
                    return [
                        {
                            g: info.group,
                            layers: [info],
                        },
                    ];
                }
                const prevGroup = acc[ln - 1];
                const cg = info.group;
                const pg = prevGroup.g;
                // Cases:
                // info.group == null && prevGroup.g == null => append
                // info.group != null && prevGroup.g != null && info.group.id == prevGroup.id => append
                if (
                    (cg === null && pg === null) ||
                    (cg !== null && pg !== null && cg.id === pg.id)
                ) {
                    prevGroup.layers.push(info);
                    return acc;
                }
                // info.group == null && prevGroup.g != null => new
                // info.group != null && prevGroup.g == null => new
                // info.group != null && prevGroup.g != null && info.group.id != prevGroup.id => new

                return acc.concat({
                    g: cg,
                    layers: [info],
                });
            });
        }, []);

const renderLegend = (groups: Group[]) =>
    groups.map((group, key) => {
        const layers = group.layers.filter(l => l.visible === true);

        if (layers.length === 0) {
            return DIV({ key }); // FIXME - we can do better than that
        }
        const items = layers.map(legendItem);
        if (group.g !== null) {
            return DIV(
                { key, className: 'legend-group named' },
                DIV(
                    { className: 'legend-group-title' },
                    fromRecord(group.g.name)
                ),
                DIV({ className: 'legend-group-items' }, ...items)
            );
        }
        return DIV({ key, className: 'legend-group anonymous' }, ...items);
    });

type InfoRender = (info: ILayerInfo) => ReactNode;

const branchInfo = (a: InfoRender, b: InfoRender) => (
    c: boolean,
    info: ILayerInfo
) => {
    if (c) {
        return a(info);
    }
    return b(info);
};

const dataActions = branchInfo(
    (
        info: ILayerInfo // normal
    ) =>
        DIV(
            { className: 'layer-actions' },
            divTooltipTopRight(
                tr.view('visible'),
                {},
                SPAN({
                    className: info.visible ? 'visible' : 'not-visible',
                    onClick: () => {
                        setLayerVisibility(info.id, !info.visible);
                    },
                })
            ),
            divTooltipTopRight(
                tr.view('tooltip:dataAccess'),
                {},
                SPAN({
                    className: 'table',
                    onClick: () => {
                        setCurrentLayer(info.id);
                        setLayout(AppLayout.MapAndTable);
                    },
                })
            )
        ),
    (
        info: ILayerInfo // bookmark
    ) =>
        DIV(
            { className: 'layer-actions bookmark' },
            divTooltipTopRight(
                tr.view('visible'),
                {},
                SPAN({
                    className: info.visible ? 'visible' : 'hidden',
                    onClick: () => {
                        setLayerVisibility(info.id, !info.visible);
                    },
                })
            )
        )
);

const dataTitle = branchInfo(
    (
        info: ILayerInfo // normal
    ) =>
        DIV(
            { className: 'layer-title' },
            getDatasetMetadataOption(info.metadataId).fold(
                info.id,
                md =>
                    getLayerData(md.uniqueResourceIdentifier).fold<ReactNode>(
                        err =>
                            SPAN(
                                {
                                    className: 'error',
                                    title: err,
                                },
                                fromRecord(getMessageRecord(md.resourceTitle))
                            ),
                        () =>
                            SPAN(
                                {},
                                fromRecord(getMessageRecord(md.resourceTitle))
                            )
                    )
            )
        ),
    (
        _info: ILayerInfo // bookmark
    ) => DIV({ className: 'layer-title bookmark' }, tr.view('bookmarks'))
);


const renderRemoteOrg = foldRemote<ResponsibleOrganisation, string, NodeOrOptional>(
    () => none,
    () => DIV({}, 'loading org'),
    (err) => DIV({}, err),
    (ro) => DIV({ className: 'ro-block' },
        DIV({ className: 'ro-name' }, fromRecord(getMessageRecord(ro.organisationName))),
        DIV({ className: 'ro-contact-name' }, ro.contactName),
        DIV({ className: 'ro-role-code' }, ro.roleCode)),
);

export const renderOrg = (id: number) => getResponsibleOrg(id).map(renderRemoteOrg);

const renderRemotePOC = foldRemote<MdPointOfContact, string, NodeOrOptional>(
    () => none,
    () => DIV({}, 'loading poc'),
    (err) => DIV({}, err),
    (poc) => DIV({ className: 'poc-block' },
        DIV({className: 'metadata-label'}, tr.view('pocLabel')),
        DIV({ className: 'poc-name' }, poc.contactName),
        DIV({ className: 'poc-org' }, fromRecord(getMessageRecord(poc.organisationName))),
        DIV({ className: 'poc-email' }, poc.email)),
);

export const renderPOC = (id: number) => getPointOfContact(id).map(renderRemotePOC);

const dataMeta = (info: ILayerInfo) => DETAILS({className: 'metadata-block-meta' },
    SUMMARY({}, tr.view('metadataLabel')),
    getDatasetMetadataOption(info.metadataId)
        .map(md => DIV({className: 'metadata-block-body'},
                        DIV({ className: 'metadata-block-abstract' },
                            DIV({className: 'metadata-label'}, tr.view('abstractLabel')),
                            fromRecord(getMessageRecord(md.resourceAbstract))),
                        DIV({ className: 'metadata-block-abstract' }, ...md.metadataPointOfContact.map(renderPOC)),
                        // DIV({ className: 'metadata-block-abstract' }, ...md.responsibleOrganisation.map(renderOrg)),
        )),
);

const dataItem = (info: ILayerInfo) =>
    DIV(
        { className: 'layer-item' },
        dataActions(info.id !== bookmarkLayerID, info),
        DIV({className: 'layer-infos'},
                dataTitle(info.id !== bookmarkLayerID, info),
                notABookmarkLayer(info).map(dataMeta),
            ),
    );

const renderData = (groups: Group[]) =>
    groups.map((group) => {
        const items = group.layers.map(dataItem);
        if (group.g !== null) {
            return DIV(
                { className: 'legend-group named' },
                DIV(
                    { className: 'legend-group-title' },
                    fromRecord(group.g.name)
                ),
                DIV({ className: 'legend-group-items' }, ...items)
            );
        }
        return DIV({ className: 'legend-group anonymous' }, ...items);
    });

const initiateHarvest = (gt: GeometryType) => {
    startDrawing(gt);
    setPage('harvest');
};

const renderHarvestBox = () =>
    DIV(
        { className: 'box box--harvest' },
        H2({}, tr.view('harvestTitle')),
        harvestPointButton(() => initiateHarvest('Point')),
        harvestLineButton(() => initiateHarvest('LineString')),
        harvestPolygonButton(() => initiateHarvest('Polygon'))
    );

const switchItem = (
    p: LegendPage,
    tk: ViewMessageKey,
    currentPage: LegendPage
) => {
    return divTooltipLeft(
        tr.view(tk),
        {
            className: `switch-item switch-${p} ${
                p === currentPage ? 'active' : ''
                }`,
            onClick: () => {
                setLayout(AppLayout.MapFS);
                unsetCurrentFeature();
                setPage(p);
            },
        },
        DIV({ className: 'picto' })
    );
};

export const switcher = () => {
    const currentPage = queries.currentPage();
    return DIV(
        {
            className: 'switcher',
        },
        switchItem('info', 'tooltip:info', currentPage),
        switchItem('data', 'tooltip:dataAndSearch', currentPage),
        switchItem('base-map', 'tooltip:base-map', currentPage),
        switchItem('print', 'tooltip:print', currentPage),
        switchItem('share', 'tooltip:ishare', currentPage),
        switchItem('measure', 'tooltip:measure', currentPage),
        switchItem('locate', 'tooltip:locate', currentPage)
    );
};

const wmsLegend = () => {
    const bl = getCurrentBaseLayer();
    if (null === bl) {
        return NODISPLAY();
    }
    if (queries.displayWMSLegend()) {
        const tl = translateMapBaseLayer(bl);
        const lyrs = tl.params.LAYERS.split(',').reverse();
        const legends = lyrs.map(lyr =>
            DIV(
                {
                    className: 'wms-legend-item',
                    key: `legend-image-${tl.url}-${lyr}`,
                },
                IMG({
                    src: `${tl.url}?SERVICE=WMS&REQUEST=GetLegendGraphic&VERSION=${tl.params.VERSION}&FORMAT=image/png&WIDTH=20&HEIGHT=20&LAYER=${lyr}`,
                })
            )
        );

        return DIV(
            { className: 'wms-legend-wrapper' },
            DIV(
                {
                    className: 'wms-legend-switch opened',
                    onClick: () => setWMSLegendVisible(false),
                },
                tr.view('wmsLegendHide')
            ),
            ...legends
        );
    }

    return DIV(
        { className: 'wms-legend-wrapper' },
        DIV(
            {
                className: 'wms-legend-switch closed',
                onClick: () => setWMSLegendVisible(true),
            },
            tr.view('wmsLegendDisplay')
        )
    );
};

const wrapLegend = (...es: ReactNode[]) =>
    DIV({ className: 'sidebar-right map-legend' }, ...es);

const renderMapInfoHeader = (mapInfo: IMapInfo, p: LegendPage) =>
    DIV(
        {
            className: `sidebar-header legend-header-${p}`,
        },
        H1({}, fromRecord(mapInfo.title)),
        DIV(
            { className: 'map-date' },
            SPAN({ className: 'map-date-label' }, tr.view('lastModified')),
            ' ',
            SPAN(
                { className: 'map-date-value' },
                formatDate(new Date(mapInfo.lastModified))
            )
        )
    );

const renderMapInfo = (mapInfo: IMapInfo) =>
    wrapLegend(
        renderMapInfoHeader(mapInfo, 'info'),
        DIV({ className: 'sidebar-main legend-main' }, info()),
        DIV(
            { className: 'sidebar-main styles-wrapper' },
            H2({}, tr.view('mapLegend')),
            ...renderLegend(groupItems(mapInfo.layers)),
            wmsLegend()
        )
    );

const renderMapData = (mapInfo: IMapInfo) =>
    wrapLegend(
        renderMapInfoHeader(mapInfo, 'data'),
        DIV(
            { className: 'sidebar-main datas-wrapper' },
            H2({}, tr.view('mapData')),
            helpText(tr.view('helptext:mapdataTool')),
            ...renderData(groupItems(mapInfo.layers)),
            renderHarvestBox()
        )
    );

const legend = () => {
    const currentPage = queries.currentPage();
    return getMapInfoOption().map(mapInfo => {
        switch (currentPage) {
            case 'base-map':
                return wrapLegend(
                    renderMapInfoHeader(mapInfo, 'base-map'),
                    webservices()
                );
            case 'data':
                return renderMapData(mapInfo);
            case 'harvest':
                return wrapLegend(
                    renderMapInfoHeader(mapInfo, 'harvest'),
                    harvest()
                );
            case 'info':
                return renderMapInfo(mapInfo);
            case 'locate':
                return wrapLegend(
                    renderMapInfoHeader(mapInfo, 'locate'),
                    location()
                );
            case 'measure':
                return wrapLegend(
                    renderMapInfoHeader(mapInfo, 'measure'),
                    measure()
                );
            case 'print':
                return wrapLegend(
                    renderMapInfoHeader(mapInfo, 'print'),
                    print()
                );
            case 'share':
                return wrapLegend(
                    renderMapInfoHeader(mapInfo, 'share'),
                    share()
                );
        }
    });
};

export default legend;

logger('loaded');
