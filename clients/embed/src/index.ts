
import * as debug from 'debug';

import 'sdi/polyfill';
import { source, AppConfigIO, getMessage } from 'sdi/source';
import { IShape, configure, defaultShape } from 'sdi/shape';
import { defaultInteraction } from 'sdi/map';

import './shape';
import './locale';

import App from './app';
import { displayException } from 'sdi/app';

const logger = debug('sdi:index');



export const main =
    (SDI: any) => {

        AppConfigIO
            .decode(SDI)
            .fold(
                (errors) => {
                    const textErrors = errors.map(e => getMessage(e.value, e.context));
                    displayException(textErrors.join('\n'));
                },
                (config) => {
                    const initialState: IShape = {
                        ...defaultShape(config),
                        'app/user': SDI.user,
                        'app/root': SDI.root,
                        'app/api-root': SDI.api,
                        'app/csrf': SDI.csrf,
                        'app/route': SDI.args,
                        'app/lang': 'fr',

                        'app/layout': 'map',
                        'app/layerId': null,
                        'app/featureId': null,
                        'app/current-feature': null,

                        'data/user': null,
                        'data/alias': [],
                        'data/map': null,
                        'data/metadata': [],
                        'data/layer': [],

                        'port/map/scale': {
                            count: 0,
                            unit: '',
                            width: 0,
                        },

                        'port/map/view': {
                            dirty: 'geo',
                            srs: 'EPSG:3857',
                            // center: [485609, 6592756],
                            center: [4.36, 50.83],
                            rotation: 0,
                            zoom: 6,
                            feature: null,
                            extent: null,
                        },

                        'port/map/interaction': defaultInteraction(),

                        'data/baselayers': {},
                    };

                    try {
                        const start = source<IShape>(['app/lang']);
                        const store = start(initialState);
                        configure(store);
                        const app = App(store);
                        logger('start rendering');
                        app();
                    }
                    catch (err) {
                        displayException(`${err}`);
                    }
                });
    };


logger('loaded');
