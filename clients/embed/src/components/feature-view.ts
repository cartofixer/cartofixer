

import { ILayerInfo, Feature } from 'sdi/source';
import { DIV } from 'sdi/components/elements';
import tr from 'sdi/locale';
import { renderConfig, renderDefault as defaultView } from 'sdi/components/feature-view';


import { getCurrentFeature, getCurrentLayerInfo } from '../queries/app';
import { setLayout } from '../events/app';
import { viewEvents } from '../events/map';


const renderZoom =
    (feature: Feature) => DIV({
        className: `zoomOnFeature`,
        title: tr.embed('zoomOnFeature'),
        onClick: () => viewEvents.updateMapView({
            dirty: 'geo/feature',
            feature,
        }),
    });

const zoomToFeature =
    () =>
        getCurrentFeature().map(renderZoom);


export const switcher =
    () =>
        DIV({ className: 'switcher infos' },
            DIV({
                className: `switch-legend`,
                title: tr.embed('mapLegend'),
                onClick: () => {
                    setLayout('map');
                },
            }),
            zoomToFeature());



const noView = () => DIV({ className: 'feature-view no' });



export const renderDefault =
    () =>
        getCurrentFeature().fold(noView(), defaultView);

const withInfo =
    (info: ILayerInfo) =>
        getCurrentFeature()
            .fold(noView(),
                feature => renderConfig(
                    info.featureViewOptions, feature));



const render =
    () =>
        getCurrentLayerInfo().fold(noView(), withInfo);


export default render;
