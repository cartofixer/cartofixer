
import * as debug from 'debug';

import { A, DIV, NodeOrOptional } from 'sdi/components/elements';
import { loop, getApiUrl } from 'sdi/app';

import { initMap, loadBaseLayer, loadAlias } from './events/app';

import map from './components/map';
import legend from './components/legend';
import feature from './components/feature-view';
import { getLayout } from './queries/app';
import tr from 'sdi/locale';

const logger = debug('sdi:app');


const wrappedMain =
    (name: string, ...elements: NodeOrOptional[]) => (
        DIV({ className: 'embed' },
            DIV({ className: `main ${name}` }, ...elements),
            A({
                href: 'https://cartofixer.be',
                target: '_blank',
                title: 'cartofixer.be',
                className: 'logo-cartofixer'
            },
                'cartofixer',
            ),
        )
    );


const render =
    () => {
        const layout = getLayout();
        switch (layout) {
            case 'map': return wrappedMain('main', map(), legend());
            case 'map-and-feature': return wrappedMain('main', map(), feature());
        }
    };

const baseLayers = [
    'urbis.irisnet.be/urbis_gray',
    'urbis.irisnet.be/ortho_2016',
];

const effects =
    () => {
        baseLayers.forEach(id =>
            loadBaseLayer(id, getApiUrl(`wmsconfig/${id}`)));
        loadAlias(getApiUrl('alias'));
        initMap();
        tr.init_edited();
    };

const app = loop('embed-app', render, effects);
export default app;

logger('loaded');
