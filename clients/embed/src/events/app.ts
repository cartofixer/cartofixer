import { fromNullable, some } from 'fp-ts/lib/Option';
import { right } from 'fp-ts/lib/Either';

import { queryK, dispatch, observe, query } from 'sdi/shape';
import { scopeOption } from 'sdi/lib';
import { getApiUrl, getRootUrl } from 'sdi/app';
import { IMapInfo, getMessageRecord, Inspire, ILayerInfo } from 'sdi/source';
import { removeLayerAll, addLayer, addFeaturesToLayer } from 'sdi/map';

import { fetchUser, fetchMap, fetchDatasetMetadata, fetchLayer, fetchBaseLayer, fetchAlias } from '../remote';
import { viewEvents } from './map';
import { AppLayout } from '../shape';
import { mapName } from '../components/map';
import { getView } from '../queries/map';
import { getMapInfo, getLayer } from '../queries/app';


export const loadUser =
    (url: string) =>
        fetchUser(url)
            .then((user) => {
                dispatch('data/user', () => user);
            });

export const setLayout =
    (l: AppLayout) => dispatch('app/layout', () => l);

export const setCurrentFeatureById =
    (lid: string, fid: string | number) => {
        dispatch('app/layerId', () => lid);
        dispatch('app/featureId', () => fid);
    };

export const unsetCurrentFeature =
    () => {
        dispatch('app/layerId', () => null);
        dispatch('app/featureId', () => null);
    };

// Very simple route mod

const getRoute = queryK('app/route');

const cleanRoute =
    () => getRoute()
        .reduce((acc, s) => {
            if (s.length > 0) {
                return acc.concat([s]);
            }
            return acc;
        }, [] as string[]);

const getNumber =
    (s?: string) => {
        if (s) {
            const n = parseFloat(s);
            if (!Number.isNaN(n)) {
                return n;
            }
        }
        return null;
    };

const setMapView =
    () => {
        const r = cleanRoute();
        scopeOption()
            .let('lat', fromNullable(getNumber(r[1])))
            .let('lon', fromNullable(getNumber(r[2])))
            .let('zoom', fromNullable(getNumber(r[3])))
            .map(({ lat, lon, zoom }) => {
                viewEvents.updateMapView({
                    dirty: 'geo',
                    center: [lat, lon],
                    zoom,
                });
            });
    };


const loadLayer =
    (info: ILayerInfo, metadata: Inspire, bbox: number[] | null) => {
        const bboxParams = (bbox) ? `?bbox=${bbox[0]},${bbox[1]},${bbox[2]},${bbox[3]}` : '';

        fetchLayer(metadata.uniqueResourceIdentifier + bboxParams)
            .then((layer) => {
                dispatch('data/layer', (state) => {
                    const layerIndex = state.findIndex(x => x[0] === info.id);

                    if (layerIndex >= 0) {
                        state[layerIndex] = [info.id, layer];
                    }
                    else {
                        state.push([info.id, layer]);
                    }

                    return state;
                });

                getLayer(info.id).fold(
                    addLayer(mapName,
                        () => some({
                            name: getMessageRecord(metadata.resourceTitle),
                            info,
                            metadata,
                        }),
                        () => right(some(layer)),
                    ),
                    () => {
                        if (bbox) {
                            return addFeaturesToLayer(
                                mapName,
                                info,
                                layer.features,
                            );
                        }
                        return null;
                    },
                );
            });
    };

observe('port/map/view',
    () => {
        getMapInfo().fold(
            null,
            (info) => {
                info.layers.forEach((layerInfo) => {
                    if (layerInfo.visible) {
                        const url = getApiUrl(`metadatas/${layerInfo.metadataId}`);
                        fetchDatasetMetadata(url)
                            .then((md) => {
                                dispatch('data/metadata', state => state.concat([md]));

                                loadLayerDataAccordingToZoom(layerInfo, md);
                            });
                    }
                });
            },
        );
    });

export const loadBaseLayer =
    (id: string, url: string) => {
        fetchBaseLayer(url)
            .then((bl) => {
                dispatch('data/baselayers', state => ({ ...state, [id]: bl }));
            });
    };

const loadMetadata =
    (info: IMapInfo) => {
        removeLayerAll(mapName);
        info.layers.forEach((layerInfo) => {
            if (layerInfo.visible) {
                const url = getApiUrl(`metadatas/${layerInfo.metadataId}`);
                fetchDatasetMetadata(url)
                    .then((md) => {
                        dispatch('data/metadata', state => state.concat([md]));

                        loadLayerDataAccordingToZoom(layerInfo, md);
                    });
            }
        });
    };

const isZoomVisible = (zoom: number, minZoom: number, maxZoom: number): boolean => {
    return ((zoom > minZoom) && (zoom < maxZoom));
};
const loadLayerDataAccordingToZoom = (layer: ILayerInfo, metadata: Inspire) => {
    const zoom = getView().zoom;
    const minZoom = fromNullable(layer.minZoom).getOrElse(0);
    const maxZoom = fromNullable(layer.maxZoom).getOrElse(30);

    if (isZoomVisible(zoom, minZoom, maxZoom)) {
        let extent = null;
        if (metadata.dataStreamUrl) {
            extent = getView().extent;
        }

        loadLayer(layer, metadata, extent);
    }
};

export const loadAlias =
    (url: string) =>
        fetchAlias(url)
            .then((alias) => {
                dispatch('data/alias', () => alias);
            });


const loadMap =
    (mid: string) =>
        fetchMap(getApiUrl(`maps/${mid}`))
            .then((info) => {
                dispatch('data/map', () => info);
                loadMetadata(info);
            }).catch((response) => {
                const user = query('app/user');

                if (response.status === 403 && !user) {
                    window.location.assign(getRootUrl(`login/embed/${mid}`));
                    return;
                }
            });


export const initMap =
    () => {
        const r = cleanRoute();
        if (r.length > 0) {
            loadMap(r[0]);
            setMapView();
        }
        else {
            // TODO
        }
    };
