! [illustration](../assets/anim/embed.gif)
_Example of an embedded card on a third party site_

This is a simplified version of _atlas_, which is used to visualize maps when they are embedded on a third party site.

This application presents the map, its title, description and legend, as well as the ability to select items on the map, and switching between language.

A link to the map in _atlas_ allows to visualize the map with all the tools of _atlas_.

## User guide

The complete user guide is available here : https://gitlab.com/cartofixer/cartofixer/-/wikis/home
