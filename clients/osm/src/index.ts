
import { displayException } from 'sdi/app';
import { configure, defaultShape, IShape } from 'sdi/shape';
import { AppConfigIO, getMessage, source } from 'sdi/source';
import { app } from './app';
import './locale';
import {
    defaultTimeserieShape,
    defaultAppShape,
    defaultMapState,
    defaultTableShape,
} from './shape';



export const main =
    (SDI: any) =>
        AppConfigIO
            .decode(SDI)
            .fold(
                (errors) => {
                    const textErrors = errors.map(e => getMessage(e.value, e.context));
                    displayException(textErrors.join('\n'));
                },
                (config) => {
                    const initialState: IShape = {
                        ...defaultShape(config),
                        ...defaultAppShape(),
                        ...defaultMapState(),
                        ...defaultTableShape(),
                        ...defaultTimeserieShape(),
                    };

                    const stateActions = source<IShape>([]);
                    const store = stateActions(initialState);
                    configure(store);
                    const start = app(config.args)(store);
                    start();
                });
