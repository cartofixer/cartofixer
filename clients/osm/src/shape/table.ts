import { IDataTable, initialTableState } from 'sdi/components/table';

declare module 'sdi/shape' {
    export interface IShape {
        'component/table': IDataTable;

    }
}


export const defaultTableShape =
    () => ({
        'component/table': initialTableState(),

    });
