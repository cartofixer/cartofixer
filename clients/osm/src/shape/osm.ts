import { GroupData } from '../remote/osm';
import { TableMode } from '../types';
import { Nullable } from 'sdi/util';

declare module 'sdi/shape' {
    export interface IShape {
        'osm/table/mode': TableMode;
        'osm/group/select': Nullable<GroupData>;
    }
}

export const defaultTimeserieShape = () => ({
    'data/osms': {},
    'osm/table/mode': 'per-station' as TableMode,
    'osm/group/select': null,
});
