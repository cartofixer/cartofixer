import * as debug from 'debug';
import { assignK } from 'sdi/shape';

const logger = debug('sdi:events/osm');

export const setTableMode = assignK('osm/table/mode');

logger('loaded');
