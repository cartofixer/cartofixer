import * as debug from 'debug';

import { dispatchK } from 'sdi/shape';
import { viewEventsFactory } from 'sdi/map';
const logger = debug('sdi:events/map');

export const updateView = viewEventsFactory(dispatchK('port/map/view'))
    .updateMapView;
export const setScaleLine = () => {};

logger('loaded');
