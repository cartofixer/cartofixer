import * as debug from 'debug';
import { index } from 'fp-ts/lib/Array';
import { Router } from 'sdi/router';
import { setLayout } from './app';
import * as io from 'io-ts';

const logger = debug('sdi:route');

// tslint:disable-next-line: variable-name
const RouteIO = io.union([
    io.literal('index'),
    io.literal('queryBuilder'),
    io.literal('metadataForm'),
]);
export type Route = io.TypeOf<typeof RouteIO>;

export const { home, route, navigate } = Router<Route>('osm');

export type Layout = Route;

// << route handlers

home('index', () => {
    setLayout('index');
});

route('queryBuilder', () => {
    setLayout('queryBuilder');
});

route('metadataForm', () => {
    setLayout('metadataForm');
});

/// route handlers >>

export const loadRoute = (initial: string[]) =>
    index(0, initial).map((prefix) =>
        RouteIO.decode(prefix).map((c) => navigate(c, initial.slice(1)))
    );

export const navigateIndex = () => navigate('index', []);
export const navigateQueryBuilder = () => navigate('queryBuilder', []);
export const navigateMetadataForm = () => navigate('metadataForm', []);

logger('loaded');
