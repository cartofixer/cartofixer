import { IMapInfo, IMapBaseLayer, Inspire, ILayerInfo } from 'sdi/source';
import { queryK } from 'sdi/shape';
import { waterMapId, ParameterType } from '../types';
import { nameToCode } from 'sdi/components/button/names';
import { SyntheticLayerInfo } from 'sdi/app';

export const getBaseLayer = (): IMapBaseLayer => ({
    name: {
        fr: 'Urbis Gray',
        nl: 'Urbis Gray',
    },
    attribution: {},
    srs: 'EPSG:31370',
    url: '/webservice/wmsproxy/urbis.irisnet.be',
    params: {
        VERSION: '1.1.1',
        LAYERS: {
            fr: 'urbisFRGray',
            nl: 'urbisNLGray',
        },
    },
});

export const getView = queryK('port/map/view');

const now = new Date().toISOString();

export const markerColor = (kind: ParameterType) => {
    switch (kind) {
        case 'surface':
            return '#6e8cff';
        case 'ground-quality':
            return '#c68700';
        case 'ground-quantity':
            return '#00729a';
    }
};

export const metadataTemplate = (kind: ParameterType): Inspire => ({
    id: kind,
    geometryType: 'Point',
    resourceTitle: { fr: kind, nl: kind },
    resourceAbstract: { fr: kind, nl: kind },
    uniqueResourceIdentifier: kind,
    topicCategory: [],
    keywords: [],
    geographicBoundingBox: { west: 0.0, north: 0.0, east: 0.0, south: 0.0 },
    temporalReference: { creation: now, revision: now },
    responsibleOrganisation: [1],
    metadataPointOfContact: [1],
    metadataDate: now,
    published: false,
    dataStreamUrl: null,
});

export const layerTemplate = (kind: ParameterType): ILayerInfo => ({
    id: kind,
    legend: null,
    group: null,
    metadataId: kind,
    visible: true,
    featureViewOptions: { type: 'default' },
    style: {
        kind: 'point-simple',
        marker: {
            color: markerColor(kind),
            size: 16,
            codePoint: nameToCode('dot-circle-o'),
        },
    },
    layerInfoExtra: null,
});

export const getMapInfo = (): IMapInfo => ({
    baseLayer: 'urbis.irisnet.be/urbis_gray',
    id: waterMapId,
    url: `/none`,
    lastModified: Date.now(),
    status: 'published',
    title: { fr: 'Water', nl: 'Water' },
    description: { fr: 'Water', nl: 'Water' },
    categories: [],
    attachments: [],
    layers: [
        layerTemplate('surface'),
        layerTemplate('ground-quality'),
        layerTemplate('ground-quantity'),
    ],
});

export const layerInfo = (kind: ParameterType): SyntheticLayerInfo => ({
    name: { fr: 'o', nl: 'o', en: 'o' },
    info: layerTemplate(kind),
    metadata: metadataTemplate(kind),
});
