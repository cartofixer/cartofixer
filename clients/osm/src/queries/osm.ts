import { queryK } from 'sdi/shape';
import debug = require('debug');
const logger = debug('sdi:module/sous-module');

/**
 * Returns an array filled with 'undefined'. This way the array can be filled
 * with a map or reduce
 * @param size
 */
export const mappableEmptyArray = (size: number): undefined[] =>
    Array(size).fill(undefined);

export const getTableMode = queryK('osm/table/mode');

logger('loaded');
