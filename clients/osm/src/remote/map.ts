import { fetchIO, FeatureCollectionIO } from 'sdi/source';
import { getApiUrl } from 'sdi/app';


export const fetchLayerSurface =
    () => fetchIO(FeatureCollectionIO, getApiUrl('geodata/water/surface/layer'));

export const fetchLayerGroundQuality =
    () => fetchIO(FeatureCollectionIO, getApiUrl('geodata/water/ground/layer/quality'));

export const fetchLayerGroundQuantity =
    () => fetchIO(FeatureCollectionIO, getApiUrl('geodata/water/ground/layer/quantity'));
