import * as io from 'io-ts';
import { MessageRecordIO } from 'sdi/source/io/io';
import { fetchIO } from 'sdi/source';
import { getApiUrl } from 'sdi/app';
import { fromNullable } from 'fp-ts/lib/Option';


// tslint:disable-next-line:variable-name
const GroupDataIO = io.interface({
    id: io.Integer,
    name: MessageRecordIO,
}, 'GroupDataIO');

// tslint:disable-next-line:variable-name
const NormIO = io.interface({
    options: io.dictionary(io.string, io.any),
    values: io.dictionary(io.string, io.number),
}, 'NormIO');

export type GroupData = io.TypeOf<typeof GroupDataIO>;
export type Norm = io.TypeOf<typeof NormIO>;


export const getNormOption = <T>(
    key: string,
) => (
    norm: Norm,
    ) => fromNullable<T>(norm.options[key])

// tslint:disable-next-line:variable-name
const ParameterIO = io.interface({
    id: io.Integer,
    code: io.union([io.string, io.null]),
    name: MessageRecordIO,
    unit: io.union([io.string, io.null]),
    groups: io.array(GroupDataIO),
    norms: io.array(NormIO),
}, 'ParameterIO');

// tslint:disable-next-line:variable-name
const ParameterListIO = io.array(ParameterIO, 'ParameterListIO');

export type Parameter = io.TypeOf<typeof ParameterIO>;
export type ParameterList = io.TypeOf<typeof ParameterListIO>;



export const fetchParameterListSurface = () =>
    fetchIO(ParameterListIO, getApiUrl('geodata/water/surface/parameters/'));


export const fetchParameterListgroundQuality = () =>
    fetchIO(ParameterListIO, getApiUrl('geodata/water/ground/parameters/quality/'));


export const fetchParameterListgroundQuantity = () =>
    fetchIO(ParameterListIO, getApiUrl('geodata/water/ground/parameters/quantity/'));




// tslint:disable-next-line:variable-name
const TimeserieDataPointIO = io.tuple([io.string, io.number, io.number], 'TimeserieDataPointIO');

// tslint:disable-next-line:variable-name
const TimeserieDataIO = io.array(TimeserieDataPointIO, 'TimeserieDataIO');

export type TimeserieDataPoint = io.TypeOf<typeof TimeserieDataPointIO>;
export type TimeserieData = io.TypeOf<typeof TimeserieDataIO>;

export const tsTime = (p: TimeserieDataPoint) => new Date(p[0]);
export const tsValue = (p: TimeserieDataPoint) => p[1];
export const tsFactor = (p: TimeserieDataPoint) => p[2];

export const fetchTimeserieSurface = (
    station: number | string,
    parameter: number,
) => fetchIO(TimeserieDataIO,
    getApiUrl(`geodata/water/surface/osm/${station}/${parameter}`));

export const fetchTimeserieGroundQuality = (
    station: number | string,
    parameter: number,
) => fetchIO(TimeserieDataIO,
    getApiUrl(`geodata/water/ground/osm/quality/${station}/${parameter}`));

export const fetchTimeserieGroundQuantity = (
    station: number | string,
    parameter: number,
) => fetchIO(TimeserieDataIO,
    getApiUrl(`geodata/water/ground/osm/quantity/${station}/${parameter}`));
