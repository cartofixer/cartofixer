import { loop } from 'sdi/app';
import { DIV } from 'sdi/components/elements';
import header from 'sdi/components/header';
import footer from './components/footer';

import { loadRoute } from './events/route';
import { getLayout } from './queries/app';

import modal from './components/modal';
import index from './components/view';
import queryBuilder from './components/view/query-builder';
import metadataForm from './components/view/metadata';

import debug = require('debug');

const logger = debug('sdi:events/map');

const wrappedMain = (
    name: string,
    ...elements: React.DOMElement<{}, Element>[]
) =>
    DIV(
        { className: 'osm-inner' },
        modal(),
        header('osm'),
        DIV({ className: `main ${name}` }, ...elements),
        footer()
    );

const renderIndex = () => wrappedMain('index', index());

const renderQueryBuilder = () => wrappedMain('queryBuilder', queryBuilder());

const renderMetadataForm = () => wrappedMain('metadataForm', metadataForm());

const render = () => {
    const layout = getLayout();
    switch (layout) {
        case 'index':
            return renderIndex();
        case 'queryBuilder':
            return renderQueryBuilder();
        case 'metadataForm':
            return renderMetadataForm();
    }
};

const effects = (initialRoute: string[]) => () => {
    loadRoute(initialRoute);
};

export const app = (initialRoute: string[]) =>
    loop('osm', render, effects(initialRoute));

logger('loaded');
