import { MessageStore, fromRecord } from 'sdi/locale';

const messages = {
    appName: {
        fr: 'OSM Data',
        nl: 'OSM Data',
    },
    welcomeMessage: {
        fr: `Portail OSM-Loader`,
        nl: '', // nltodo
    },
    'helptext:home': {
        fr: `Positionner la carte sur la zone de recherche souhaitée`,
        nl: '', // nltodo
    },

    'helptext:metadata': {
        fr: `Entrer les informations relative à cet ensemble de données.`,
        nl: '', // nltodo
    },

    'helptext:overpassTextArea': {
        fr: `Insérez une requête Overpass`,
        nl: '', // nltodo
    },

    runQuery: {
        fr: `Executer la requête`,
        nl: '', // nltodo
    },

    downloadGeoJSON: {
        fr: `Télécharger en .geoJSON`,
        nl: '', // nltodo
    },

    tableView: {
        fr: `tableau des données`,
        nl: '', // nltodo
    },

    goToMetadataForm: {
        fr: `Publier ce résulat sur cartofixer`,
        nl: '', // nltodo
    },

    home: {
        fr: 'accueil',
        nl: 'home', // nltodo
    },
};

type MDB = typeof messages;
export type TimeserieMessageKey = keyof MDB;

declare module 'sdi/locale' {
    export interface MessageStore {
        osm(k: TimeserieMessageKey): Translated;
    }
}

MessageStore.prototype.osm = (k: TimeserieMessageKey) =>
    fromRecord(messages[k]);
