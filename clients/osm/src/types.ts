

export type ParameterType = 'surface' | 'ground-quality' | 'ground-quantity';
export interface ParameterSelection {
    kind: ParameterType;
    id: number;
}

export interface StationSelection {
    kind: ParameterType;
    id: number | string;
}

export interface PointSelection {
    id: number;
    start: Date;
    end: Date;
    x: number;
    y: number;
}


export const waterMapId = 'water-map';


export type TableMode = 'per-station' | 'per-parameter';
