import { makeLabelAndIcon, makeIcon } from 'sdi/components/button';
import tr from 'sdi/locale';
import { navigateQueryBuilder, navigateMetadataForm } from '../events/route';

export const renderDeselectButton = makeIcon('cancel', 3, 'times');
export const renderSelectButton = makeIcon('reset', 2, 'refresh');

// Go to QueryBuilder

const queryBuilder = makeLabelAndIcon('navigate', 2, 'arrow-right', () =>
    tr.core('validate')
);
export const renderQueryBuilderBtn = () =>
    queryBuilder(() => navigateQueryBuilder());

// Run query

const runQuery = makeLabelAndIcon('navigate', 2, 'play', () =>
    tr.osm('runQuery')
);

export const renderRunQueryBtn = () => runQuery(() => '');

// Export GeoJSON

const exportGeojson = makeLabelAndIcon('navigate', 2, 'download', () =>
    tr.osm('downloadGeoJSON')
);

export const renderExportGeojsonBtn = () => exportGeojson(() => '');

// Table view

const tableView = makeLabelAndIcon('navigate', 2, 'table', () =>
    tr.osm('tableView')
);

export const renderTableViewBtn = () => tableView(() => '');

// Go to metadata form

const metadataForm = makeLabelAndIcon('navigate', 2, 'check', () =>
    tr.osm('goToMetadataForm')
);

export const renderMetadataFormBtn = () =>
    metadataForm(() => navigateMetadataForm());

// Save metadata

const metadataSave = makeLabelAndIcon('navigate', 2, 'check', () =>
    tr.core('save')
);

export const renderMetadataSaveBtn = () => metadataSave(() => '');

// expand button

export const renderButtonExpand = makeIcon('open', 3, 'chevron-down');
