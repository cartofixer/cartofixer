import { DIV, H1, INPUT, TEXTAREA } from 'sdi/components/elements';
import { markdown } from 'sdi/ports/marked';
import map from '../map';
import tr from 'sdi/locale';
import { renderMetadataSaveBtn } from '../button';

const renderTitle = () => H1({}, '~titleMetadata');

const renderHelptext = () =>
    DIV({ className: 'pitch' }, markdown(tr.osm('helptext:metadata')));

const metadataTitle = () =>
    DIV(
        {},
        DIV({ className: 'label' }, '~metadataTitle'),
        INPUT({}) // should be replaced with import { inputText } from 'sdi/components/input';
    );

const metadataDescription = () =>
    DIV(
        {},
        DIV({ className: 'label' }, '~metadataDescription'),
        TEXTAREA({}) // should be replaced with import { inputText } from 'sdi/components/input';
    );

const renderForm = () => DIV({}, metadataTitle(), metadataDescription());

const renderSidebar = () =>
    DIV(
        { className: 'content__side' },
        renderTitle(),
        renderHelptext(),
        renderForm(),
        renderMetadataSaveBtn()
    );

const renderMain = () => DIV({ className: 'content__main' }, map());

const render = () =>
    DIV({ className: 'content content--index' }, renderSidebar(), renderMain());

export default render;
