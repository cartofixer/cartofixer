import { DIV, H1, TEXTAREA } from 'sdi/components/elements';
import { markdown } from 'sdi/ports/marked';
import map from '../map';
import tr from 'sdi/locale';
import {
    renderExportGeojsonBtn,
    renderMetadataFormBtn,
    renderTableViewBtn,
    renderRunQueryBtn,
} from '../button';

const renderTitle = () => H1({}, '~titleQueryBuilder');

const renderHelptextHome = () =>
    DIV({ className: 'pitch' }, markdown(tr.osm('helptext:overpassTextArea')));

const renderOverpassTextArea = () => TEXTAREA({});

const renderSidebar = () =>
    DIV(
        { className: 'content__side' },
        renderTitle(),
        renderHelptextHome(),
        renderOverpassTextArea(),
        renderRunQueryBtn(),
        renderTableViewBtn(),
        renderExportGeojsonBtn(),
        renderMetadataFormBtn()
    );

const renderMain = () => DIV({ className: 'content__main' }, map());

const render = () =>
    DIV({ className: 'content content--index' }, renderSidebar(), renderMain());

export default render;
