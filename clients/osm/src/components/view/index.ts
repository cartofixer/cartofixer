import { DIV, H1 } from 'sdi/components/elements';
import { markdown } from 'sdi/ports/marked';
import map from '../map';
import tr from 'sdi/locale';
import { renderQueryBuilderBtn } from '../button';

const renderTitle = () => H1({}, tr.osm('welcomeMessage'));

const renderHelptextHome = () =>
    DIV({ className: 'pitch' }, markdown(tr.osm('helptext:home')));

const renderSidebar = () =>
    DIV(
        { className: 'content__side' },
        renderTitle(),
        renderHelptextHome(),
        renderQueryBuilderBtn()
    );

const renderMain = () => DIV({ className: 'content__main' }, map());

const render = () =>
    DIV({ className: 'content content--index' }, renderSidebar(), renderMain());

export default render;
