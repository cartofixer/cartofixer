import { Option, none, some } from 'fp-ts/lib/Option';

import { create } from 'sdi/map';
import { DIV } from 'sdi/components/elements';

import { getBaseLayer, getView, getMapInfo } from 'osm/src/queries/map';
import { updateView, setScaleLine } from 'osm/src/events/map';
import { waterMapId } from '../types';

import * as debug from 'debug';

const logger = debug('sdi:route');

let mapUpdate: Option<() => void> = none;
let mapSetTarget: Option<(e: HTMLElement) => void> = none;

const attachMap = (element: HTMLElement | null) => {
    mapUpdate = mapUpdate.foldL(
        () => {
            const { update, setTarget } = create(waterMapId, {
                getBaseLayer,
                getView,
                getMapInfo,

                updateView,
                setScaleLine,

                element,
            });

            mapSetTarget = some(setTarget);
            update();
            return some(update);
        },
        (update) => some(update)
    );

    if (element) {
        mapSetTarget.map((f) => f(element));
    }
};

const render = () => {
    mapUpdate.map((f) => f());
    return DIV(
        { className: 'map-wrapper' },
        DIV({
            id: waterMapId,
            key: waterMapId,
            className: 'map',
            ref: attachMap,
        })
    );
};

export default render;

logger('loaded');
