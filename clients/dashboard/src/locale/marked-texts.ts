export const homeTextFR = `
Bienvenue sur le tableau de bord de *cartofixer*.

Chacune des applications propose un set de fonctionnalités spécifiques :

- *Atlas* permet de visualiser et partager les cartes de la plateforme, créées dans le *Studio*.  
- *Studio* permet de créer et publier des cartes à partir des données disponibles.  
- *Collecte* [beta] est une application accompagnant la mise en place de campagnes d'observations de terrain.

Pour toute information, et si vous souhaitez un compte d'utilisateur-rice pour utiliser *Studio* ou *Collectes*, contactez nous à **contact@cartofixer.be**

`;

export const homeTextNL = `
Welkom op het *cartofixer* dashboard.

Elke applicatie biedt een specifieke set van functies :

- *Atlas* stelt u in staat om de kaarten van het platform te bekijken en te delen.  
- *Studio* stelt u in staat om kaarten te maken en te publiceren op basis van de gegevens die beschikbaar zijn op het platform.  
- *Collecte* [beta-fr] is een toepassing die het mogelijk maakt om veldobservatiecampagnes op te zetten.

Voor informatie en als u een gebruikersaccount wilt, kunt u ons een e-mail sturen naar contact@cartofixer.be.

`;

export const homeDisclaimerFR = ``; // to be defined in admin/lingua
export const homeDisclaimerNL = ``; // to be defined in admin/lingua
