/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


import { DIV } from 'sdi/components/elements';
import { AppManifest, appDisplayName, appName, appUrl } from 'sdi/source';
import { fromRecord } from 'sdi/locale';
import { getApps } from 'sdi/app';




const renderApp =
    (app: AppManifest) =>
        appDisplayName(app)
            .map(
                displayName => DIV({
                    className: `app-item ${appName(app)}`,
                    onClick: () => window.location.assign(appUrl(app)),
                },
                    DIV({ className: 'app__wrapper' },
                        DIV({ className: 'app-picto' }),
                        DIV({ className: 'app-name' }, fromRecord(displayName)),
                    ),
                ),
            );


const renderAppsWrapper =
    () => DIV({
        className: 'app-list',
    }, ...(getApps().map(renderApp)));

const render =
    () => DIV({ className: 'my-apps' },
        renderAppsWrapper());

export default render;
