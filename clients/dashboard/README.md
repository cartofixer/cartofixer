![dashboard](../assets/image/dashboard.png)

_dashboard_ allows users to access applications that were configured at the time of _cartofixer_ deployment.

For ease of use, it is also possible to not show applications that are still configured in the deployment (such as _login_ or _embed_ applications, which are not useful to show in the dashboard).

These configurations are made at the time of _cartofixer_ deployment.

## User guide

The complete user guide is available here : https://gitlab.com/cartofixer/cartofixer/-/wikis/home
