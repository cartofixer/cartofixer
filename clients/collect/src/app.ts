import { loop, getAppManifest } from "sdi/app";
import { DIV } from "sdi/components/elements";
import header from "sdi/components/header";
import footer from "sdi/components/footer";

import { loadRoute } from "./events/route";
import { getLayout } from "./queries/app";

import home from "./components/home";
import map from "./components/map";
import collect from "./components/collect";
import observation from "./components/observation";
import observationForm from "./components/form/observation";
import observationFormPrint from "./components/form/print";
import answerForm from "./components/form/answer";
import annotationForm from "./components/form/annotation";
import { appDisplayName } from "sdi/source";
import tr, { fromRecord } from "sdi/locale";
import { loadAllUsers } from "./events/app";

const wrappedMain = (
  name: string,
  ...elements: React.DOMElement<{}, Element>[]
) =>
  DIV(
    { className: "collect" },
    getAppManifest("collect")
      .chain(appDisplayName)
      .map((dn) => header(fromRecord(dn))),
    DIV({ className: `main ${name}` }, ...elements),
    footer()
  );

const renderHome = () => wrappedMain("home", home());

const renderMap = () => wrappedMain("map", map());

const renderCollect = () => wrappedMain("collect", collect());

const renderObservation = () => wrappedMain("observation", observation());

const renderObservationForm = () =>
  wrappedMain("observation-form", observationForm());
const renderObservationFormPrint = () =>
  wrappedMain("observation-form-print", observationFormPrint());

const renderAnswerForm = () => wrappedMain("answer-form", answerForm());

const renderAnnotationForm = () =>
  wrappedMain("annotation-form", annotationForm());

const render = () => {
  const layout = getLayout();
  switch (layout) {
    case "home":
      return renderHome();
    case "map":
      return renderMap();
    case "collect":
      return renderCollect();
    case "observation":
      return renderObservation();
    case "observation-form":
      return renderObservationForm();
    case "observation-form-print":
      return renderObservationFormPrint();
    case "answer-form":
      return renderAnswerForm();
    case "annotation-form":
      return renderAnnotationForm();
  }
};

const effects = (initialRoute: string[]) => () => {
  loadRoute(initialRoute);
  loadAllUsers();
  tr.init_edited();
};

export const app = (initialRoute: string[]) =>
  loop("collect", render, effects(initialRoute));
