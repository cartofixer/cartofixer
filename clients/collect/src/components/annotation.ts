import {
  getFormAnswerObservation,
  getAnnotationsFromObs,
  getAnnotationsFromQuestion,
  getAnnotationsFromAnswer,
} from "../queries/collect";
import { DIV, IMG, NodeOrOptional } from "sdi/components/elements";
import { buttonAnnotateText, buttonAnnotateImage } from "./buttons";
import { navigateAnnotationForm } from "../events/route";
import {
  Answer,
  Annotation,
  AnnotationText,
  AnnotationImage,
  Question,
  Observation,
} from "../remote";
import {
  findUser,
  isAnnotationAnswerOpen,
  isAnnotationObsOpen,
  isAnnotationQuestionOpen,
} from "../queries/app";
import tr, { formatDate } from "sdi/locale";
import { markdown } from "sdi/ports/marked";
import {
  changeAnnotationStatusAnswer,
  changeAnnotationStatusObs,
  changeAnnotationStatusQuestion,
} from "../events/app";

export const annotationTriggerObs = (o: Observation) => {
  const nb = getAnnotationsFromObs(o).fold(0, (list) => list.length);
  return DIV(
    {
      className: "annotate annotate-trigger",
      onClick: () => changeAnnotationStatusObs(o.id),
    },
    DIV({ className: "annotation-number" }, nb)
    // DIV({className: 'annotation-label'},
    //     nb > 1
    //         ? `${tr.collect('multipleNotesObservation')}`
    //         : `${tr.collect('singleNoteObservation')}`,
    // ),
  );
};
export const annotationTriggerQuestion = (q: Question) => {
  const nb = getAnnotationsFromQuestion(q).fold(0, (list) => list.length);
  return DIV(
    {
      className: "annotate annotate-trigger",
      onClick: () => changeAnnotationStatusQuestion(q.id),
    },
    DIV({ className: "annotation-number" }, nb)
    // DIV({className: 'annotation-label'},
    //     nb > 1
    //         ? `${tr.collect('multipleComments')}`
    //         : `${tr.collect('singleComment')}`,
    // ),
  );
};
export const annotationTriggerAnswer = (a: Answer, q: Question) => {
  const nb = getAnnotationsFromAnswer(a, q).fold(0, (list) => list.length);
  console.log(`annotationTrigger: answer ${a.id} (question: ${q.id})`);
  return DIV(
    {
      className: "annotate annotate-trigger",
      onClick: () => changeAnnotationStatusAnswer(a.id),
    },
    DIV({ className: "annotation-number" }, nb)
    // DIV(
    //   { className: "annotation-label" },
    //   nb > 1
    //     ? `${tr.collect("multipleNotesAnswer")}`
    //     : `${tr.collect("singleNoteAnswer")}`
    // )
  );
};

export const renderAnnotationWrapperAnswer = (a: Answer, q: Question) =>
  isAnnotationAnswerOpen(a.id).map(() =>
    DIV(
      { className: "annotation__wrapper answer" },
      renderAnnotateAnswer(a),
      renderAnswerAnnotations(a, q)
    )
  );

export const renderAnnotationWrapperQuestion = (q: Question) =>
  isAnnotationQuestionOpen(q.id).map(() =>
    DIV(
      { className: "annotation__wrapper question" },
      renderAnnotateQuestion(q),
      renderQuestionAnnotations(q)
    )
  );

export const renderAnnotationWrapperObservation = (o: Observation) =>
  isAnnotationObsOpen(o.id).map(() =>
    DIV(
      { className: "annotation__wrapper observation" },
      renderAnnotateObservation(),
      renderObservationAnnotations(o)
    )
  );

export const renderAnnotateWrapperObservation = (o: Observation) =>
  isAnnotationObsOpen(o.id).map(() =>
    DIV(
      { className: "annotation__wrapper observation" },
      renderAnnotateObservation()
    )
  );

export const renderAnnotateObservation = () =>
  getFormAnswerObservation().map((obsId) =>
    DIV(
      {
        className: "annotate annotate-observation",
      },
      buttonAnnotateText(() =>
        navigateAnnotationForm("text", "observation", obsId)
      ),
      buttonAnnotateImage(() =>
        navigateAnnotationForm("image", "observation", obsId)
      )
    )
  );

export const renderAnnotateAnswer = (a: Answer) =>
  DIV(
    {
      className: "annotate annotate-answer",
    },
    buttonAnnotateText(() => navigateAnnotationForm("text", "answer", a.id)),
    buttonAnnotateImage(() => navigateAnnotationForm("image", "answer", a.id))
  );

export const renderAnnotateQuestion = (q: Question) =>
  DIV(
    {
      className: "annotate annotate-answer",
    },
    buttonAnnotateText(() => navigateAnnotationForm("text", "question", q.id)),
    buttonAnnotateImage(() => navigateAnnotationForm("image", "question", q.id))
  );

const renderAnnotationMeta = (a: Annotation) =>
  DIV(
    {
      className: "note meta",
    },
    DIV(
      { className: "username" },
      findUser(a.user)
        .map((u) => u.name)
        .getOrElse(tr.collect("anonymous"))
    ),
    DIV({ className: "date" }, formatDate(new Date(a.timestamp)))
  );

const renderAnnotationText = (a: AnnotationText) =>
  DIV(
    {
      className: "note text",
    },
    renderAnnotationMeta(a),
    markdown(a.text)
  );

const renderAnnotationImage = (a: AnnotationImage) =>
  DIV(
    {
      className: "note image",
    },
    renderAnnotationMeta(a),
    IMG({ className: "user-image", src: `/documents/images/${a.image}` })
  );

const renderAnnotation = (a: Annotation) => {
  switch (a.kind) {
    case "text":
      return renderAnnotationText(a);
    case "image":
      return renderAnnotationImage(a);
  }
};

const wrapNotes = (...nodes: NodeOrOptional[]) =>
  DIV({ className: "annotation__list" }, ...nodes);

export const renderAnswerAnnotations = (a: Answer, q: Question) =>
  wrapNotes(getAnnotationsFromAnswer(a, q).map((a) => a.map(renderAnnotation)));

export const renderQuestionAnnotations = (q: Question) =>
  wrapNotes(getAnnotationsFromQuestion(q).map((a) => a.map(renderAnnotation)));

export const renderObservationAnnotations = (o: Observation) =>
  wrapNotes(getAnnotationsFromObs(o).map((a) => a.map(renderAnnotation)));
