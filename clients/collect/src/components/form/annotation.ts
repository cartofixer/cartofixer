import { DIV, H1, H2, H3, IMG } from "sdi/components/elements";
import {
  getAnnotationKind,
  getAnnotationTarget,
  getAnnotationText,
  findAnswer,
  getQuestionForAnswer,
  getChoices,
  imageUploaderQueries,
  findObservation,
  getObservationAnnotations,
  getAnswerAnnotations,
  getQuestionAnnotations,
  findQuestion,
} from "collect/src/queries/collect";
import { inputLongText } from "sdi/components/input";
import {
  setAnnotationText,
  saveTextAnnotation,
  imageUploaderEvents,
  saveImageAnnotation,
} from "collect/src/events/collect";
import { buttonSave } from "../buttons";
import { navigateBack } from "collect/src/events/route";
import {
  AnnotationText,
  AnnotationImage,
  Annotation,
  Answer,
  Question,
  Observation,
} from "collect/src/remote";
import { markdown } from "sdi/ports/marked";
import { getRootUrl } from "sdi/app";
import { makeImageUploader } from "sdi/components/upload";
import tr from "sdi/locale";

const renderAnnotationX = (_id: number) =>
  DIV({ className: "target annotation" }, "todo");

const renderAnswerAnnotationText = (a: AnnotationText) =>
  DIV(
    {
      className: "note text",
    },
    markdown(a.text)
  );

const renderAnswerAnnotationImage = (a: AnnotationImage) =>
  DIV(
    {
      className: "note image",
    },
    IMG({ src: getRootUrl(`documents/image/${a.image}`) })
  );

const renderAnnotation = (a: Annotation) => {
  switch (a.kind) {
    case "text":
      return renderAnswerAnnotationText(a);
    case "image":
      return renderAnswerAnnotationImage(a);
  }
};

const renderAnswerAnnotations = (a: Answer, q: Question) =>
  DIV(
    {
      className: "annotation__list",
    },
    getAnswerAnnotations(a, q).map((annotations) =>
      annotations.map(renderAnnotation)
    )
  );

const renderAnswer = (id: number) =>
  DIV(
    { className: "target answer" },
    //  H2({},
    //     `~Commentaires précédents sur cette réponse`,
    // ),
    findAnswer(id).map((a) =>
      DIV(
        {
          className: "answer__wrapper",
          key: `answer-${a.id}`,
        },
        getQuestionForAnswer(a).map((q) =>
          DIV(
            {
              className: "question",
            },
            H3({}, q.title),
            getChoices(q.id).map((c) =>
              DIV(
                {
                  className: c.id === a.choice ? "choice selected" : "choice",
                  key: c.id,
                },
                c.title
              )
            ),
            renderAnswerAnnotations(a, q)
          )
        )
      )
    )
  );

const renderChoice = (_id: number) =>
  DIV({ className: "target choice" }, "choice");

const renderObservationAnnotations = (o: Observation) =>
  DIV(
    {
      className: "annotation__list",
    },
    getObservationAnnotations(o).map((ans) => ans.map(renderAnnotation))
  );

const renderObservation = (id: number) =>
  DIV(
    { className: "target observation" },
    // H2({}, `~Commentaires précédents sur l'observation n°${id} :`),
    findObservation(id).map((o) =>
      DIV(
        {
          className: "observation__wrapper",
        },
        renderObservationAnnotations(o)
      )
    )
  );

const renderQuestionAnnotations = (q: Question) =>
  DIV(
    {
      className: "annotation__list",
    },
    getQuestionAnnotations(q).map((ans) => ans.map(renderAnnotation))
  );

const renderQuestion = (id: number) =>
  DIV(
    { className: "target observation" },
    H2(
      {},
      `~Commentaires précédents à la question : "`,
      findQuestion(id).map((q) => q.title),
      `"`
    ),
    findQuestion(id).map((q) =>
      DIV(
        {
          className: "question__infos",
        },
        renderQuestionAnnotations(q)
      )
    )
  );

// const renderQuestion = (
//     _id: number,
// ) => DIV({ className: 'target question' }, 'question');

const renderTarget = () =>
  getAnnotationTarget().map(({ kind, id }) => {
    switch (kind) {
      case "annotation":
        return renderAnnotationX(id);
      case "answer":
        return renderAnswer(id);
      case "choice":
        return renderChoice(id);
      case "observation":
        return renderObservation(id);
      case "question":
        return renderQuestion(id);
    }
  });

const renderText = () =>
  DIV(
    {
      className: "text-form",
    },
    H1({}, tr.collect("addTextTitle")),
    inputLongText(() => getAnnotationText().getOrElse(""), setAnnotationText, {
      rows: 5,
    }),
    getAnnotationText().map(() =>
      buttonSave(() => {
        saveTextAnnotation();
        navigateBack();
      })
    ),
    renderTarget()
  );

const imageUploader = makeImageUploader(
  "image",
  imageUploaderQueries,
  imageUploaderEvents
);

const renderImage = () =>
  DIV(
    {
      className: "image-form",
    },
    H1({}, tr.collect("addImageTitle")),
    imageUploader(),
    imageUploaderQueries.getImageId().map(() =>
      buttonSave(() => {
        saveImageAnnotation();
        navigateBack();
      })
    ),
    renderTarget()
  );

const render = () => {
  switch (getAnnotationKind()) {
    case "text":
      return renderText();
    case "image":
      return renderImage();
  }
};

export default render;
