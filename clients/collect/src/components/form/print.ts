import { DIV, H3, H1, BUTTON } from "sdi/components/elements";
import { Collect, Question } from "collect/src/remote";
import { markdown } from "sdi/ports/marked";
import {
  getChoices,
  getNbOfObservations,
  getObservedQuestions,
  getObserverQuestions,
  getSelectedCollect,
} from "collect/src/queries/collect";
import { navigateHome } from "collect/src/events/route";
import { renderButtonHome } from "../buttons";
import tr from "sdi/locale";
import {
  annotationTriggerQuestion,
  renderAnnotationWrapperQuestion,
} from "../annotation";

const renderErrorSelection = () =>
  DIV({ className: "error" }, tr.collect("error.noCampaign"));

// FIX ME
const renderQuestionChoices = (q: Question) =>
  DIV(
    { className: "choice__list" },
    getChoices(q.id).map((c) =>
      DIV(
        {
          className: "choice__wrapper",
          key: c.id,
        },
        DIV({ className: "choice__label" }, c.title),
        DIV({ className: "choice__description" }, markdown(c.description))
      )
    )
  );
// END FIX ME

const renderPrintDisclamer = () =>
  DIV(
    { className: "print-disclaimer" },
    BUTTON(
      {
        className: "btn btn-1 btn-print",
        onClick: () => window.print(),
      },
      tr.core("print")
    ),
    `~Super print, on Chrome Only`
  );

const renderPrintSpecificQuestionsTop = () =>
  DIV(
    { className: "question print-questions" },
    H3({ className: "print-question date" }, tr.collect("printFormDate")),
    H3({ className: "print-question place" }, tr.collect("printFormPlace"))
  );

const renderPrintSpecificQuestionsBottom = () =>
  DIV(
    { className: "question print-questions" },
    H3({ className: "print-question name" }, tr.collect("printFormName")),
    H3({ className: "print-question comment" }, tr.collect("printFormComment"))
  );

const renderPrintFooter = () =>
  DIV(
    { className: "print-footer" },
    DIV({ className: "footer-item" }, `~guideline encodage appli + Lien`),
    DIV({ className: "footer-item" }, `~Logos innoviris`)
  );

const renderQuestionNumber = (n: number, total: number, q: Question) =>
  DIV(
    {
      className: `question__number ${q.kind}`,
    },
    `${n + 1}/${total}`
  );

const renderQuestion = (q: Question, index: number, questions: Question[]) =>
  DIV(
    {
      className: "question",
      key: `question-${q.id}`,
    },
    DIV(
      { className: "question__infos" },
      renderQuestionNumber(index, questions.length, q),
      H3({}, q.title),
      markdown(q.description),
      renderQuestionChoices(q),
      annotationTriggerQuestion(q)
    ),
    renderAnnotationWrapperQuestion(q)
  );

const collectForm = (c: Collect) =>
  DIV(
    { className: "page-2 question-lists__wrapper" },
    H1(
      {
        className: "collect__title",
      },
      c.title
    ),
    renderPrintSpecificQuestionsTop(),
    getObservedQuestions(c.id).map(renderQuestion),
    getObserverQuestions(c.id).map(renderQuestion),
    renderPrintSpecificQuestionsBottom(),
    renderPrintFooter()

    // DIV(
    // { className: "question-list observed" },
    // DIV({className:'question__type'}, tr.collect('observedObjectTitle')),
    // getObservedQuestions(c.id).map(renderQuestion)
    // ),
    // DIV(
    // { className: "question-list observer" },
    // DIV({className:'question__type'}, tr.collect('situatedObserverTitle')),
    // getObserverQuestions(c.id).map(renderQuestion)
    // )
  );

const renderSelection = (c: Collect) =>
  DIV(
    { className: "collect-infos__body" },
    renderButtonHome(() => navigateHome()),
    DIV(
      { className: "page-1 collect__description" },

      DIV({ className: "logo-print" }),
      H1(
        {
          className: "collect__title",
        },
        c.title
      ),
      markdown(c.description),
      DIV(
        { className: "collect__meta" },
        getNbOfObservations(c.id) > 1
          ? `${getNbOfObservations(c.id)} ${tr.collect(
              "labelObservationMultiple"
            )}`
          : `${getNbOfObservations(c.id)} ${tr.collect(
              "labelObservationSingle"
            )}`
      )
      // let's keep that for later
      // DIV({className: 'collect__meta'}, `${tr.collect('lastObservation')} : ~todo`),
      // DIV({className: 'collect__meta'}, `${tr.collect('contactPoint')} : ~todo`),
      // renderActions(c),
    ),
    collectForm(c)
  );

export const render = () =>
  DIV(
    { className: "collect-print" },
    renderPrintDisclamer(),
    getSelectedCollect().foldL(renderErrorSelection, renderSelection)
  );

export default render;
