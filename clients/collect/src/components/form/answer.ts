import {
  DETAILS,
  DIV,
  H1,
  H2,
  NODISPLAY,
  SUMMARY,
} from "sdi/components/elements";
import { Question, Choice, Collect } from "collect/src/remote";
import { saveAnswer } from "collect/src/events/collect";
import {
  getChoices,
  getSelectedCollect,
  getFormAnswerObservation,
  findObservation,
  isSelectedChoice,
  getObservedQuestions,
  getObserverQuestions,
  getSelectedObservation,
  getSelectedObservationUser,
  getAnswerAndQuestionForChoice,
} from "collect/src/queries/collect";
import { markdown } from "sdi/ports/marked";
import {
  renderButtonAddObervation,
  renderButtonHome,
  renderButtonViewObservation,
} from "../buttons";
import {
  navigateHome,
  navigateObservation,
  navigateObservationForm,
} from "collect/src/events/route";
import {
  renderObservationAnnotations,
  renderAnnotationWrapperQuestion,
  renderAnnotateWrapperObservation,
  annotationTriggerQuestion,
  annotationTriggerObs,
  annotationTriggerAnswer,
  renderAnnotationWrapperAnswer,
} from "../annotation";
import tr from "sdi/locale";
import { getUserId } from "sdi/app";
import { getLayout } from "collect/src/queries/app";

const choiceClassName = (c: Choice) =>
  isSelectedChoice(c) ? "choice__label selected" : "choice__label";

const renderAnwserAnnotation = (c: Choice) =>
  getAnswerAndQuestionForChoice(c).map(({ a, q }) =>
    DIV({}, annotationTriggerAnswer(a, q), renderAnnotationWrapperAnswer(a, q))
  );

const renderUnselectableChoice = (c: Choice) =>
  DIV(
    { className: "choice__wrapper unselectable" },
    DIV(
      {
        className: `${choiceClassName(c)} choice__label disabled`,
        key: c.id,
        // title: markdown(c.description),
      },
      c.title
    )
  );

const renderSelectableChoice = (c: Choice) =>
  DIV(
    { className: "choice__wrapper" },
    DIV(
      {
        className: choiceClassName(c),
        key: c.id,
        // title: markdown(c.description),
        onClick: () => saveAnswer(c.id),
      },
      c.title
    ),
    DETAILS({}, SUMMARY({}), DIV({}, markdown(c.description))),
    isSelectedChoice(c) ? renderAnwserAnnotation(c) : NODISPLAY()
  );

const renderChoice = (c: Choice) => {
  if (getLayout() === "answer-form") {
    // To check (nw)
    return renderSelectableChoice(c);
  } else {
    return getUserId().fold(renderUnselectableChoice(c), (id) =>
      id === getSelectedObservationUser()
        ? renderSelectableChoice(c)
        : renderUnselectableChoice(c)
    );
  }
};

const renderChoiceList = (q: Question) =>
  DIV(
    {
      className: "choice__list",
    },
    ...getChoices(q.id).map(renderChoice)
  );

const renderChoiceMeta = (q: Question) =>
  DIV(
    {
      className: "question__meta",
    },
    H2({}, q.title),
    DIV({ className: "helptext" }, markdown(q.description))
  );

const renderChoiceNumber = (n: number, total: number, q: Question) =>
  DIV(
    {
      className: `question__number ${q.kind}`,
    },
    `${n + 1}/${total}`
  );

export const renderQuestion = (
  q: Question,
  index: number,
  questions: Question[]
) =>
  DIV(
    {
      className: `question ${q.kind}`,
    },
    renderChoiceNumber(index, questions.length, q),
    DIV(
      { className: "question__infos" },
      renderChoiceMeta(q),
      annotationTriggerQuestion(q)
    ),
    renderAnnotationWrapperQuestion(q),
    renderChoiceList(q)
  );

const renderHelptext = () =>
  DIV(
    { className: "helptext" },
    markdown(tr.collect("helptext.observationAnswer"))
  );

const renderCollectTitle = (c: Collect) => {
  const o = getSelectedObservation();
  return DIV(
    { className: "collect-head__wrapper" },
    DIV(
      { className: "collect-title__wrapper" },
      H1({}, c.title),
      o.map(annotationTriggerObs)
    ),
    o.map(renderAnnotateWrapperObservation),
    renderHelptext()
  );
};

const renderUnknownCollectTitle = () => H1({}, "unknown collect");

const breadCrumb = () =>
  DIV(
    { className: "breadcrumb" },
    renderButtonHome(navigateHome),
    DIV({ className: "edition-mode__label" }, tr.collect("labelEditionMode"))
  );

const renderActions = (c: Collect) =>
  DIV(
    { className: "actions" },
    renderButtonViewObservation(() =>
      getFormAnswerObservation().map(navigateObservation)
    ),
    renderButtonAddObervation(() => navigateObservationForm(c.id))
  );

const render = () =>
  DIV(
    { className: "answer-form__body" },
    breadCrumb(),
    getSelectedCollect().foldL(renderUnknownCollectTitle, renderCollectTitle),
    getFormAnswerObservation()
      .chain(findObservation)
      .map(renderObservationAnnotations),
    DIV(
      { className: "question__list observed" },
      // DIV({ className: 'question__type' }, tr.collect('observedObjectTitle')),
      getSelectedCollect().map((collect) =>
        getObservedQuestions(collect.id).map(renderQuestion)
      )
    ),
    DIV(
      { className: "question__list observer" },
      // DIV({ className: 'question__type' }, tr.collect('situatedObserverTitle')),
      getSelectedCollect().map((collect) =>
        getObserverQuestions(collect.id).map(renderQuestion)
      )
    ),
    getSelectedCollect().foldL(renderUnknownCollectTitle, renderActions)
  );

export default render;
