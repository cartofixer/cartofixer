import {
  DIV,
  H1,
  H2,
  NODISPLAY,
  IMG,
  DETAILS,
  SUMMARY,
} from "sdi/components/elements";
import {
  getSelectedObservation,
  getAnswers,
  getQuestionForAnswer,
  getChoices,
  getObservedQuestions,
  getObserverQuestions,
  getSelectedCollect,
  getNextObservation,
  getPreviousObservation,
} from "../queries/collect";
import { Observation, Answer, Question, Collect } from "../remote";
import tr, { formatDate } from "sdi/locale";
import { notEmpty } from "sdi/util";
import {
  navigateHome,
  navigateObservation,
  navigateObservationForm,
} from "../events/route";
import {
  annotationTriggerObs,
  renderAnnotationWrapperAnswer,
  renderAnnotationWrapperObservation,
} from "./annotation";
// import { getUserIdAstNumber } from 'sdi/app';
import { findUser } from "../queries/app";
import { renderQuestion } from "./form/answer";
import { renderButtonAddObervation, renderButtonHome } from "./buttons";
import { annotationTriggerAnswer } from "./annotation";

import miniMap, {
  miniMapLoading,
  miniMapLoaded,
  miniMapFailed,
} from "sdi/map/mini";
import { Point, Position } from "sdi/source";
import { getMiniMap } from "../queries/map";
import { setMiniMap } from "../events/map";
import debug = require("debug");
import { fromLonLat } from "ol/proj";
import { markdown } from "sdi/ports/marked";
import { makeLabel } from "sdi/components/button";

const logger = debug("sdi:collect/read/geometry");

const map = miniMap("https://ows.terrestris.de/osm-gray/service", {
  LAYERS: "OSM-WMS",
  WIDTH: 412,
  HEIGHT: 412,
});

const hashCoordinate = (p: Position) => `${p[0]},${p[1]}`;

const pointFromLonLat = (point: Point) => ({
  ...point,
  coordinates: fromLonLat(point.coordinates, "EPSG:3857"),
});

const renderMiniMap = (point: Point) => {
  const pointProjected = pointFromLonLat(point);
  const coordHash = hashCoordinate(pointProjected.coordinates);
  return DIV(
    { className: "minimap" },
    getMiniMap(coordHash).foldL(
      () => {
        logger(`starting minimap ${coordHash.substr(0, 16)}`);
        setMiniMap(coordHash, miniMapLoading());
        map(pointProjected)
          .then((opt) =>
            opt.map((urlString) =>
              setMiniMap(coordHash, miniMapLoaded(urlString))
            )
          )
          .catch(() => setMiniMap(coordHash, miniMapFailed()));
        return NODISPLAY();
      },
      (step) => {
        logger(`stepping minimap ${coordHash.substr(0, 16)} -> ${step.step}`);
        switch (step.step) {
          case "loading":
            return DIV({ className: "_loader" }, "~loading");
          case "failed":
            return DIV({ className: "_failed" }, "~failed");
          case "loaded":
            return IMG({ src: step.data, alt: "" });
        }
      }
    )
  );
};

const renderErrorSelection = () =>
  DIV({ className: "error" }, "Sorry, could not find the selected observation");

const renderAnswer = (a: Answer) =>
  DIV(
    {
      className: `answer__wrapper`,
      key: `answer-${a.id}`,
    },
    getQuestionForAnswer(a).map((q) =>
      DIV(
        {
          className: "question__infos",
        },
        H2({ className: "question" }, q.title),
        DIV({ className: "question__description" }, q.description),
        DIV(
          { className: "choice__list" },
          getChoices(q.id).map(
            (c) =>
              DIV(
                {
                  className:
                    c.id === a.choice
                      ? "choice__wrapper selected"
                      : "choice__wrapper unselected",
                  key: c.id,
                },
                DIV({ className: "choice__label" }, c.title),
                DETAILS({}, SUMMARY({}), DIV({}, markdown(c.description)))
              ),
            annotationTriggerAnswer(a, q)
          ),
          renderAnnotationWrapperAnswer(a, q)
        )
      )
    )
  );

const renderNonAnsweredQuestion = (
  q: Question,
  index: number,
  questions: Question[]
) => renderQuestion(q, index, questions);

const renderNonAnsweredQuestions = (qs: Question[]) =>
  DIV(
    {
      className: "non-anwswered-questions",
    },
    ...qs.map(renderNonAnsweredQuestion)
  );

const renderCollectTitle = (c: Collect) => H1({}, c.title);

const renderAnswered = (answers: Answer[]) =>
  DIV(
    { className: "answered-questions" },
    notEmpty(
      answers.filter((a) =>
        getQuestionForAnswer(a)
          .map((q) => q.kind === "observed")
          .getOrElse(false)
      )
    ).map((ad) =>
      DIV(
        { className: "observed" },
        // H3({className: 'question-type__label'}, tr.collect('observedObjectTitle')),
        ...ad.map(renderAnswer)
      )
    ),
    notEmpty(
      answers.filter((a) =>
        getQuestionForAnswer(a)
          .map((q) => q.kind === "observer")
          .getOrElse(false)
      )
    ).map((ar) =>
      DIV(
        { className: "observer" },
        // H3({className: 'question-type__label'}, tr.collect('situatedObserverTitle')),
        ...ar.map(renderAnswer)
      )
    )
  );

const renderNonAnswered = (o: Observation, answers: Answer[]) => {
  const answeredQuestionIds = answers
    .map((a) =>
      getQuestionForAnswer(a)
        .map((q) => q.id)
        .getOrElse(0)
    )
    .filter((qid) => qid > 0);

  const nonAnsweredObserverQuestions = getObserverQuestions(o.collect).filter(
    (q) => answeredQuestionIds.indexOf(q.id) < 0
  );
  const nonAnsweredObservedQuestions = getObservedQuestions(o.collect).filter(
    (q) => answeredQuestionIds.indexOf(q.id) < 0
  );

  return notEmpty(
    nonAnsweredObserverQuestions.concat(nonAnsweredObservedQuestions)
  ).map(() =>
    DIV(
      { className: "non-answered-questions" },
      H2(
        { className: "non-anwswered-questions-title" },
        tr.collect("unansweredQuestions")
      ),
      notEmpty(nonAnsweredObservedQuestions).map((questions) =>
        DIV(
          { className: "question__list observed" },
          renderNonAnsweredQuestions(questions)
        )
      ),
      notEmpty(nonAnsweredObserverQuestions).map((questions) =>
        DIV(
          { className: "question__list observer" },
          renderNonAnsweredQuestions(questions)
        )
      )
    )
  );
};

const renderAnswers = (o: Observation) =>
  notEmpty(getAnswers(o.id)).map((answers) =>
    DIV(
      { className: "answers" },
      renderAnswered(answers),
      renderNonAnswered(o, answers)
    )
  );

const btnNext = makeLabel("switch", 3, () => tr.collect("next"));
const btnPrevious = makeLabel("switch", 3, () => tr.collect("previous"));

const renderButtonNext = (o: Observation) =>
  getNextObservation(o).map((obs) =>
    btnNext(() => navigateObservation(obs.id))
  );

const renderButtonPrevious = (o: Observation) =>
  getPreviousObservation(o).map((obs) =>
    btnPrevious(() => navigateObservation(obs.id))
  );

const renderObservation = (o: Observation) =>
  DIV(
    {
      className: "observation__body",
    },
    renderButtonHome(navigateHome),
    renderButtonPrevious(o),
    renderButtonNext(o),
    getSelectedCollect().map(renderCollectTitle),
    DIV(
      { className: "observation-head__wrapper" },
      DIV(
        { className: "observation__meta" },
        tr.collect("observedOn"),
        " ",
        formatDate(new Date(o.timestamp)),
        " ",
        tr.collect("observedBy"),
        " ",
        findUser(o.user)
          .map((u) => u.name)
          .getOrElse(tr.collect("anonymous"))
      ),
      renderMiniMap(o.observed),
      annotationTriggerObs(o)
    ),
    renderAnnotationWrapperObservation(o),
    renderAnswers(o),
    renderButtonAddObervation(() => navigateObservationForm(o.collect))
  );

export const render = () =>
  DIV(
    { className: "observation__wrapper" },
    getSelectedObservation().foldL(renderErrorSelection, (o) =>
      renderObservation(o)
    )
  );

export default render;

logger("loaded");
