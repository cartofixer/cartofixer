import { DIV, H2, NodeOrOptional, H1 } from 'sdi/components/elements';
import { Collect } from '../remote';
import { markdown } from 'sdi/ports/marked';
import { getNbOfObservations, getRemoteCollects } from '../queries/collect';
import { foldRemote } from 'sdi/source';
import { navigateCollect } from '../events/route';
import tr from 'sdi/locale';


const renderEdito = () => DIV({ className: 'app-edito' }, markdown(tr.collect('helptext.home'), 'md'));


const renderCollect = (
    c: Collect,
) => DIV({
    className: 'collect__item',
    key: `collect-${c.id}`,
    onClick: () => navigateCollect(c.id)
},
    H2({
        className: 'collect__title',
    }, c.title),
    DIV({className: 'collect__description'},
        markdown(c.description),
        DIV({ className: 'collect__meta' },
                   getNbOfObservations(c.id) > 1
                       ? `${getNbOfObservations(c.id)} ${tr.collect('labelObservationMultiple')}`
                       :`${getNbOfObservations(c.id)} ${tr.collect('labelObservationSingle')}`),
    ),
);

const renderCollectList = foldRemote<Collect[], string, NodeOrOptional>(
    () => DIV({}, 'Application did not start yet'),
    () => DIV({}, 'Campaigns are loading'),
    (error) => DIV({ className: 'error' }, `Could not load Campaigns because of: ${error}`),
    (collects) => DIV({ className: 'collect__list' },
        collects.map(renderCollect)),
);

export const render =
    () =>
        DIV({ className: 'home-wrapper' },
            H1({}, tr.collect('homeTitle')),
            DIV({ className: 'home-content' },
                renderEdito(),
                renderCollectList(getRemoteCollects()),
            ),
        );

export default render;
