import { DIV, H2, H3, H1, BUTTON } from "sdi/components/elements";
import { Collect, Question } from "../remote";
import { markdown } from "sdi/ports/marked";
import {
  getChoices,
  getNbOfObservations,
  getObservedQuestions,
  getObserverQuestions,
  getSelectedCollect,
} from "../queries/collect";
import {
  navigateHome,
  navigateObservationForm,
  navigateObservationFormPrint,
} from "../events/route";
import { renderButtonHome, renderButtonPrintForm } from "./buttons";
import tr from "sdi/locale";
import { renderSimpleCollectMap } from "./map";
import {
  annotationTriggerQuestion,
  renderAnnotationWrapperQuestion,
} from "./annotation";

const renderErrorSelection = () =>
  DIV({ className: "error" }, tr.collect("error.noCampaign"));

// FIX ME
const renderQuestionChoices = (q: Question) =>
  DIV(
    { className: "choice__list" },
    getChoices(q.id).map((c) =>
      DIV(
        {
          className: "choice__wrapper",
          key: c.id,
        },
        DIV({ className: "choice__label" }, c.title)
        // DIV({ className: 'choice__description' },
        //     markdown(c.description),
        // )
      )
    )
  );
// END FIX ME

const renderQuestionNumber = (n: number, total: number, q: Question) =>
  DIV(
    {
      className: `question__number ${q.kind}`,
    },
    `${n + 1}/${total}`
  );

const renderQuestion = (q: Question, index: number, questions: Question[]) =>
  DIV(
    {
      className: "question",
      key: `question-${q.id}`,
    },
    DIV(
      { className: "question__infos" },
      renderQuestionNumber(index, questions.length, q),
      H3({}, q.title),
      markdown(q.description),
      renderQuestionChoices(q),
      annotationTriggerQuestion(q)
    ),
    renderAnnotationWrapperQuestion(q)
  );

const collectMapSide = () =>
  DIV(
    { className: "collect-map__side" },
    DIV({ className: "map-legend__title" }, tr.core("legend")),
    DIV({ className: "map-legend__content" }, "~TO BE FIXED")
  );

const collectMap = () =>
  DIV(
    { className: "collect-map__wrapper" },
    H2({}, tr.collect("observationsMap")),
    DIV(
      { className: "collect-map__body" },
      renderSimpleCollectMap(),
      collectMapSide()
    )
  );

const collectForm = (c: Collect) =>
  DIV(
    { className: "question-lists__wrapper" },
    H2({}, tr.collect("labelCollectForm")),
    DIV(
      { className: "question-list observed" },
      // DIV({className:'question__type'}, tr.collect('observedObjectTitle')),
      getObservedQuestions(c.id).map(renderQuestion)
    ),
    DIV(
      { className: "question-list observer" },
      // DIV({className:'question__type'}, tr.collect('situatedObserverTitle')),
      getObserverQuestions(c.id).map(renderQuestion)
    )
  );

const renderAddObservation = (c: Collect) =>
  BUTTON(
    {
      className: "add-observation",
      onClick: () => navigateObservationForm(c.id),
    },
    tr.collect("newObservation")
  );

const renderSelection = (c: Collect) =>
  DIV(
    { className: "collect-infos__body" },
    renderButtonHome(() => navigateHome()),
    DIV(
      { className: "collect-meta__wrapper" },
      H1(
        {
          className: "collect__title",
        },
        c.title
      )
    ),
    DIV({ className: "add-observation__wrapper" }, renderAddObservation(c)),
    DIV(
      { className: "collect__description" },
      markdown(c.description),
      DIV(
        { className: "collect__meta" },
        getNbOfObservations(c.id) > 1
          ? `${getNbOfObservations(c.id)} ${tr.collect(
              "labelObservationMultiple"
            )}`
          : `${getNbOfObservations(c.id)} ${tr.collect(
              "labelObservationSingle"
            )}`,
        renderButtonPrintForm(() => navigateObservationFormPrint(c.id))
      )
      // let's keep that for later
      // DIV({className: 'collect__meta'}, `${tr.collect('lastObservation')} : ~todo`),
      // DIV({className: 'collect__meta'}, `${tr.collect('contactPoint')} : ~todo`),
      // renderActions(c),
    ),
    collectMap(),
    collectForm(c)
  );

export const render = () =>
  DIV(
    { className: "collect-infos__wrapper" },
    getSelectedCollect().foldL(renderErrorSelection, renderSelection)
  );

export default render;
