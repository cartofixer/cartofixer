import { makeLabelAndIcon, makeLabel, makeIcon } from "sdi/components/button";
import { Translated } from "sdi/locale";

// export const renderButtonMap =
//     makeLabelAndIcon('navigate', 3, 'map', () => 'Carte' as Translated);

export const renderButtonViewCollect = makeLabelAndIcon(
  "navigate",
  3,
  "info-circle",
  () => "Informations" as Translated
);

export const renderButtonPrintForm = makeLabelAndIcon(
  "navigate",
  3,
  "print",
  () => "Imprimer" as Translated
);

export const renderButtonAddObervation = makeLabelAndIcon(
  "navigate",
  2,
  "plus",
  () => "Nouvelle observation" as Translated
);

export const renderButtonHome = makeLabelAndIcon(
  "navigate",
  3,
  "chevron-left",
  () => "Accueil" as Translated
);

export const renderButtonViewObservation = makeLabelAndIcon(
  "navigate",
  2,
  "eye",
  () => `Voir l'observation` as Translated
);

export const buttonSave = makeLabel("save", 2, () => "Save" as Translated);

export const buttonAnnotateText = makeIcon("navigate", 3, "i-cursor");
export const buttonAnnotateImage = makeIcon("navigate", 3, "camera");

// export const buttonLargeAnnotateText = makeLabelAndIcon('navigate', 3, 'comment', () => tr.collect('annotateText'));
// export const buttonLargeAnnotateImage = makeLabelAndIcon('navigate', 3, 'camera', () => tr.collect('annotateImage'));
