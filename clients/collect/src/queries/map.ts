import { IMapInfo, IMapBaseLayer, Feature, ILayerInfo, StyleConfig, Inspire, FeatureCollection } from 'sdi/source';
import { query, queryK } from 'sdi/shape';
import { getObservations, getAnswers, getQuestionForAnswer, getSelectedCollect } from './collect';
import { Observation, Collect } from '../remote';
import { nameToCode } from 'sdi/components/button/names';
import { getRootUrl } from 'sdi/app';
import { fromNullable, some } from 'fp-ts/lib/Option';

export const getBaseLayer = (): IMapBaseLayer => ({
  name: {
    fr: 'OpenStreetMap',
    nl: 'OpenStreetMap'
  },
  attribution: {},
  srs: 'EPSG:3857',
  // url: 'https://ows.terrestris.de/osm-gray/service',
  url: 'https://wms.ngi.be/cartoweb/1/service',
  params: {
    VERSION: '1.1.1',
    LAYERS: {
      // fr: 'OSM-WMS',
      // nl: 'OSM-WMS'
      fr: 'topo',
      nl: 'topo'
    },
  }
});




export const getMapView = queryK('port/map/view');
export const getMapInteraction = queryK('port/map/interaction');
export const getMapSelection = queryK('port/map/selection');

export const isTracking = () => getMapInteraction().label === 'track';

export const getMapObservationView = queryK('port/map/observation/view');


export const MAP_COLLECT_NAME = 'collect-map';

export const getMapInfo = (): IMapInfo | null =>
  getSelectedCollect()
    .map<IMapInfo>((c) => ({
      baseLayer: 'urbis.irisnet.be/urbis_gray',
      id: c.id.toString(),
      url: getRootUrl(`collect/map/${c.id}`),
      lastModified: c.timestamp,
      status: 'published',
      title: { en: c.title },
      description: { en: c.description },
      categories: [],
      attachments: [],
      layers: [
        makeLayerInfo(c.id),
      ],
    }))
    .toNullable();


const linkLayerInfo = (
  collectId: number,
): ILayerInfo => ({
  id: `${collectId}-link`,
  featureViewOptions: { type: 'default' },
  group: null,
  layerInfoExtra: null,
  legend: null,
  metadataId: `${collectId}-link`,
  visible: true,
  style: linkLayerStyle,
});

const linkLayerStyle: StyleConfig = {
  kind: 'line-simple',
  dash: [],
  strokeColor: 'black',
  strokeWidth: 1,
};


export const linkLayerMetadata = (
  collect: Collect,
): Inspire => ({
  id: `${collect.id}-link`,
  geometryType: 'LineString',
  resourceTitle: { en: collect.title },
  resourceAbstract: { en: collect.description },
  uniqueResourceIdentifier: `none://${collect.id}.link`,
  topicCategory: [],
  keywords: [],
  geographicBoundingBox: { west: 0.0, north: 0.0, east: 0.0, south: 0.0 },
  temporalReference: {
    creation: (new Date(collect.timestamp)).toLocaleDateString(),
    revision: (new Date()).toLocaleDateString(),
  },
  responsibleOrganisation: [1],
  metadataPointOfContact: [1],
  metadataDate: Date(),
  published: false,
  dataStreamUrl: null,
});

const featureLinkFromObservation = (
  o: Observation,
): Feature => ({
  id: o.id,
  type: 'Feature',
  geometry: {
    type: 'Point',
    coordinates:
      o.observed.coordinates
  },
  properties: makeObsProps(o)
});

export const makeLinkLayerFromCollect = (
  collectId: number,
): FeatureCollection => ({
  type: 'FeatureCollection',
  features: getObservations(collectId).map(featureLinkFromObservation),
});


const makeLayerInfo = (
  collectId: number,
): ILayerInfo => ({
  id: `${collectId}`,
  featureViewOptions: { type: 'default' },
  group: null,
  layerInfoExtra: null,
  legend: null,
  metadataId: `${collectId}`,
  visible: true,
  style: makeStyle(),
});


const makeObsProps = (
  o: Observation,
) => {
  const props: { [k: number]: number } = {};
  getAnswers(o.id)
    .forEach(a =>
      getQuestionForAnswer(a)
        .map(q => {
          props[q.id] = a.choice;
        }));
  return props;
};

const featureFromObservation = (
  o: Observation,
): Feature => ({
  id: o.id,
  type: 'Feature',
  geometry: o.observed,
  properties: makeObsProps(o)
});

export const makeLayerFromCollect = (
  collectId: number,
): FeatureCollection => ({
  type: 'FeatureCollection',
  features: getObservations(collectId).map(featureFromObservation),
});

const makeStyle = (): StyleConfig => ({
  kind: 'point-simple',
  marker: {
    codePoint: nameToCode('circle'),
    size: 12,
    color: 'blue',
  },
});



export const makeMetadataFromCollect = (
  collect: Collect,
): Inspire => ({
  id: `${collect.id}`,
  geometryType: 'Point',
  resourceTitle: { en: collect.title },
  resourceAbstract: { en: collect.description },
  uniqueResourceIdentifier: `none://${collect.id}`,
  topicCategory: [],
  keywords: [],
  geographicBoundingBox: { west: 0.0, north: 0.0, east: 0.0, south: 0.0 },
  temporalReference: { creation: Date(), revision: Date() },
  responsibleOrganisation: [1],
  metadataPointOfContact: [1],
  metadataDate: Date(),
  published: false,
  dataStreamUrl: null,
});


export const makeLayerInfoOptionFromCollect = (
  collect: Collect,
) =>
  () => some({
    name: { en: collect.title },
    info: makeLayerInfo(collect.id),
    metadata: makeMetadataFromCollect(collect),
  });

export const makeLinkLayerInfoOptionFromCollect = (
  collect: Collect,
) =>
  () => some({
    name: { en: collect.title },
    info: linkLayerInfo(collect.id),
    metadata: linkLayerMetadata(collect),
  });

export const getMiniMap =
  (k: string) => fromNullable(query('component/harvest/minimap')[k])