import { query } from 'sdi/shape';
import { fromNullable, some, none } from 'fp-ts/lib/Option';


export const getLayout =
    () => query('app/layout');


export const getUsername =
    () => fromNullable(query('data/user')).map(u => u.name);


export const getAppName = () => query('app/name');


export const findUser = (
    id: number | null
) => fromNullable(id)
    .map(id => id.toString())
    .chain(uid =>
        fromNullable(query('data/users')
            .find(u => u.id === uid)));



export const getUnkownUsers = (
    ids: number[]
) => {
    // const uids = ids.map(i => i.toString());
    const knowns = query('data/users').map(u => parseInt(u.id, 10));
    return ids.filter(i => knowns.indexOf(i) < 0);
};

export const isAnnotationObsOpen = (id: number) =>
    fromNullable(query('annotation/open/observation')).chain(n => n === id ? some(id) : none);

export const isAnnotationQuestionOpen = (id: number) =>
    fromNullable(query('annotation/open/question')).chain(n => n === id ? some(id) : none);

export const isAnnotationAnswerOpen = (id: number) =>
    fromNullable(query('annotation/open/answer')).chain(n => n === id ? some(id) : none);

