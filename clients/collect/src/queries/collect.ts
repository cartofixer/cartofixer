import { queryK, query } from 'sdi/shape';
import { fromNullable, none, some } from 'fp-ts/lib/Option';
import { AnnotationTargetKind, Answer, Choice, Observation, Question } from '../remote';
import { remoteToOption, ILayerInfo, Inspire, IMapInfo, Feature } from 'sdi/source';
import { nameToCode } from 'sdi/components/button/names';
import { getCollectionItem, point } from 'sdi/util';
import { makeImageUploaderQueries } from 'sdi/components/upload';
import { Extent } from 'ol/extent';
import { InteractionTrack, TrackerCoordinate, tryCoord2D, withInteractionOpt } from 'sdi/map';
import { getMapInteraction } from './map';
import { last } from 'fp-ts/lib/Array';
import { scopeOption } from 'sdi/lib/scope';



const collects = queryK('data/collect/collect');
const questions = queryK('data/collect/question');
const choices = queryK('data/collect/choice');
const observations = queryK('data/collect/observation');
const answers = queryK('data/collect/answer');


export const getRemoteCollects = collects;
export const getRemoteQuestions = questions;
export const getRemoteObservations = observations;
export const getRemoteAnswers = answers;
export const getRemoteChoices = choices;


export const getQuestions = (
    collectId: number,
) => remoteToOption(questions())
    .getOrElse([])
    .filter(q => q.collect === collectId)
    .sort((a, b) => a.order < b.order ? -1 : 1);

export const getObserverQuestions = (
    collectId: number,
) => remoteToOption(questions())
    .getOrElse([])
    .filter(q => q.collect === collectId && q.kind === 'observer')
    .sort((a, b) => a.order < b.order ? -1 : 1);


export const getObservedQuestions = (
    collectId: number,
) => remoteToOption(questions())
    .getOrElse([])
    .filter(q => q.collect === collectId && q.kind === 'observed')
    .sort((a, b) => a.order < b.order ? -1 : 1);

export const getSelectedObservationUser = () => getSelectedObservation().chain(o => fromNullable(o.user)).map(u => u.toString()).getOrElse('')

export const getAnswerAndQuestionForChoice = (c: Choice) =>
    getFormAnswerObservation()
        .chain(o => {
            return scopeOption()
                .let('a', findAnswerForChoice(c.id, o))
                .let('q', findQuestion(c.question))
        })

export const getChoices = (
    questionId: number,
) => remoteToOption(choices())
    .getOrElse([])
    .filter(c => c.question === questionId)
    .sort((a, b) => a.order < b.order ? -1 : 1);



export const getObservations = (
    collectId: number,
) => remoteToOption(observations())
    .getOrElse([])
    .filter(obs => obs.collect === collectId);

export const getNbOfObservations = (
    collectId: number
) => getObservations(collectId).length;

export const getAnswers = (
    observationId: number,
) => remoteToOption(answers())
    .getOrElse([])
    .filter(a => a.observation === observationId);



export const findCollect = (
    id: number,
) => remoteToOption(collects())
    .chain(items =>
        fromNullable(items.find(c => c.id === id))
    );


export const findQuestion = (
    id: number,
) => remoteToOption(questions())
    .chain(items =>
        fromNullable(items.find(c => c.id === id))
    );

export const findChoice = (
    id: number,
) => remoteToOption(choices())
    .chain(items =>
        fromNullable(items.find(c => c.id === id))
    );


export const findObservation = (
    id: number,
) => remoteToOption(observations())
    .chain(items =>
        fromNullable(items.find(c => c.id === id))
    );

export const findAnswer = (
    id: number,
) => remoteToOption(answers())
    .chain(items =>
        fromNullable(items.find(c => c.id === id))
    );

export const findAnswerForChoice = (
    choiceId: number,
    obsId: number
) => remoteToOption(answers())
    .chain(items =>
        fromNullable(items.filter(a => a.choice === choiceId).find(a => a.observation === obsId)));

export const getQuestionForAnswer = (
    a: Answer,
) => findChoice(a.choice)
    .chain(c => findQuestion(c.question));

export const isSelectedChoice = (
    c: Choice,
) => getFormAnswerObservation()
    .map(obsId => getAnswers(obsId).findIndex(a => a.choice === c.id) >= 0)
    .getOrElse(false);



export const pickChoice = (a: Answer) => a.choice;
export const pickQuestion = (c: Choice) => c.question;
export const pickCollect = (q: Question) => q.collect;



export const getSelectedCollect = () =>
    fromNullable(query('collect/selected/collect'))
        .chain(findCollect);


export const getSelectedObservation = () =>
    fromNullable(query('collect/selected/observation'))
        .chain(findObservation);

const getNext = <A>(arr: A[], id: number) => (id >= 0 && arr.length > id + 1) ? some(arr[id + 1]) : none;

export const getNextObservation = (obs: Observation) => {
    const observations = getObservations(obs.collect).sort((a, b) => a.timestamp - b.timestamp);
    const thisObsId = observations.findIndex(o => o.id === obs.id)
    return getNext(observations, thisObsId);
}
export const getPreviousObservation = (obs: Observation) => {
    const observations = getObservations(obs.collect).sort((a, b) => b.timestamp - a.timestamp);
    const thisObsId = observations.findIndex(o => o.id === obs.id)
    return getNext(observations, thisObsId);
}

export const getFormObservationInteraction = queryK('collect/map/interaction');



export const getFormObservation = () => fromNullable(query('collect/form/observation/observed'));
export const getFormObservationCollect = () => fromNullable(query('collect/form/observation/collect'));


export const getFormAnswerObservation = () => fromNullable(query('collect/form/answer/observation'));


// form map
export const MAP_OBSERVATION_FORM = 'map-observation-form';
export const LAYER_OBSERVATION_FORM = 'layer-observation-form';
export const LAYER_TRACKER_OBSERVATION_FORM = 'layer-observation-form';

const observationMetadata: Inspire = {
    id: LAYER_OBSERVATION_FORM,
    geometryType: 'Point',
    resourceTitle: { en: LAYER_OBSERVATION_FORM },
    resourceAbstract: { en: LAYER_OBSERVATION_FORM },
    uniqueResourceIdentifier: LAYER_OBSERVATION_FORM,
    topicCategory: [],
    keywords: [],
    geographicBoundingBox: { west: 0.0, north: 0.0, east: 0.0, south: 0.0 },
    temporalReference: { creation: Date(), revision: Date() },
    responsibleOrganisation: [1],
    metadataPointOfContact: [1],
    metadataDate: Date(),
    published: false,
    dataStreamUrl: null,
};
const observationTrackerMetadata: Inspire = {
    id: LAYER_TRACKER_OBSERVATION_FORM,
    geometryType: 'Point',
    resourceTitle: { en: LAYER_TRACKER_OBSERVATION_FORM },
    resourceAbstract: { en: LAYER_TRACKER_OBSERVATION_FORM },
    uniqueResourceIdentifier: LAYER_TRACKER_OBSERVATION_FORM,
    topicCategory: [],
    keywords: [],
    geographicBoundingBox: { west: 0.0, north: 0.0, east: 0.0, south: 0.0 },
    temporalReference: { creation: Date(), revision: Date() },
    responsibleOrganisation: [1],
    metadataPointOfContact: [1],
    metadataDate: Date(),
    published: false,
    dataStreamUrl: null,
};

const observationLayerInfo: ILayerInfo = {
    id: LAYER_OBSERVATION_FORM,
    legend: null,
    group: null,
    metadataId: LAYER_OBSERVATION_FORM,
    visible: true,
    featureViewOptions: { type: 'default' },
    style: {
        kind: 'point-simple',
        marker: {
            codePoint: nameToCode('circle'),
            size: 12,
            color: '#3668b3',
        },
    },
    layerInfoExtra: null,
};

const observationTrackerLayerInfo: ILayerInfo = {
    id: LAYER_TRACKER_OBSERVATION_FORM,
    legend: null,
    group: null,
    metadataId: LAYER_TRACKER_OBSERVATION_FORM,
    visible: true,
    featureViewOptions: { type: 'default' },
    style: {
        kind: 'point-simple',
        marker: {
            codePoint: nameToCode('circle-o'),
            size: 30,
            color: '#3399CC',
        },
    },
    layerInfoExtra: null,
};

const observationMapInfo: IMapInfo = ({
    baseLayer: 'urbis.irisnet.be/urbis_gray',
    id: MAP_OBSERVATION_FORM,
    url: `/dev/null/write/`,
    lastModified: Date.now(),
    status: 'published',
    title: { fr: '', nl: '', en: '' },
    description: { fr: '', nl: '', en: '' },
    categories: [],
    attachments: [],
    layers: [
        observationTrackerLayerInfo,
        observationLayerInfo
    ],
});

export const observationInfoOption = () => some({
    name: { en: LAYER_OBSERVATION_FORM },
    info: observationLayerInfo,
    metadata: observationMetadata,
});

export const observationTrackerInfoOption = () => some({
    name: { en: 'trackerinfo' },
    info: observationTrackerLayerInfo,
    metadata: observationTrackerMetadata,
});

export const getObservationMapInfo = () => observationMapInfo;

const withTrack = withInteractionOpt<InteractionTrack>('track', ({ state }) => state.track)

export const getObservationFeatures = () => getFormObservation().map<Feature[]>(geometry => ([{
    type: 'Feature',
    id: Date.now(),
    properties: {},
    geometry
}]))
    .getOrElse([]);

export const getTrackerObservationFeatures = () => withTrack(getMapInteraction())
    .chain<TrackerCoordinate>(last)
    .pick('coord')
    .chain(tryCoord2D)
    .map<Feature[]>(coord => ([{
        type: 'Feature',
        id: Date.now(),
        properties: {},
        geometry: point(coord),
    }]))
    .getOrElse([]);

// end of form map


export const getAnnotationKind = () => query('collect/annotation-form/kind');

export const getAnnotationTarget = () => fromNullable(query('collect/annotation-form/target'));

export const getAnnotationText = () => fromNullable(query('collect/annotation-form/text'));

export const getAnnotationImage = () => fromNullable(query('collect/annotation-form/image'));

export const getAllAnnotations = () => query('data/collect/annotations');

export const getAnnotations = (
    collectId: number,
) => getCollectionItem(collectId.toString(), getAllAnnotations());

export const getAnnotationsFromObs = (o: Observation) =>
    getAnnotations(o.collect)
        .map(annotations =>
            annotations
                .filter(({ target }) =>
                    target.kind === 'observation'
                    && target.id === o.id)
        );

export const getAnnotationsFromQuestion = (q: Question) =>
    getAnnotations(q.collect)
        .map(annotations =>
            annotations
                .filter(({ target }) =>
                    target.kind === 'question'
                    && target.id === q.id)
        );

export const getAnnotationsFromAnswer = (a: Answer, q: Question) =>
    getAnnotations(q.collect)
        .map(annotations =>
            annotations
                .filter(({ target }) =>
                    target.kind === 'answer'
                    && target.id === a.id));

export const getTargetAnnotations = (
    collectId: number,
    targetKind: AnnotationTargetKind,
    targetId: number,
) => getAnnotations(collectId)
    .map(annotations => annotations.filter(({ target }) =>
        target.kind === targetKind
        && target.id === targetId));


export const getObservationAnnotations = (
    o: Observation,
) => getTargetAnnotations(o.collect, 'observation', o.id)

export const getQuestionAnnotations = (
    q: Question,
) => getTargetAnnotations(q.collect, 'question', q.id)

export const getAnswerAnnotations = (
    a: Answer,
    q: Question,
) => getTargetAnnotations(q.collect, 'answer', a.id)

export const imageUploaderQueries = makeImageUploaderQueries(queryK('collect/annotation-form/image'));




export const getCollectExtent = (
    collectId: number,
) => findCollect(collectId)
    .map((collect) => {
        const exteriorRing = collect.extent.coordinates[0];
        const extent = exteriorRing
            .reduce<Extent>(([
                minx,
                miny,
                maxx,
                maxy,
            ], coord) => [
                    Math.min(minx, coord[0]),
                    Math.min(miny, coord[1]),
                    Math.max(maxx, coord[0]),
                    Math.max(maxy, coord[1]),
                ], [
                Number.MAX_VALUE,
                Number.MAX_VALUE,
                Number.MIN_VALUE,
                Number.MIN_VALUE,
            ]);
        return extent;
    });
