
import {
    fetchIO,
    IUser,
    IUserIO,
    IUserList,
    IUserListIO,
} from 'sdi/source';


export const fetchUser =
    (url: string): Promise<IUser> =>
        fetchIO(IUserIO, url);


export const fetchAllUsers =
    (url: string): Promise<IUserList> =>
        fetchIO(IUserListIO, url);

