// tslint:disable: variable-name
import * as io from 'io-ts';
import { fetchIO, PointIO, PolygonIO, postIO } from 'sdi/source';
import { getApiUrl } from 'sdi/app';
import { nullable } from 'sdi/source/io/io';


const CollectIO = io.interface({
    id: io.number,
    title: io.string,
    description: io.string,
    timestamp: io.number,
    extent: PolygonIO,
}, 'CollectIO');


const QuestionKindIO = io.union([
    io.literal('observer'),
    io.literal('observed'),
], 'QuestionKindIO');

const QuestionIO = io.interface({
    kind: QuestionKindIO,
    id: io.number,
    title: io.string,
    description: io.string,
    collect: io.number,
    order: io.number,
}, 'QuestionIO');


const ChoiceIO = io.interface({
    id: io.number,
    title: io.string,
    description: io.string,
    question: io.number,
    order: io.number,
}, 'ChoiceIO');


const ObservationIO = io.interface({
    id: io.number,
    user: nullable(io.number),
    collect: io.number,
    timestamp: io.number,
    observed: PointIO,
}, 'ObservationIO');


const AnswerIO = io.interface({
    id: io.number,
    observation: io.number,
    choice: io.number,
    timestamp: io.number,
}, 'AnswerIO');


const CollectListIO = io.array(CollectIO, 'CollectListIO');
const QuestionListIO = io.array(QuestionIO, 'QuestionListIO');
const ChoiceListIO = io.array(ChoiceIO, 'ChoiceListIO');
const ObservationListIO = io.array(ObservationIO, 'ObservationListIO');
const AnswerListIO = io.array(AnswerIO, 'AnswerListIO');


export type Collect = io.TypeOf<typeof CollectIO>;
export type Question = io.TypeOf<typeof QuestionIO>;
export type Choice = io.TypeOf<typeof ChoiceIO>;
export type Observation = io.TypeOf<typeof ObservationIO>;
export type Answer = io.TypeOf<typeof AnswerIO>;
export type QuestionKind = io.TypeOf<typeof QuestionKindIO>;


const url = (path: string) => getApiUrl(`geodata/collect/${path}`);

export const fetchCollectList = () =>
    fetchIO(CollectListIO, url('collect/'));


export const fetchQuestionList = () =>
    fetchIO(QuestionListIO, url('question/'));


export const fetchChoiceList = () =>
    fetchIO(ChoiceListIO, url('choice/'));


export const fetchObservationList = () =>
    fetchIO(ObservationListIO, url('observation/'));


export const fetchAnswerList = () =>
    fetchIO(AnswerListIO, url('answer/'));


export const postObservation = (
    collectId: number,
    observation: Partial<Observation>,
) => postIO(ObservationIO, url(`observation/${collectId}`), observation);

export const postAnswer = (
    observarionId: number,
    answer: Partial<Answer>,
) => postIO(AnswerIO, url(`answer/${observarionId}/`), answer);



export const AnnotationTargetKindIO = io.union([
    io.literal('question'),
    io.literal('choice'),
    io.literal('observation'),
    io.literal('answer'),
    io.literal('annotation'),
], 'AnnotationTargetKindIO');

const AnnotationTargetIO = io.interface({
    kind: AnnotationTargetKindIO,
    id: io.number,
}, 'AnnotationTargetIO');


const AnnotationBaseIO = io.interface({
    id: io.number,
    collect: io.number,
    target: AnnotationTargetIO,
    user: nullable(io.number),
    timestamp: io.number,
}, 'AnnotationBaseIO');



const AnnotationTextKindIO = io.literal('text');
const AnnotationImageKindIO = io.literal('image');
export const AnnotationKindIO = io.union([
    AnnotationTextKindIO,
    AnnotationImageKindIO,
], 'AnnotationKindIO');


const AnnotationTextIO = io.intersection([
    AnnotationBaseIO,
    io.interface({ kind: AnnotationTextKindIO }),
    io.interface({
        text: io.string,
    })
], 'AnnotationTextIO');

const AnnotationImageIO = io.intersection([
    AnnotationBaseIO,
    io.interface({ kind: AnnotationImageKindIO }),
    io.interface({
        image: io.string, // actually UUIDv4, so we might even do better validation at some point
    })
], 'AnnotationImageIO');


const AnnotationIO = io.union([
    AnnotationTextIO,
    AnnotationImageIO,
], 'AnnotationIO');


export type AnnotationTargetKind = io.TypeOf<typeof AnnotationTargetKindIO>;
export type AnnotationTarget = io.TypeOf<typeof AnnotationTargetIO>;
export type AnnotationKind = io.TypeOf<typeof AnnotationKindIO>;
export type AnnotationText = io.TypeOf<typeof AnnotationTextIO>;
export type AnnotationImage = io.TypeOf<typeof AnnotationImageIO>;
export type Annotation = io.TypeOf<typeof AnnotationIO>;




export const fetchAnnotationList = (
    collectId: number
) => fetchIO(io.array(AnnotationIO), url(`annotation/${collectId}/`));


export const postTextAnnotation = (
    annotation: Omit<AnnotationText, 'id' | 'timestamp' | 'user'>
) => postIO(AnnotationIO, url(`annotation/`), annotation);


export const postImageAnnotation = (
    annotation: Omit<AnnotationImage, 'id' | 'timestamp' | 'user'>
) => postIO(AnnotationIO, url(`annotation/`), annotation);



