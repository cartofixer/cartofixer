export const homeTextFR = `
Bienvenue sur l'application *collectes*, actuellement en version beta-fr, et activement développée.

Cet outil permet de créer des campagnes d'observations sur mesure, sous forme de questions à choix multiples, et définies collectivement. 

Les questions peuvent concerner l'observation et le contexte de cette dernière, afin d'enrichir la lecture des données collectées.

Un système d'annotations permet d'associer du texte libre et des images à une observation, mais aussi de commenter des relevés effectués par d'autres.

L'analyse et la visualisation des données collectées constituent également un moment collectif, permettant de dégager du sens vis à vis de l'hypothèse de collecte initiale.

L'objectif de cet outil est de permettre la récolte de données structurées, tout en en encadrant le dispositif technique par un cadre social, collectif et inclusif.

Pour démarrer une nouvelles campagne d'observation, contactez nous à contact@cartofixer.be

`;

export const homeTextNL = `
Welkom bij de *collectes* applicatie, momenteel in beta en FR versie, en actief ontwikkeld.

Deze tool maakt het mogelijk om op maat gemaakte observatiecampagnes te maken, in de vorm van meerkeuzevragen, en collectief te definiëren. 

De vragen kunnen zowel betrekking hebben op de waarneming als op de context van de waarneming, om het lezen van de verzamelde gegevens te verrijken.

Een annotatie- en commentaarsysteem maakt het ook mogelijk om vrije tekst en afbeeldingen te koppelen aan een observatie, maar ook om commentaar te geven op enquêtes die door anderen zijn uitgevoerd.

De analyse van de verzamelde gegevens en hun visualisatie is ook een collectief moment, dat het mogelijk maakt om de initiële verzamelhypothese te begrijpen.

Het doel van dit instrument is om het verzamelen van gestructureerde gegevens mogelijk te maken door het technische systeem in een sociaal, collectief en inclusief kader te plaatsen.

Om een nieuwe observatiecampagne te starten, kunt u contact met ons opnemen via contact@cartofixer.be.

`;

export const helpObservationFR = `
Les choix présentés dans ce formulaire sont **facultatifs**.

Les réponses sont **enregistrées au fur et à mesure**, il n'y a donc pas besoin de sauver vos réponses à la fin.
`;

export const helpObservationNL = `
De keuzes in dit formulier zijn **optioneel**.

De antwoorden worden **opgenomen terwijl u bezig bent**, dus u hoeft uw antwoorden aan het eind niet op te slaan.
`;

export const helpObservationEN = `
The choices presented in this form are **optional**.

Answers are **recorded as you go**, so there is no need to save your answers at the end.
`;


export const homeDisclaimerFR = ``; // to be defined in admin/lingua
export const homeDisclaimerNL = ``; // to be defined in admin/lingua
