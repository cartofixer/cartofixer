import { MessageStore, formatMessage } from "sdi/locale";
import * as texts from "./marked-texts";

const messages = {
  "helptext.home": {
    fr: texts.homeTextFR,
    nl: texts.homeTextNL,
    en: "welcome",
  },

  "helptext.observationAnswer": {
    fr: texts.helpObservationFR,
    nl: texts.helpObservationNL,
    en: texts.helpObservationEN,
  },

  "error.noCampaign": {
    fr: "Désolé, impossible de trouuver la campagne sélectionnée",
    nl: "Sorry, kon de geselecteerde campagne niet vinden",
    en: "Sorry, could not find the selected campaign",
  },

  homeTitle: {
    fr: `Campagnes d'observation`,
    en: "Observation campaign",
  },

  labelObservationSingle: {
    fr: `observation`,
    nl: "to do",
    en: "observation",
  },

  labelObservationMultiple: {
    fr: `observations`,
    nl: "to do",
    en: "observations",
  },

  labelCollectForm: {
    fr: `Prévisualisation du formulaire de collecte`,
    nl: "to do",
    en: "Collect form preview",
  },

  lastObservation: {
    fr: `Dernière`,
    nl: "to do",
    en: "Last",
  },

  contactPoint: {
    fr: `Point de contact`,
    nl: "to do",
    en: "Point of contact",
  },

  addTextTitle: {
    fr: `Commenter avec une note`,
    en: "Comment with a note",
  },

  addImageTitle: {
    fr: `Commenter avec une image`,
    en: "Comment with an image",
  },

  annotateText: {
    fr: "Ajouter une note",
    en: "Add a note",
  },

  annotateImage: {
    fr: "Ajouter une image",
    en: "Add an image",
  },

  newObservation: {
    fr: "Nouvelle observation",
    nl: "to do",
    en: "to do",
  },

  findPosition: {
    fr: "Se géolocaliser",
    nl: "Geolocalisatie",
    en: "",
  },

  observedObjectTitle: {
    fr: `Au sujet de l'observation`,
    nl: "to do",
    en: "to do",
  },

  situatedObserverTitle: {
    fr: "À propos de la personne qui observe",
    nl: "to do",
    en: "to do",
  },

  unansweredQuestions: {
    fr: "Questions sans réponses",
    nl: "Onbeantwoorde vragen",
    en: "Unanswered questions",
  },

  anonymous: {
    fr: "anonyme",
    nl: "anoniem",
    en: "anonymous",
  },

  observedOn: {
    fr: "Observation du",
    nl: "Waarneming van",
    en: "Observation of",
  },

  observedBy: {
    fr: "par",
    nl: "door",
    en: "by",
  },

  locateObservation: {
    fr: `Localiser l'observation`,
    nl: "to do",
    en: "to do",
  },

  saveAndContinue: {
    fr: "Valider et continuer",
    nl: "to do",
    en: "to do",
  },

  observationsMap: {
    fr: "Carte des observations",
    nl: "to do",
    en: "to do",
  },
  stop: {
    fr: "Arrêter la geolocalisation",
    nl: "Stop geolocatie",
    en: "Stop geolocation",
  },
  singleNoteObservation: {
    fr: "note pour cette observation",
    nl: "todo",
    en: "note",
  },
  multipleNotesObservation: {
    fr: "notes pour cette observation",
    nl: "todo",
    en: "notes",
  },
  singleNoteAnswer: {
    fr: "note pour cette réponse",
    nl: "todo",
    en: "note",
  },
  multipleNotesAnswer: {
    fr: "notes pour cette réponse",
    nl: "todo",
    en: "notes",
  },
  singleComment: {
    fr: "commentaire sur cette question",
    nl: "todo",
    en: "comment",
  },
  multipleComments: {
    fr: "commentaires sur cette question",
    nl: "todo",
    en: "comments",
  },
  labelEditionMode: {
    fr: "mode édition",
    nl: "todo",
    en: "edition mode",
  },
  next: {
    fr: "suivant",
    nl: "volgende",
    en: "next",
  },
  previous: {
    fr: "précédente",
    nl: "vorige",
    en: "previous",
  },
  printFormPlace: {
    fr: "Lieu :",
    nl: "Locatie :",
    en: "Location :",
  },
  printFormName: {
    fr: "(Sur)Nom :",
    nl: "(Bij)Naam :",
    en: "(Nick)Name :",
  },
  printFormComment: {
    fr: "Commentaire :",
    nl: "Commentaar :",
    en: "Comment :",
  },
  printFormDate: {
    fr: "Date : ",
    nl: "Datum : ",
    en: "Date : ",
  },
};

type MDB = typeof messages;
export type CollectMessageKey = keyof MDB;

declare module "sdi/locale" {
  export interface MessageStore {
    collect(k: CollectMessageKey): Translated;
  }
}

MessageStore.prototype.collect = function (k: CollectMessageKey) {
  return this.getEdited("collect", k, () => formatMessage(messages[k]));
};
