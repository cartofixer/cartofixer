import { Nullable } from 'sdi/util';
import { IUser, IUserList } from 'sdi/source';
import { Layout, Route } from '../events/route';
import { ButtonComponent } from 'sdi/components/button';

// export const annotationStatusDefault = {
//     'observation': false,
//     'question': false,
//     'answer': false
// }
// export type AnnotationStatus = typeof annotationStatusDefault;
// export type AnnotationType = keyof AnnotationStatus;

// State Augmentation
declare module 'sdi/shape' {
    export interface IShape {
        'app/layout': Layout;
        'app/selectd-item': Nullable<string>;
        'data/user': Nullable<IUser>;
        'component/button': ButtonComponent;
        'annotation/open/observation': Nullable<number>;
        'annotation/open/question': Nullable<number>;
        'annotation/open/answer': Nullable<number>;

        'navigate/next': Nullable<Route>;
        'app/name': string;
        'data/users': IUserList;
    }
}


export const defaultAppShape =
    () => ({
        'app/layout': 'home' as Layout,
        'app/selectd-item': null,
        'data/user': null,
        'component/button': {},
        'annotation/open/observation': null,
        'annotation/open/question': null,
        'annotation/open/answer': null,

        'navigate/next': null,
        'app/name': '',
        'data/users': [],
    });
