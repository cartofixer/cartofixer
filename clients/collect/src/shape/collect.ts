import {
    Collect,
    Question,
    Choice,
    Observation,
    Answer,
    AnnotationTarget,
    Annotation,
    AnnotationKind,
} from '../remote';
import { RemoteResource, remoteNone, Point } from 'sdi/source';
import { Nullable, Collection } from 'sdi/util';
import { Interaction, InteractionCreate } from 'sdi/map';
import { ImageUploaderState, defaultImageUploaderState } from 'sdi/components/upload';

declare module 'sdi/shape' {
    export interface IShape {
        'data/collect/collect': RemoteResource<Collect[]>;
        'data/collect/question': RemoteResource<Question[]>;
        'data/collect/choice': RemoteResource<Choice[]>;
        'data/collect/observation': RemoteResource<Observation[]>;
        'data/collect/answer': RemoteResource<Answer[]>;
        'data/collect/annotations': Collection<Annotation[]>;

        'collect/selected/collect': Nullable<number>;
        'collect/selected/observation': Nullable<number>;

        'collect/form/observation/observed': Nullable<Point>;
        'collect/form/observation/collect': Nullable<number>;
        'collect/form/answer/observation': Nullable<number>;
        'collect/form/answer/question': Nullable<number>;

        'collect/map/interaction': Interaction;

        'collect/annotation-form/target': Nullable<AnnotationTarget>;
        'collect/annotation-form/text': Nullable<string>;
        'collect/annotation-form/image': ImageUploaderState;
        'collect/annotation-form/kind': AnnotationKind;
    }
}


export const defaultCollectShape =
    () => ({
        'data/collect/collect': remoteNone,
        'data/collect/question': remoteNone,
        'data/collect/choice': remoteNone,
        'data/collect/observation': remoteNone,
        'data/collect/answer': remoteNone,
        'data/collect/annotations': {},

        'collect/selected/collect': null,
        'collect/selected/observation': null,
        'collect/form/observation/observed': null,
        'collect/form/observation/collect': null,
        'collect/form/answer/observation': null,
        'collect/form/answer/question': null,
        'collect/map/interaction': { label: 'create', state: { geometryType: 'Point' } } as InteractionCreate,

        'collect/annotation-form/target': null,
        'collect/annotation-form/text': null,
        'collect/annotation-form/image': defaultImageUploaderState(),
        'collect/annotation-form/kind': 'text' as AnnotationKind,
    });
