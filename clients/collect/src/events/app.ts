import { assign, assignK, dispatch } from 'sdi/shape';
import { IUser } from 'sdi/source';
import { Layout } from './route';
import { fetchAllUsers } from '../remote';
import { getApiUrl } from 'sdi/app';
import { isAnnotationAnswerOpen, isAnnotationObsOpen, isAnnotationQuestionOpen } from '../queries/app';

export const setLayout =
    (l: Layout) => assign('app/layout', l);


export const setUserData =
    (u: IUser) => assign('data/user', u);


export const setAppName = (n: string) => assign('app/name', n);


export const loadAllUsers = () =>
    fetchAllUsers(getApiUrl('users'))
        .then(assignK('data/users'))
        .catch(err => console.error(err));

export const changeAnnotationStatusObs = (obsId: number) => {
    isAnnotationObsOpen(obsId).fold(
        dispatch('annotation/open/observation', () => obsId),
        id => id === obsId ? dispatch('annotation/open/observation', () => null) :
            dispatch('annotation/open/observation', () => obsId)
    );
}
export const changeAnnotationStatusQuestion = (questionId: number) => {
    isAnnotationQuestionOpen(questionId).fold(
        dispatch('annotation/open/question', () => questionId),
        id => id === questionId ? dispatch('annotation/open/question', () => null) :
            dispatch('annotation/open/question', () => id)
    );
}
export const changeAnnotationStatusAnswer = (answerId: number) => {
    console.log(`ANSWER: ${answerId}`)
    isAnnotationAnswerOpen(answerId).fold(
        dispatch('annotation/open/answer', () => answerId),
        id => id === answerId ? dispatch('annotation/open/answer', () => null) :
            dispatch('annotation/open/answer', () => answerId)
    );
}