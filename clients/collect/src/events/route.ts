import * as debug from "debug";
import { index } from "fp-ts/lib/Array";
import { TypeOf, union, literal } from "io-ts";
import { Router, Path } from "sdi/router";
import { tryNumber, noop } from "sdi/util";
import { setLayout } from "./app";
import { foldRemote, onRemoteNone } from "sdi/source";
import {
  loadCollectList,
  selectCollect,
  loadQuestionList,
  loadObservationList,
  selectObservation,
  loadAnswerList,
  loadChoiceList,
  setFormObservationCollect,
  resetFormObservationObserved,
  addObservationLayer,
  setFormAnswerObservation,
  setAnnotationFormTarget,
  setAnnotationFormKind,
  resetAnnotationText,
  resetAnnotationImage,
} from "./collect";
import {
  getRemoteCollects,
  getRemoteQuestions,
  getRemoteObservations,
  getRemoteAnswers,
  getRemoteChoices,
  getSelectedCollect,
  findObservation,
} from "../queries/collect";
import { fromEither } from "fp-ts/lib/Option";
import { loadCollectMap, zoomToExtent } from "./map";
import { scopeOption } from "sdi/lib";
import {
  AnnotationTargetKindIO,
  AnnotationKindIO,
  AnnotationKind,
  AnnotationTargetKind,
} from "../remote";

const logger = debug("sdi:route");

// tslint:disable-next-line: variable-name
const RouteIO = union([
  literal("home"),
  literal("collect"),
  literal("map"),
  literal("observation"),
  literal("observation-form"),
  literal("observation-form-print"),
  literal("answer-form"),
  literal("annotation-form"),
]);
export type Route = TypeOf<typeof RouteIO>;

const { home, route, navigate, back } = Router<Route>("collect");

export const navigateBack = back;

export type Layout = Route;

// parsers

const collectParser = (p: Path) => index(0, p).chain((n) => tryNumber(n));

const answerParser = (p: Path) =>
  scopeOption()
    .let(
      "collectId",
      index(0, p).chain((n) => tryNumber(n))
    )
    .let(
      "observationId",
      index(1, p).chain((n) => tryNumber(n))
    );

const annotationParser = (p: Path) =>
  scopeOption()
    .let(
      "collectId",
      index(0, p).chain((n) => tryNumber(n))
    )
    .let(
      "kind",
      index(1, p).chain((t) => fromEither(AnnotationKindIO.decode(t)))
    )
    .let(
      "targetKind",
      index(2, p).chain((t) => fromEither(AnnotationTargetKindIO.decode(t)))
    )
    .let(
      "targetId",
      index(3, p).chain((n) => tryNumber(n))
    );

// Declare route handlers

home("home", () => {
  foldRemote(loadCollectList, noop, noop, noop)(getRemoteCollects());
  foldRemote(loadObservationList, noop, noop, noop)(getRemoteObservations());
  setLayout("home");
});

const onCollectsNone = onRemoteNone(getRemoteCollects);
const onQuestionsNone = onRemoteNone(getRemoteQuestions);
const onChoicesNone = onRemoteNone(getRemoteChoices);
const onObservationsNone = onRemoteNone(getRemoteObservations);
const onAnswersNone = onRemoteNone(getRemoteAnswers);

route(
  "collect",
  (optId) => {
    optId.map((id) => {
      const select = () => {
        selectCollect(id);
        setLayout("collect");
        loadCollectMap(id);
      };

      onCollectsNone(() => loadCollectList().then(select));
      onQuestionsNone(() => loadQuestionList().then(select));
      onObservationsNone(() => loadObservationList().then(select));
      onChoicesNone(() => loadChoiceList().then(select));

      select();
    });
  },
  collectParser
);

route(
  "map",
  (optId) => {
    optId.map((id) => {
      const select = () => {
        setLayout("map");
        selectCollect(id);
        loadCollectMap(id);
      };

      onCollectsNone(() => loadCollectList().then(select));
      onObservationsNone(() => loadObservationList().then(select));

      select();
    });
  },
  collectParser
);

route(
  "observation",
  (optId) => {
    optId.map((id) => {
      const select = () => {
        selectObservation(id);
        findObservation(id).map((obs) => {
          selectCollect(obs.collect);
          setFormAnswerObservation(obs.id);
        });
        setLayout("observation");
      };

      onCollectsNone(() => loadCollectList().then(select));
      onQuestionsNone(() => loadQuestionList().then(select));
      onChoicesNone(() => loadChoiceList().then(select));
      onAnswersNone(() => loadAnswerList().then(select));
      onObservationsNone(() => loadObservationList().then(select));

      select();
    });
  },
  collectParser
);

route(
  "observation-form",
  (optId) => {
    optId.map((id) => {
      const select = () => {
        selectCollect(id);
        setFormObservationCollect(id);
        resetFormObservationObserved();
        addObservationLayer();
        setLayout("observation-form");
        zoomToExtent(id, "observation");
      };

      onCollectsNone(() => loadCollectList().then(select));
      onQuestionsNone(() => loadQuestionList().then(select));
      onChoicesNone(() => loadChoiceList().then(select));
      onAnswersNone(() => loadAnswerList().then(select));
      onObservationsNone(() => loadObservationList().then(select));

      select();
    });
  },
  collectParser
);

route(
  "observation-form-print",
  (optId) => {
    optId.map((id) => {
      const select = () => {
        selectCollect(id);
        setFormObservationCollect(id);
        resetFormObservationObserved();
        addObservationLayer();
        setLayout("observation-form-print");
      };

      onCollectsNone(() => loadCollectList().then(select));
      onQuestionsNone(() => loadQuestionList().then(select));
      onChoicesNone(() => loadChoiceList().then(select));
      onAnswersNone(() => loadAnswerList().then(select));
      onObservationsNone(() => loadObservationList().then(select));

      select();
    });
  },
  collectParser
);

route(
  "answer-form",
  (scope) => {
    scope.map(({ collectId, observationId }) => {
      const select = () => {
        selectCollect(collectId);
        setFormAnswerObservation(observationId);
        setLayout("answer-form");
      };

      onCollectsNone(() => loadCollectList().then(select));
      onQuestionsNone(() => loadQuestionList().then(select));
      onChoicesNone(() => loadChoiceList().then(select));
      onAnswersNone(() => loadAnswerList().then(select));
      onObservationsNone(() => loadObservationList().then(select));

      select();
    });
  },
  answerParser
);

route(
  "annotation-form",
  (scope) => {
    scope.map(({ collectId, kind, targetKind, targetId }) => {
      const select = () => {
        selectCollect(collectId);
        resetAnnotationText();
        resetAnnotationImage();
        setAnnotationFormKind(kind);
        setAnnotationFormTarget(targetKind, targetId);
        setLayout("annotation-form");
      };

      onCollectsNone(() => loadCollectList().then(select));
      onQuestionsNone(() => loadQuestionList().then(select));
      onChoicesNone(() => loadChoiceList().then(select));
      onAnswersNone(() => loadAnswerList().then(select));
      onObservationsNone(() => loadObservationList().then(select));

      select();
    });
  },
  annotationParser
);

// end of route handlers

export const loadRoute = (initial: string[]) =>
  index(0, initial)
    .chain((prefix) =>
      fromEither(
        RouteIO.decode(prefix)
          .map((c) => navigate(c, initial.slice(1)))
          .swap()
      )
    )
    .map(() => navigateHome());

export const navigateHome = () => navigate("home", []);

export const navigateCollect = (collecId: number) =>
  navigate("collect", [collecId]);

export const navigateMap = (collecId: number) => navigate("map", [collecId]);

export const navigateObservation = (obsId: number) =>
  navigate("observation", [obsId]);

export const navigateObservationForm = (collectId: number) =>
  navigate("observation-form", [collectId]);

export const navigateObservationFormPrint = (collectId: number) =>
  navigate("observation-form-print", [collectId]);

export const navigateAnswerForm = (collectId: number, observationId: number) =>
  navigate("answer-form", [collectId, observationId]);

export const navigateAnnotationForm = (
  kind: AnnotationKind,
  targetKind: AnnotationTargetKind,
  targetId: number
) =>
  getSelectedCollect().map((collect) =>
    navigate("annotation-form", [collect.id, kind, targetKind, targetId])
  );

logger("loaded");
