import {
    fetchCollectList,
    fetchQuestionList,
    fetchChoiceList,
    fetchObservationList,
    fetchAnswerList,
    postObservation,
    Observation,
    postAnswer,
    Answer,
    AnnotationTargetKind,
    fetchAnnotationList,
    postImageAnnotation,
    postTextAnnotation,
} from '../remote';
import { assign, makeSetReset, query, dispatch, observe, dispatchK } from 'sdi/shape';
import { remoteLoading, remoteSuccess, remoteError, mapRemote } from 'sdi/source';
import { scopeOption } from 'sdi/lib';
import { fromNullable, some } from 'fp-ts/lib/Option';
import {
    getSelectedCollect,
    observationInfoOption,
    getFormAnswerObservation,
    getAnnotationTarget,
    getAnnotationText,
    getAnnotationImage,
    getAnnotations,
    MAP_OBSERVATION_FORM,
    findChoice,
    getTrackerObservationFeatures,
    getObservationFeatures,
    observationTrackerInfoOption,
} from '../queries/collect';
import { navigateAnswerForm } from './route';
import { FetchData, addLayer, removeLayerAll } from 'sdi/map';
import { right } from 'fp-ts/lib/Either';
import { updateCollection, getCollectionItem } from 'sdi/util';
import { CLEAR_INPUT_TEXT } from 'sdi/components/input';
import { makeImageUploaderEvents } from 'sdi/components/upload';



observe('collect/selected/collect',
    s => fromNullable(s)
        .map(collectId => getAnnotations(collectId)
            .foldL(
                () => {
                    loadAnnotations(collectId);
                },
                () => { }
            )));



export const loadCollectList = () => {
    assign('data/collect/collect', remoteLoading);

    return fetchCollectList()
        .then(ls => assign('data/collect/collect', remoteSuccess(ls)))
        .catch(err => assign('data/collect/collect', remoteError(err.toString())));
};

export const loadQuestionList = () => {
    assign('data/collect/question', remoteLoading);

    return fetchQuestionList()
        .then(ls => assign('data/collect/question', remoteSuccess(ls)))
        .catch(err => assign('data/collect/question', remoteError(err.toString())));
};

export const loadChoiceList = () => {
    assign('data/collect/choice', remoteLoading);

    return fetchChoiceList()
        .then(ls => assign('data/collect/choice', remoteSuccess(ls)))
        .catch(err => assign('data/collect/choice', remoteError(err.toString())));
};

export const loadObservationList = () => {
    assign('data/collect/observation', remoteLoading);

    return fetchObservationList()
        .then(ls => assign('data/collect/observation', remoteSuccess(ls)))
        .catch(err => assign('data/collect/observation', remoteError(err.toString())));
};

export const loadAnswerList = () => {
    assign('data/collect/answer', remoteLoading);

    return fetchAnswerList()
        .then(ls => assign('data/collect/answer', remoteSuccess(ls)))
        .catch(err => assign('data/collect/answer', remoteError(err.toString())));
};



export const [
    selectCollect,
    resetCollectSelection
] = makeSetReset('collect/selected/collect', null);


export const [
    selectObservation,
    resetObservationSelection
] = makeSetReset('collect/selected/observation', null);


export const [
    setFormObservationCollect,
    resetFormObservationCollect
] = makeSetReset('collect/form/observation/collect', null);




export const [
    setFormObservationObserved,
    resetFormObservationObserved
] = makeSetReset('collect/form/observation/observed', null);



export const [
    setFormAnswerObservation,
    resetFormAnswerObservation
] = makeSetReset('collect/form/answer/observation', null);


export const addObservationLayer = () => {
    removeLayerAll(MAP_OBSERVATION_FORM);
    const fetchData: FetchData = () => right(
        some({ type: 'FeatureCollection', features: getObservationFeatures() }));
    addLayer(MAP_OBSERVATION_FORM, observationInfoOption, fetchData);
};

export const addTrackObservationLayer = () => {
    removeLayerAll(MAP_OBSERVATION_FORM);
    const fetchData: FetchData = () => right(
        some({ type: 'FeatureCollection', features: getTrackerObservationFeatures() }));
    addLayer(MAP_OBSERVATION_FORM, observationTrackerInfoOption, fetchData);
};



const addObservation = (
    obs: Observation,
) => dispatch(
    'data/collect/observation',
    resource => mapRemote(resource, data => data.concat(obs))
);


export const saveFormObservation = () =>
    scopeOption()
        .let('observed', fromNullable(query('collect/form/observation/observed')))
        .let('collect', getSelectedCollect())
        .map(({ observed, collect }) =>
            postObservation(collect.id, { observed, collect: collect.id })
                .then((observation) => {
                    addObservation(observation);
                    navigateAnswerForm(collect.id, observation.id);
                })
                .catch((err) => {
                    // TODO
                    console.error(err);
                })
        );

const addAnswer = (
    a: Answer,
) => dispatch(
    'data/collect/answer',
    (resource) => {
        const questionId = findChoice(a.choice).map(c => c.question).getOrElse(-1);
        return mapRemote(resource,
            data => data
                .filter(b =>
                    b.observation !== a.observation
                    || questionId !== findChoice(b.choice).map(c => c.question).getOrElse(-1))
                .concat(a));
    }
);


export const saveAnswer = (
    choice: number
) => getFormAnswerObservation()
    .map(observation =>
        postAnswer(observation, { observation, choice })
            .then(addAnswer)
            .catch((err) => {
                // TODO
                console.error(err);
            }));


export const [
    setAnnotationFormKind,
    resetAnnotationFormKind
] = makeSetReset('collect/annotation-form/kind', 'text');


export const [
    setAnnotationText,
    resetAnnotationText
] = makeSetReset('collect/annotation-form/text', CLEAR_INPUT_TEXT);

export const setAnnotationFormTarget = (
    kind: AnnotationTargetKind,
    id: number,
) => assign('collect/annotation-form/target', { kind, id });


export const resetAnnotationFormTarget = () => assign('collect/annotation-form/target', null);


export const loadAnnotations = (
    collectId: number,
) => fetchAnnotationList(collectId)
    .then(ans =>
        dispatch('data/collect/annotations',
            s => updateCollection(s, collectId.toString(), ans)))
    .catch((_err) => {
        // TODO
    });


export const saveTextAnnotation = () =>
    scopeOption()
        .let('collect', getSelectedCollect())
        .let('target', getAnnotationTarget())
        .let('text', getAnnotationText())
        // .let('user', getUserIdAstNumber())
        .map(({ collect, target, text }) => postTextAnnotation({
            kind: 'text',
            collect: collect.id,
            target,
            // user,
            text,
        })
            .then(annotation =>
                dispatch('data/collect/annotations',
                    s => getCollectionItem(collect.id.toString(), s)
                        .foldL(
                            () => updateCollection(s, collect.id.toString(), [annotation]),
                            ans => updateCollection(s, collect.id.toString(), ans.concat(annotation)),
                        )))
            .catch((_err) => {
                // TODO
            }));


export const saveImageAnnotation = () =>
    scopeOption()
        .let('collect', getSelectedCollect())
        .let('target', getAnnotationTarget())
        .let('image', getAnnotationImage()
            .chain(s => fromNullable(s.imageId)))
        // .let('user', getUserIdAstNumber())
        .map(({ collect, target, image }) => postImageAnnotation({
            kind: 'image',
            collect: collect.id,
            target,
            image,
        })
            .then(annotation =>
                dispatch('data/collect/annotations',
                    s => getCollectionItem(collect.id.toString(), s)
                        .foldL(
                            () => updateCollection(s, collect.id.toString(), [annotation]),
                            ans => updateCollection(s, collect.id.toString(), ans.concat(annotation)),
                        )))
            .catch((_err) => {
                // TODO
            }));


export const imageUploaderEvents = makeImageUploaderEvents(dispatchK('collect/annotation-form/image'));

export const resetAnnotationImage = imageUploaderEvents.clearUploadState;

