import { dispatchK, dispatch, observe, assign } from 'sdi/shape';
import { removeLayerAll, addLayer, FetchData, mapExists, viewEventsFactory, trackerEventsFactory, defaultInteraction } from 'sdi/map';
import { MAP_COLLECT_NAME, makeLayerInfoOptionFromCollect, makeLayerFromCollect, getMapInteraction } from '../queries/map';
import { findCollect, getCollectExtent, getRemoteObservations } from '../queries/collect';
import { right } from 'fp-ts/lib/Either';
import { some } from 'fp-ts/lib/Option';
import { remoteToOption } from 'sdi/source';
import { getLayout } from '../queries/app';
import { Coordinate } from 'ol/coordinate';
import { MiniStep } from 'sdi/map/mini';

export const updateView = viewEventsFactory(dispatchK('port/map/view')).updateMapView;
export const updateObservationView = viewEventsFactory(dispatchK('port/map/observation/view')).updateMapView;
export const setScaleLine = () => { };
export const setMapSelection = dispatchK('port/map/selection');

export const setMapInteraction = dispatchK('port/map/interaction');
export const trackerEvents = trackerEventsFactory(setMapInteraction, getMapInteraction);


observe('port/map/view', (v) => {
    if (getLayout() === 'map') {
        assign('port/map/observation/view', v);
    }
});

export const startMark =
    () => setMapInteraction((s) => {
        if (s.label === 'mark') {
            return { ...s, state: { ...s.state, started: true } };
        }
        return s;
    });

export const endMark =
    () => setMapInteraction(() => defaultInteraction());

export const putMark =
    (coordinates: Coordinate) =>
        setMapInteraction(() => ({
            label: 'mark',
            state: {
                started: false,
                endTime: Date.now() + 12000,
                coordinates,
            },
        }));

export const loadCollectMap = (
    id: number,
) => {
    findCollect(id)
        .map((c) => {
            if (mapExists(MAP_COLLECT_NAME)
                && remoteToOption(getRemoteObservations()).isSome() // It calls for a more general fix (like not loading all obervations at once)
            ) {
                removeLayerAll(MAP_COLLECT_NAME);
                const observedInfo = makeLayerInfoOptionFromCollect(c);
                const observedFetcher: FetchData = () => right(some(makeLayerFromCollect(c.id)));

                addLayer(
                    MAP_COLLECT_NAME,
                    observedInfo,
                    observedFetcher,
                );

                zoomToExtent(id, 'map');
            }
            else {
                window.setTimeout(() => loadCollectMap(id), 32);
            }
        });
};





export const zoomToExtent = (
    collectId: number,
    view: 'map' | 'observation'
) => getCollectExtent(collectId)
    .map((extent) => {

        dispatch(view === 'map' ? 'port/map/view' : 'port/map/observation/view', state => ({
            dirty: 'geo/extent',
            center: state.center,
            rotation: state.rotation,
            zoom: state.zoom,
            srs: state.srs,
            feature: null,
            extent,
        }));
    });

export const setMiniMap =
    (k: string, step: MiniStep) =>
        dispatch('component/harvest/minimap', (mm) => {
            mm[k] = step;
            return mm;
        });


export const mainViewEvents = viewEventsFactory(dispatchK('port/map/view'));
export const observationViewEvents = viewEventsFactory(dispatchK('port/map/observation/view'));